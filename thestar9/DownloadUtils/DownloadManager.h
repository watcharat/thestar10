//
//  DownloadManager.h
//  Byrdland
//
//  Created by อิศรา ก้อนสมบัติ on 10/17/55 BE.
//
//

#import <Foundation/Foundation.h>

#import "ASIHTTPRequest.h"

@interface DownloadManager : NSObject<ASIHTTPRequestDelegate, ASIProgressDelegate, UIAlertViewDelegate>{
    NSMutableArray* arDownloadList;
    NSMutableArray* arProgress;
    
    ASIHTTPRequest* request;
    BOOL downloading;
    
    int alert_code;
    NSString *gmmd_code;
    NSString* service_id;
    NSString *savetype;
    
    // for stamp to db
    NSString *title;
    NSString *desc;
    NSString *date;
    NSData* cover;
    NSString* path;
}
@property (retain, nonatomic) NSMutableArray* arDownloadList;
@property (retain, nonatomic) NSMutableArray* arProgress;

@property (retain, nonatomic) NSString* service_id;
@property (retain, nonatomic) NSString *gmmd_code;
@property (retain, nonatomic) NSString *savetype;

@property (retain, nonatomic) ASIHTTPRequest* request;

@property (retain, nonatomic)NSString *title;
@property (retain, nonatomic)NSString *desc;
@property (retain, nonatomic)NSString *date;
@property (retain, nonatomic)NSData* cover;
@property (retain, nonatomic)NSString* path;

@property (readwrite, assign) int alert_code;

//-(void)setStatupValue;

//-(void)addDownloadEpisode:(int)episode_code;
-(void)addDownloadQueueWithGMMDCode:(NSString*)gmmdcode contentCode:(NSString*)contentCode service_id:(NSString*)service_id save_type:(NSString*)save_type title:(NSString*)title desc:(NSString*)desc date:(NSString*)date cover:(NSData*)cover;

-(void)startDownload;
//-(void)setDownloadProgress:(id)progressView;
-(void)combackDownload;
-(void)cancelNowDownload;
-(void)addProgressView:(UIProgressView*)pg;


@end
