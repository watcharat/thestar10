//
//  DownloadManager.m
//  Byrdland
//
//  Created by อิศรา ก้อนสมบัติ on 10/17/55 BE.
//
//

#import "DownloadManager.h"

#import "AppDelegate.h"
#import "GDataXMLNode.h"

#import "DBManager.h"

#import "Song.h"
#import "MV.h"
#import "SpecialClip.h"

#import "PlayerProgressView.h"

#define SAVETYPE_SONG "SONG"
#define SAVETYPE_MV "MV"
#define SAVETYPE_SPECIALCLIP "SPECIALCLIP"

@implementation DownloadManager

@synthesize arDownloadList;
@synthesize arProgress;
@synthesize gmmd_code;
@synthesize request;
@synthesize service_id;
@synthesize savetype;

@synthesize title, desc, date, cover, path;

@synthesize alert_code;

-(id)init{
    if(self = [super init]){
        [self setStartup];
    }
    return self;
}


-(void)dealloc{
    [self.request release];
    self.request = nil;
    
    [arDownloadList release];
    arDownloadList = nil;
    
    
    [arProgress release];
    arProgress = nil;
    
    [super dealloc];
}

// set startup value
-(void)setStartup{
    
    // initial
    arDownloadList = [[NSMutableArray alloc] init];
    
    arProgress = [[NSMutableArray alloc] init];
    
    downloading = false;

}


// download file
// -----------------------------------------------------------

-(void)downloadFile:(NSURL*)url filename:(NSString*)filename{
    NSLog(@"start download file : %@", filename);
    
    // asi download
    self.request = [ASIHTTPRequest requestWithURL:url];
    [self.request setDelegate:self];
//    [request setDownloadProgressDelegate:self];
    
    PlayerProgressView *pg = [arProgress objectAtIndex:0];
    if(pg!=nil){
        [self.request setDownloadProgressDelegate:pg];
    }
    

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory ,NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *finalFileDesc = [documentDirectory stringByAppendingPathComponent:filename];
    
    NSLog(@"file save path = %@", finalFileDesc);
    
    [self.request setDownloadDestinationPath:finalFileDesc];
    [self.request setTemporaryFileDownloadPath:[NSString stringWithFormat:@"%@_tmp", finalFileDesc]];
    
    [self.request setAllowResumeForFileDownloads:YES];
    
    [self.request setShouldContinueWhenAppEntersBackground:YES];
    
    [self.request startAsynchronous];

}

// external call method
// ---------------------
-(void)startDownload{
    NSLog(@">> start Download");
    if(!downloading && [arDownloadList count] > 0){
        downloading = true;
        
        NSLog(@"start download");
        // get value
        NSMutableDictionary *dic = [arDownloadList objectAtIndex:0];

        NSString *gmmdcode = [dic objectForKey:@"GMMD_CODE"];
        self.service_id = [dic objectForKey:@"SERVICE_ID"];
        self.savetype = [dic objectForKey:@"SAVETYPE"];
        
        self.title = [dic objectForKey:@"TITLE"];
        self.desc = [dic objectForKey:@"DESC"];
        self.date = [dic objectForKey:@"DATE"];
        self.cover = [dic objectForKey:@"COVER"];
        
        self.gmmd_code = gmmdcode;
        
        // call api
        [self callAPIDownload:gmmdcode service_id:self.service_id];
        
    }else{
        if(downloading){
            NSLog(@" ... downloading ... ");
        }
        if([arDownloadList count] <= 0){
            
            NSLog(@" arDownloadList count < 0 [not found list download");
        }
        
    }
}

// call api for download
// --------------------------------
-(void)callAPIDownload:(NSString*)gmmdcode service_id:(NSString*)serviceid{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id

    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    

    
    // call api
    NSString* urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/download.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&GMMD_CODE=%@&SERVICE_ID=%@&MSISDN=%@&CP_ID=1&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         gmmdcode,
                         serviceid,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        [data release];
        [url release];
        return;
    }
    NSLog(@"parse xml download ");
    
    //parse XML
    // ---------
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
    if(doc==nil){
        NSLog(@"parse xml document error");
    }else{
        NSLog(@"xml format correctly.");
    }

//    NSString *status = [[[elmXML elementsForName:@"status"]objectAtIndex:0] stringValue];
    NSString *status_code = [[[doc.rootElement elementsForName:@"status_code"] objectAtIndex:0] stringValue];
    NSString *detail = [[[doc.rootElement elementsForName:@"detail"] objectAtIndex:0] stringValue];
    NSString *dl_link = [[[doc.rootElement elementsForName:@"dl_link"] objectAtIndex:0] stringValue];
    
    NSString *filename;
    if([service_id isEqualToString:@"1"]){
        filename = [NSString stringWithFormat:@"%@.mp3", gmmdcode];
    }else{
        filename = [NSString stringWithFormat:@"%@_%@.mp4", gmmdcode, serviceid];
    }
    
    // check status_code
    // status 200 can download
    if([status_code isEqualToString:@"200"]){
        NSLog(@"on status_code = 200");
        
        [self downloadFile:[NSURL URLWithString:dl_link] filename:filename];
        
    }
    else if([status_code isEqualToString:@"202"] || [status_code isEqualToString:@"201"]){
        NSLog(@"on status_code = 202 && 201");
        
        // top up serial
        self.alert_code = 202;
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Topup"
                              message: detail
                              delegate: self
                              cancelButtonTitle:@"ยืนยัน"
                              otherButtonTitles:@"ปิด", nil];
        
//        CGRect rect = {12, 60, 260, 25};
//        UITextField *txtTopup = [[[UITextField alloc] initWithFrame:rect] autorelease];
//        txtTopup.backgroundColor = [UIColor whiteColor];
//        [txtTopup becomeFirstResponder];
//        txtTopup.tag = 202;
//        [alert addSubview:txtTopup];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
        
        [alert show];
        [alert release];
        downloading = false;
        
    }else if([status_code isEqualToString:@"203"]){
        NSLog(@"on status_code = 203");
        // cannot buy
        self.alert_code = 203;
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"เพลงยังไม่ซื้อ"
                              message: detail
                              delegate: self
                              cancelButtonTitle:@"ไป"
                              otherButtonTitles:@"ยกเลิก", nil];
        
        [alert show];
        [alert release];
        
        downloading = false;
    }else{
        NSLog(@"error status = %@", status_code);
    }
    
    [doc release];
    [data release];
    [url release];
    
}


// --------------------------------
//-(void)setDownloadProgress:(id)progressView{
//    [request setDownloadProgressDelegate:progressView];
//}
//
//

// add to list
-(void)addDownloadQueueWithGMMDCode:(NSString*)gmmdcode contentCode:(NSString*)contentCode service_id:(NSString*)serviceid save_type:(NSString*)save_type title:(NSString*)_title desc:(NSString*)_desc date:(NSString*)_date cover:(NSData*)_cover{
    
    NSLog(@"add download queue");
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:gmmdcode forKey:@"GMMD_CODE"];
    [dic setValue:contentCode forKey:@"CONTENT_CODE"];
    [dic setValue:serviceid forKey:@"SERVICE_ID"];
    [dic setValue:save_type forKey:@"SAVETYPE"];
    
    [dic setValue:_title forKey:@"TITLE"];
    [dic setValue:_desc forKey:@"DESC"];
    [dic setValue:_date forKey:@"DATE"];
    [dic setValue:_cover forKey:@"COVER"];
    
    NSString* keycode = [NSString stringWithFormat:@"%@%@",gmmdcode, service_id];
    [dic setValue:keycode forKey:@"KEYCODE"];
    
    NSLog(@"add key value");
    [arDownloadList addObject:dic];
    NSLog(@"add key value completed");
    [dic release];
}


-(void)addProgressView:(UIProgressView*)pg{
    if(arProgress==nil){
        arProgress = [[NSMutableArray alloc] init];
    }
    [arProgress addObject:pg];
}



// delegate of request
// -----------------------------------------
-(void)requestStarted:(ASIHTTPRequest *)request{
    
}

-(void)requestFinished:(ASIHTTPRequest *)asirequest{

    downloading = false;
    
    // stamp to database
    NSMutableDictionary *dic =[arDownloadList objectAtIndex:0] ;
    NSString* gmmdcode = [dic objectForKey:@"GMMD_CODE"];
    NSString* content_code = [dic objectForKey:@"CONTENT_CODE"];
    
    NSLog(@">> download completed %@",  [dic objectForKey:@"TITLE"]);
//    NSString* service_id = [dic objectForKey:@"SERVICE_ID"];
    
    DBManager *dbMgr = [[DBManager alloc] init];
    
    if(self.savetype== @"SONG"){
        Song *song = [dbMgr getSchemaSong];
        song.gmmd_code = gmmdcode;
        song.content_code = content_code;
        song.title = self.title;
        song.desc = self.desc;
        song.date = self.date;
        song.cover = self.cover;
        
        [dbMgr insertSong:song];
    }else if(self.savetype == @"MV"){
        MV *mv = [dbMgr getSchemaMV];
        mv.gmmd_code = gmmdcode;
        mv.content_code = content_code;
        mv.title = self.title;
        mv.desc = self.desc;
        mv.date = self.date;
        mv.cover = self.cover;
        
        [dbMgr insertMV:mv];
    }else{
        SpecialClip *sc = [dbMgr getSchemaSpecialClip];
        sc.gmmd_code = gmmdcode;
        sc.content_code = content_code;
        sc.title = self.title;
        sc.desc = self.desc;
        sc.date = self.date;
        sc.cover = self.cover;
        
        [dbMgr insertSpecialClip:sc];
    }

    [dbMgr release];
    
    // delete first row
    if([arDownloadList count] > 0){
        [arDownloadList removeObjectAtIndex:0];
        PlayerProgressView* pg = [arProgress objectAtIndex:0];
        [pg setHidden:YES];
        [arProgress removeObjectAtIndex:0];
        
        // refresh table
        AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        if(appDel.tableMusicDownload){
            [appDel.tableMusicDownload reloadData];
        }

    }
    
    // continue download
    [self startDownload];
    
    // refresh table
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if(appDel.tableMusicDownload){
        [appDel.tableMusicDownload reloadData];
    }

}

//-(void)request:(ASIHTTPRequest *)request didReceiveData:(NSData *)data{
//    NSLog(@"receive data");
//}




-(void)combackDownload{
    downloading = false;
}

-(void)cancelNowDownload{
    if(downloading){
//        NSLog(@"cancel downloading");
        
        downloading = false;
        
        [request cancel];
        [request clearDelegatesAndCancel];

    }
}

// Delegate Popup (UIAlert)
// ------------------------------
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(self.alert_code == 202 || self.alert_code == 201){
        if(buttonIndex == 0){
            
//            UITextField* txtPopup = (UITextField*)[alertView viewWithTag:202];
//            NSString *serial = txtPopup.text;
            NSString *serial = [[alertView textFieldAtIndex:0] text];
        
            // call topup api
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/topup.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&SERIAL_CODE=%@&APPVERSION=%@&APIVERSION=%@",
                             appDel.app_id,
                             appDel.device,
                             appDel.charset,
                             appDel.msisdn,
                             serial,
                             appDel.appversion,
                             appDel.apiversion
                             ];
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
        
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
        
            if([data isEqualToString:@""]){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                [data release];
                [url release];
                return;
            }else{
                NSLog(@"data => %@", data);
            }
        
            //parse XML
            // ------------------------------------
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
            if(doc==nil){
                
                NSLog(@"parse xml document error");
            }else{
                
            }
        
            NSString *status_code = [[[doc.rootElement elementsForName:@"STATUS_CODE"]objectAtIndex:0] stringValue];
            if([status_code isEqualToString:@"200"]){
                NSLog(@"topup compeleted");
                [self startDownload];
            }else{
                // delete first row
                NSLog(@"status_code = %@", status_code);
                if([arDownloadList count] > 0){
                    [arDownloadList removeObjectAtIndex:0];
                    PlayerProgressView* pg = [arProgress objectAtIndex:0];
                    [pg setHidden:YES];
                    [arProgress removeObjectAtIndex:0];
                    
                    // refresh table
                    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                    if(appDel.tableMusicDownload){
                        [appDel.tableMusicDownload reloadData];
                    }
                }
            }
        }else if(buttonIndex==2){
            // cancel charging
            // go to cancel charging
            
            NSString* sv_id = self.service_id;
            if([sv_id isEqualToString:@"3"]){
                sv_id = @"";
            }
            
            NSString *link = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/chargingCancel.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=%@",
                              appDel.app_id,
                              appDel.device,
                              appDel.msisdn,
                              sv_id
                              ];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
            // delete first row
            if([arDownloadList count] > 0){
                [arDownloadList removeObjectAtIndex:0];
                PlayerProgressView* pg = [arProgress objectAtIndex:0];
                [pg setHidden:YES];
                [arProgress removeObjectAtIndex:0];
                // refresh table
                AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                if(appDel.tableMusicDownload){
                    [appDel.tableMusicDownload reloadData];
                }
            }
            
            
        }else{
            // delete first row
            if([arDownloadList count] > 0){
                [arDownloadList removeObjectAtIndex:0];
                PlayerProgressView* pg = [arProgress objectAtIndex:0];
                [pg setHidden:YES];
                [arProgress removeObjectAtIndex:0];
                // refresh table
                AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                if(appDel.tableMusicDownload){
                    [appDel.tableMusicDownload reloadData];
                }
                
            }
        }
        
        
    }else if(self.alert_code == 203){
        // not buy
        if (buttonIndex == 0) {
            
            NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=%@&GMMD_CODE=%@", appDel.app_id, appDel.device, appDel.msisdn, self.service_id, self.gmmd_code];

            NSLog(@"go out : %@", strPath);

            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];

        }else{}
        
        // delete first row
        if([arDownloadList count] > 0){
            [arDownloadList removeObjectAtIndex:0];
            PlayerProgressView* pg = [arProgress objectAtIndex:0];
            [pg setHidden:YES];
            [arProgress removeObjectAtIndex:0];
            
            // refresh table
            AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if(appDel.tableMusicDownload){
                [appDel.tableMusicDownload reloadData];
            }
        }
        //

    }
}

// -- progress delegate

//- (void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes{
//    
//    NSLog(@"didReceiveBytes");
//}
//
//- (void)request:(ASIHTTPRequest *)request didSendBytes:(long long)bytes{
//    NSLog(@"didSendBytes");
//}
//
//- (void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength{
//    NSLog(@"incrementDownloadSizeBy");
//}
//
//- (void)request:(ASIHTTPRequest *)request incrementUploadSizeBy:(long long)newLength{
//    NSLog(@"incrementUploadSizeBy");
//}


@end
