//
//  TheStarProfile.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/1/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "TheStarProfile.h"


@implementation TheStarProfile

@dynamic cover_img;
@dynamic cover_img_profile;
@dynamic star_id;
@dynamic thumb_img;
@dynamic updated;
@dynamic name;
@dynamic desc;
@dynamic sms_code;

@end
