//
//  TheStarProfile.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/1/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TheStarProfile : NSManagedObject

@property (nonatomic, retain) NSData * cover_img;
@property (nonatomic, retain) NSData * cover_img_profile;
@property (nonatomic, retain) NSString * star_id;
@property (nonatomic, retain) NSData * thumb_img;
@property (nonatomic, retain) NSString * updated;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * sms_code;

@end
