//
//  Starvoice.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/20/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Starvoice : NSManagedObject

@property (nonatomic, retain) NSString * star_id;
@property (nonatomic, retain) NSData * thumb_img;
@property (nonatomic, retain) NSString * updated;

@end
