//
//  Song.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/7/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "Song.h"


@implementation Song

@dynamic content_code;
@dynamic gmmd_code;
@dynamic title;
@dynamic desc;
@dynamic date;
@dynamic cover;

@end
