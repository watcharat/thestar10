//
//  DBManager.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/27/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "DBManager.h"

#import "AppDelegate.h"

@implementation DBManager
// ------------------------------------
// Song table
// ------------------------------------
-(NSMutableArray*)fetchSong:(NSString*)cond{
    NSMutableArray *ar =nil;
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"Song" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    if( ![cond isEqualToString:@""]){
        // cond --> sql syntax in where
        NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:cond];
        [request setPredicate:predrequesticate];
    }
    
    // sorting
    //    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"create_date" ascending:YES];
    //    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //    [request setSortDescriptors:sortDescriptors];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"gmmd_code" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    [sortDescriptor release];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if([objects count] > 0){
        ar = [[NSMutableArray alloc] initWithCapacity: [objects count]];
        
        for(int i=0; i< [objects count]; i++){
            Song *song = [objects objectAtIndex:i];
            [ar addObject:song];
        }
    }
    
    [request release];
    return ar;
}

-(Song*)getSchemaSong{
    NSManagedObjectContext *context = [self getManageObjectContext];
    Song *newSong = [NSEntityDescription insertNewObjectForEntityForName:@"Song" inManagedObjectContext:context];
    return newSong;
}
-(BOOL)insertSong:(Song*)song{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : insert error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : insert complete");
        return true;
    }
    return false;
}
-(BOOL)updateSong:(Song*)song{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : update error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : update complete");
        return true;
    }
    return false;
}
-(BOOL)deleteSong:(Song*)song{
    [[self getManageObjectContext] deleteObject:song];
    NSLog(@"[DBManager] : delete complete");
    
    return true;
}
// ------------------------------------
// MV table
// ------------------------------------
-(NSMutableArray*)fetchMV:(NSString*)cond{
    NSMutableArray *ar =nil;
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"MV" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    if( ![cond isEqualToString:@""]){
        // cond --> sql syntax in where
        NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:cond];
        [request setPredicate:predrequesticate];
    }
    
    // sorting
    //    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"create_date" ascending:YES];
    //    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //    [request setSortDescriptors:sortDescriptors];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"gmmd_code" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    [sortDescriptor release];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if([objects count] > 0){
        ar = [[NSMutableArray alloc] initWithCapacity: [objects count]];
        
        for(int i=0; i< [objects count]; i++){
            MV *mv = [objects objectAtIndex:i];
            [ar addObject:mv];
        }
    }
    
    [request release];
    return ar;
}

-(MV*)getSchemaMV{
    NSManagedObjectContext *context = [self getManageObjectContext];
    MV *newMV = [NSEntityDescription insertNewObjectForEntityForName:@"MV" inManagedObjectContext:context];
    return newMV;
}
-(BOOL)insertMV:(MV*)mv{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : insert error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : insert complete");
        return true;
    }
    return false;
}
-(BOOL)updateMV:(MV*)mv{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : update error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : update complete");
        return true;
    }
    return false;
}
-(BOOL)deleteMV:(MV*)mv{
    [[self getManageObjectContext] deleteObject:mv];
    NSLog(@"[DBManager] : delete complete");
    
    return true;
}

// ------------------------------------
// Special table
// ------------------------------------
-(NSMutableArray*)fetchSpecialClip:(NSString *)cond{
    NSMutableArray *ar =nil;
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"SpecialClip" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    if( ![cond isEqualToString:@""]){
        // cond --> sql syntax in where
        NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:cond];
        [request setPredicate:predrequesticate];
    }
    
    // sorting
    //    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"create_date" ascending:YES];
    //    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //    [request setSortDescriptors:sortDescriptors];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"gmmd_code" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    [sortDescriptor release];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if([objects count] > 0){
        ar = [[NSMutableArray alloc] initWithCapacity: [objects count]];
        
        for(int i=0; i< [objects count]; i++){
            SpecialClip *sc = [objects objectAtIndex:i];
            [ar addObject:sc];
        }
    }
    
    [request release];
    return ar;
}

-(SpecialClip*)getSchemaSpecialClip{
    NSManagedObjectContext *context = [self getManageObjectContext];
    SpecialClip *newSpecialClip = [NSEntityDescription insertNewObjectForEntityForName:@"SpecialClip" inManagedObjectContext:context];
    return newSpecialClip;
}
-(BOOL)insertSpecialClip:(SpecialClip*)sc{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : insert error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : insert complete");
        return true;
    }
    return false;
}
-(BOOL)updateSpecialClip:(SpecialClip*)sp{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : update error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : update complete");
        return true;
    }
    return false;
}
-(BOOL)deleteSpecialClip:(SpecialClip*)sc{
    [[self getManageObjectContext] deleteObject:sc];
    NSLog(@"[DBManager] : delete complete");
    
    return true;
}

// ------------------------------------
// Profile table
// ------------------------------------
-(NSMutableArray*)fetchProfile:(NSString*)cond{
    NSMutableArray *ar =nil;
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"Profile" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    if( ![cond isEqualToString:@""]){
        // cond --> sql syntax in where
        NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:cond];
        [request setPredicate:predrequesticate];
    }
    
    // sorting
    //    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"create_date" ascending:YES];
    //    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    //    [request setSortDescriptors:sortDescriptors];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"msisdn" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    [sortDescriptor release];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if([objects count] > 0){
        ar = [[NSMutableArray alloc] initWithCapacity: [objects count]];
        
        for(int i=0; i< [objects count]; i++){
            Profile *profile = [objects objectAtIndex:i];
            [ar addObject:profile];
        }
    }
    
    [request release];
    return ar;
}

-(Profile*)getSchemaProfile{
    NSManagedObjectContext *context = [self getManageObjectContext];
    Profile *newProfile = [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:context];
    return newProfile;
}
-(BOOL)insertProfile:(Profile*)profile{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : insert error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : insert complete");
        return true;
    }
    return false;
}
-(BOOL)updateProfile:(Profile*)profile{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : update error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : update complete");
        return true;
    }
    return false;
}
-(BOOL)deleteProfile:(Profile*)profile{
    [[self getManageObjectContext] deleteObject:profile];
    NSLog(@"[DBManager] : delete complete");
    
    return true;
}

// ---------------------------------------------------------------------------
// TheStarProfile 
// ---------------------------------------------------------------------------
-(NSMutableArray*)fetchTheStarProfile:(NSString*)cond{
    NSMutableArray *ar =nil;
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"TheStarProfile" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    if( ![cond isEqualToString:@""]){
        // cond --> sql syntax in where
        NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:cond];
        [request setPredicate:predrequesticate];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"star_id" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    [sortDescriptor release];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if([objects count] > 0){
        ar = [[NSMutableArray alloc] initWithCapacity: [objects count]];
        
        for(int i=0; i< [objects count]; i++){
            TheStarProfile *thestarProfile = [objects objectAtIndex:i];
            [ar addObject:thestarProfile];
        }
    }
    
    [request release];
    return ar;
}

-(TheStarProfile*)getSchemaTheStarProfile{
    NSManagedObjectContext *context = [self getManageObjectContext];
    TheStarProfile *newStarProfile = [NSEntityDescription insertNewObjectForEntityForName:@"TheStarProfile" inManagedObjectContext:context];
    return newStarProfile;
}
-(BOOL)insertTheStarProfile:(TheStarProfile*)thestarProfile{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : insert error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : insert complete");
        return true;
    }
    return false;
}
-(BOOL)updateTheStarProfile:(TheStarProfile*)thestarProfile{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : update error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : update complete");
        return true;
    }
    return false;
}
-(BOOL)deleteTheStarProfile:(TheStarProfile*)thestarProfile{
    [[self getManageObjectContext] deleteObject:thestarProfile];
    NSLog(@"[DBManager] : delete complete");
    
    return true;
}
// ---------------------------------------------------------------------------
// Vote
// ---------------------------------------------------------------------------
-(NSMutableArray*)fetchVote:(NSString*)cond{
    NSMutableArray *ar =nil;
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"Vote" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    if( ![cond isEqualToString:@""]){
        // cond --> sql syntax in where
        NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:cond];
        [request setPredicate:predrequesticate];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"star_id" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    [sortDescriptor release];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if([objects count] > 0){
        ar = [[NSMutableArray alloc] initWithCapacity: [objects count]];
        
        for(int i=0; i< [objects count]; i++){
            Vote *vote = [objects objectAtIndex:i];
            [ar addObject:vote];
        }
    }
    
    [request release];
    return ar;
}

-(Vote*)getSchemaVote{
    NSManagedObjectContext *context = [self getManageObjectContext];
    Vote *newVote = [NSEntityDescription insertNewObjectForEntityForName:@"Vote" inManagedObjectContext:context];
    return newVote;
}
-(BOOL)insertVote:(Vote*)vote{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : insert error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : insert complete");
        return true;
    }
    return false;
}
-(BOOL)updateVote:(Vote*)vote{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : update error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : update complete");
        return true;
    }
    return false;
}
-(BOOL)deleteVote:(Vote*)vote{
    [[self getManageObjectContext] deleteObject:vote];
    NSLog(@"[DBManager] : delete complete");
    
    return true;
}
// ---------------------------------------------------------------------------
// Star Voice
// ---------------------------------------------------------------------------
-(NSMutableArray*)fetchStarvoice:(NSString*)cond{
    NSMutableArray *ar =nil;
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"Starvoice" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    if( ![cond isEqualToString:@""]){
        // cond --> sql syntax in where
        NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:cond];
        [request setPredicate:predrequesticate];
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"star_id" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    [sortDescriptor release];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if([objects count] > 0){
        ar = [[NSMutableArray alloc] initWithCapacity: [objects count]];
        
        for(int i=0; i< [objects count]; i++){
            Starvoice *starvoice = [objects objectAtIndex:i];
            [ar addObject:starvoice];
        }
    }
    
    [request release];
    return ar;
}

-(Starvoice*)getSchemaStarvoice{
    NSManagedObjectContext *context = [self getManageObjectContext];
    Starvoice *newStarvoice = [NSEntityDescription insertNewObjectForEntityForName:@"Starvoice" inManagedObjectContext:context];
    return newStarvoice;
}
-(BOOL)insertStarStarvoice:(Starvoice*)starvoice{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : insert error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : insert complete");
        return true;
    }
    return false;
}
-(BOOL)updateStarvoice:(Starvoice*)starvoice{
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"[DBManager] : update error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"[DBManager] : update complete");
        return true;
    }
    return false;
}
-(BOOL)deleteStarvoice:(Starvoice*)starvoice{
    [[self getManageObjectContext] deleteObject:starvoice];
    NSLog(@"[DBManager] : delete complete");
    
    return true;
}


// ---------------------------------------------------------------------------
// util
// ---------------------------------------------------------------------------

-(NSManagedObjectContext*)getManageObjectContext{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    return context;
}

@end
