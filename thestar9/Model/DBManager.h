//
//  DBManager.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/27/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Song.h"
#import "MV.h"
#import "SpecialClip.h"

#import "Profile.h"
#import "TheStarProfile.h"
#import "Vote.h"
#import "Starvoice.h"

@interface DBManager : NSObject




// Profile table
// --------------------------------------------------------------
-(NSMutableArray*)fetchProfile:(NSString*)cond;                                  // select
-(Profile*)getSchemaProfile;                                                     // get for new
-(BOOL)insertProfile:(Profile*)profile;                                          // save new
-(BOOL)updateProfile:(Profile*)profile;                                          // update
-(BOOL)deleteProfile:(Profile*)profile;                                          // delete


// TheStarProfile Song
// --------------------------------------------------------------
-(NSMutableArray*)fetchTheStarProfile:(NSString*)cond;                        // select
-(TheStarProfile*)getSchemaTheStarProfile;                                    // get for new
-(BOOL)insertTheStarProfile:(TheStarProfile*)thestarProfile;                  // save new
-(BOOL)updateTheStarProfile:(TheStarProfile*)thestarProfile;                  // update
-(BOOL)deleteTheStarProfile:(TheStarProfile*)thestarProfile;                  // delete

// TheStarProfile Song
// --------------------------------------------------------------
-(NSMutableArray*)fetchVote:(NSString*)cond;                                    // select
-(Vote*)getSchemaVote;                                                          // get for new
-(BOOL)insertVote:(Vote*)vote;                                                  // save new
-(BOOL)updateVote:(Vote*)vote;                                                  // update
-(BOOL)deleteVote:(Vote*)vote;                                                  // delete


// Song table
// --------------------------------------------------------------
-(NSMutableArray*)fetchSong:(NSString*)cond;                                    // select
-(Song*)getSchemaSong;                                                          // get for new
-(BOOL)insertSong:(Song*)song;                                                  // save new
-(BOOL)updateSong:(Song*)song;                                                  // update
-(BOOL)deleteSong:(Song*)song;                                                  // delete

// MV table
// --------------------------------------------------------------
-(NSMutableArray*)fetchMV:(NSString*)cond;                                      // select
-(MV*)getSchemaMV;                                                              // get for new
-(BOOL)insertMV:(MV*)mv;                                                        // save new
-(BOOL)updateMV:(MV*)mv;                                                        // update
-(BOOL)deleteMV:(MV*)mv;                                                        // delete

// Special Clip table
// --------------------------------------------------------------
-(NSMutableArray*)fetchSpecialClip:(NSString*)cond;                                    // select
-(SpecialClip*)getSchemaSpecialClip;                                            // get for new
-(BOOL)insertSpecialClip:(SpecialClip*)spClip;                                  // save new
-(BOOL)updateSpecialClip:(SpecialClip*)spClip;                                  // update
-(BOOL)deleteSpecialClip:(SpecialClip*)spClip;                                  // delete

// Star Voice
// ---------------------------------------------------------------------------
-(NSMutableArray*)fetchStarvoice:(NSString*)cond;
-(Starvoice*)getSchemaStarvoice;
-(BOOL)insertStarStarvoice:(Starvoice*)starvoice;
-(BOOL)updateStarvoice:(Starvoice*)starvoice;
-(BOOL)deleteStarvoice:(Starvoice*)starvoice;


// utility

-(NSManagedObjectContext*)getManageObjectContext;

@end
