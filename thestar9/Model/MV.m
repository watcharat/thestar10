//
//  MV.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/7/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "MV.h"


@implementation MV

@dynamic content_code;
@dynamic gmmd_code;
@dynamic date;
@dynamic desc;
@dynamic title;
@dynamic cover;

@end
