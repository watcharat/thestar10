//
//  Song.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/7/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Song : NSManagedObject

@property (nonatomic, retain) NSString * content_code;
@property (nonatomic, retain) NSString * gmmd_code;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSData * cover;

@end
