//
//  Profile.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 3/1/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Profile : NSManagedObject

@property (nonatomic, retain) NSString * follow_star_id;
@property (nonatomic, retain) NSString * msisdn;
@property (nonatomic, retain) NSString * devicetoken;

@end
