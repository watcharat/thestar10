//
//  Vote.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/8/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Vote : NSManagedObject

@property (nonatomic, retain) NSString * active;
@property (nonatomic, retain) NSString * star_id;
@property (nonatomic, retain) NSData * thumb_img;
@property (nonatomic, retain) NSString * updated;

@end
