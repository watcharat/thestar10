//
//  AppDelegate.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 12/11/55 BE.
//  Copyright (c) 2555 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "AppDelegate.h"

#import "JWAppDelegate.h"
#import "JWSlideMenuController.h"
#import "JWNavigationController.h"
#import "FirstViewController.h"
#import "SecondViewController.h"

// add controller menu
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "TheStarProfileViewController.h"
#import "VideoHighlightViewController.h"
#import "NewsViewController.h"
#import "GalleryViewController.h"

#import "DownloadViewController.h"
//#import "FanCafeViewController.h"
#import "GameAndActivityViewController.h"

#import "DBManager.h"
#import "Profile.h"

#import "NotiViewController.h"
#import "GDataXMLNode.h"


#define ALERTMODE_FILLNUMBER 0
#define ALERTMODE_OTP 1


#define ALERTMODE_SUBSCRIBE_FILLNUMBER 2
#define ALERTMODE_SUBSCRIBE_OTP 3
#define ALERTMODE_SUBSCRIBE_NOSUBSCRIBE_SHOW 4
#define ALERTMODE_SUBSCRIBE_TOPUPSERIAL 5




//facebook
NSString *const SCSessionStateChangedNotification = @"com.gmm.thestar9:SCSessionStateChangedNotification";

@implementation AppDelegate



@synthesize app_id;
@synthesize device;
@synthesize charset;
@synthesize msisdn;
@synthesize appversion;
@synthesize apiversion;
@synthesize tmpPhoneNum;

@synthesize downloadMgr;

@synthesize tableMusicDownload;

@synthesize splashCon;

@synthesize fakeApple;
@synthesize subscriber;
@synthesize facebook;

@synthesize accessToken;
@synthesize fb_id;

@synthesize datFacebookProfileImg;

@synthesize slideMenu;
//core data
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

// vote controller
@synthesize voteController;

@synthesize lbThumbFacebookMenu;
@synthesize imThumbFacebookMenu;

@synthesize lbProFacebookName;
@synthesize ivProFacebookImage;

@synthesize email;
@synthesize fullname;
@synthesize arShelfMusic;
@synthesize arShelfMV;
@synthesize arShelfSpecialClip;

- (void)dealloc
{
    [self.slideMenu release];
    
    [_window release];
    [downloadMgr release];
    
    [self.voteController release];
    
    [super dealloc];
    
    // core data
    [_managedObjectContext release];
    [_managedObjectModel release];
    [_persistentStoreCoordinator release];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.window.backgroundColor = [UIColor whiteColor];
    
    [self prepareData];
//    [self checkFakeAppleAPI];  // for test internal
    
    // download manager
    self.downloadMgr = [[DownloadManager alloc] init];
    
    // create controller
    ProfileViewController *profileCon = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    
    
    TheStarProfileViewController *theStarProCon = [[TheStarProfileViewController alloc] initWithNibName:@"TheStarProfileViewController" bundle:nil];
    
    VideoHighlightViewController *vdoHCon = [[VideoHighlightViewController alloc] initWithNibName:@"VideoHighlightViewController" bundle:nil];
    
    NewsViewController *newsCon = [[NewsViewController alloc] initWithNibName:@"NewsViewController" bundle:nil];
    
    GalleryViewController *galleryCon = [[GalleryViewController alloc] initWithNibName:@"GalleryViewController" bundle:nil];
    self.voteController = [[VoteViewController alloc] initWithNibName:@"VoteViewController" bundle:nil];
    
    DownloadViewController *downloadCon = [[DownloadViewController alloc] initWithNibName:@"DownloadViewController" bundle:nil];
//    FanCafeViewController *fanCon = [[FanCafeViewController alloc] initWithNibName:@"FanCafeViewController" bundle:nil];
    GameAndActivityViewController *gameActCon = [[GameAndActivityViewController alloc] initWithNibName:@"GameAndActivityViewController" bundle:nil];
    
    self.slideMenu = [[JWSlideMenuController alloc] init];
    
    [self.slideMenu addViewController:self.voteController withTitle:@"Vote"
                        andImage:[UIImage imageNamed:@"ico_vote"]
                 andSeletedImage:[UIImage imageNamed:@"ico_vote_active"]];
    [self.slideMenu addViewController:profileCon withTitle:@"My Profile"
                        andImage:[UIImage imageNamed:@"ico_profile"]
                 andSeletedImage:[UIImage imageNamed:@"ico_profile_active"]];
    [self.slideMenu addViewController:theStarProCon withTitle:@"The Star Profile"
                        andImage:[UIImage imageNamed:@"ico_thestar_profile"]
                 andSeletedImage:[UIImage imageNamed:@"ico_thestar_profile_active"]];
    [self.slideMenu addViewController:vdoHCon withTitle:@"Video Highlight"
                        andImage:[UIImage imageNamed:@"ico_video_highlight"]
                 andSeletedImage:[UIImage imageNamed:@"ico_video_highlight_active"]];
    [self.slideMenu addViewController:newsCon withTitle:@"News"
                        andImage:[UIImage imageNamed:@"ico_news"]
                 andSeletedImage:[UIImage imageNamed:@"ico_news_active"]];
    [self.slideMenu addViewController:galleryCon withTitle:@"Gallery"
                        andImage:[UIImage imageNamed:@"ico_gallery"]
                 andSeletedImage:[UIImage imageNamed:@"ico_gallery_active"]];
    [self.slideMenu addViewController:downloadCon withTitle:@"Download"
                        andImage:[UIImage imageNamed:@"ico_download"]
                 andSeletedImage:[UIImage imageNamed:@"ico_download_active"]];
//    [slideMenu addViewController:fanCon withTitle:@"FanCafe"
//                        andImage:[UIImage imageNamed:@"ico_fancafe"]
//                 andSeletedImage:[UIImage imageNamed:@"ico_fancafe_active"]];
    
    
    if(!self.fakeApple){
        [self.slideMenu addViewController:gameActCon withTitle:@"Game & Activity"
                            andImage:[UIImage imageNamed:@"ico_game_activity"]
                     andSeletedImage:[UIImage imageNamed:@"ico_game_activity_active"]];

        
    }
    
    //add splash
    //create splash screen
    if(splashCon==nil){
        NSLog(@"create splash screen");
        splashCon = [[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:nil];
        [self.slideMenu.view addSubview:splashCon.view];

    }
    
    // for first slide to share menu
    [self.slideMenu toggleMenu];
    
    self.window.rootViewController = self.slideMenu;
    
    
    // binding profile facebook to ui
    self.ivProFacebookImage = profileCon.imProfile;
    self.lbProFacebookName = profileCon.lbName;
    
    
    [profileCon release];
    [theStarProCon release];
    [vdoHCon release];
    [newsCon release];
    [galleryCon release];

    [downloadCon release];
//    [fanCon release];
    [gameActCon release];

  
    
    
    [self.window makeKeyAndVisible];
    
    //register receive notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"ReceivePushNotification"
                                               object:nil];
    
    
    return YES;
    
}


- (void) receiveNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"ReceivePushNotification"])
        NSLog (@"push notification ");
}


// send request notification
-(void)sendRequestNotification{
    NSLog(@"send request notification");
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeAlert |
      UIRemoteNotificationTypeBadge |
      UIRemoteNotificationTypeSound)];
}

// ------------------------------------------
// delegate of register notification
// ------------------------------------------
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSLog(@"Receive Notification");
    
    NSString* strDeviceToken = [NSString stringWithFormat:@"%@", deviceToken];
    NSLog(@"original token = %@", strDeviceToken);
    
    strDeviceToken = [strDeviceToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strDeviceToken = [strDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"new token = %@", strDeviceToken);
    
    
    
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/RegToken.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&APPVERSION=%@&APIVERSION=%@&MSISDN=%@&GACCOUNT=&STATUS=Y&TOKEN=%@",
                         self.app_id,
                         self.device,
                         self.charset,
                         self.appversion,
                         self.apiversion,
                         self.msisdn,
                         strDeviceToken
                         ];
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    
    if(doc){
        NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
        NSString* status_code = [[ar objectAtIndex:0] stringValue];;

        if([status_code isEqualToString:@"200"]){
            NSLog(@"Send API Notification Completed");
        }else{
            NSLog(@"ERROR : status_code = %@", status_code);
        }
    }else{
        NSLog(@"[XML ERROR] : %@", [error description]);
    }
    
    
    //update devicetoken to database
    DBManager *dbMgr = [[DBManager alloc] init];

    NSMutableArray *ar = [dbMgr fetchProfile:@""];
    if(ar !=nil && [ar count] > 0){
        Profile *profile = [ar objectAtIndex:0];

        profile.devicetoken = strDeviceToken;

        [dbMgr updateProfile:profile];
    }
    [ar release];
    [dbMgr release];


}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}


//-----------------
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    application.applicationIconBadgeNumber = 0;

    // clear data shelf download
    self.arShelfMusic = nil;
    self.arShelfMV = nil;
    self.arShelfSpecialClip = nil;
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)prepareData{
    self.app_id = @"283";
//    self.device = @"mockup_test";
    self.device = [[UIDevice currentDevice] uniqueDeviceIdentifier];
    self.charset = @"utf-8";
    self.msisdn = @"";
    self.appversion = @"1.2";
    self.apiversion = @"2.0.0";
    
    self.subscriber = false;
    
    // load msisdn from database
    DBManager *dbMgr = [[DBManager alloc] init];
    
    NSMutableArray *ar = [dbMgr fetchProfile:@""];
    if(ar !=nil && [ar count] > 0){
        // found data
        Profile *profile = [ar objectAtIndex:0];
        self.msisdn = profile.msisdn;
        if([profile.devicetoken isEqualToString:@""]){
            
            // send notification
            [self sendRequestNotification];
        }
    }else{
        // new profile
        Profile *profile = [dbMgr getSchemaProfile];
        profile.msisdn = @"";
        profile.devicetoken = @"";
        profile.follow_star_id = @"";
        [dbMgr insertProfile:profile];
        
        // send notification
        [self sendRequestNotification];
    }
    
    //for test download & game
//    msisdn = @"66818667415";
    
    self.datFacebookProfileImg = NULL;
    self.email = @"";
    
    [dbMgr release];
    
    // clear data shelf download
    self.arShelfMusic = nil;
    self.arShelfMV = nil;
    self.arShelfSpecialClip = nil;
}

// ------------------------------------------------------------
// core data
// ------------------------------------------------------------

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext
{
//    if (_managedObjectContext != nil) {
//        return _managedObjectContext;
//    }
//    
//    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
//    if (coordinator != nil) {
//        _managedObjectContext = [[NSManagedObjectContext alloc] init];
//        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
//    }
//    return _managedObjectContext;
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;

}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
//    if (_managedObjectModel != nil) {
//        return _managedObjectModel;
//    }
//    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TheStarModel" withExtension:@"momd"];
//    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
//    return _managedObjectModel;

    NSString *path = [[NSBundle mainBundle] pathForResource:@"MusicDirectory" ofType:@"momd"];
	
	if(path != nil) {
		if([path length] >0) {
            
			NSURL *momURL = [NSURL fileURLWithPath:path];
            
			_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
			
			return _managedObjectModel;
		}
	}
	
	_managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];
	
	return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    
//    if (_persistentStoreCoordinator != nil) {
//        return _persistentStoreCoordinator;
//    }
//    
//    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"thestar.sqlite"];
//    
//    NSError *error = nil;
//    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
//    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
//        /*
//         Replace this implementation with code to handle the error appropriately.
//         
//         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//         
//         Typical reasons for an error here include:
//         * The persistent store is not accessible;
//         * The schema for the persistent store is incompatible with current managed object model.
//         Check the error message to determine what the actual problem was.
//         
//         
//         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
//         
//         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
//         * Simply deleting the existing store:
//         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
//         
//         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
//         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
//         
//         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
//         
//         */
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//        abort();
//    }
//    
//    return _persistentStoreCoordinator;

    if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
    //	NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"ThaiPray.sqlite"]];
    NSURL *storeUrl = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"thestar.sqlite"];
	
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
							 
							 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
							 
							 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
	
	NSError *error;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
	
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
		// Handle error
		NSLog(@"Problem with PersistentStoreCoordinator: %@",error);
	}
	
	return _persistentStoreCoordinator;

}

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

// -----------------------------------------
// facebook
// -----------------------------------------
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    // FBSample logic
    // Any time the session is closed, we want to display the login controller (the user
    // cannot use the application unless they are logged in to Facebook). When the session
    // is opened successfully, hide the login controller and show the main UI.
    switch (state) {
        case FBSessionStateOpen: {
            
            // FBSample logic
            // Pre-fetch and cache the friends for the friend picker as soon as possible to improve
            // responsiveness when the user tags their friends.
//            NSLog(@"FBSessionStateOpen");
            // ---
            if (nil == self.facebook) {
                self.facebook = [[Facebook alloc]
                                 initWithAppId:FBSession.activeSession.appID
                                 andDelegate:nil];
                // Store the Facebook session information
                self.facebook.accessToken = FBSession.activeSession.accessToken;
                self.facebook.expirationDate = FBSession.activeSession.expirationDate;
            }
            
            
            if(splashCon){
                [splashCon.view removeFromSuperview];
            }
            
            [self checkFacebookProfile];
        }
            break;
        case FBSessionStateClosed: {
            
            NSLog(@"FBSessionStateClosed");
            
            // FBSample logic
            // Once the user has logged out, we want them to be looking at the root view.
            
            
            
//            [FBSession.activeSession closeAndClearTokenInformation];
//            [FBSession.activeSession close];
//            [FBSession setActiveSession:nil];
//            [self.facebook logout];
            
            self.facebook = nil;

            
        }
            break;
        case FBSessionStateClosedLoginFailed: {
            // if the token goes invalid we want to switch right back to
            // the login view, however we do it with a slight delay in order to
            // account for a race between this and the login view dissappearing
            // a moment before
            NSLog(@"FBSessionStateClosedLoginFailed");
            
        }
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SCSessionStateChangedNotification
                                                        object:session];
    
    if (error) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error: %@",
//                                                                     [AppDelegate FBErrorCodeDescription:error.code]]
//                                                            message:error.localizedDescription
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//        [alertView show];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    

    return [FBSession openActiveSessionWithReadPermissions:nil
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {

//                                             NSLog(@"session %@", session);
                                             
                                             [self sessionStateChanged:session state:state error:error];
                                         }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // FBSample logic
    // We need to handle URLs by passing them to FBSession in order for SSO authentication
    // to work.
    return [FBSession.activeSession handleOpenURL:url];
}
+ (NSString *)FBErrorCodeDescription:(FBErrorCode) code {
    switch(code){
        case FBErrorInvalid :{
            return @"FBErrorInvalid";
        }
        case FBErrorOperationCancelled:{
            return @"FBErrorOperationCancelled";
        }
        case FBErrorLoginFailedOrCancelled:{
            return @"FBErrorLoginFailedOrCancelled";
        }
        case FBErrorRequestConnectionApi:{
            return @"FBErrorRequestConnectionApi";
        }case FBErrorProtocolMismatch:{
            return @"FBErrorProtocolMismatch";
        }
        case FBErrorHTTPError:{
            return @"FBErrorHTTPError";
        }
        case FBErrorNonTextMimeTypeReturned:{
            return @"FBErrorNonTextMimeTypeReturned";
        }
        case FBErrorNativeDialog:{
            return @"FBErrorNativeDialog";
        }
        default:
            return @"[Unknown]";
    }
}

// check msisdn
-(void)checkMSISDN{
    
    if(self.msisdn == nil || [self.msisdn isEqualToString:@""]){
        NSLog(@"[MSISDN] not found");
        alertMode = ALERTMODE_FILLNUMBER;
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Telephone Number"
                              message: @"กรุณากรอกเบอร์โทรศัพท์"
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:@"Cancel", nil];
        
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
        [alert show];
        [alert release];
    }else{
        NSLog(@"[MSISDN] = %@", self.msisdn);
    }
    
}

// alert message
// ------------------------------------
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    //
    if(alertMode == ALERTMODE_FILLNUMBER){
        if (buttonIndex == 0) {
            
            NSLog(@"ok button fillnumber");
            NSString *strPhoneNum = [[alertView textFieldAtIndex:0] text];
            
            
            //validate phone number 08 to 668
            if ([strPhoneNum rangeOfString:@"0"].location == 0){
                self.tmpPhoneNum = [NSString stringWithFormat:@"66%@", [strPhoneNum substringFromIndex:1]];
            }
            NSLog(@"phone num = %@", self.tmpPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/sendOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                                 self.app_id,
                                 self.device,
                                 self.charset,
                                 self.tmpPhoneNum,
                                 self.appversion,
                                 self.apiversion
                                 ];
            
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
            if(data==nil){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                return;
            }
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                // -----
                
                if([status_code isEqualToString:@"200"]){
                    alertMode = ALERTMODE_OTP;
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: @"OTP Number"
                                          message: @"กรุณากรอกรหัส OTP จาก SMS"
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:@"Cancel", nil];
                    
                    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
                    [alert show];
                    [alert release];
                    
                }
            }
            [data release];
            [doc release];
            [url release];
        }
        else {
            NSLog(@"cancel button fillnumber");
        }
    // OTP fill number 
    }else if(alertMode== ALERTMODE_OTP){
        if (buttonIndex == 0) {

            NSString *txtOTP = [[alertView textFieldAtIndex:0] text];

            NSLog(@"phone msisdn = %@", self.msisdn);
            NSLog(@"phone tmpNum = %@", self.tmpPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/validateOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&ONETIMEPASS=%@&APPVERSION=%@&APIVERSION=%@",
                                 self.app_id,
                                 self.device,
                                 self.charset,
                                 self.tmpPhoneNum,
                                 txtOTP,
                                 self.appversion,
                                 self.apiversion
                                 ];
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
            if(data==nil){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                return;
            }
            
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                        options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                //
                if([status_code isEqualToString:@"200"]){
                    NSLog(@"complete");
                    
                    // update number
                    self.msisdn = self.tmpPhoneNum;
                    
                    // load msisdn from database
                    DBManager *dbMgr = [[DBManager alloc] init];
                    
                    NSMutableArray *ar = [dbMgr fetchProfile:@""];
                    if(ar !=nil && [ar count] > 0){
                        Profile *profile = [ar objectAtIndex:0];
                        profile.msisdn = self.tmpPhoneNum;
                        [dbMgr updateProfile:profile];
                    }else{
                        Profile *newProfile = [dbMgr getSchemaProfile];
                        newProfile.msisdn = self.tmpPhoneNum;
                        [dbMgr insertProfile:newProfile];
                    }
                    
                    [dbMgr release];
                    
                }else{
                    NSLog(@"error status_code = %@", status_code);
                }
            }
            [data release];
            [doc release];
            [url release];
        }
        else {
            NSLog(@"cancel button otp");
        }
    // for check subscribe
    }else if(alertMode == ALERTMODE_SUBSCRIBE_FILLNUMBER){
        if (buttonIndex == 0) {
            NSLog(@"ok button fillnumber");
            NSString *strPhoneNum = [[alertView textFieldAtIndex:0] text];
            
            
            //validate phone number 08 to 668
            if ([strPhoneNum rangeOfString:@"0"].location == 0){
                self.tmpPhoneNum = [NSString stringWithFormat:@"66%@", [strPhoneNum substringFromIndex:1]];
            }
            NSLog(@"phone num = %@", self.tmpPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/sendOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                                 self.app_id,
                                 self.device,
                                 self.charset,
                                 self.tmpPhoneNum,
                                 self.appversion,
                                 self.apiversion
                                 ];
            
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
            if(data==nil){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                return;
            }
            
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                        options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                //
                if([status_code isEqualToString:@"200"]){
                    // check otp
                    alertMode = ALERTMODE_SUBSCRIBE_OTP;
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: @"OTP Number"
                                          message: @"กรุณากรอกรหัส OTP จาก SMS"
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:@"Cancel", nil];
                    
                    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
                    [alert show];
                    [alert release];
                    
                }else{
                    NSLog(@"error status_code = %@", status_code);
                }
            }
            [data release];
            [doc release];
            [url release];
        }else{
            NSLog(@"cancel");
        }
    }else if(alertMode == ALERTMODE_SUBSCRIBE_OTP){
        // ok button
        if (buttonIndex == 0) {
            NSString *txtOTP = [[alertView textFieldAtIndex:0] text];
            
            NSLog(@"phone msisdn = %@", self.msisdn);
            NSLog(@"phone tmpNum = %@", self.tmpPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/validateOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&ONETIMEPASS=%@&APPVERSION=%@&APIVERSION=%@",
                                 self.app_id,
                                 self.device,
                                 self.charset,
                                 self.tmpPhoneNum,
                                 txtOTP,
                                 self.appversion,
                                 self.apiversion
                                 ];
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
            if(data==nil){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                return;
            }
            
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                        options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                //
                if([status_code isEqualToString:@"200"]){
                    NSLog(@"complete");
                    
                    // update number
                    self.msisdn = self.tmpPhoneNum;
                    
                    // load msisdn from database
                    DBManager *dbMgr = [[DBManager alloc] init];
                    
                    NSMutableArray *ar = [dbMgr fetchProfile:@""];
                    if(ar !=nil && [ar count] > 0){
                        Profile *profile = [ar objectAtIndex:0];
                        profile.msisdn = self.tmpPhoneNum;
                        [dbMgr updateProfile:profile];
                    }else{
                        Profile *newProfile = [dbMgr getSchemaProfile];
                        newProfile.msisdn = self.tmpPhoneNum;
                        [dbMgr insertProfile:newProfile];
                    }
                    
                    [dbMgr release];
                    [self checkMemberSubscribe];
                    
                }else{
                    NSLog(@"error status_code = %@", status_code);
                }
            }
            [data release];
            [doc release];
            [url release];
        }
        else {
            NSLog(@"cancel button otp");
        }
    }else if(alertMode == ALERTMODE_SUBSCRIBE_NOSUBSCRIBE_SHOW){
        if(buttonIndex == 0){
            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=&GMMD_CODE=", appDel.app_id, appDel.device, appDel.msisdn];
        
            NSLog(@"go out to charging: %@", strPath);
        
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
        }
        

    }else if(alertMode == ALERTMODE_SUBSCRIBE_TOPUPSERIAL){
        // call check
        NSString *serial = [[alertView textFieldAtIndex:0] text];
        
        // call topup api
        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/topup.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&SERIAL_CODE=%@&APPVERSION=%@&APIVERSION=%@",
                             appDel.app_id,
                             appDel.device,
                             appDel.charset,
                             appDel.msisdn,
                             serial,
                             appDel.appversion,
                             appDel.apiversion
                             ];
        NSLog(@"CALL API : %@", urlLink);
        NSURL *url = [[NSURL alloc] initWithString:urlLink];
        NSError *error;
        
        // connect api for return xml
        NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
        
        if([data isEqualToString:@""]){
            NSLog(@"[Connection ERROR]:%@", [error description]);
            [data release];
            [url release];
            return;
        }else{
            NSLog(@"data => %@", data);
        }
        
        //parse XML
        // ------------------------------------
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
        if(doc==nil){
            
            NSLog(@"parse xml document error");
        }else{
            
        }
        
        NSString *status_code = [[[doc.rootElement elementsForName:@"STATUS_CODE"]objectAtIndex:0] stringValue];
        if([status_code isEqualToString:@"200"]){
            NSLog(@"topup compeleted");
            self.subscriber = true;
        }       
    }
	
}

// check FakeApple
// ------------------------------------
-(void)checkFakeAppleAPI{
    // send api fakeApple
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/appleMSISDN.jsp?APP_ID=%@&APP_VERSION=%@&API_VERSION=%@",
                         self.app_id,
                         self.appversion,
                         self.apiversion
                         ];
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }

    // check found number is fakeApple == true
    NSString *fakeNumber = [data stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    if(![fakeNumber isEqualToString:@""] && [fakeNumber length] == 11){
        self.fakeApple = true;
        
        self.msisdn = fakeNumber;
        NSLog(@"fake apple api enable. msisdn = %@", self.msisdn);
        
    }else{
        self.fakeApple = false;
        NSLog(@"fake apple api disable");
    }
}

// ------------------------------------
// check Subscribe
// ------------------------------------
-(void)checkSubscribe{
    
    // check msisdn
    if(self.msisdn == nil || [self.msisdn isEqualToString:@""]){
        NSLog(@"[MSISDN] not found");
        alertMode = ALERTMODE_SUBSCRIBE_FILLNUMBER;
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Telephone Number"
                              message: @"กรุณากรอกเบอร์โทรศัพท์"
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:@"Cancel", nil];
        
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
        [alert show];
        [alert release];
    }else{
        NSLog(@"[MSISDN] = %@", self.msisdn);
        [self checkMemberSubscribe];
    }
    
}
-(void)checkMemberSubscribe{
    // check subscribe
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/getAccountDetailFromMSISDN.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         self.app_id,
                         self.device,
                         self.charset,
                         self.msisdn,
                         self.appversion,
                         self.apiversion
                         ];
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    
    // check packet_id = 1
    NSArray *ar = [doc.rootElement elementsForName:@"PACKET_ID"];
    NSString *packet_id = [[ar objectAtIndex:0] stringValue];
    NSLog(@"PACKET_ID = %@", packet_id);
    
    NSArray *arx = [doc.rootElement elementsForName:@"PACKET_STATUS"];
    NSString *charging_status = [[arx objectAtIndex:0] stringValue];
    
    
    if([packet_id isEqualToString:@"0"]){
        // alert show to charging
        NSLog(@"charging status= %@", charging_status);
        if([charging_status isEqualToString:@"N"]){
            alertMode = ALERTMODE_SUBSCRIBE_NOSUBSCRIBE_SHOW;
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"ท่านไม่ได้เป็นสมาชิก"
                                  message: @"สมัครสมาชิก"
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:@"Cancel", nil];
            
            //        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            //        [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
            [alert show];
            [alert release];
            
            
            
        }else if([charging_status isEqualToString:@"W"]){
            alertMode = ALERTMODE_SUBSCRIBE_TOPUPSERIAL;
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"TOPUP"
                                  message: @"กรุณากรอก SERIAL CODE ที่ได้จาก SMS"
                                  delegate: self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:@"Cancel", nil];
            
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
            
            [alert show];
            [alert release];
        }
        
        
    }else{
        // for subscribe
        self.subscriber = true;
    }
}

//Share Facebook
- (void)shareDataToFBByLinkURL:(NSString*)_linkURL imageURL:(NSString*)_imgURL name:(NSString*)_name caption:(NSString*)_caption description:(NSString*)_desc{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"191949534281277",@"app_id",
                                   _linkURL, @"link",
                                   _imgURL, @"picture",
                                   _name, @"name",
                                   _caption, @"caption",
                                   _desc, @"description",
                                   nil];
    
    [[self facebook] dialog:@"feed" andParams:params andDelegate:self];
}


// Facebook Delegate
-(NSDictionary*)parseURLParams:(NSString*)query{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val = [[kv objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    
    return params;
    
}

-(void)dialogCompleteWithUrl:(NSURL *)url{
//    NSDictionary *params = [self parseURLParams:[url query]];
    
//    if ([params valueForKey:@"post_id"]) {
//        NSString *msg = [NSString stringWithFormat:@""];
//        
//        // alert msg
//    }
    
}

-(void)checkFacebookProfile{
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 NSLog(@"load facebook user profile >>");
                 AppDelegate* appDel = (AppDelegate*)([UIApplication sharedApplication].delegate);
                 
                 appDel.fullname = user.name;
                 appDel.lbThumbFacebookMenu.text = user.name;
                 
                 NSString *fbid = [user objectForKey:@"id"];
                 appDel.email = [user objectForKey:@"email"];
                 
//                 NSLog(@"email = %@", appDel.email);
//                 NSLog(@"accessToken = %@",[appDel.facebook accessToken]);
                 appDel.accessToken = [appDel.facebook accessToken];
                 appDel.fb_id = fbid;
                 
                 NSString *imgPath = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", fbid];
                 if(self.datFacebookProfileImg == nil){
                     
                     self.datFacebookProfileImg = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgPath]];
                 }
                 
                 [appDel.imThumbFacebookMenu setImage:[UIImage imageWithData:self.datFacebookProfileImg]];
                 [appDel.imThumbFacebookMenu setHighlightedImage:[UIImage imageWithData:self.datFacebookProfileImg]];
                 CGRect rec = CGRectMake(10, 2, 40, 40);
                 [appDel.imThumbFacebookMenu setFrame:rec];
                 
                 //profile
                 appDel.lbProFacebookName.text = user.name;
                 [appDel.ivProFacebookImage setImage:[UIImage imageWithData:datFacebookProfileImg]];
                 
             }else{
                 NSLog(@"error ");
             }
         }];
    }else{
        
        // default
        NSLog(@"! active session");
        AppDelegate* appDel = (AppDelegate*)([UIApplication sharedApplication].delegate);
        [appDel.imThumbFacebookMenu setImage:[UIImage imageNamed:@"ico_profile"]];
        lbThumbFacebookMenu.text = @"My Profile";
        //clear data thumb
        self.datFacebookProfileImg = nil;
        
    }
    
}


// -- receive push notification
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    // clear data shelf download
    self.arShelfMusic = nil;
    self.arShelfMV = nil;
    self.arShelfSpecialClip = nil;
    
    NSLog(@"come back with remoteNotification");
//    [self.slideMenu openNotiPage];
    
    NotiViewController *notCon = [[NotiViewController alloc] initWithNibName:@"NotiViewController" bundle:nil];
    [self.window.rootViewController presentModalViewController:notCon animated:YES];
    [notCon release];
}

-(void)fbDidNotLogin:(BOOL)cancelled{
    
}


@end
