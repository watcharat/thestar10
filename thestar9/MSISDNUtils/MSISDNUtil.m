//
//  MSISDNUtil.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 3/3/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "MSISDNUtil.h"

#import "AppDelegate.h"
#import "DBManager.h"
#import "GDataXMLNode.h"


@implementation MSISDNUtil

@synthesize delegate;

@synthesize alertMode;
@synthesize tmpPhoneNum;

-(void)alertshow{
    NSLog(@"[MSISDN] not found");
    self.alertMode = ALERTMODE_FILLNUMBER;
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Telephone Number"
                          message: @"กรุณากรอกเบอร์โทรศัพท์"
                          delegate: self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:@"Cancel", nil];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [alert show];
    [alert release];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(self.alertMode == ALERTMODE_FILLNUMBER){
        if (buttonIndex == 0) {
            NSLog(@"ok button fillnumber");
            NSString *strPhoneNum = [[alertView textFieldAtIndex:0] text];
            
            
            //validate phone number 08 to 668
            if ([strPhoneNum rangeOfString:@"0"].location == 0){
                self.tmpPhoneNum = [NSString stringWithFormat:@"66%@", [strPhoneNum substringFromIndex:1]];
            }
            NSLog(@"phone num = %@", self.tmpPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/sendOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                                 appDel.app_id,
                                 appDel.device,
                                 appDel.charset,
                                 self.tmpPhoneNum,
                                 appDel.appversion,
                                 appDel.apiversion
                                 ];
            
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
            if(data==nil){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                return;
            }
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                // -----
                
                if([status_code isEqualToString:@"200"]){
                    self.alertMode = ALERTMODE_OTP;
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: @"OTP Number"
                                          message: @"กรุณากรอกรหัส OTP จาก SMS"
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:@"Cancel", nil];
                    
                    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
                    [alert show];
                    [alert release];
                    
                }
            }
            [data release];
            [doc release];
            [url release];
        }else{
            //button cancel
        }
    }else if(alertMode== ALERTMODE_OTP){
        if (buttonIndex == 0) {
            
            NSString *txtOTP = [[alertView textFieldAtIndex:0] text];
            
            NSLog(@"phone tmpNum = %@", self.tmpPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/validateOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&ONETIMEPASS=%@&APPVERSION=%@&APIVERSION=%@",
                                 appDel.app_id,
                                 appDel.device,
                                 appDel.charset,
                                 self.tmpPhoneNum,
                                 txtOTP,
                                 appDel.appversion,
                                 appDel.apiversion
                                 ];
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
            if(data==nil){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                return;
            }
            
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                        options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                NSString *status_message = [[[doc.rootElement elementsForName:@"STATUS_CODE"] objectAtIndex:0] stringValue];
                //
                if([status_code isEqualToString:@"200"]){
                    NSLog(@"complete");
                    
                    // update number
                    appDel.msisdn = self.tmpPhoneNum;
                    
                    // load msisdn from database
                    DBManager *dbMgr = [[DBManager alloc] init];
                    
                    NSMutableArray *ar = [dbMgr fetchProfile:@""];
                    if(ar !=nil && [ar count] > 0){
                        Profile *profile = [ar objectAtIndex:0];
                        profile.msisdn = self.tmpPhoneNum;
                        [dbMgr updateProfile:profile];
                    }else{
                        Profile *newProfile = [dbMgr getSchemaProfile];
                        newProfile.msisdn = self.tmpPhoneNum;
                        [dbMgr insertProfile:newProfile];
                    }
                    
                    [dbMgr release];
                    
                    if ([delegate respondsToSelector:@selector(receptOTPSuccess:)]){
                        [delegate receptOTPSuccess:appDel.msisdn];
                    }

                }else{
//                    NSLog(@"error status_code = %@", status_code);
                    
                    if ([delegate respondsToSelector:@selector(receptOTPFails:)]){
                        [delegate receptOTPFails:status_code status_message:status_message];
                    }
                }
            }
            [data release];
            [doc release];
            [url release];
        }
        else {
            NSLog(@"cancel button otp");
        }
    }
}


@end
