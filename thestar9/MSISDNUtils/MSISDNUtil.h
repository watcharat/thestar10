//
//  MSISDNUtil.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 3/3/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ALERTMODE_FILLNUMBER 0
#define ALERTMODE_OTP 1



// delegate
@protocol MSISDNUtilDelegate <NSObject>

@required
-(void) receptOTPSuccess:(NSString*)msisdn;
-(void) receptOTPFails:(NSString*)status_code status_message:(NSString*)status_message;
@end


// main class

@interface MSISDNUtil : NSObject<UIAlertViewDelegate>
{
    id<MSISDNUtilDelegate> delegate;
    int alertMode;
    
    NSString *tmpPhoneNum;
}
@property (nonatomic, assign) id<MSISDNUtilDelegate> delegate;
@property (readwrite, assign) int alertMode;
@property (nonatomic, retain) NSString *tmpPhoneNum;

-(void)alertshow;
@end

