//
//  AppDelegate.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 12/11/55 BE.
//  Copyright (c) 2555 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DownloadManager.h"

#import <CoreData/CoreData.h>

// splash screen
#import "SplashViewController.h"
#import "UIDevice+IdentifierAddition.h"

// facebook
//#import <FacebookSDK/FacebookSDK.h>

#import "Facebook.h"
#import "FBSBJSON.h"

//extern NSString *const SCSessionStateChangedNotification;

#import "VoteViewController.h"

#import "JWSlideMenuController.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, FBDialogDelegate, FBRequestDelegate, FBSessionDelegate>{
    NSString* app_id;
    NSString* device;
    NSString* charset;
    NSString* msisdn;
    NSString* appversion;
    NSString* apiversion;
    
    DownloadManager *downloadMgr;
    
    UITableView *tableMusicDownload;
    
    SplashViewController *splashCon;
    
    // alert msisdn, otp
    int alertMode;
    NSString *tmpPhoneNum;

    BOOL fakeApple;
    
    BOOL subscriber;
    
    VoteViewController *voteController;
    
    UILabel* lbThumbFacebookMenu;
    UIImageView* imThumbFacebookMenu;
    
    UILabel* lbProFacebookName;
    UIImageView* ivProFacebookImage;
    
    NSString* accessToken;
    NSString* fb_id;
    
    NSData *datFacebookProfileImg;
    JWSlideMenuController* slideMenu;
    
    NSMutableArray *arShelfMusic;
    NSMutableArray *arShelfMV;
    NSMutableArray *arShelfSpecialClip;
    
}

@property (retain, nonatomic) NSString* app_id;
@property (retain, nonatomic) NSString* device;
@property (retain, nonatomic) NSString* charset;
@property (retain, nonatomic) NSString* msisdn;
@property (retain, nonatomic) NSString* appversion;
@property (retain, nonatomic) NSString* apiversion;
@property (retain, nonatomic) NSString* tmpPhoneNum;

@property (strong, nonatomic) UIWindow* window;

@property (readwrite, assign) BOOL fakeApple;
@property (readwrite, assign) BOOL subscriber;

@property (retain, nonatomic) DownloadManager *downloadMgr;

@property (retain, nonatomic) UITableView *tableMusicDownload;

@property (retain, nonatomic) SplashViewController *splashCon;

// vote controller
@property (retain, nonatomic)VoteViewController *voteController;

// fb thum image
@property (retain, nonatomic)UILabel* lbThumbFacebookMenu;
@property (retain, nonatomic)UIImageView* imThumbFacebookMenu;

@property (retain, nonatomic)UILabel* lbProFacebookName;
@property (retain, nonatomic)UIImageView* ivProFacebookImage;


@property (retain, nonatomic)NSString* email;
@property (retain, nonatomic)NSString* fullname;
@property (nonatomic, retain)Facebook *facebook;

@property (nonatomic, retain)NSString* accessToken;
@property (nonatomic, retain)NSString* fb_id;

@property (nonatomic, retain)NSData *datFacebookProfileImg;

@property (nonatomic, retain)JWSlideMenuController* slideMenu;

// tmp data
@property (nonatomic, retain)NSMutableArray *arShelfMusic;
@property (nonatomic, retain)NSMutableArray *arShelfMV;
@property (nonatomic, retain)NSMutableArray *arShelfSpecialClip;


// core data
// -----------------------------------------------------------
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


// facebook api
// ------------
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
+ (NSString *)FBErrorCodeDescription:(FBErrorCode) code;


// check msisdn
-(void)checkMSISDN;
-(void)checkSubscribe;

- (void)shareDataToFBByLinkURL:(NSString*)_linkURL imageURL:(NSString*)_imgURL name:(NSString*)_name caption:(NSString*)_caption description:(NSString*)_desc;

-(void)checkFacebookProfile;

// notification
-(void)sendRequestNotification;

@end
