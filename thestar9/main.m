//
//  main.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 12/11/55 BE.
//  Copyright (c) 2555 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
