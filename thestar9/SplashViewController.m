//
//  SplashViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/8/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "SplashViewController.h"

//#import <FacebookSDK/FacebookSDK.h>
#import "Facebook.h"
#import "AppDelegate.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        // back button
//        [[UIBarButtonItem appearance]
//         setBackButtonBackgroundImage:[UIImage imageNamed:@"btn_backbg.png"]
//         forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        // create back button
        
        int imageSize = 16; //REPLACE WITH YOUR IMAGE WIDTH
        UIImage *barBackBtnImg = [[UIImage imageNamed:@"btn_backbg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, imageSize, 0, 0)];
        
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:barBackBtnImg
                                                          forState:UIControlStateNormal
                                                        barMetrics:UIBarMetricsDefault];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

-(void)viewWillAppear:(BOOL)animated{
//    NSLog(@"viewWillAppear");
    [self checkFacebook];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// button press
- (IBAction)btnSkipPressed:(id)sender {
    NSLog(@"skip button pressed");
    [[self view] removeFromSuperview];
}


-(void) checkFacebook{
    // See if we have a valid token for the current state.
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        // To-do, show logged in view
        NSLog(@"logged facebook");
        
        if(FBSession.activeSession.isOpen){
            NSLog(@"isOpen");
            [self.view removeFromSuperview];
        }else{
            
            NSLog(@"!isOpen");
            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [appDel openSessionWithAllowLoginUI:YES];
            
            
//            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [appDel checkFacebookProfile];
        }
        
        
        //load cache sessiongOpen
        
        
    } else {
        // No, display the login page.
        NSLog(@"not login");
    }
}
- (IBAction)btnFacebookConnectPressed:(id)sender {
    [self showLoginView];
}


-(void)showLoginView{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel openSessionWithAllowLoginUI:YES];
}


-(void)viewDidDisappear:(BOOL)animated{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel checkFacebookProfile];
}

@end
