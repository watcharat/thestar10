//
//  JWSlideMenuController.h
//  JWSlideMenu
//
//  Created by Jeremie Weldin on 11/14/11.
//  Copyright (c) 2011 Jeremie Weldin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JWNavigationController;
@class JWSlideMenuViewController;

@interface JWSlideMenuController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    float con_pos_x;
    float ref_x;
    
}

@property (retain, nonatomic) UITableView *menuTableView;
@property (retain, nonatomic) UIView *menuView;
@property (retain, nonatomic) UIToolbar *contentToolbar;
@property (retain, nonatomic) UIView *contentView;
@property (retain, nonatomic) UIColor *menuLabelColor;

//---
@property (readwrite, assign) float con_pos_x;
@property (readwrite, assign) float ref_x;

-(IBAction)toggleMenu;


-(JWNavigationController *)addViewController:(JWSlideMenuViewController *)controller withTitle:(NSString *)title andImage:(UIImage *)image;

-(JWNavigationController *)addViewController:(JWSlideMenuViewController *)controller withTitle:(NSString *)title andImage:(UIImage *)image andSeletedImage:(UIImage *)andSeletedImage;

-(void)openVotePage;
-(void)openNotiPage;

@end
