//
//  JWSlideMenuController.m
//  JWSlideMenu
//
//  Created by Jeremie Weldin on 11/14/11.
//  Copyright (c) 2011 Jeremie Weldin. All rights reserved.
//

#import "JWSlideMenuController.h"
#import "JWNavigationController.h"
#import "JWSlideMenuViewController.h"

#import "VoteViewController.h"

#import "AppDelegate.h"
#import "VoteViewController.h"

#import "ProfileViewController.h"
//#import "NoticeViewController.h"

#import "NotiViewController.h"

@implementation JWSlideMenuController

@synthesize menuTableView;
@synthesize menuView;
@synthesize contentToolbar;
@synthesize contentView;
@synthesize menuLabelColor;


@synthesize con_pos_x, ref_x;

- (id)init
{
    self = [super init];
    if (self) {
        
        [self createHeaderMenu];
        [self createMenuTable];
//        [self createMenuView];
        [self createContentView];
        
        [self createSwipeMenuAction];

    }
    return self;
}


-(void)createHeaderMenu{
    UIImageView *imHeader = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_head_menu.png"]];
    [self.view addSubview:imHeader];

}

/*
-(void)createMenuView{
    CGRect masterRect = self.view.bounds;
    float menuWidth = 260.0; //masterRect.size.width - 53
    
    float headerH = 44;
    CGRect menuFrame = CGRectMake(0.0, headerH, menuWidth, masterRect.size.height-headerH);
    
    self.menuView = [[[UIView alloc] initWithFrame:menuFrame] autorelease];
    
    [self.menuView setBackgroundColor:[UIColor blackColor]];
    
    //scrollable
    //-----------------------------------------
    
//    float h = 40;
    UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, menuWidth, masterRect.size.height-headerH)];

    scrollview.contentSize = CGSizeMake(260, 480-60);
    
    scrollview.delegate = self;
    [scrollview setShowsVerticalScrollIndicator:NO];
    [self.menuView addSubview:scrollview];
    
    UIView *vwBgMenu = [[UIView alloc] init];
    [vwBgMenu setFrame:CGRectMake(0, 0, 260, 480-60)];
    [vwBgMenu setBackgroundColor:[UIColor blackColor]];
    [scrollview addSubview:vwBgMenu];
    
    
    NSMutableArray *arButton = [[NSMutableArray alloc] init];
    [arButton addObject:@"btn_vote"];
    [arButton addObject:@"btn_profile"];
    [arButton addObject:@"btn_thestar_profile"];
    [arButton addObject:@"btn_video_highlight"];
    [arButton addObject:@"btn_news"];
    [arButton addObject:@"btn_gallery"];
    [arButton addObject:@"btn_download"];
    [arButton addObject:@"btn_game_activity"];
    
    for(int i=0; i< [arButton count]; i++){
        UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString* btnFile = [NSString stringWithFormat:@"%@.png", [arButton objectAtIndex:i]];
        NSString* btnFileAct = [NSString stringWithFormat:@"%@_active.png", [arButton objectAtIndex:i]];
        [btn setBackgroundImage:[UIImage imageNamed:btnFile] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:btnFileAct] forState:UIControlStateHighlighted];
        btn.tag = i;
        [btn addTarget:self action:@selector(btnMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
        CGRect rec = CGRectMake(0, i*48, 262, 48);
        [btn setFrame:rec];
        [vwBgMenu addSubview:btn];
    }
//    scrollview.contentSize = CGSizeMake(260, 600);
    
    
    [arButton release];
    [vwBgMenu release];
    [scrollview release];
    [self.view addSubview:self.menuView];
    //-----------------------------------------
}
*/

-(IBAction)btnMenuPressed:(id)sender{
    UIButton *btn = (UIButton*)sender;
    NSLog(@"btnMenuPressed = %d", btn.tag);
    
    if([contentView.subviews count] == 1){
        [[contentView.subviews objectAtIndex:0] removeFromSuperview];
    }
    
    UIViewController* controller = (UIViewController*)[self.childViewControllers objectAtIndex:btn.tag];
    controller.view.frame = self.contentView.bounds;
    
    [contentView addSubview:controller.view];
    
    [self toggleMenu];
}



-(void)createMenuTable{
    
    CGRect masterRect = self.view.bounds;
    float menuWidth = 260.0; //masterRect.size.width - 53
    
    CGRect menuFrame = CGRectMake(0.0, 22, menuWidth, masterRect.size.height-22);
    
    self.menuLabelColor = [UIColor whiteColor];
    
    self.menuTableView = [[[UITableView alloc] initWithFrame:menuFrame] autorelease];
    self.menuTableView.dataSource = self;
    self.menuTableView.delegate = self;
//    self.menuTableView.backgroundColor = [UIColor darkGrayColor];
    self.menuTableView.backgroundColor = [UIColor blackColor];
    self.menuTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.menuTableView.separatorColor = [UIColor darkGrayColor];
    
    self.menuView = [[[UIView alloc] initWithFrame:menuFrame] autorelease];
    [self.menuView addSubview:self.menuTableView];
    [self.view addSubview:self.menuView];
    
}

-(void)createContentView{
    CGRect masterRect = self.view.bounds;
    CGRect contentFrame = CGRectMake(0.0, 0.0, masterRect.size.width, masterRect.size.height);
    
    self.contentView = [[[UIView alloc] initWithFrame:contentFrame] autorelease];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    self.contentView.backgroundColor = [UIColor grayColor];
    self.contentView.backgroundColor = [UIColor blackColor];
    
    
    [self.view insertSubview:self.contentView aboveSubview:self.menuView];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(IBAction)toggleMenu
{
//    NSLog(@"toggleMenu");
    /*
    [UIView beginAnimations:@"Menu Slide" context:nil];
    [UIView setAnimationDuration:0.2];
        
    if(self.contentView.frame.origin.x == 0) //Menu is hidden
    {
        CGRect newFrame = CGRectOffset(self.contentView.frame, self.menuView.frame.size.width, 0.0);
        self.contentView.frame = newFrame;
    }
    else //Menu is shown
    {
        //[menuTableView reloadData];
        CGRect newFrame = CGRectOffset(self.contentView.frame, -(self.menuView.frame.size.width), 0.0);
        self.contentView.frame = newFrame;
    }
    
    [UIView commitAnimations];
     */
    
    [UIView beginAnimations:@"Menu Slide" context:nil];
    [UIView setAnimationDuration:0.2];
    
    if(self.contentView.frame.origin.x == 0){
        CGRect newFrame = CGRectMake(260, 0, 320, 480);
        self.contentView.frame = newFrame;
    }else{
        
        CGRect newFrame = CGRectMake(0, 0, 320, 480);
        self.contentView.frame = newFrame;
    }
    [UIView commitAnimations];
    
    [UIView commitAnimations];
}

// -----
-(JWNavigationController *)addViewController:(JWSlideMenuViewController *)controller withTitle:(NSString *)title andImage:(UIImage *)image
{
    JWNavigationController *navController = [[[JWNavigationController alloc] initWithRootViewController:controller] autorelease];
    navController.slideMenuController = self;
    navController.title = title;
    navController.tabBarItem.image = image;


    [self addChildViewController:navController];
    
    if([self.childViewControllers count] == 1)
    {
        [self.contentView addSubview:navController.view];
    }
    
    return navController;
}

//-- modify by kha increase
-(JWNavigationController *)addViewController:(JWSlideMenuViewController *)controller withTitle:(NSString *)title andImage:(UIImage *)image andSeletedImage:(UIImage *)andSeletedImage
{
    JWNavigationController *navController = [[[JWNavigationController alloc] initWithRootViewController:controller] autorelease];
    navController.slideMenuController = self;
    navController.title = title;
    
    navController.selectImage = image;
    navController.highlightImage = andSeletedImage;

    
    [self addChildViewController:navController];
    
    if([self.childViewControllers count] == 1)
    {
        [self.contentView addSubview:navController.view];
    }
    
    return navController;
}


#pragma mark - UITableViewDataSource/Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
                                      reuseIdentifier:cellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        // Do something here......................
    }
    
    UIView *selectionColor = [[UIView alloc] init];
    UIColor *selectedColor = [UIColor colorWithRed:162/255.0 green:121/255.0 blue:29/255.0 alpha:1];
    selectionColor.backgroundColor = selectedColor;
    cell.selectedBackgroundView = selectionColor;
    
    //TODO: either support tabbaritem or a protocol in order to handle images in the menu.
    
    JWNavigationController *controller = (JWNavigationController *)[self.childViewControllers objectAtIndex:indexPath.row] ;
    cell.textLabel.text = controller.title;

    [cell.textLabel setFont:[UIFont systemFontOfSize:16]];  // font menu size
    
    cell.textLabel.textColor = menuLabelColor;
    [cell setBackgroundColor:[UIColor greenColor]];
    
    cell.imageView.image = controller.selectImage;
    cell.imageView.highlightedImage = controller.highlightImage;
    
    if(indexPath.row == 1){
        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        appDel.imThumbFacebookMenu = cell.imageView;
        appDel.lbThumbFacebookMenu = cell.textLabel;
        
        [appDel checkFacebookProfile];
    }
    
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.childViewControllers count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    //UIViewController *previousChildViewController =
    //[self transitionFromViewController:previousChildViewController toViewController:newController duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:NULL completion:NULL];
    
    if([contentView.subviews count] == 1){
        [[contentView.subviews objectAtIndex:0] removeFromSuperview];
    }
    
    UIViewController* controller = (UIViewController*)[self.childViewControllers objectAtIndex:indexPath.row];
    controller.view.frame = self.contentView.bounds;
    
    [contentView addSubview:controller.view];
    
//    NSLog(@"indexPath.row = %d", indexPath.row);
//    if(indexPath.row==0){
//        
//        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
//        VoteViewController* voteCon = appDel.voteController;
//        [voteCon reloadDataForVote];
//        
//    }
    [self toggleMenu];
}
 

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [self setFirstRowSelected];
}

-(void)setFirstRowSelected{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.menuTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
}

-(void)setMyProfileRowSelected{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.menuTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
}

- (void)viewDidUnload
{
    [self setMenuView:nil];
    [self setContentView:nil];
    [self setMenuTableView:nil];
    [self setMenuLabelColor:nil];

    [super viewDidUnload];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return YES;
//}

- (void)dealloc {
    [menuView release];
    [contentView release];
    [contentToolbar release];
    [menuTableView release];
    [menuLabelColor release];
    [super dealloc];
}

// ------------------------------
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



-(void)createSwipeMenuAction{
    
//    UISwipeGestureRecognizer *swipeLGest = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(fingerSwipeLeft:)];
//    [swipeLGest setDirection:UISwipeGestureRecognizerDirectionLeft];
//    [self.view addGestureRecognizer:swipeLGest];
//    [swipeLGest release];
//    //
//  
//    UISwipeGestureRecognizer *swipeRGest = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(fingerSwipeRight:)];
//    [swipeRGest setDirection:UISwipeGestureRecognizerDirectionLeft];
//    [self.view addGestureRecognizer:swipeRGest];
//    [swipeRGest release];


    // pan gesture
    UIPanGestureRecognizer *panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecogonizer:)];
    
    [self.view addGestureRecognizer:panGest];
    [panGest release];
    
}



//-(void)fingerSwipeLeft:(UISwipeGestureRecognizer *) sender {
//    NSLog(@"fingerSwipeLeft");
//}
//
//-(void)fingerSwipeRight:(UISwipeGestureRecognizer *) sender {
//    NSLog(@"fingerSwipeRight");
//}


-(void)panRecogonizer:(UIPanGestureRecognizer *)recognizer {

//    CGPoint translation = [recognizer translationInView:self.view];
//    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
//                                         recognizer.view.center.y + translation.y);
//    
//    
//    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    
    CGPoint translation = [recognizer translationInView:self.view];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.con_pos_x = translation.x;
        self.ref_x = self.contentView.frame.origin.x;

        
    }else if(recognizer.state == UIGestureRecognizerStateChanged){
        
        float dif_x = translation.x - self.con_pos_x;
        
//        NSLog(@"dif_x = %f", dif_x);
//        NSLog(@"total position all : %f", self.ref_x+dif_x);
        CGFloat menuWidth = 260.0;
        if(self.ref_x+dif_x > menuWidth || self.ref_x+dif_x < 0){
            return;
        }
        
        [self.contentView setFrame:CGRectMake(self.ref_x+dif_x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        
        
    }else if(recognizer.state == UIGestureRecognizerStateEnded){
        float dif_x = translation.x - self.con_pos_x;
                
        CGFloat menuWidth = 260.0;
        if(self.ref_x+dif_x > menuWidth || self.ref_x+dif_x < 0){
            return;
        }
        
//        [self.contentView setFrame:CGRectMake(self.ref_x+dif_x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        
        [self moveWithAnimation:self.ref_x+dif_x];
    }
    
}

-(void)moveWithAnimation:(float)dif{
    float center_point = 320.0f/2;
    [UIView beginAnimations:@"MoveNearSide" context:nil];
    [UIView setAnimationDuration:0.2];
    
    if(dif > center_point){
        CGRect newFrame = CGRectMake(260, 0, 320, 480);
        self.contentView.frame = newFrame;
    }else{

        CGRect newFrame = CGRectMake(0, 0, 320, 480);
        self.contentView.frame = newFrame;
    }
    [UIView commitAnimations];
    
    
}


-(void)openVotePage{
    
    if([contentView.subviews count] == 1){
        [[contentView.subviews objectAtIndex:0] removeFromSuperview];
    }
    
    UIViewController* controller = (UIViewController*)[self.childViewControllers objectAtIndex:0];
    controller.view.frame = self.contentView.bounds;
    
    [contentView addSubview:controller.view];
    
    
    [self setFirstRowSelected];
    
}


-(void)openNotiPage{
    [self setMyProfileRowSelected];
    
    if([contentView.subviews count] == 1){
        [[contentView.subviews objectAtIndex:0] removeFromSuperview];
    }
    
    ProfileViewController* controller = (ProfileViewController*)[self.childViewControllers objectAtIndex:1];
    controller.view.frame = self.contentView.bounds;
    [contentView addSubview:controller.view];
    
    [self toggleMenu];
    
    // add child notice
    NotiViewController *notiCon = [[NotiViewController alloc] initWithNibName:@"NotiViewController" bundle:nil];
    
    [controller presentModalViewController:notiCon animated:YES];

}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
