//
//  MusicPlayerController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/26/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "MusicPlayerController.h"

#import "GDataXMLNode.h"
#import "AppDelegate.h"

#import <Twitter/Twitter.h>


@interface MusicPlayerController ()

@end

@implementation MusicPlayerController
@synthesize contentCode, gmmdCode;
@synthesize cover, pathPreview;
@synthesize songname;

@synthesize imBg;
@synthesize lbSongname;
@synthesize btnPlayPause;

@synthesize audioPlayer;

@synthesize arData;
@synthesize arListDownloaded;

@synthesize seqNum;

@synthesize timer;

@synthesize pgbarView;
@synthesize lbStartTime;
@synthesize lbFinishTime;

@synthesize lyricView;

@synthesize btnRepeat;
@synthesize playlistTable;

@synthesize panelShare;
@synthesize thmCover;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setDataPlayerArray:(NSMutableArray*)ardata seq:(int)seq{
    self.seqNum = seq;
    self.arData = ardata;
}

-(void)loadDataPlayer:(NSMutableArray*)ardata listDownloaded:(NSMutableArray *)listDownloaded seq:(int)se{
    self.seqNum = se;
    self.arData = ardata;
    self.arListDownloaded = listDownloaded;
}

// other
- (id)init
{
    self = [super init];
    if (self) {
        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
        [self.view setBackgroundColor:[UIColor blackColor]];
        //        self.title = @"Video Detail";
        
        [self createUI];
    }
    
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [self initPlaylistTable];
    //load image
    [self renderData];
    
}

-(void)createUI{
    float top_button = 5;
    float top_sidedown = 390;
    float top_btn_down = top_sidedown+12;
    
    //create background image (with cover)
    //-------------------------------------
    CGRect rectBg = CGRectMake(0, 41, 320, 350);
    imBg = [[UIImageView alloc] initWithFrame:rectBg];
    imBg.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imBg];
    
    UIImageView* imFillBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fill_player.png"]];
    [imFillBg setFrame:rectBg];
    [self.view addSubview:imFillBg];
    
    
    //bar button section (playlist, lyrics, download, songname)
    //-------------------------------------
    CGRect rectBar = CGRectMake(0, 0, 320, 41);
    UIImageView *bar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bar_player.png"]];
    
    [bar setFrame:rectBar];
    [self.view addSubview:bar];
    
    // button back
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack addTarget:self
                action:@selector(btnBackPressed)
      forControlEvents:UIControlEventTouchDown];
    
    btnBack.frame = CGRectMake(0, top_button, 47, 30);
    [btnBack setImage:[UIImage imageNamed:@"bt-Back_off.png"] forState:UIControlStateNormal];
    [btnBack setImage:[UIImage imageNamed:@"bt-Back_on.png"] forState:UIControlStateHighlighted];
    [self.view addSubview:btnBack];
    
    // block now play & text
    UIImageView *imBlock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"block_nowplay.png"]];
    [imBlock setFrame:CGRectMake(70, top_button, 118, 30)];
    [self.view addSubview:imBlock];
    
    UIImageView *imNowPlay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"txt_nowPlaying.png"]];
    [imNowPlay setFrame:CGRectMake(105, top_button+2, 48, 10)];
    [self.view addSubview:imNowPlay];
    
    // label
    lbSongname = [[UILabel alloc] initWithFrame:CGRectMake(80, top_button+2, 100, 27)];
    [lbSongname setText:@""];
    [lbSongname setTextColor:[UIColor whiteColor]];
    [lbSongname setFont:[UIFont systemFontOfSize:14]];
    [lbSongname setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:lbSongname];
    
    // playlist
    UIButton *btnPlaylist = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPlaylist addTarget:self
                    action:@selector(btnPlaylistPressed)
          forControlEvents:UIControlEventTouchDown];
    
    btnPlaylist.frame = CGRectMake(198, top_button, 37, 30);
    [btnPlaylist setImage:[UIImage imageNamed:@"bt_playlist_off.png"] forState:UIControlStateNormal];
    [btnPlaylist setImage:[UIImage imageNamed:@"bt_playlist_on.png"] forState:UIControlStateHighlighted];
    [self.view addSubview:btnPlaylist];
    // Hide playlist Button
    [btnPlaylist setHidden:YES];
    
    // lyric
    UIButton *btnLyrics = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLyrics addTarget:self
                  action:@selector(btnLyricsPressed)
        forControlEvents:UIControlEventTouchDown];
    
    btnLyrics.frame = CGRectMake(239, top_button, 37, 30);
    [btnLyrics setImage:[UIImage imageNamed:@"bt_Lyrics_off.png"] forState:UIControlStateNormal];
    [btnLyrics setImage:[UIImage imageNamed:@"bt_Lyrics_on.png"] forState:UIControlStateHighlighted];
    [self.view addSubview:btnLyrics];
    
    // download
    UIButton *btnDownload = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDownload addTarget:self
                    action:@selector(btnDownloadPressed)
          forControlEvents:UIControlEventTouchDown];
    
    btnDownload.frame = CGRectMake(279, top_button, 37, 30);
    [btnDownload setImage:[UIImage imageNamed:@"bt_downPly_off.png"] forState:UIControlStateNormal];
    [btnDownload setImage:[UIImage imageNamed:@"bt_downPly_on.png"] forState:UIControlStateHighlighted];
    [self.view addSubview:btnDownload];
    
    
    //center section (lyric)
    //-------------------------------------
    
    
    //bottom section (volume, repeat, <<, ||, >>, replay, share)
    //-------------------------------------
    [self createPanelShare];
    
    
    // background
    UIImageView *imBgSideBottom = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_player.png"]];
    
    [imBgSideBottom setFrame:CGRectMake(0, top_sidedown, 320, 54)];
    
    [self.view addSubview:imBgSideBottom];
    
    
    //button speaker
    UIButton *btnSpeaker = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSpeaker addTarget:self
                   action:@selector(btnSpeakerPressed)
         forControlEvents:UIControlEventTouchDown];
    
    
    [btnSpeaker setImage:[UIImage imageNamed:@"icon_specker_on.png"] forState:UIControlStateNormal];
    //    [btnSpeaker setImage:[UIImage imageNamed:@"icon_specker_on.png"] forState:UIControlStateHighlighted];
    
    btnSpeaker.frame = CGRectMake(10, top_btn_down, 33, 30);
    
    [self.view addSubview:btnSpeaker];
    
    // button previous
    UIButton *btnPrevious = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPrevious addTarget:self
                    action:@selector(btnPreviousPressed)
          forControlEvents:UIControlEventTouchDown];
    
    
    [btnPrevious setImage:[UIImage imageNamed:@"bt_fwBack_off.png"] forState:UIControlStateNormal];
    [btnPrevious setImage:[UIImage imageNamed:@"bt_fwBack_on.png"] forState:UIControlStateHighlighted];
    
    btnPrevious.frame = CGRectMake(84, top_sidedown+4, 46, 46);
    
    [self.view addSubview:btnPrevious];
    
    
    // button forward
    UIButton *btnForward = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnForward addTarget:self
                   action:@selector(btnForwardPressed)
         forControlEvents:UIControlEventTouchDown];
    
    
    [btnForward setImage:[UIImage imageNamed:@"bt_fwNext_off.png"] forState:UIControlStateNormal];
    [btnForward setImage:[UIImage imageNamed:@"bt_fwNext_on.png"] forState:UIControlStateHighlighted];
    
    btnForward.frame = CGRectMake(190, top_sidedown+4, 46, 46);
    
    [self.view addSubview:btnForward];
    
    // button play/ pause
    btnPlayPause = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPlayPause addTarget:self
                     action:@selector(btnPlayPausePressed)
           forControlEvents:UIControlEventTouchDown];
    
    
    [btnPlayPause setImage:[UIImage imageNamed:@"bt_pause.png"] forState:UIControlStateNormal];
    float pxPlay = 160-30;
    btnPlayPause.frame = CGRectMake(pxPlay, top_sidedown-2, 60, 60);
    
    [self.view addSubview:btnPlayPause];
    
    //button share
    UIButton *btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnShare addTarget:self
                 action:@selector(btnSharePressed)
       forControlEvents:UIControlEventTouchDown];
    
    
    [btnShare setImage:[UIImage imageNamed:@"icon_share_off.png"] forState:UIControlStateNormal];
    btnShare.frame = CGRectMake(280, top_btn_down, 26, 28);
    
    [self.view addSubview:btnShare];
    
    
    // button suffer
    self.btnSuffer = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnSuffer addTarget:self
                       action:@selector(btnSufferPressed)
             forControlEvents:UIControlEventTouchDown];
    
    
    [self.btnSuffer setImage:[UIImage imageNamed:@"bt_suff_off.png"] forState:UIControlStateNormal];
    [self.btnSuffer setImage:[UIImage imageNamed:@"bt_suff_on.png"] forState:UIControlStateHighlighted];
    
    self.btnSuffer.frame = CGRectMake(49, top_sidedown+4, 46, 46);
    
    [self.view addSubview:self.btnSuffer];
    
    
    // button repeat
    self.btnRepeat = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnRepeat addTarget:self
                       action:@selector(btnRepeatPressed)
             forControlEvents:UIControlEventTouchDown];
    
    
    [self.btnRepeat setImage:[UIImage imageNamed:@"bt_Repeat_off.png"] forState:UIControlStateNormal];
    [self.btnRepeat setImage:[UIImage imageNamed:@"bt_Repeat_on.png"] forState:UIControlStateHighlighted];
    
    self.btnRepeat.frame = CGRectMake(225, top_sidedown+4, 46, 46);
    
    [self.view addSubview:self.btnRepeat];
    
    
    //time line section
    // ------------------------------
    pgbarView = [[PlayerProgressView alloc] initWithFrame:CGRectMake(42, 450, 236, 2)];
    pgbarView.progress = 0;
    [self.view addSubview:pgbarView];
    
    //label start
    lbStartTime = [[UILabel alloc] initWithFrame:CGRectMake(10, 436, 100, 27)];
    [lbStartTime setFont:[UIFont systemFontOfSize:11]];
    [lbStartTime setText:@"0:00"];
    [lbStartTime setTextColor:[UIColor whiteColor]];
    [lbStartTime setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:lbStartTime];
    
    //label end
    
    lbFinishTime = [[UILabel alloc] initWithFrame:CGRectMake(282, 436, 100, 27)];
    [lbFinishTime setFont:[UIFont systemFontOfSize:11]];
    [lbFinishTime setText:@"0:00"];
    [lbFinishTime setTextColor:[UIColor whiteColor]];
    [lbFinishTime setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:lbFinishTime];
}


// Amp: Lyrics View
-(void)initLyricsView{
    
    if (self.lyricView == nil) {
        self.lyricView = [[UITextView alloc] initWithFrame:CGRectMake(0, 50, 320.0, 340.0)];
        [self.lyricView setContentSize:CGSizeMake(320.0, 4200.0)];
        [self.lyricView setBackgroundColor:[UIColor blackColor]];
        [self.lyricView setAlpha:0.8];
        
        [self.lyricView setTextAlignment:NSTextAlignmentCenter];
        [self.lyricView setTextColor:[UIColor whiteColor]];
        [self.lyricView setEditable:NO];
    }
    
}

// Amp: Lyrics View
-(void)initPlaylistTable{
    NSLog(@"init initPlaylistTable");
    if (self.playlistTable == nil) {
        
        self.playlistTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, 320.0, 340.0)];
        [self.playlistTable setBackgroundColor:[UIColor clearColor]];
        [self.playlistTable setDelegate:self];
        [self.playlistTable setDataSource:self];
    }
}


// create
// ---------------------------
-(void)createPanelShare{
    
    self.panelShare = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [self.view addSubview:panelShare];
    
    // image background
    UIImageView *ivBg = [[UIImageView alloc] initWithFrame:CGRectMake(27, 200, 264, 56)];
    [ivBg setImage:[UIImage imageNamed:@"bg_menu.png"]];
    [self.panelShare addSubview:ivBg];
    
    
    // button share facebook
    // -------------------------------
    UIButton *btnShareFB = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imageFB = [UIImage imageNamed:@"btn_share_comment_fb.png"];
    
    [btnShareFB setBackgroundImage:imageFB forState:UIControlStateNormal];
    [btnShareFB addTarget:self action:@selector(btnShareFBPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnShareFB.frame = CGRectMake(37, 220, imageFB.size.width, imageFB.size.height);
    [self.panelShare addSubview:btnShareFB];
    
    // button share twitter
    // -------------------------------
    UIButton *btnShareTW = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imageTW = [UIImage imageNamed:@"btn_share_comment_tw.png"];
    
    [btnShareTW setBackgroundImage:imageTW forState:UIControlStateNormal];
    [btnShareTW addTarget:self action:@selector(btnShareTWPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnShareTW.frame = CGRectMake(171, 220, imageTW.size.width, imageTW.size.height);
    [self.panelShare addSubview:btnShareTW];
    
    
    // button close
    // -------------------------------
    UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *imClose = [UIImage imageNamed:@"bt_close_p.png"];
    
    [btnClose setBackgroundImage:imClose forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(btnClosePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnClose.frame = CGRectMake(291, 200-(imClose.size.height/2), imClose.size.width/2, imClose.size.height/2);
    [self.panelShare addSubview:btnClose];
    
    // hidden panel
    [self.panelShare setHidden:YES];
    
}

-(IBAction)btnClosePressed:(id)sender{
    [self.panelShare setHidden:YES];
}


-(IBAction)btnShareFBPressed:(id)sender{
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString *linkURL = @"http://thestar9.gmmwireless.com/thestar9/mobilesite/index.php/store";
    NSString *strImageShare = self.thmCover;
    NSString *name = [NSString stringWithFormat:@"%@ กำลังฟังเพลง %@", appDel.fullname, lbSongname.text];
    NSString *caption = @"ฝากเชียร์ ฝากโหวต ให้น้องด้วยนะ ไปดูวิธีการโหวตกันได้ที่  http://thestar.gmember.com";
    NSString *desc = @"for iPhone and  Android - Avaiable in the App Store";
    [appDel shareDataToFBByLinkURL:linkURL imageURL:strImageShare name:name caption:caption description:desc];
    
}

-(IBAction)btnShareTWPressed:(id)sender{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* name = appDel.fullname;
    
    NSString* txtShare = [NSString stringWithFormat:@"%@ กำลังฟังเพลง %@ บน The Star 9 Application. Download App http://goo.gl/5TSJM", name, lbSongname.text];
    // share twitter
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet =
        [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:txtShare];
        [self presentModalViewController:tweetSheet animated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have                                   at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}



// button event
//-------------------------------------
-(void)btnDownloadPressed{
    
    
}

-(void)btnLyricsPressed{
    
    if (isLyricOn) {
        [self.lyricView removeFromSuperview];
        isLyricOn = NO;
    }else{
        [self.view addSubview:self.lyricView];
        isLyricOn = YES;
    }
    
    
    
}

-(void)btnPlaylistPressed{
    if (isPlaylistTableOn) {
        [self.playlistTable removeFromSuperview];
        isPlaylistTableOn = NO;
    }else{
        [self.view addSubview:self.playlistTable];
        isPlaylistTableOn = YES;
    }
    NSLog(@"reloadData");
    [self.playlistTable reloadData];
}

-(void)btnRepeatPressed{
    
    if (isRepeate) {
        isRepeate = NO;
        [self.btnRepeat setImage:[UIImage imageNamed:@"bt_Repeat_off.png"] forState:UIControlStateNormal];
    }else{
        isRepeate = YES;
        [self.btnRepeat setImage:[UIImage imageNamed:@"bt_Repeat_on.png"] forState:UIControlStateNormal];
    }
    
}

-(void)btnSufferPressed{
    if (isShuffle) {
        isShuffle = NO;
        [self.btnSuffer setImage:[UIImage imageNamed:@"bt_suff_off.png"] forState:UIControlStateNormal];
    }else{
        isShuffle = YES;
        [self.btnSuffer setImage:[UIImage imageNamed:@"bt_suff_on.png"] forState:UIControlStateNormal];
    }
}

// test
-(void)btnPlayPausePressed{
    if([audioPlayer isPlaying]){
        [btnPlayPause setImage:[UIImage imageNamed:@"bt_play.png"] forState:UIControlStateNormal];
        [audioPlayer pause];
    }else{
        [btnPlayPause setImage:[UIImage imageNamed:@"bt_pause.png"] forState:UIControlStateNormal];
        [audioPlayer play];
    }
}


// previous button event
-(void)btnPreviousPressed{
    [audioPlayer stop];
    //next song
    NSLog(@"finish song");
    
    [self seqControllerByForword:NO];
    //    if(seqNum>0){
    //        seqNum -=1;
    //    }else{
    //        seqNum =[arData count]-1;
    //    }
    //    // first create
    //    if(audioPlayer!=nil){
    //        [audioPlayer release];
    //        audioPlayer = nil;
    //    }
    //
    //    [self loadDataWithRow:seqNum];
    //    [self playstreamMusic];
    
}

// forward button
-(void)btnForwardPressed{
    [audioPlayer stop];
    //next song
    NSLog(@"finish song");
    
    [self seqControllerByForword:YES];
    /*
     if(seqNum< [arData count]-1){
     seqNum +=1;
     }else{
     seqNum =0;
     }
     // first create
     if(audioPlayer!=nil){
     [audioPlayer release];
     audioPlayer = nil;
     }
     
     [self loadDataWithRow:seqNum];
     [self playstreamMusic];
     */
}

// button speaker event
-(void)btnSpeakerPressed{
    NSLog(@"speaker pressed");
    
}

// button share event
-(void)btnSharePressed{
    NSLog(@"share pressed");
    [self.panelShare setHidden:NO];
    
        
    
}

// button back event
-(void)btnBackPressed{
    [audioPlayer stop];
    [self dismissModalViewControllerAnimated:YES];
}


-(void)renderData{
    // load image
    
    [self initLyricsView];
    
    [self loadDataWithRow:seqNum];
    
    [self playstreamMusic];
    
}

// load data with row
-(void)loadDataWithRow:(int)row{
    NSLog(@"row = %d", row);
    NSDictionary *dic = [arData objectAtIndex:seqNum];
    
    // data
    self.cover = [dic objectForKey:@"cover"];
    self.songname = [dic objectForKey:@"SongNameTH"];
    self.pathPreview = [dic objectForKey:@"preview"];
    
    self.contentCode = [dic objectForKey:@"ContentCode"];
    self.gmmdCode = [dic objectForKey:@"GMMDCode"];
    self.thmCover = [dic objectForKey:@"thmCover"];
    
//    NSData *datImg = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.cover]];
    NSData *datImg = [dic objectForKey:@"datThmCover"];
    [imBg setImage:[UIImage imageWithData:datImg]];
//    NSLog(@"load data cover completed");
    
//    NSLog(@"dic = %@", dic);
    
    // songname
    [lbSongname setText:self.songname];
    
    // Amp : Lyrics
    [self getLyrics:self.gmmdCode];
    
}

//play stream method
-(void)playstreamMusic{
    
    // timer
    if(timer != nil){
        [timer invalidate];
        timer = nil;
    }
    
    // create timer
    float timeInterval = 0.1; // frequency of timer = 1/timeinterval
    timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(timerMethod) userInfo:nil repeats:YES];
    
    if(![audioPlayer play]){
        [btnPlayPause setImage:[UIImage imageNamed:@"bt_pause.png"] forState:UIControlStateNormal];
    }
    
    NSLog(@"player stream");
    if(audioPlayer == nil){
        
        NSLog(@"audio stream path = %@", pathPreview);
        NSURL *url;
        // check download
        BOOL downloaded = false;
        if(self.arListDownloaded!=nil){
            for (int i=0; i< [self.arListDownloaded count]; i++) {
                NSString *gmmd_code = [self.arListDownloaded objectAtIndex:i];
                if([gmmd_code isEqualToString:self.gmmdCode]){
                    downloaded = true;
                }
            }
        }
        
        if(downloaded){
            //file save
            
            // get fullpath file
            NSString* filename = [NSString stringWithFormat:@"%@.mp3", self.gmmdCode];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory ,NSUserDomainMask, YES);
            NSString *documentDirectory = [paths objectAtIndex:0];
            NSString *filepath = [documentDirectory stringByAppendingPathComponent:filename];
            
            url = [NSURL fileURLWithPath:filepath];
            
        }else{
            // link stream
            url = [NSURL URLWithString:self.pathPreview];
        }
        
        NSError *error;
        
        NSData *dat = [NSData dataWithContentsOfURL:url];
        
        audioPlayer = [[AVAudioPlayer alloc] initWithData:dat error:&error];
        // validate audio player
        if(audioPlayer == nil){
            NSLog(@"[Audio Player Error]: %@", [error description]);
            return;
        }
        audioPlayer.delegate = self;
        audioPlayer.numberOfLoops = 0;
        
        
        if([audioPlayer play]){
            NSLog(@"play song");
        }else{
            NSLog(@"cannot play song");
        }
        
        //        [dat release];
    }
    
    // calculate time
    [self updateTextTimeAndProgress];
}

-(void)timerMethod{
    [self updateTextTimeAndProgress];
}

-(void)updateTextTimeAndProgress{
    float curr = (float)audioPlayer.currentTime;
    float duration = (float)(float)audioPlayer.duration;
    //    NSLog(@"duration = %.1f/%.1f = %.1f percent", curr, duration, curr/duration*100);
    
    lbStartTime.text = [NSString stringWithFormat: @"%02d:%02d",
                        (int) curr/ (int)60,
                        (int) curr%60];
    
    lbFinishTime.text = [NSString stringWithFormat: @"%02d:%02d",
                         (int) (duration-curr)/ (int)60,
                         (int) (duration-curr)%60];
    
    pgbarView.progress = curr/duration;
    
}


// delegate audio finish
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)playedPlayer successfully:(BOOL)flag {
    //next song
    NSLog(@"finish song");
    
    [self seqControllerByForword:YES];
    
}

- (void)seqControllerByForword:(BOOL)isForword{
    
    // first create
    if(audioPlayer!=nil){
        [audioPlayer release];
        audioPlayer = nil;
    }
    
    if (isForword) {
        if (isRepeate) {
            
            if (isShuffle) {
                seqNum = (arc4random() % ([arData count]-0+1)) + 0;
            }else{
                if(seqNum< [arData count]-1){
                    seqNum +=1;
                }else{
                    seqNum =0;
                }
            }
            
            [self loadDataWithRow:seqNum];
            [self playstreamMusic];
        }else{
            
            if (isShuffle) {
                seqNum = (arc4random() % ([arData count]-0+1)) + 0;
                
                [self loadDataWithRow:seqNum];
                [self playstreamMusic];
            }else{
                
                if(seqNum< [arData count]-1){
                    seqNum +=1;
                    
                    [self loadDataWithRow:seqNum];
                    [self playstreamMusic];
                }else{
                    seqNum =0;
                }
            }
            
        }
    }else{
        if (isRepeate) {
            
            if(seqNum>0){
                seqNum -=1;
            }else{
                seqNum =[arData count]-1;
            }
            
            [self loadDataWithRow:seqNum];
            [self playstreamMusic];
        }else{
            
            if(seqNum>0){
                seqNum -=1;
                
                [self loadDataWithRow:seqNum];
                [self playstreamMusic];
            }else{
                seqNum =[arData count]-1;
            }
            
        }
    }
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    if([timer isValid]){
        [timer invalidate];
    }
}

-(void)dealloc{
    [audioPlayer release];
    [super dealloc];
}

//Amp: Parse Lyrics
- (void)getLyrics:(NSString*)gmmdcode{
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSString *lyric_data = @"";
    // send otp
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/lyric.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&GMMD_CODE=%@&CP_ID=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         gmmdcode,
                         @"1",
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        lyric_data = @"";
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){
        lyric_data = [doc.rootElement stringValue];
    }
    
    [self.lyricView setText:lyric_data];
    
}

// Tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"numberOfRowsInSection");
    return [arData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier;
    NSLog(@"cellForRowAtIndexPath");
    CellIdentifier = @"PlaylistCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc]
                 initWithStyle:UITableViewCellStyleSubtitle
                 reuseIdentifier:CellIdentifier]
                autorelease];
    }
    
    NSDictionary *dic = [arData objectAtIndex:indexPath.row];
    // songname
    [lbSongname setText:self.songname];
    
    [cell.textLabel setText:[dic objectForKey:@"SongNameTH"]];
    
    return cell;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}



@end
