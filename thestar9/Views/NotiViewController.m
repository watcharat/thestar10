//
//  NotiViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 3/21/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "NotiViewController.h"
#import "AppDelegate.h"

@interface NotiViewController ()

@end

@implementation NotiViewController
@synthesize webview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadWebview];
}

-(void)loadWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* path = [NSString stringWithFormat:@"http://gmmnotification.gmmwireless.com/feed/index.jsp?APP_ID=%@&DEVICE=%@", appDel.app_id, appDel.device];
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
    [self.webview loadRequest:req];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [webview release];
    [super dealloc];
}
- (void)viewDidUnload {
    [webview release];
    webview = nil;
    [self setWebview:nil];
    [super viewDidUnload];
}

- (IBAction)btnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
