//
//  VideoDetailViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/17/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
#import "MSISDNUtil.h"

@interface VideoDetailViewController : JWSlideMenuViewController<UITableViewDataSource, UITableViewDelegate, MSISDNUtilDelegate>{
    UITableView *tbMain;
    UIButton *btnBanner;
    NSString* videoId;
    NSMutableArray *arDataTable;
    MSISDNUtil* msisdnUtil;
    NSString* title_vdo;
    NSString* desc;
}

@property (retain, nonatomic)UITableView *tbMain;
@property (retain, nonatomic)UIButton *btnBanner;
@property (retain, nonatomic)NSString* videoId;
@property (retain, nonatomic)NSMutableArray *arDataTable;
@property (retain, nonatomic)MSISDNUtil* msisdnUtil;
@property (retain, nonatomic)NSString* title_vdo;
@property (retain, nonatomic)NSString* desc;

//m
@property (retain, nonatomic) IBOutlet UIImageView *bigVideoThumb;
@property (retain, nonatomic) IBOutlet UIView *subVideoView;
@property (retain, nonatomic) IBOutlet UIScrollView *videoScrollView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (retain, nonatomic) IBOutlet UILabel *lblCaption;

@property (retain, nonatomic) IBOutlet UILabel *lblHeadline;
@property (retain, nonatomic) NSDictionary* videoDic;
@end
