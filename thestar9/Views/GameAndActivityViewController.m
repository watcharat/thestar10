//
//  GameAndActivityViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "GameAndActivityViewController.h"

#import "AppDelegate.h"
#import "SVMainViewController.h"
#import "MainQuizViewController.h"

#import "StarFrameViewController.h"

#import "QRViewController.h"

@interface GameAndActivityViewController ()

@end

@implementation GameAndActivityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = @"Game &Activity";
    // set title
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-games.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    // button vote in right navigation
    [self createVoteButton];
}

-(void)createVoteButton{
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
    
}

// vote button pressed
-(IBAction)votePressed:(id)sender{
    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
//    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    [appDel checkMSISDN];
    
    
}

- (IBAction)QrStarCodePressed:(id)sender {
    QRViewController *qrViewController = [[[QRViewController alloc] init] autorelease];
    [self presentModalViewController:qrViewController animated:YES];
}
- (IBAction)StarvoicePressed:(id)sender {
    SVMainViewController *svMainVC = [[SVMainViewController alloc]init];
    [self presentModalViewController:svMainVC animated:YES];
    
    [svMainVC release];
    
}
- (IBAction)StarQuizPressed:(id)sender {
    MainQuizViewController *quizCon = [[MainQuizViewController alloc] initWithNibName:@"MainQuizViewController" bundle:nil];
    [self presentModalViewController:quizCon animated:YES];
    [quizCon release];
}

- (IBAction)StarFramePressed:(id)sender {
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle: @"Comming Soon"
//                          message: @"เตรียมพบกับ The Star Frame เร็ว ๆ นี้"
//                          delegate: nil
//                          cancelButtonTitle:@"OK"
//                          otherButtonTitles:@"Cancel", nil];
//    
//    [alert show];
//    [alert release];
    StarFrameViewController *starFrameViewController = [[StarFrameViewController alloc] init];
    [self presentModalViewController:starFrameViewController animated:YES];
    [starFrameViewController release];
    
}

// ----

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
