//
//  ListShelfDownloadController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/24/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

#import "MSISDNUtil.h"

@interface ListShelfDownloadController : JWSlideMenuViewController<UITableViewDataSource, UITableViewDelegate, MSISDNUtilDelegate>{
    int shelftype;
    
    UITableView* tbMain;
    
    NSMutableArray *arDataTable;
    
    NSMutableArray *arListDB;
    UIActivityIndicatorView* spinner;
    MSISDNUtil *msisdnUtil;
    
    BOOL loadAPI;

}
@property (readwrite,assign)int shelftype;

@property (retain, nonatomic) UITableView* tbMain;
@property (retain, nonatomic) NSMutableArray* arDataTable;

@property (retain, nonatomic) NSMutableArray *arListDB;

@property(retain, nonatomic) UIActivityIndicatorView* spinner;

@property(readwrite, assign)BOOL loadAPI;

@property(retain, nonatomic)MSISDNUtil *msisdnUtil;
@end
