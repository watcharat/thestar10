//
//  MyDownloadController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/25/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "MyDownloadController.h"

#import "DBManager.h"

#import "MyDownloadCell.h"
#import "MusicPlayerController.h"

#import <mediaPlayer/MediaPlayer.h>

#define LISTTYPE_CLIP   0
#define LISTTYPE_MV     1
#define LISTTYPE_SONG   2


@interface MyDownloadController ()

@end

@implementation MyDownloadController

@synthesize arDataTable;

@synthesize btnMyClip, btnMyMV, btnMySong;
@synthesize downloadtype;
@synthesize tbMain;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImage* imTitle = [UIImage imageNamed:@"txt_mydown.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// other
- (id)init
{
    self = [super init];
    if (self) {
        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
        [self.view setBackgroundColor:[UIColor blackColor]];
        //        self.title = @"Video Detail";
        
        [self createUI];
    }
    
    return self;
}

-(void)createUI{
    // background image
    UIImageView *imBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bar_mydown.png"]];
    [self.view addSubview:imBackground];
    
    // button 3
    btnMySong = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMySong setFrame:CGRectMake(3, 6, 102, 34)];
    [btnMySong setBackgroundImage:[UIImage imageNamed:@"bt_mysong_off.png"] forState:UIControlStateNormal];
    [btnMySong addTarget:self action:@selector(btnMySongPressed:)
        forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnMySong];
    
    btnMyMV = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMyMV setFrame:CGRectMake(109, 6, 102, 34)];
    [btnMyMV setBackgroundImage:[UIImage imageNamed:@"bt_mymv_off.png"] forState:UIControlStateNormal];
    [btnMyMV addTarget:self action:@selector(btnMyMVPressed:)
      forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnMyMV];
    
    btnMyClip = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnMyClip setFrame:CGRectMake(215, 6, 102, 34)];
    [btnMyClip setBackgroundImage:[UIImage imageNamed:@"bt_myclip_off.png"] forState:UIControlStateNormal];
    [btnMyClip addTarget:self action:@selector(btnMyClipPressed:)
    forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnMyClip];
    
    // table main
    tbMain = [[UITableView alloc] initWithFrame:CGRectMake(0, 51, 320, 480-51-44-20) style:UITableViewStylePlain];
    tbMain.delegate = self;
    tbMain.dataSource = self;
    tbMain.backgroundColor = [UIColor clearColor];
    tbMain.separatorColor = [UIColor clearColor];
    [self.view addSubview:tbMain];
    
}


// button event click
-(IBAction)btnMySongPressed:(id)sender{
    listType = LISTTYPE_SONG;
    [self renderButton];
    [self loadListDatabase];
}

-(IBAction)btnMyMVPressed:(id)sender{
    listType = LISTTYPE_MV;
    [self renderButton];
    [self loadListDatabase];
}

-(IBAction)btnMyClipPressed:(id)sender{
    listType = LISTTYPE_CLIP;
    [self renderButton];
    [self loadListDatabase];
}


-(void)renderButton{
    if(listType == LISTTYPE_SONG){
        [btnMySong setImage:[UIImage imageNamed:@"bt_mysong_on.png"] forState:UIControlStateNormal];
        [btnMyMV setImage:[UIImage imageNamed:@"bt_mymv_off.png"] forState:UIControlStateNormal];
        [btnMyClip setImage:[UIImage imageNamed:@"bt_myclip_off.png"] forState:UIControlStateNormal];
    }else if(listType == LISTTYPE_MV){
        [btnMySong setImage:[UIImage imageNamed:@"bt_mysong_off.png"] forState:UIControlStateNormal];
        [btnMyMV setImage:[UIImage imageNamed:@"bt_mymv_on.png"] forState:UIControlStateNormal];
        [btnMyClip setImage:[UIImage imageNamed:@"bt_myclip_off.png"] forState:UIControlStateNormal];
        
    }else if(listType == LISTTYPE_CLIP){
        [btnMySong setImage:[UIImage imageNamed:@"bt_mysong_off.png"] forState:UIControlStateNormal];
        [btnMyMV setImage:[UIImage imageNamed:@"bt_mymv_off.png"] forState:UIControlStateNormal];
        [btnMyClip setImage:[UIImage imageNamed:@"bt_myclip_on.png"] forState:UIControlStateNormal];
    }
}

// load first
-(void)viewDidAppear:(BOOL)animated{
    [self loadFirstView];
}



// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-
-(void)loadFirstView{

    listType = LISTTYPE_SONG;
    [self renderButton];
    [self loadListDatabase];
    
}

-(void)loadListDatabase{
    
    // check list
    if(arDataTable==nil){
        arDataTable = [[NSMutableArray alloc] init];
    }else{
        [arDataTable removeAllObjects];
    }
    
    DBManager *dbMgr = [[DBManager alloc] init];
    if(listType == LISTTYPE_SONG){
        NSMutableArray *ar = [dbMgr fetchSong:@""];
        if(ar!=Nil && [ar count] > 0){
            
            for(int i=0; i< [ar count]; i++){
                Song *song = [ar objectAtIndex:i];
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                [dic setValue:song.gmmd_code forKey:@"GMMD_CODE"];
                [dic setValue:song.title forKey:@"TITLE"];
                [dic setValue:song.desc forKey:@"DESC"];
                [dic setValue:song.date forKey:@"DATE"];
                [dic setValue:song.cover forKey:@"COVER"];
                
                // custom path following list type 
                NSString* path = [NSString stringWithFormat:@"%@.mp3", song.gmmd_code];
                [dic setValue:path forKey:@"PATH"];
                
                NSString* shotname = [NSString stringWithFormat:@"%@", song.gmmd_code];
                [dic setValue:shotname forKey:@"SHOTNAME"];
                
                // data format follow list shelf
                // ---
                
                [dic setValue:song.content_code forKey:@"ContentCode"];
                [dic setValue:song.gmmd_code forKey:@"GMMDCode"];
                [dic setValue:song.title forKey:@"SongNameEN"];
                [dic setValue:song.title forKey:@"SongNameTH"];
                
                // image
                
                [dic setValue:song.cover forKey:@"datThmCover"];
                [dic setValue:@"cover" forKey:@"cover"];
                [dic setValue:song.cover forKey:@"datcover"];
                
                [dic setValue:song.title forKey:@"AlbumTH"];
                [dic setValue:song.title forKey:@"ArtistEN"];
                [dic setValue:song.title forKey:@"ArtistTH"];
                [dic setValue:song.title forKey:@"preview"];
                
//                [dic setValue:song.cover forKey:@"cover"];
//                [dic setValue:song.title forKey:@"SongNameTH"];
//
//                [dic setValue:song.cover forKey:@"preview"];
//                
//                [dic setValue:song.content_code forKey:@"ContentCode"];
//                [dic setValue:song.gmmd_code forKey:@"GMMDCode"];
                
                
                // add to list all
                [arDataTable addObject:dic];
                
                // check data
//                NSLog(@"data GMMD_CODE %@", [dic objectForKey:@"GMMD_CODE"]);
//                NSLog(@"data TITLE %@", [dic objectForKey:@"TITLE"]);
//                NSLog(@"data DESC %@", [dic objectForKey:@"DESC"]);                                                                                                                                                      
//                NSLog(@"data DATE %@", [dic objectForKey:@"DATE"]);
//                NSLog(@"data COVER %@", [dic objectForKey:@"COVER"]);
//                NSLog(@"data PATH %@", [dic objectForKey:@"PATH"]);
            }
            NSLog(@"load data complete.");
        }
    }else if(listType == LISTTYPE_MV){
        NSMutableArray *ar = [dbMgr fetchMV:@""];
        if(ar!=nil && [ar count] > 0){
            for(int i=0; i< [ar count]; i++){
                MV *mv = [ar objectAtIndex:i];
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                [dic setValue:mv.gmmd_code forKey:@"GMMD_CODE"];
                [dic setValue:mv.title forKey:@"TITLE"];
                [dic setValue:mv.desc forKey:@"DESC"];
                [dic setValue:mv.date forKey:@"DATE"];
                [dic setValue:mv.cover forKey:@"COVER"];
                
                // custom path following list type
                NSString* path = [NSString stringWithFormat:@"%@_2.mp4", mv.gmmd_code];
                [dic setValue:path forKey:@"PATH"];
                
                NSString* shotname = [NSString stringWithFormat:@"%@_2", mv.gmmd_code];
                [dic setValue:shotname forKey:@"SHOTNAME"];
                
                // add to list all
                [arDataTable addObject:dic];

            }
            NSLog(@"load data complete.");
        }
        
    }else if(listType == LISTTYPE_CLIP){
        NSMutableArray *ar = [dbMgr fetchSpecialClip:@""];
        if(ar!=nil && [ar count] > 0){
            if(ar!=nil && [ar count] > 0){
                for(int i=0; i< [ar count]; i++){
                    SpecialClip* sp = [ar objectAtIndex:i];
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                    [dic setValue:sp.gmmd_code forKey:@"GMMD_CODE"];
                    [dic setValue:sp.title forKey:@"TITLE"];
                    [dic setValue:sp.desc forKey:@"DESC"];
                    [dic setValue:sp.date forKey:@"DATE"];
                    [dic setValue:sp.cover forKey:@"COVER"];
                    
                    // custom path following list type
                    NSString* path = [NSString stringWithFormat:@"%@_3.mp4", sp.gmmd_code];
                    [dic setValue:path forKey:@"PATH"];
                    
                    NSString* shotname = [NSString stringWithFormat:@"%@_3", sp.gmmd_code];
                    [dic setValue:shotname forKey:@"SHOTNAME"];
                    
                    // add to list all
                    [arDataTable addObject:dic];   
                }
                NSLog(@"load data complete.");
                
            }
        }
    }
    [dbMgr release];
    
    // reload table
    [tbMain reloadData];
    
}


// ---------------------------------------------------------
// delegate cell table
// ---------------------------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //    NSLog(@"numberOfSectionsInTableView");
    return 1;
}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"numberOfRowsInSection");
    return [arDataTable count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"heightForRowAtIndexPath");
    if(listType == LISTTYPE_CLIP){
        return 244;
    }else {
        return 65;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"DownloadCell";
    
    //    NSLog(@"cellForRowAtIndexPath");
    
    MyDownloadCell *cell = (MyDownloadCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil){
        // load xib file
        NSArray *nib;
        if(listType == LISTTYPE_SONG){
            NSLog(@"MyDownloadCell ");
            nib = [[NSBundle mainBundle] loadNibNamed:@"MyDownloadCell" owner:self options:nil];
        }else if(listType == LISTTYPE_MV){
            nib = [[NSBundle mainBundle] loadNibNamed:@"MyDownloadMVCell" owner:self options:nil];
        }else{
            nib = [[NSBundle mainBundle] loadNibNamed:@"MyDownloadSpecialClipCell" owner:self options:nil];
        }
        cell = [nib objectAtIndex:0];
    }
    
    if(arDataTable !=nil){
//        NSLog(@"load data dictionary");
        NSMutableDictionary *dicRow = [arDataTable objectAtIndex:indexPath.row];
        if(dicRow!=nil){
            
            //set title
            // check data
//            NSLog(@"data GMMD_CODE %@", [dicRow objectForKey:@"GMMD_CODE"]);
//            NSLog(@"data TITLE %@", [dicRow objectForKey:@"TITLE"]);
//            NSLog(@"data DESC %@", [dicRow objectForKey:@"DESC"]);
//            NSLog(@"data DATE %@", [dicRow objectForKey:@"DATE"]);
//            NSLog(@"data COVER %@", [dicRow objectForKey:@"COVER"]);
//            NSLog(@"data PATH %@", [dicRow objectForKey:@"PATH"]);
            
            
            [cell.lbTitle setText:[dicRow objectForKey:@"TITLE"]];
            [cell.lbDesc setText:[dicRow objectForKey:@"DESC"]];
            [cell.lbDate setText:[dicRow objectForKey:@"DATE"]];
            
            NSData *datImage = [dicRow objectForKey:@"COVER"];
            
            //title
            if(datImage != nil){
                [cell.imCover setImage:[UIImage imageWithData:datImage]];
            }       
        }
    }
    return cell;
}

//           
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    // preview
    
    //            NSLog(@"data GMMD_CODE %@", [dicRow objectForKey:@"GMMD_CODE"]);
    //            NSLog(@"data TITLE %@", [dicRow objectForKey:@"TITLE"]);
    //            NSLog(@"data DESC %@", [dicRow objectForKey:@"DESC"]);
    //            NSLog(@"data DATE %@", [dicRow objectForKey:@"DATE"]);
    //            NSLog(@"data COVER %@", [dicRow objectForKey:@"COVER"]);
    //            NSLog(@"data PATH %@", [dicRow objectForKey:@"PATH"]);
    
    //

    
    NSMutableDictionary *dicRow = [arDataTable objectAtIndex:indexPath.row];
    if(listType == LISTTYPE_SONG){
        NSDictionary *dicRow = (NSDictionary*)[arDataTable objectAtIndex:indexPath.row];
        
        if(dicRow!=nil){
            
            // create datalist
            NSMutableArray *arListDB = [[NSMutableArray alloc] init];
            for(int i=0; i< [arDataTable count]; i++){                
                NSMutableDictionary *dataDic = [arDataTable objectAtIndex:i];
                NSString* gmmd_code = [dataDic objectForKey:@"GMMD_CODE"];
                [arListDB addObject:gmmd_code];
            }
            
            
            
            MusicPlayerController *musicPlayerCon = [[MusicPlayerController alloc] init];
            [musicPlayerCon loadDataPlayer:arDataTable listDownloaded:arListDB seq:indexPath.row];
            
            [self presentModalViewController:musicPlayerCon animated:YES];
            [musicPlayerCon release];

            [arListDB release];
        }else{
            NSLog(@"data row fails");
        }
       
    }else{
        // video player
        
        NSString* filename = [dicRow objectForKey:@"PATH"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory ,NSUserDomainMask, YES);
        NSString *documentDirectory = [paths objectAtIndex:0];
        
        NSString *finalFileDesc = [documentDirectory stringByAppendingPathComponent:filename];
        
        NSURL *fileURL = [NSURL fileURLWithPath:finalFileDesc];
        MPMoviePlayerController *mpPlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
        [self.view addSubview:mpPlayerController.view];
        mpPlayerController.fullscreen = YES;
        [mpPlayerController play];
    }

        

}

-(void)dealloc{
//    [btnMyClip release];
//    [btnMyMV release];
//    [btnMySong release];
    [super dealloc];
}
@end
