//
//  DownloadViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#define SHELF_MUSIC     1
#define SHELF_SPECIAL   2
#define SHELF_MV        3

#import "DownloadViewController.h"
#import "ListShelfDownloadController.h"

#import "AppDelegate.h"
#import "MyDownloadController.h"

@interface DownloadViewController ()

@end

@implementation DownloadViewController
@synthesize msisdnUtil;
@synthesize btnBanner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    self.title = @"Download";
    // set title
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-download.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    // button vote in right navigation
    [self createVoteButton];
    
    // load banner
    [self loadBanner];

}

-(void)loadBanner{
    NSString* urlPath = @"http://thestar9.gmmwireless.com/thestar9/image/baner/pack/640x100.jpg";
    NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPath]];
    [btnBanner setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
}

-(void)createVoteButton{
    
    NSLog(@"createVoteButton");
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
    
}

// vote button pressed
-(IBAction)votePressed:(id)sender{
    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMusicDownload:(id)sender {
    ListShelfDownloadController *lstMusicCon = [[ListShelfDownloadController alloc] init];
    lstMusicCon.shelftype = SHELF_MUSIC;
//    lstMusicCon.title = @"Music";
    UIImage* imTitle = [UIImage imageNamed:@"txt_down-music.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    lstMusicCon.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    [self.navigationController pushViewController:lstMusicCon];
    
    [lstMusicCon release];
}


- (IBAction)btnMVDownload:(id)sender {
    ListShelfDownloadController *lstMVCon = [[ListShelfDownloadController alloc] init];
    lstMVCon.shelftype = SHELF_MV;
//    lstMVCon.title = @"Music Video";
    UIImage* imTitle = [UIImage imageNamed:@"txt_down-mv.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    lstMVCon.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    [self.navigationController pushViewController:lstMVCon];
    [lstMVCon release];
}

- (IBAction)btnSpecialClip:(id)sender {
    ListShelfDownloadController *lstSpecialCon = [[ListShelfDownloadController alloc] init];
    lstSpecialCon.shelftype = SHELF_SPECIAL;
//    lstSpecialCon.title = @"Special Clip";
    
    UIImage* imTitle = [UIImage imageNamed:@"txt_down_clip.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    lstSpecialCon.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    
    [self.navigationController pushViewController:lstSpecialCon];
    [lstSpecialCon release];
}

- (IBAction)btnMyDownload:(id)sender {
    MyDownloadController* myCon = [[MyDownloadController alloc] init];
    [self.navigationController pushViewController:myCon];
    [myCon release];
}

// banner
// -------------------------------------------
- (IBAction)btnBannerPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@"appDel.msisdn = %@, count = %d",appDel.msisdn, [appDel.msisdn length] );
    if(appDel.msisdn ==nil || [appDel.msisdn isEqualToString:@""]){
        [self checkMSISDN];
    }else{
        [self goOutBuyWebview];
    }
}


-(void)checkMSISDN{
    
    if(self.msisdnUtil ==nil){
        self.msisdnUtil = [[MSISDNUtil alloc] init];
        self.msisdnUtil.delegate = self;
    }
    [self.msisdnUtil alertshow];
    
    
}
// delegate
-(void) receptOTPSuccess:(NSString*)msisdn{
    NSLog(@"OTP success");
    [self goOutBuyWebview];
}

-(void) receptOTPFails:(NSString*)status_code status_message:(NSString*)status_message{
    NSLog(@"OTP fails");
}


-(void)goOutBuyWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=&GMMD_CODE=", appDel.app_id, appDel.device, appDel.msisdn];
    
    NSLog(@"go out to charging: %@", strPath);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
}

-(void)dealloc{
    [msisdnUtil release];
    [btnBanner release];
    [super dealloc];
}

- (void)viewDidUnload {
    [btnBanner release];
    btnBanner = nil;
    [self setBtnBanner:nil];
    [super viewDidUnload];
}
@end
