//
//  MyDownloadCell.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/7/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "MyDownloadCell.h"

@implementation MyDownloadCell
@synthesize imCover;

@synthesize lbTitle;
@synthesize lbDesc;
@synthesize lbDate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [imCover release];
    [lbTitle release];
    [lbDesc release];
    [lbDate release];
    [super dealloc];
}
@end
