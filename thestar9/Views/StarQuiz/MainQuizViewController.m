//
//  MainQuizViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/15/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "MainQuizViewController.h"
#import "QuizSubViewController.h"

#import "AppDelegate.h"

@interface MainQuizViewController ()

@end

@implementation MainQuizViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// event button

- (IBAction)btnPlayPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel checkSubscribe];
    
    if(appDel.subscriber){
        NSString* urlPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview//games/quiz/landingPage.jsp?APP_ID=%@&DEVICE=%@", appDel.app_id, appDel.device];
    
        [self gotoSecondView:urlPath];
    }
}

- (IBAction)btnMyScorePressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* urlPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview//games/quiz/myScore.jsp?APP_ID=%@&DEVICE=%@", appDel.app_id, appDel.device];
    
    [self gotoSecondView:urlPath];
}

- (IBAction)btnRankingPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* urlPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview//games/quiz/ranking.jsp?APP_ID=%@&DEVICE=%@", appDel.app_id, appDel.device];
    
    [self gotoSecondView:urlPath];
}

- (IBAction)btnHowToPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* urlPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview//games/quiz/howToQuiz.jsp?APP_ID=%@&DEVICE=%@", appDel.app_id, appDel.device];
    
    [self gotoSecondView:urlPath];
}
- (IBAction)btnExitPressed:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}


// go to second view
-(void)gotoSecondView:(NSString*)urlPath{
    QuizSubViewController *quizView = [[QuizSubViewController alloc] initWithNibName:@"QuizSubViewController" bundle:nil];
    [quizView setUrlPath:urlPath];
    [self presentModalViewController:quizView animated:YES];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
