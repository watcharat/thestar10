//
//  QuizSubViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/15/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "QuizSubViewController.h"

@interface QuizSubViewController ()

@end

@implementation QuizSubViewController
@synthesize urlPath;
@synthesize webview;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // load request
    [self loadDataWebview];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [webview release];
    [super dealloc];
}
- (void)viewDidUnload {
    [webview release];
    webview = nil;
    [self setWebview:nil];
    [super viewDidUnload];
}

// -----
// load data webview
-(void)loadDataWebview{
    NSURL *url = [NSURL URLWithString:self.urlPath];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:url];
    [self.webview loadRequest:req];
}
- (IBAction)btnBackPressed:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
