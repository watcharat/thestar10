//
//  QuizSubViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/15/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizSubViewController : UIViewController{
    NSString *urlPath;
    
    IBOutlet UIWebView *webview;
}

@property(retain, nonatomic) NSString *urlPath;
@property (retain, nonatomic) IBOutlet UIWebView *webview;

@end
