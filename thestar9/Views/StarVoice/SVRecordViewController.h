//
//  SVRecordViewController.h
//  thestar9
//
//  Created by อภินพ ศุภวารี on 2/8/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Recorder.h"

@interface SVRecordViewController : UIViewController{
    BOOL readyRec;
    BOOL readyPlay;
    
    int timeCount;
    Recorder* recorder;
    NSTimer *timer;
    
    float timeLeft;
    Boolean onTimer;
    
    BOOL haveFileRec;
    
    NSArray * imageArray_sound_gray;
    NSArray * imageArray_sound_orange;
    IBOutlet UILabel *lbTextRecord;
}

@property (retain, nonatomic) IBOutlet UIProgressView *pgbar;
@property (retain, nonatomic) IBOutlet UIView *recFinishView;
@property (retain, nonatomic) IBOutlet UIView *recBarView;
@property (retain, nonatomic) IBOutlet UIView *playBarView;
@property (retain, nonatomic) IBOutlet UIProgressView *pgPlayBar;
@property (retain, nonatomic) IBOutlet UIButton *btnPlayPreview;
@property (retain, nonatomic) IBOutlet UIButton *btnRec;
@property (retain, nonatomic) Recorder* recorder;
@property (retain, nonatomic) IBOutlet UILabel *recTimeStar;
@property (retain, nonatomic) IBOutlet UILabel *recTimeEnd;
@property (retain, nonatomic) IBOutlet UILabel *PlayTimeStart;
@property (retain, nonatomic) IBOutlet UILabel *PlayTimeEnd;
@property (retain, nonatomic) IBOutlet UIImageView *imgWave;
@property (retain, nonatomic) NSString *star_id;
@property (retain, nonatomic) IBOutlet UILabel *lbTextRecord;

- (IBAction)btnRecordPress:(id)sender;
- (IBAction)reRecPress:(id)sender;
- (IBAction)btnPreview:(id)sender;
- (IBAction)backPress:(id)sender;
- (IBAction)sendPress:(id)sender;


@end
