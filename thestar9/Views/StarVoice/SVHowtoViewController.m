//
//  SVHowtoViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/20/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "SVHowtoViewController.h"

@interface SVHowtoViewController ()

@end

@implementation SVHowtoViewController
@synthesize webview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadWebData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {

    [webview release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setWebview:nil];
    [webview release];
    webview = nil;
    [super viewDidUnload];
}

-(void)loadWebData{
    NSString* path = @"http://thestar9.gmmwireless.com/thestar9/mobilesite/index.php/howto/starvoice";
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:path]];
    [webview loadRequest:req];
    
    
}

- (IBAction)btnBackPressed:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
