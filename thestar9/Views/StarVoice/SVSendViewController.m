//
//  SVSendViewController.m
//  thestar9
//
//  Created by Aphinop Supawaree on 2/9/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "SVSendViewController.h"

#import "Recorder.h"

@interface SVSendViewController ()

@end

@implementation SVSendViewController

@synthesize star_id;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSArray * imageArray  = [[NSArray alloc] initWithObjects:
                             [UIImage imageNamed:@"sending_text0.png"],
                             [UIImage imageNamed:@"sending_text1.png"],
                             [UIImage imageNamed:@"sending_text2.png"],
                             [UIImage imageNamed:@"sending_text3.png"],
                             nil];
	
	self.imgSending.animationImages = imageArray;
	self.imgSending.animationDuration = 1.1;
	
}

- (void)viewWillAppear:(BOOL)animated{
    [self.imgSending startAnimating];
    [self.imgSending setHidden:NO];
    [self.lblPercent setHidden:YES];
    NSLog(@"star id : %@", self.star_id);
    [self uploadFile];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_lblPercent release];
    [_imgStatus release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLblPercent:nil];
    [self setImgStatus:nil];
    [super viewDidUnload];
}
- (IBAction)backPress:(id)sender {
    NSLog(@"backPress");
     [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];

}

- (IBAction)homePress:(id)sender {

    int count = [self findRootViewController:self];
    
    if(count == 2)
        [self dismissViewControllerAnimated:YES completion:^{}];
    if(count == 3)
        [self.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 4)
        [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 5)
        [self.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 6)
        [self.presentingViewController.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
}
- (int) findRootViewController:(UIViewController*)vc
{
    if(vc)
    {
        return 1 + [self findRootViewController:vc.presentingViewController];
    }
    return 0;
}

- (void)uploadFile{
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    Recorder *recorder = [[Recorder alloc]init];
    
//    NSLog(@"Song Path : %@", [recorder getSongPath]);
    
    
    NSString *uploadPath = @"http://thestar9.gmmwireless.com/thestar9//upload/games/voice/upload.jsp";
    NSURL *url = [NSURL URLWithString:uploadPath];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setPostValue:appDel.app_id forKey:@"APP_ID"];
    [request setPostValue:appDel.appversion forKey:@"APPVERSION"];
    [request setPostValue:appDel.apiversion forKey:@"APIVERSION"];
    [request setPostValue:appDel.device forKey:@"DEVICE"];
    [request setPostValue:appDel.charset forKey:@"CHARSET"];
    [request setPostValue:appDel.msisdn forKey:@"MSISDN"];
    [request setPostValue:self.star_id forKey:@"STAR_ID"];
    
    [request setFile:[recorder getSongPathString] forKey:@"FILE_SEND"];
    
    [request startAsynchronous];
    
    [recorder release];
}

- (void)requestFinished:(ASIHTTPRequest *)request{
    [self.imgSending stopAnimating];
    [self.imgSending setHidden:YES];
    [self.lblPercent setHidden:NO];
    
    [self.imgStatus setImage:[UIImage imageNamed:@"sending_pic_complete_iphone.png"]];
    
    NSLog(@"request : %@", request.responseString);
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}

@end
