//
//  Recorder.m
//  musiccupid
//
//  Created by  on 5/16/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Recorder.h"

@implementation Recorder

@synthesize audioRecorder;

-(id)init{
    if(self = [super init]){
            
        // init audio player
        [self parepareDataRecord];
        
    }
    return self;
}

-(void) parepareDataRecord{
    
    NSArray* dirPaths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* docsDir = [dirPaths objectAtIndex:0];
//  soundFilePath = [docsDir stringByAppendingPathComponent:@"capture_audio.caf"];
    soundFilePath = [docsDir stringByAppendingPathComponent:@"capture_audio.aac"];
    
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
//    NSMutableDictionary *recordSettings = [NSDictionary 
//                                    dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:AVAudioQualityMin],
//                                    AVEncoderAudioQualityKey,
//                                    [NSNumber numberWithInt:16], 
//                                    AVEncoderBitRateKey,
//                                    [NSNumber numberWithInt: 2], 
//                                    AVNumberOfChannelsKey,
//                                    [NSNumber numberWithFloat:44100.0], 
//                                    AVSampleRateKey,
//                                    nil];
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] init]; 
    [recordSettings setObject:[NSNumber numberWithInt:AVAudioQualityMin] forKey:AVEncoderAudioQualityKey]; 
    [recordSettings setObject:[NSNumber numberWithInt:16] forKey:AVEncoderBitRateKey]; 
    [recordSettings setObject:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey]; 
    [recordSettings setObject:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey]; 
    
     NSNumber *formatObject = [NSNumber numberWithInt: kAudioFormatMPEG4AAC];   // set format to mp4
    [recordSettings setObject:formatObject forKey:AVFormatIDKey];               // set format to mp4
    
    error = nil;
    self.audioRecorder = [[AVAudioRecorder alloc] initWithURL:soundFileURL settings:recordSettings error:&error ];
    [self.audioRecorder setDelegate:self];
    if(error){
        NSLog(@"[RECORDER][ERROR]  !!! %@", [error localizedDescription]);
    }else{
        [audioPlayer prepareToPlay];
    }

}

// --------------
-(void)recordAudio{
    int timeLimit =  30;
    NSLog(@"[RECORDER][INFO] record audio");
    [self.audioRecorder recordForDuration:timeLimit];
    [self.audioRecorder record];
    
}

// --------------
-(void) playAudio{
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
    audioPlayer.delegate = self;
    if(error){
        NSLog(@"[RECORDER][ERROR] !!! %@", [error localizedDescription] );
    }else{
        [audioPlayer play];
        NSLog(@"[RECORDER][INFO] play audio");
    }   
}

// --------------
-(void) stopRecord{
    if(self.audioRecorder.recording){
        [self.audioRecorder stop];
        NSLog(@"[RECORDER][INFO] stop record");
    }
}

// --------------
-(void) stopPlay{
    [audioPlayer stop];
    NSLog(@"[RECORDER][INFO] stop play");
    [audioPlayer release];
}

// --------------
-(NSURL*) getSongPath{
    return soundFileURL;
}

// --------------
-(NSString*) getSongPathString{
    return soundFilePath;
}


// -----------------------------------------------------
// delegate 
// -----------------------------------------------------

// delegate of audio player
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    NSLog(@"[RECORDER][AUDIOPLAYER] PLAY SUCCESSFULLY");
}

// delegate of audio recorder
-(void) audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag{
    NSLog(@"[RECORDER][AUDIORECORDER] RECORD SUCCESSFULLY");
}

-(float)getAudioDuration{
    if(audioPlayer){
        return (float)audioPlayer.duration;
    }
    return 0;
    
}

// dealloc
-(void) dealloc{
    [self.audioRecorder release];
    [audioPlayer release];
    
    [super dealloc];
}


@end
