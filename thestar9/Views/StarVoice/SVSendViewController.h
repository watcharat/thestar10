//
//  SVSendViewController.h
//  thestar9
//
//  Created by Aphinop Supawaree on 2/9/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASIFormDataRequest.h"
#import "AppDelegate.h"

@interface SVSendViewController : UIViewController<ASIHTTPRequestDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *imgSending;
@property (retain, nonatomic) IBOutlet UILabel *lblPercent;
@property (retain, nonatomic) NSString *star_id;
@property (retain, nonatomic) IBOutlet UIImageView *imgStatus;

- (IBAction)backPress:(id)sender;
- (IBAction)homePress:(id)sender;
@end
