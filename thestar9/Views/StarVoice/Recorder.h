//
//  Recorder.h
//  musiccupid
//
//  Created by  on 5/16/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>

@interface Recorder : NSObject<AVAudioRecorderDelegate, AVAudioPlayerDelegate>{
    AVAudioRecorder *audioRecorder;
    AVAudioPlayer *audioPlayer;
    
    NSString *soundFilePath;
    NSURL *soundFileURL;
    NSError *error;
    
}

@property (nonatomic, retain) AVAudioRecorder *audioRecorder;

-(void) recordAudio;
-(void) playAudio;
-(void) stopRecord;
-(void) stopPlay;

-(float)getAudioDuration;

-(NSURL*) getSongPath;
-(NSString*) getSongPathString;

-(void) parepareDataRecord;

@end
