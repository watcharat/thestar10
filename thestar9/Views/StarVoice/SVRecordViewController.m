//
//  SVRecordViewController.m
//  thestar9
//
//  Created by อภินพ ศุภวารี on 2/8/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "SVRecordViewController.h"

#import "SVSendViewController.h"


@interface SVRecordViewController ()

@end

@implementation SVRecordViewController

@synthesize recorder;
@synthesize star_id;
@synthesize lbTextRecord;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.recorder = [[Recorder alloc] init];
    
    readyRec = true;
    readyPlay = true;
    
    onTimer = false;
    haveFileRec = false;
    
    [self.imgWave setImage:[UIImage imageNamed:@"sound_gray3.png"]];
    [self.recFinishView setHidden:YES];
    [self.recBarView setHidden:NO];
    [self.playBarView setHidden:YES];
    
    imageArray_sound_gray  = [[NSArray alloc] initWithObjects:
                             [UIImage imageNamed:@"sound_gray3.png"],
                             [UIImage imageNamed:@"sound_gray1.png"],
                             [UIImage imageNamed:@"sound_gray2.png"],
                             nil];
	
	

    
    imageArray_sound_orange  = [[NSArray alloc] initWithObjects:
                             [UIImage imageNamed:@"sound_orange3.png"],
                             [UIImage imageNamed:@"sound_orange1.png"],
                             [UIImage imageNamed:@"sound_orange2.png"],
                             nil];
	

    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// record button
// --------------------------------------
- (IBAction)btnRecordPress:(id)sender {
    //record
    
    if([lbTextRecord isHidden]){
        [lbTextRecord setHidden:NO];
    }else{
        [lbTextRecord setHidden:YES];
    }
    
    
    if(readyRec){
        
        //========//
        NSLog(@"btn record click : start");
        [self.recorder recordAudio];
        
        timeCount = 0;
        onTimer = true;
        readyRec = false;
        
        self.imgWave.animationImages = imageArray_sound_gray;
        self.imgWave.animationDuration = 1.1;
        
        [self.imgWave startAnimating];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateProgress) userInfo:nil repeats:YES];
        //        [timer fire];
        
        onTimer = true;
        
        timeLeft = 30.0f;
        
        
        
        //change image
        [self.btnRec setImage:[UIImage imageNamed:@"recording.png"] forState:UIControlStateNormal];
    }else{
        //click stop
        if(!readyRec){
            NSLog(@"btn record click : stop");
            [self.recorder stopRecord];
            readyRec = true;
            [timer invalidate];
            onTimer = false;
            
            [self.imgWave stopAnimating];
            [self.imgWave setImage:[UIImage imageNamed:@"sound_gray3.png"]];
            
            
            timeCount = 0;
            self.pgbar.progress = 0;
            self.recTimeStar.text = @"0:00";
            self.recTimeEnd.text = @"0:00";

            readyRec = true;
            
            // change image button
            [self.btnRec setImage:[UIImage imageNamed:@"record.png"] forState:UIControlStateNormal];
            
            haveFileRec = true;
            [self renderButtonView:haveFileRec];
        }
    }
}


// preview button
// -------------------------------------
- (IBAction)btnPreview:(id)sender {
    
    
    
    if(readyPlay){
        readyPlay = false;
        [self.recorder playAudio];
        NSLog(@"btn play click : start");
        
        self.imgWave.animationImages = imageArray_sound_orange;
        self.imgWave.animationDuration = 1.1;
        
        [self.imgWave startAnimating];
        
        timeCount = 0;
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateProgressPlay) userInfo:nil repeats:YES];
        [timer fire];
        timeLeft = self.recorder.getAudioDuration;
        self.pgPlayBar.progress = 0;
        onTimer = true;
        
        // button
        [self.btnPlayPreview setImage:[UIImage imageNamed:@"player_pause.png"] forState:UIControlStateNormal];
        
        return;
    }else{
        //click stop
        if(!readyPlay){
            readyPlay = true;
            NSLog(@"btn play click : stop");
            
            [self.recorder stopPlay];
            
            [self.imgWave stopAnimating];
            [self.imgWave setImage:[UIImage imageNamed:@"sound_orange3.png"]];

            
            readyPlay = true;
            
            if(onTimer){
                [timer invalidate];
                onTimer = false;
            }
            
            timeCount = 0;
            self.pgPlayBar.progress = 0;
            self.PlayTimeStart.text = @"0:00";
            self.PlayTimeEnd.text = @"0:00";

            
            // button
            [self.btnPlayPreview setImage:[UIImage imageNamed:@"player_play.png"] forState:UIControlStateNormal];
        }
    }
    
}

- (IBAction)backPress:(id)sender {
    if(!readyPlay){
        readyPlay = true;
        NSLog(@"btn play click : stop");
        
        [self.recorder stopPlay];
        readyPlay = true;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)sendPress:(id)sender {
    SVSendViewController *svSendVC = [[SVSendViewController alloc] init];
    [svSendVC setStar_id:self.star_id];
    [self presentModalViewController:svSendVC animated:YES];
    [svSendVC release];
}

- (IBAction)reRecPress:(id)sender {
    [self.imgWave setImage:[UIImage imageNamed:@"sound_gray3.png"]];
    [self.recFinishView setHidden:YES];
    [self.recBarView setHidden:NO];
    [self.playBarView setHidden:YES];
}

// ----
-(void) renderButtonView:(BOOL) recFile{
    if(recFile){
        NSLog(@"show");        
        [self.imgWave stopAnimating];
        [self.imgWave setImage:[UIImage imageNamed:@"sound_orange3.png"]];
        [self.recFinishView setHidden:NO];
        [self.recBarView setHidden:YES];
        [self.playBarView setHidden:NO];
        
    }else{
        NSLog(@"hide");
        [self.imgWave stopAnimating];
        [self.imgWave setImage:[UIImage imageNamed:@"sound_gray3.png"]];
        [self.recFinishView setHidden:YES];
        [self.recBarView setHidden:NO];
        [self.playBarView setHidden:YES];
    }
    
}

// void
-(void) updateProgress{
    timeCount +=1;
    
    float prog =  timeCount/timeLeft;
    
    NSLog(@"update timer = %d, time = %f",timeCount, prog);
    
    self.pgbar.progress = prog;
    
    self.recTimeStar.text = [NSString stringWithFormat:@"0:%02d", timeCount];
    self.recTimeEnd.text = [NSString stringWithFormat:@"0:%02d", (int)(timeLeft - timeCount)];
    
    NSLog(@"timeleft = %f, progress", timeLeft);
    
    // final 30 s
    if(timeCount >= timeLeft){
        timeCount = 0;
        if(onTimer){
            
            [timer invalidate];
            onTimer = false;
            self.pgbar.progress = 0;
            
            
            if(!readyRec){
//                [self.recorder stopRecord];
                readyRec = true;
                [timer invalidate];
                onTimer = false;
                
                timeCount = 0;
                self.pgbar.progress = 0;
                self.recTimeStar.text = @"0:00";
                self.recTimeEnd.text = @"0:00";
                
                readyRec = true;
                
                // change image button
                [self.btnRec setImage:[UIImage imageNamed:@"record.png"] forState:UIControlStateNormal];
               
                
                haveFileRec = true;
                [self renderButtonView:haveFileRec];
            }
        }
        
        self.pgbar.progress = 0;
        
        if(!readyRec){
            readyRec = true;
        }
        if(!readyPlay){
            readyPlay = true;
        }
        //
    }
}

-(void) updateProgressPlay{
    timeCount +=1;
    
    float prog =  timeCount/timeLeft;
    
    NSLog(@"update timer = %d, time = %f",timeCount, prog);
    
    self.pgPlayBar.progress = prog;
    
    NSLog(@"timeleft = %f, progress", timeLeft);
    
    self.PlayTimeStart.text = [NSString stringWithFormat:@"0:%02d", timeCount];
    self.PlayTimeEnd.text = [NSString stringWithFormat:@"0:%02d", (int)([self.recorder getAudioDuration] - timeCount)];
    
    
    // final 30 s
    if(timeCount >= timeLeft){
        timeCount = 0;
        if(onTimer){
            
            [timer invalidate];
            onTimer = false;
            self.pgPlayBar.progress = 0;
            
            
            if(!readyPlay){
                [self.recorder stopPlay];
                [self.btnPlayPreview setImage:[UIImage imageNamed:@"player_play.png"] forState:UIControlStateNormal];
                
                [self.imgWave stopAnimating];
                [self.imgWave setImage:[UIImage imageNamed:@"sound_orange3.png"]];
                
                self.PlayTimeStart.text = @"0:00";
                self.PlayTimeEnd.text = @"0:00";
            }
           
        }
        
        self.pgPlayBar.progress = 0;
        
        if(!readyPlay){
            readyPlay = true;
        }
        //
    }
}


- (void)dealloc {
    [_pgbar release];
    [_recFinishView release];
    [_recBarView release];
    [_playBarView release];
    [_pgbar release];
    [_pgPlayBar release];
    [_btnPlayPreview release];
    [_btnRec release];
    [_recTimeStar release];
    [_recTimeEnd release];
    [_PlayTimeStart release];
    [_PlayTimeEnd release];
    [_imgWave release];
    [lbTextRecord release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setPgbar:nil];
    [self setRecFinishView:nil];
    [self setRecBarView:nil];
    [self setPlayBarView:nil];
    [self setPgbar:nil];
    [self setPgPlayBar:nil];
    [self setBtnPlayPreview:nil];
    [self setBtnRec:nil];
    [self setRecTimeStar:nil];
    [self setRecTimeEnd:nil];
    [self setPlayTimeStart:nil];
    [self setPlayTimeEnd:nil];
    [self setImgWave:nil];
    [lbTextRecord release];
    lbTextRecord = nil;
    [self setLbTextRecord:nil];
    [super viewDidUnload];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}

@end
