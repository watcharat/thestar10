//
//  SVSelectViewController.m
//  thestar9
//
//  Created by อภินพ ศุภวารี on 2/6/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "SVSelectViewController.h"
#import "AppDelegate.h"
#import "GDataXMLNode.h"
#import "DBManager.h"
#import "SVRecordViewController.h"

@interface SVSelectViewController ()

@end

@implementation SVSelectViewController

@synthesize arData;

@synthesize btnVote1, btnVote2, btnVote3, btnVote4, btnVote5, btnVote6, btnVote7, btnVote8;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
//    [self loadListTheStarAPI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadListTheStarAPI{
    // call api
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/theStarVoiceList.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){
        /*
         //test out put
         NSArray *ar = [doc.rootElement elementsForName:@"STATUS"];
         GDataXMLElement *status = [ar objectAtIndex:0];
         NSLog(@"STATUS = %@", [status stringValue]);
         */
        
        // render data by xml
        [self renderData:doc];
        
    }else{
        NSLog(@"[XML ERROR] : %@", [error description] );
        
    }
    
    [data release];
    [doc release];
    [url release];
}

// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
-(void)updateThumbVote:(NSMutableDictionary*)dic{
    // get data
    
    NSString* star_id = [dic objectForKey:@"STAR_ID"];
    NSString* thumb_cover = [dic objectForKey:@"THUMBS_IMG"];
    NSString* last_updated = [dic objectForKey:@"LAST_MODIFIED_DATE"];

    
    // database manager
    DBManager *dbMgr = [[DBManager alloc] init];
    NSString *cond = [NSString stringWithFormat:@"star_id = %@", star_id];
    NSMutableArray* ar = [dbMgr fetchStarvoice:cond];
    
    //
    //    NSLog(@"[xml ] star_id = %@, update = %@",star_id, updated);
    //
    //    NSMutableArray *arx = [dbMgr fetchTheStarProfile:@""];
    //    NSLog(@"data all record = %d", [arx count]);
    //    for(int i=0; i< [arx count]; i++){
    //        TheStarProfile *p = [arx objectAtIndex:i];
    //        NSLog(@"star_id = %@, last update = %@", p.star_id, p.updated);
    //    }
    
    
    // check found database
    if(ar !=nil &&[ar count] > 0){
        
        // found to update
        // -------------------- 
        Starvoice *voice = [ar objectAtIndex:0];
        voice.star_id = star_id;
        
        // check update image
        // -------------------
        if(![last_updated isEqualToString:voice.updated]){
            NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
            voice.thumb_img = datThub;
            voice.updated = last_updated;
            [dbMgr updateStarvoice:voice];
        }
        
        NSLog(@"update vote");
        
    }else{
        // insert new
        Starvoice *voice = [dbMgr getSchemaStarvoice];
        voice.star_id = star_id;
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        voice.thumb_img = datThub;
        voice.updated = last_updated;
        [dbMgr insertStarStarvoice:voice];
        NSLog(@"insert vote");
    }
    
    [dbMgr release];
}

// render data
-(void)renderData:(GDataXMLDocument*)doc{
    NSArray *ar = [doc.rootElement elementsForName:@"PROFILE"];
    NSLog(@"count list data =  %d", [ar count]);
    
    // data collection
    
    if(arData !=nil){
        [arData removeAllObjects];
    }else{
        arData = [[NSMutableArray alloc] init];
    }
    
    for(int i=0; i< [ar count]; i++){
        
        GDataXMLElement *elm = [ar objectAtIndex:i];
        NSString* star_id = [[[elm elementsForName:@"STAR_ID"] objectAtIndex:0] stringValue];
        NSString* name = [[[elm elementsForName:@"NAME"] objectAtIndex:0] stringValue];
        NSString* desc = [[[elm elementsForName:@"DESC"] objectAtIndex:0] stringValue];
        NSString* last_modified_date = [[[elm elementsForName:@"LAST_MODIFIED_DATE"] objectAtIndex:0] stringValue];
        
        NSString* thumbs_img = [[[elm elementsForName:@"THUMBS_IMG"] objectAtIndex:0] stringValue];
        NSString* cover_img = [[[elm elementsForName:@"COVER_IMG"] objectAtIndex:0] stringValue];
        NSString* cover_img_profile = [[[elm elementsForName:@"COVER_IMG_PROFILE"] objectAtIndex:0] stringValue];
        NSString* is_cover = [[[elm elementsForName:@"IS_ACTIVE"] objectAtIndex:0] stringValue];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        
        [dic setValue:star_id forKey:@"STAR_ID"];
        [dic setValue:name forKey:@"NAME"];
        [dic setValue:desc forKey:@"DESC"];
        [dic setValue:last_modified_date forKey:@"LAST_MODIFIED_DATE"];
        
        [dic setValue:thumbs_img forKey:@"THUMBS_IMG"];
        [dic setValue:cover_img forKey:@"COVER_IMG"];
        [dic setValue:cover_img_profile forKey:@"COVER_IMG_PROFILE"];
        NSLog(@"name : %@", name);
        
        switch ([star_id intValue]) {
            case 1:
                if ([is_cover isEqualToString:@"Y"]) {
                    [btnVote1 setEnabled:YES];
                }else{
                    [btnVote1 setEnabled:NO];
                }
                break;
            case 2:
                if ([is_cover isEqualToString:@"Y"]) {
                    [btnVote2 setEnabled:YES];
                }else{
                    [btnVote2 setEnabled:NO];
                }
                break;
            case 3:
                if ([is_cover isEqualToString:@"Y"]) {
                    [btnVote3 setEnabled:YES];
                }else{
                    [btnVote3 setEnabled:NO];
                }
                break;
            case 4:
                if ([is_cover isEqualToString:@"Y"]) {
                    [btnVote4 setEnabled:YES];
                }else{
                    [btnVote4 setEnabled:NO];
                }
                break;
            case 5:
                if ([is_cover isEqualToString:@"Y"]) {
                    [btnVote5 setEnabled:YES];
                }else{
                    [btnVote5 setEnabled:NO];
                }
                break;
            case 6:
                if ([is_cover isEqualToString:@"Y"]) {
                    [btnVote6 setEnabled:YES];
                }else{
                    [btnVote6 setEnabled:NO];
                }
                break;
            case 7:
                if ([is_cover isEqualToString:@"Y"]) {
                    [btnVote7 setEnabled:YES];
                }else{
                    [btnVote7 setEnabled:NO];
                }
                break;
            case 8:
                if ([is_cover isEqualToString:@"Y"]) {
                    [btnVote8 setEnabled:YES];
                }else{
                    [btnVote8 setEnabled:NO];
                }
                break;
                
            default:
                break;
        }
        
        [self updateThumbVote:dic];
        
        [arData addObject:dic];
        
    }
    
    [self renderTheStarProfile];
}


// render data
// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
-(void)renderTheStarProfile{
    // load all data from database
    //
    //    NSLog(@"[xml ] star_id = %@, update = %@",star_id, updated);
    //
    DBManager *dbMgr = [[DBManager alloc] init];
    NSMutableArray *arPro = [dbMgr fetchStarvoice:@""];
    
    NSLog(@"data the star record = %d", [arPro count]);
    
    //    for(int i=0; i< [arPro count]; i++){
    //        TheStarProfile *p = [arPro objectAtIndex:i];
    //        NSLog(@"star_id = %@, last update = %@", p.star_id, p.updated);
    //    }
    TheStarProfile *starPro1 = [arPro objectAtIndex:0];
    [btnVote1 setImage:[UIImage imageWithData:starPro1.thumb_img] forState:UIControlStateNormal];
    [btnVote1 setTag:starPro1.star_id];
    
    TheStarProfile *starPro2 = [arPro objectAtIndex:1];
    [btnVote2 setImage:[UIImage imageWithData:starPro2.thumb_img] forState:UIControlStateNormal];
    [btnVote2 setTag:starPro2.star_id];
    
    TheStarProfile *starPro3 = [arPro objectAtIndex:2];
    [btnVote3 setImage:[UIImage imageWithData:starPro3.thumb_img] forState:UIControlStateNormal];
    [btnVote3 setTag:starPro3.star_id];
    
    TheStarProfile *starPro4 = [arPro objectAtIndex:3];
    [btnVote4 setImage:[UIImage imageWithData:starPro4.thumb_img] forState:UIControlStateNormal];
    [btnVote4 setTag:starPro4.star_id];
    
    TheStarProfile *starPro5 = [arPro objectAtIndex:4];
    [btnVote5 setImage:[UIImage imageWithData:starPro5.thumb_img] forState:UIControlStateNormal];
    [btnVote5 setTag:starPro5.star_id];
    
    TheStarProfile *starPro6 = [arPro objectAtIndex:5];
    [btnVote6 setImage:[UIImage imageWithData:starPro6.thumb_img] forState:UIControlStateNormal];
    [btnVote6 setTag:starPro6.star_id];
    
    TheStarProfile *starPro7 = [arPro objectAtIndex:6];
    [btnVote7 setImage:[UIImage imageWithData:starPro7.thumb_img] forState:UIControlStateNormal];
    [btnVote7 setTag:starPro7.star_id];
    
    TheStarProfile *starPro8 = [arPro objectAtIndex:7];
    [btnVote8 setImage:[UIImage imageWithData:starPro8.thumb_img] forState:UIControlStateNormal];
    [btnVote8 setTag:starPro8.star_id];
}

// Action
- (IBAction)backPress:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

// button event
// ------------------------------------------------
- (IBAction)btn1Pressed:(id)sender {
    SVRecordViewController*SVRecVC = [[SVRecordViewController alloc] init];
    [SVRecVC setStar_id:@"1"];
    [self presentModalViewController:SVRecVC animated:YES];
    [SVRecVC release];
}

- (IBAction)btn2Pressed:(id)sender {
    SVRecordViewController*SVRecVC = [[SVRecordViewController alloc] init];
    [SVRecVC setStar_id:@"2"];
    [self presentModalViewController:SVRecVC animated:YES];
    [SVRecVC release];
}

- (IBAction)btn3Pressed:(id)sender {
    SVRecordViewController*SVRecVC = [[SVRecordViewController alloc] init];
    [SVRecVC setStar_id:@"3"];
    [self presentModalViewController:SVRecVC animated:YES];
    [SVRecVC release];
}

- (IBAction)btn4Pressed:(id)sender {
    SVRecordViewController*SVRecVC = [[SVRecordViewController alloc] init];
    [SVRecVC setStar_id:@"4"];
    [self presentModalViewController:SVRecVC animated:YES];
    [SVRecVC release];
}

- (IBAction)btn5Pressed:(id)sender {
    SVRecordViewController*SVRecVC = [[SVRecordViewController alloc] init];
    [SVRecVC setStar_id:@"5"];
    [self presentModalViewController:SVRecVC animated:YES];
    [SVRecVC release];
}

- (IBAction)btn6Pressed:(id)sender {
    SVRecordViewController*SVRecVC = [[SVRecordViewController alloc] init];
    [SVRecVC setStar_id:@"6"];
    [self presentModalViewController:SVRecVC animated:YES];
    [SVRecVC release];
}

- (IBAction)btn7Pressed:(id)sender {
    SVRecordViewController*SVRecVC = [[SVRecordViewController alloc] init];
    [SVRecVC setStar_id:@"7"];
    [self presentModalViewController:SVRecVC animated:YES];
    [SVRecVC release];
}

- (IBAction)btn8Pressed:(id)sender {
    SVRecordViewController*SVRecVC = [[SVRecordViewController alloc] init];
    [SVRecVC setStar_id:@"8"];
    [self presentModalViewController:SVRecVC animated:YES];
    [SVRecVC release];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}

@end
