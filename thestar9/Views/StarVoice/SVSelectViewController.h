//
//  SVSelectViewController.h
//  thestar9
//
//  Created by อภินพ ศุภวารี on 2/6/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVSelectViewController : UIViewController

@property (retain, nonatomic) NSMutableArray *arData;

@property (retain, nonatomic) IBOutlet UIButton *btnVote1;
@property (retain, nonatomic) IBOutlet UIButton *btnVote2;
@property (retain, nonatomic) IBOutlet UIButton *btnVote3;

@property (retain, nonatomic) IBOutlet UIButton *btnVote4;
@property (retain, nonatomic) IBOutlet UIButton *btnVote5;
@property (retain, nonatomic) IBOutlet UIButton *btnVote6;

@property (retain, nonatomic) IBOutlet UIButton *btnVote7;
@property (retain, nonatomic) IBOutlet UIButton *btnVote8;

- (IBAction)backPress:(id)sender;

- (IBAction)btn1Pressed:(id)sender;
- (IBAction)btn2Pressed:(id)sender;
- (IBAction)btn3Pressed:(id)sender;
- (IBAction)btn4Pressed:(id)sender;
- (IBAction)btn5Pressed:(id)sender;
- (IBAction)btn6Pressed:(id)sender;
- (IBAction)btn7Pressed:(id)sender;
- (IBAction)btn8Pressed:(id)sender;

@end
