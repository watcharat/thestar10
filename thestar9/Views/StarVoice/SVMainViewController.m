//
//  SVMainViewController.m
//  thestar9
//
//  Created by อภินพ ศุภวารี on 2/6/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "SVMainViewController.h"
#import "SVSelectViewController.h"
#import "SVHowtoViewController.h"

#import "AppDelegate.h"

@interface SVMainViewController ()

@end

@implementation SVMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnPlayPress:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel checkSubscribe];

    if(appDel.subscriber){
        SVSelectViewController *svSelectVC = [[SVSelectViewController alloc]init];
        [self presentModalViewController:svSelectVC animated:YES];
        [svSelectVC release];
    }
    
}
// exit
- (IBAction)exitPress:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)btnHowToPressed:(id)sender {
    SVHowtoViewController *svHowtoCon = [[SVHowtoViewController alloc] init];
    [self presentModalViewController:svHowtoCon animated:YES];
    [svHowtoCon release];
    
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
