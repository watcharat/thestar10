//
//  SVMainViewController.h
//  thestar9
//
//  Created by อภินพ ศุภวารี on 2/6/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVMainViewController : UIViewController
- (IBAction)btnPlayPress:(id)sender;
- (IBAction)exitPress:(id)sender;

@end
