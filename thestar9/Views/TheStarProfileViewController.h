//
//  TheStarProfileViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
#import "MSISDNUtil.h"

@interface TheStarProfileViewController : JWSlideMenuViewController<MSISDNUtilDelegate>{
    NSMutableArray *arData;
    
    IBOutlet UIButton *btnVote1;
    IBOutlet UIButton *btnVote2;
    IBOutlet UIButton *btnVote3;
    
    IBOutlet UIButton *btnVote4;
    IBOutlet UIButton *btnVote5;
    IBOutlet UIButton *btnVote6;
    
    IBOutlet UIButton *btnVote7;
    IBOutlet UIButton *btnVote8;
    MSISDNUtil* msisdnUtil;
    
    
    IBOutlet UIButton *btnBanner;
    
}

@property (retain, nonatomic) NSMutableArray *arData;

@property (retain, nonatomic) IBOutlet UIButton *btnBanner;

@property (retain, nonatomic) IBOutlet UIButton *btnVote1;
@property (retain, nonatomic) IBOutlet UIButton *btnVote2;
@property (retain, nonatomic) IBOutlet UIButton *btnVote3;

@property (retain, nonatomic) IBOutlet UIButton *btnVote4;
@property (retain, nonatomic) IBOutlet UIButton *btnVote5;
@property (retain, nonatomic) IBOutlet UIButton *btnVote6;

@property (retain, nonatomic) IBOutlet UIButton *btnVote7;
@property (retain, nonatomic) IBOutlet UIButton *btnVote8;
@property (retain, nonatomic) MSISDNUtil* msisdnUtil;


@end
