//
//  ListShelfDownloadCell.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/24/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PlayerProgressView.h"

@interface ListShelfDownloadCell : UITableViewCell<UIAlertViewDelegate>{
 
    IBOutlet UIImageView *imThumb;

    IBOutlet UILabel *lbTitle;
    IBOutlet UILabel *lbDesc;
    IBOutlet UILabel *lbDate;
    
    NSString *contentCode;
    NSString *gmmdCode;
    NSString *service_id;
    NSString *downloadtype;
    
    NSData* datCover;
    
    IBOutlet UIButton *btnDownload;
    
    IBOutlet PlayerProgressView *pgbarView;
    
    int alertMode;
    NSString* strPhoneNum;
}



@property (retain, nonatomic) IBOutlet UIImageView *imThumb;
@property (retain, nonatomic) IBOutlet UILabel *lbTitle;
@property (retain, nonatomic) IBOutlet UILabel *lbDesc;
@property (retain, nonatomic) IBOutlet UILabel *lbDate;

@property (retain, nonatomic) NSString* strPhoneNum;

@property (retain, nonatomic) NSData* datCover;

@property (retain, nonatomic) NSString *contentCode;

@property (retain, nonatomic) NSString *gmmdCode;
@property (retain, nonatomic) NSString *service_id;
@property (retain, nonatomic) NSString *downloadtype;

@property (retain, nonatomic) IBOutlet UIButton *btnDownload;

@property (retain, nonatomic) IBOutlet PlayerProgressView *pgbarView;

@end
