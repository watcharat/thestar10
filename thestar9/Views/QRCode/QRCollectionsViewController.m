//
//  QRCollectionsViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/14/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "QRCollectionsViewController.h"
#import "QRScanViewController.h"

#import "AppDelegate.h"
#import "GDataXMLNode.h"
#import "QRCalendarViewController.h"

#import <Twitter/Twitter.h>


@interface QRCollectionsViewController ()

@end

@implementation QRCollectionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id) init {
    self = [super init];
    
    // init frame size
    CGRect masterRect = [[UIScreen mainScreen] bounds];
    CGRect navBarFrame = CGRectMake(0.0, 0.0, masterRect.size.width, 44.0);
    
    // TODO :: init navigate
    UIView *navBar = [[[UIView alloc] initWithFrame:navBarFrame] autorelease];
    [navBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_head_bar.png"]]];
    
    UIButton *navBarBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [navBarBackBtn setFrame:CGRectMake(5.0, 6.0, 54.00, 30.00)];
    [navBarBackBtn setBackgroundImage:[UIImage imageNamed:@"qr_btn_subback.png"] forState:UIControlStateNormal];
    [navBarBackBtn addTarget:self action:@selector(navBarBackBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:navBarBackBtn];
    
    UIButton *btnScanQR = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnScanQR setFrame:CGRectMake(masterRect.size.width-67-5, 6, 67, 30)];
    [btnScanQR setBackgroundImage:[UIImage imageNamed:@"qr_btn_scan_blue.png"] forState:UIControlStateNormal];
    [btnScanQR addTarget:self action:@selector(btnScanQRPress:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:btnScanQR];
    
    UIImage *headerLabelImage = [UIImage imageNamed:@"qr_h_collections.png"];
    UIImageView *headerLabelImageView = [[UIImageView alloc] initWithImage:headerLabelImage];
    headerLabelImageView.frame = CGRectMake(80.0, 8.0, 100, 34.0);
    [navBar addSubview:headerLabelImageView];
    
    [self.view addSubview:navBar];
    

    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    // webview
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 44, 320, 480-(44+63))];
    NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/mobilesite/index.php/qr/collections/%@/%@/%@/%@/%@/%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.appversion,
                         appDel.apiversion,
                         appDel.msisdn
                         ];
    NSLog(@"web collection = %@", strPath);
    NSURLRequest *urlReq = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:strPath]];
    [webview loadRequest:urlReq];
    [self.view addSubview:webview];
    
    // bottom bar (down-side bar)
    // ---------------------------------
    UIImageView *imBottomBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"totalscore_bar.png"]];
    [imBottomBar setFrame:CGRectMake(0, 480-63-20, 320, 63)];
    [self.view addSubview:imBottomBar];
    
    // button
    UIButton *btnCal = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCal setBackgroundImage:[UIImage imageNamed:@"qr_btn_calendar.png"] forState:UIControlStateNormal];
    [btnCal addTarget:self action:@selector(btnCalendarPress:) forControlEvents:UIControlEventTouchUpInside];
    [btnCal setFrame:CGRectMake(24, 406, 50, 47)];
    [self.view addSubview:btnCal];
    
    // share button
    // button
    UIButton *btnShareFb = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnShareFb setBackgroundImage:[UIImage imageNamed:@"btn_facebook.png"] forState:UIControlStateNormal];
    [btnShareFb addTarget:self action:@selector(btnShareFBPressed:) forControlEvents:UIControlEventTouchUpInside];
    [btnShareFb setFrame:CGRectMake(234, 416, 25, 25)];
    [self.view addSubview:btnShareFb];
    
    UIButton *btnShareTw = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnShareTw setBackgroundImage:[UIImage imageNamed:@"btn_twitter.png"] forState:UIControlStateNormal];
    [btnShareTw addTarget:self action:@selector(btnShareTwPressed:) forControlEvents:UIControlEventTouchUpInside];
    [btnShareTw setFrame:CGRectMake(280, 416, 25, 25)];
    [self.view addSubview:btnShareTw];
    
    // label
    // ---------------------------------
    // TODO :: init my score point
    NSString *myScorePointURLStr = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/games/qr/checkAccountPoint.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&APPVERSION=%@&APIVERSION=%@&MSISDN=%@", appDel.app_id, appDel.device, appDel.charset, appDel.appversion, appDel.apiversion, appDel.msisdn];
    NSURL *myScorePointURL = [[NSURL alloc] initWithString:myScorePointURLStr];
    NSError *myScorePointError;
    NSString *myScorePointData = [[NSString alloc] initWithContentsOfURL:myScorePointURL encoding:NSUTF8StringEncoding error:&myScorePointError];
    
    if(myScorePointData == nil){
        NSLog(@"[Connection ERROR]:%@", [myScorePointError description]);
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:myScorePointData
                                                                options:0 error:&myScorePointError];
    NSArray *dataArray;
    NSString *point = @"0";
    if(doc){
        //test out put
        dataArray = [doc.rootElement elementsForName:@"ACCOUNT_POINT"];
        GDataXMLElement *gDataPoint = [dataArray objectAtIndex:0];
        //NSLog(@"STATUS = %@", [status stringValue]);
        point = [gDataPoint stringValue];
    }else{
        NSLog(@"[XML ERROR] : %@", [myScorePointError description] );
    }
    UILabel *myScorePoint = [[UILabel alloc] initWithFrame:CGRectMake(130, 422, 60, 25)];
    myScorePoint.text = point;
    [myScorePoint setBackgroundColor:[UIColor clearColor]];
    myScorePoint.textColor = [UIColor blackColor];
    myScorePoint.textAlignment = UITextAlignmentCenter;
    myScorePoint.font = [UIFont boldSystemFontOfSize:16.0f];
    [self.view addSubview:myScorePoint];
    
    return self;
}


-(IBAction)btnShareFBPressed:(id)sender{
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString* linkURL = @"http://thestar9.gmmwireless.com/thestar9/mobilesite/index.php/store";
    NSString* imgURL = @"http://zixboo.gmember.com/thestar9/share/images/qr.png";
    NSString* name = @"The Star 9  Game  : QR Star Code เก็บแต้มให้ครบ แล้วลุ้นไปซบไหล่ The Star9";
    NSString* caption = @"เก็บสะสมแต้ม ..แล้วลุ้นไปสนุกแบบ One day trip กับ The Star9 กัน";
    NSString* description = @"for iPhone and  Android - Avaiable in the App Store";
    
    [appDel shareDataToFBByLinkURL:linkURL imageURL:imgURL name:name caption:caption description:description];
}

-(IBAction)btnShareTwPressed:(id)sender{
//    NSLog(@"share twitter");
    if ([TWTweetComposeViewController canSendTweet])
    {
        NSString* strTwitter = @"The Star 9  Game  : QR Star Code เก็บแต้มให้ครบ แล้วลุ้นไปซบไหล่ The Star9. Download App http://goo.gl/5TSJM";
        
        TWTweetComposeViewController *tweetSheet =
        [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:strTwitter];
        [self presentModalViewController:tweetSheet animated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure                                   your device has an internet connection and you have                                   at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

-(IBAction)btnCalendarPress:(id)sender{
    NSLog(@"btnCalendarPress");
    QRCalendarViewController *calCon = [[[QRCalendarViewController alloc] init] autorelease];
    [self presentModalViewController:calCon animated:YES];
}

- (IBAction) navBarBackBtnPress:(id)sender {
    int count = [self findRootViewController:self];
    
    if(count == 2)
        [self dismissViewControllerAnimated:YES completion:^{}];
    if(count == 3)
        [self.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 4)
        [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 5)
        [self.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 6)
        [self.presentingViewController.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction) btnScanQRPress:(id)sender {
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel checkSubscribe];
    
    if(!appDel.subscriber){
        return;
    }
    
   int count = [self findRootViewController:self];
    NSLog(@"findRootViewController count = %d", count);
    if(count == 5){
        [self dismissViewControllerAnimated:YES completion:^{}];
    }else if(count ==3){
        UIViewController *qrScanViewController = [[[QRScanViewController alloc] init] autorelease];
        [self presentModalViewController:qrScanViewController animated:YES];
    }
}

- (int) findRootViewController:(UIViewController*)vc
{
    if(vc)
    {
        return 1 + [self findRootViewController:vc.presentingViewController];
    }
    return 0;
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
