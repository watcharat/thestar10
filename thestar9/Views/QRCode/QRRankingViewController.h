//
//  QRRankingViewController.h
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/15/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

@interface QRRankingViewController : JWSlideMenuViewController {
    UIScrollView *scrollView;
}

@property(retain, nonatomic) UIScrollView *scrollView;

@end
