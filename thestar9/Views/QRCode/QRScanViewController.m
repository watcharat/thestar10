//
//  QRScanViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/12/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "QRScanViewController.h"
#import "AppDelegate.h"
#import "QRScanRankingViewController.h"
#import "QRCollectionsViewController.h"

@interface QRScanViewController ()

@end

@implementation QRScanViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (id)init {
    
    //AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self = [super init];
    
    CGRect masterRect = [[UIScreen mainScreen] bounds];
    CGRect contentFrame = CGRectMake(0.0, 44.0, masterRect.size.width, masterRect.size.height - 44.0);
    CGRect navBarFrame = CGRectMake(0.0, 0.0, masterRect.size.width, 44.0);
    
    self.view = [[[UIView alloc] initWithFrame:masterRect] autorelease];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    UIImage *image = [UIImage imageNamed:@"qr_scan_cover.png"];
    UIImageView *contentView = [[UIImageView alloc] initWithImage:image];
    contentView.frame = contentFrame;

    
    
//    UIView *contentView = [[[UIView alloc] initWithFrame:contentFrame] autorelease];
//    contentView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"qr_scan_cover.png"]];
//    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    
    [reader.scanner setSymbology:0 config:ZBAR_CFG_ENABLE to:0];
    [reader.scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_ENABLE to:1];
    
    reader.readerView.zoom = 1.0;
    reader.showsCameraControls = NO;
    reader.showsZBarControls = NO;
    reader.wantsFullScreenLayout = NO;
    
    reader.view.frame = CGRectMake(37, 74, 247, 259);
    reader.view.backgroundColor = [UIColor whiteColor];
    
    [reader viewDidLoad];
    [reader viewWillAppear:NO];
    [reader viewDidAppear:NO];

    
    [contentView addSubview:reader.view];
    
    [self.view addSubview:contentView];
    
    UIView *navBar = [[UIView alloc] initWithFrame:navBarFrame];
    [navBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_head_bar.png"]]];
    
    
    UIButton *navBarBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [navBarBackBtn setFrame:CGRectMake(5.0, 6.0, 54.00, 30.00)];
    [navBarBackBtn setBackgroundImage:[UIImage imageNamed:@"qr_btn_subback.png"] forState:UIControlStateNormal];
    [navBarBackBtn addTarget:self action:@selector(navBarBackBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:navBarBackBtn];
    
    UIImage *headerLabelImage = [UIImage imageNamed:@"qr_h_scanqr.png"];
    UIImageView *headerLabelImageView = [[UIImageView alloc] initWithImage:headerLabelImage];
    headerLabelImageView.frame = CGRectMake(80.0, 8.0, 74.0, 34.0);
    [navBar addSubview:headerLabelImageView];
    [headerLabelImageView release];
    
    [self.view insertSubview:navBar aboveSubview:contentView];
    
    
    return self;
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
    ZBarSymbolSet *symbolSet = [info objectForKey:ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    NSString *qrCode = nil;
    
    for (symbol in symbolSet) {
        if(symbol !=nil){
            qrCode = [NSString stringWithFormat:symbol.data];
        }
    }
    
    NSLog(@"QR-Code = %@", qrCode);
    
    // TODO :: call api
    NSString *urlCheckQRCodeString = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/games/qr/checkQRCode.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&APPVERSION=%@&APIVERSION=%@&MSISDN=%@&QRCODE=%@",
                                appDel.app_id
                                , appDel.device
                                , appDel.charset
                                , appDel.appversion
                                , appDel.apiversion
                                , appDel.msisdn
                                , qrCode
                                ];
    
    NSLog(@"urlCheckQRCode = %@", urlCheckQRCodeString);
    NSURL *urlCheckQRCode = [[NSURL alloc] initWithString:urlCheckQRCodeString];
    
    NSError *checkQRCodeError;
    NSString *checkQRCodeData = [[NSString alloc] initWithContentsOfURL:urlCheckQRCode encoding:NSUTF8StringEncoding error:&checkQRCodeError];
    
    if(checkQRCodeData == nil){
        NSLog(@"[Connection Error] : %@", [checkQRCodeError description]);
        return;
    }
    
    
    // TODO :: goto collection view
    UIViewController *collectionsViewController = [[[QRCollectionsViewController alloc] init] autorelease];
    [self presentModalViewController:collectionsViewController animated:YES];
    
    
}

- (IBAction) navBarBackBtnPress:(id)sender {
    int count = [self findRootViewController:self];
    
    if(count == 2)
        [self dismissViewControllerAnimated:YES completion:^{}];
    if(count == 3)
        [self.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 4)
        [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 5)
        [self.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    if(count == 6)
        [self.presentingViewController.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
}

- (int) findRootViewController:(UIViewController*)vc
{
    if(vc)
    {
        return 1 + [self findRootViewController:vc.presentingViewController];
    }
    return 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}

@end
