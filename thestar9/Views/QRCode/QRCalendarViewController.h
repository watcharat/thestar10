//
//  QRCalendarViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/17/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PopupView.h"

@interface QRCalendarViewController : UIViewController{
    PopupView *vwDetail;
    NSMutableDictionary *dicDate;

}

@property (retain, nonatomic)PopupView *vwDetail;
@property (retain, nonatomic)NSMutableDictionary *dicDate;


@end
