//
//  QRViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/12/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "QRViewController.h"
#import "QRHowToViewController.h"
#import "QRScanViewController.h"

#import "QRScanRankingViewController.h"
#import "QRRankingViewController.h"
#import "QRCollectionsViewController.h"

#import "QRCalendarViewController.h"
#import "AppDelegate.h"

@interface QRViewController ()

@end

@implementation QRViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (id)init {
    
    self = [super init];
    self.title = @"The Star QR Game";
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated {
    
}

- (IBAction)btnExitPress:(id)sender {
    NSLog(@"btnExitPress click");
    //[self.navigationController dismissModalViewControllerAnimated:YES];
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)btnHowToPress:(id)sender {
    UIViewController *howtoController = [[[QRHowToViewController alloc] init] autorelease];
    [self presentModalViewController:howtoController animated:YES];
}

- (IBAction)btnPlayPress:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel checkSubscribe];
    
    if(appDel.subscriber){
        UIViewController *qrScanRankingViewController = [[[QRScanRankingViewController alloc] init] autorelease];
        [self presentModalViewController:qrScanRankingViewController animated:YES];
    }
}

- (IBAction)btnCollectionPress:(id)sender {
    NSLog(@"collection button press");
    
    QRCollectionsViewController *qrCollectCon = [[[QRCollectionsViewController alloc] init] autorelease];
    [self presentModalViewController:qrCollectCon animated:YES];
}

- (IBAction)btnRankingPress:(id)sender {
    NSLog(@"ranking button press");
    UIViewController *qrRankingViewController = [[[QRRankingViewController alloc] init] autorelease];
    [self presentModalViewController:qrRankingViewController animated:YES];
}


// event of calendar button click
- (IBAction)btnCalendarPressed:(id)sender {
    QRCalendarViewController *qrCalCon = [[[QRCalendarViewController alloc] init] autorelease];
    [self presentModalViewController:qrCalCon animated:YES];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
