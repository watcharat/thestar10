//
//  HowToViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/12/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "QRHowToViewController.h"
#import "AppDelegate.h"

@interface QRHowToViewController ()

@end

@implementation QRHowToViewController

@synthesize webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)init {
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self = [super init];
    self.title = @"How To";
    
    CGRect masterRect = [[UIScreen mainScreen] bounds];
    CGRect contentFrame = CGRectMake(0.0, 44.0, masterRect.size.width, masterRect.size.height - 44.0);
    CGRect navBarFrame = CGRectMake(0.0, 0.0, masterRect.size.width, 44.0);
    
    
    self.view = [[[UIView alloc] initWithFrame:contentFrame] autorelease];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    
    NSString *strWebViewUrl = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/games/qr/howToUseStarQR.jsp?APP_ID=%@&DEVICE=%@", appDel.app_id, appDel.device];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strWebViewUrl]]];
    
    [self.view addSubview:webView];
    
    UIView *navBar = [[UIView alloc] initWithFrame:navBarFrame];
    [navBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_head_bar.png"]]];
    
    
    UIButton *navBarBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [navBarBackBtn setFrame:CGRectMake(5.0, 6.0, 54.00, 30.00)];
    [navBarBackBtn setBackgroundImage:[UIImage imageNamed:@"qr_btn_subback.png"] forState:UIControlStateNormal];
    [navBarBackBtn addTarget:self action:@selector(navBarBackBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    
    [navBar addSubview:navBarBackBtn];
    
    UIImage *headerLabelImage = [UIImage imageNamed:@"qr_h_howto.png"];
    UIImageView *headerLabelImageView = [[UIImageView alloc] initWithImage:headerLabelImage];
    headerLabelImageView.frame = CGRectMake(80.0, 8.0, 74.0, 34.0);
    [navBar addSubview:headerLabelImageView];
    [headerLabelImageView release];
    
    [self.view insertSubview:navBar aboveSubview:self.webView];
    
    
    return self;
}

- (IBAction) navBarBackBtnPress:(id)sender {
    NSLog(@"navBarBackBtn Press");
    [self dismissModalViewControllerAnimated:YES];
}

- (void) dealloc {
    [webView release];
    [super dealloc];
}

- (void) viewDidUnload {
    self.webView = nil;
    [super viewDidUnload];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}

@end
