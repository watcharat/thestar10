//
//  PopupView.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/18/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "PopupView.h"

@implementation PopupView

@synthesize bg0, bg1, bg2;
@synthesize cover_img0, cover_img1, cover_img2;
@synthesize lbName0, lbName1, lbName2;
@synthesize lbDesc0, lbDesc1, lbDesc2;
@synthesize lbTime0, lbTime1, lbTime2;

@synthesize lbTitle;
@synthesize arData;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    [self initUI];
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)initUI{
    float bgDW = 251;
    float bgDH = 265;
    
    [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    
    //background
    CGRect recDetail = CGRectMake((320-bgDW)/2, (480-bgDH)/2, bgDW, bgDH);
    UIImageView *ivBgPopup = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popup_bg.png"]];
    [ivBgPopup setFrame:recDetail];
    [self addSubview:ivBgPopup];
    
    // button back
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(((320-bgDW)/2)+bgDW - 28, (480 - bgDH)/2, 28, 28)];
    [btnBack setBackgroundImage:[UIImage imageNamed:@"popup_cancel.png"] forState:UIControlStateNormal];
    [btnBack setBackgroundImage:[UIImage imageNamed:@"popup_cancel_active.png"] forState:UIControlStateHighlighted];
    [btnBack addTarget:self action:@selector(btnPopupBackPressed:)
      forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnBack];
    
    // header label
    self.lbTitle = [[UILabel alloc] initWithFrame:CGRectMake(((320-bgDW)/2)+30, 130, 190, 52)];
    [self.lbTitle setText:@"test"];
    [self.lbTitle setTextAlignment:NSTextAlignmentCenter];
    [self.lbTitle setFont:[UIFont boldSystemFontOfSize:15]];
    [self.lbTitle setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.lbTitle];
    
    
    //first 
    self.bg0 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popup_table.png"]];
    [self.bg0 setFrame:CGRectMake(((320-bgDW)/2)+30, 180+(52*0), 190, 52)];
    [self addSubview:self.bg0];

    self.cover_img0 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"popup_10tv.png"]];
    [cover_img0 setFrame:CGRectMake(74, 180, 55, 55)];
    [self addSubview:cover_img0];
    
    self.lbName0 = [[UILabel alloc] initWithFrame:CGRectMake(140, 175+(52*0), 100, 26)];
    [self.lbName0 setText:@"xxxx"];
    [self.lbName0 setBackgroundColor:[UIColor clearColor]];
    [self.lbName0 setTextColor:[UIColor whiteColor]];
    
    [self addSubview:lbName0];
    
    self.lbDesc0 = [[UILabel alloc] initWithFrame:CGRectMake(140, 190+(52*0), 100, 26)];
    [self.lbDesc0 setText:@"yyyy"];
    [self.lbDesc0 setBackgroundColor:[UIColor clearColor]];
    [self.lbDesc0 setTextColor:[UIColor whiteColor]];
    [self addSubview:lbDesc0];
    
    self.lbTime0 = [[UILabel alloc] initWithFrame:CGRectMake(140, 205+(52*0), 100, 26)];
    [self.lbTime0 setText:@"zzzz"];
    [self.lbTime0 setBackgroundColor:[UIColor clearColor]];
    [self.lbTime0 setTextColor:[UIColor whiteColor]];
    [self addSubview:lbTime0];

    
    //second
    self.bg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popup_table.png"]];
    [self.bg1 setFrame:CGRectMake(((320-bgDW)/2)+30, 180+(52*1), 190, 52)];
    [self addSubview:self.bg1];
    // cover
    self.cover_img1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"popup_10tv.png"]];
    [cover_img1 setFrame:CGRectMake(74, 180+(52*1), 55, 55)];
    [self addSubview:cover_img1];
    
    self.lbName1 = [[UILabel alloc] initWithFrame:CGRectMake(140, 175+(52*1), 100, 26)];
    [self.lbName1 setText:@"xxxx"];
    [self.lbName1 setBackgroundColor:[UIColor clearColor]];
    [self.lbName1 setTextColor:[UIColor whiteColor]];
    
    [self addSubview:lbName1];
    
    self.lbDesc1 = [[UILabel alloc] initWithFrame:CGRectMake(140, 190+(52*1), 100, 26)];
    [self.lbDesc1 setText:@"yyyy"];
    [self.lbDesc1 setBackgroundColor:[UIColor clearColor]];
    [self.lbDesc1 setTextColor:[UIColor whiteColor]];
    [self addSubview:lbDesc1];
    
    self.lbTime1 = [[UILabel alloc] initWithFrame:CGRectMake(140, 205+(52*1), 100, 26)];
    [self.lbTime1 setText:@"zzzz"];
    [self.lbTime1 setBackgroundColor:[UIColor clearColor]];
    [self.lbTime1 setTextColor:[UIColor whiteColor]];
    [self addSubview:lbTime1];
    
    

    //third
    self.bg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popup_table.png"]];
    [self.bg2 setFrame:CGRectMake(((320-bgDW)/2)+30, 180+(52*2), 190, 52)];
    [self addSubview:self.bg2];
    // cover
    self.cover_img2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"popup_10tv.png"]];
    [cover_img2 setFrame:CGRectMake(74, 180+(52*2), 55, 55)];
    [self addSubview:cover_img2];
    
    self.lbName2 = [[UILabel alloc] initWithFrame:CGRectMake(140, 175+(52*2), 100, 26)];
    [self.lbName2 setText:@"xxxx"];
    [self.lbName2 setBackgroundColor:[UIColor clearColor]];
    [self.lbName2 setTextColor:[UIColor whiteColor]];
    
    [self addSubview:lbName2];
    
    self.lbDesc2 = [[UILabel alloc] initWithFrame:CGRectMake(140, 190+(52*2), 100, 26)];
    [self.lbDesc2 setText:@"yyyy"];
    [self.lbDesc2 setBackgroundColor:[UIColor clearColor]];
    [self.lbDesc2 setTextColor:[UIColor whiteColor]];
    [self addSubview:lbDesc2];
    
    self.lbTime2 = [[UILabel alloc] initWithFrame:CGRectMake(140, 205+(52*2), 100, 26)];
    [self.lbTime2 setText:@"zzzz"];
    [self.lbTime2 setBackgroundColor:[UIColor clearColor]];
    [self.lbTime2 setTextColor:[UIColor whiteColor]];
    [self addSubview:lbTime2];
    
}

// button back head bar pressed
- (IBAction) btnPopupBackPressed:(id)sender {
    
    [self setHidden:YES];
}

-(void)loadDataPreview:(NSMutableArray*)ar{
    //
    self.arData = ar;
}

-(void)reDraw{
    
//    NSLog(@"%@", arData);
    // data first
    NSMutableDictionary* dic0 = [arData objectAtIndex:0];

    NSString* cover0 = [dic0 objectForKey:@"cover"];
    
    NSString* name0 = [dic0 objectForKey:@"name"];
    NSString* desc0 = [dic0 objectForKey:@"desc"];
    NSString* time0 = [dic0 objectForKey:@"time"];
    
    self.lbName0.text = name0;
    self.lbDesc0.text = desc0;
    self.lbTime0.text = time0;
    
    NSData *dat0 = [NSData dataWithContentsOfURL:[NSURL URLWithString:cover0]];
    [self.cover_img0 setImage:[UIImage imageWithData:dat0]];
    
    // data second

    NSMutableDictionary* dic1 = [arData objectAtIndex:1];
    NSLog(@"dic = %@", dic1);    
    NSString* cover1 = [dic1 objectForKey:@"cover"];
    
    NSString* name1 = [dic1 objectForKey:@"name"];
    NSString* desc1 = [dic1 objectForKey:@"desc"];
    NSString* time1 = [dic1 objectForKey:@"time"];
    
    self.lbName1.text = name1;
    self.lbDesc1.text = desc1;
    self.lbTime1.text = time1;
    
    NSData *dat1 = [NSData dataWithContentsOfURL:[NSURL URLWithString:cover1]];
    [self.cover_img1 setImage:[UIImage imageWithData:dat1]];
    
    // data third
    NSMutableDictionary* dic2 = [arData objectAtIndex:2];
    
    NSString* cover2 = [dic2 objectForKey:@"cover"];
    
    NSString* name2 = [dic2 objectForKey:@"name"];
    NSString* desc2 = [dic2 objectForKey:@"desc"];
    NSString* time2 = [dic2 objectForKey:@"time"];
    
    self.lbName2.text = name2;
    self.lbDesc2.text = desc2;
    self.lbTime2.text = time2;
    
    NSData *dat2 = [NSData dataWithContentsOfURL:[NSURL URLWithString:cover2]];
    [self.cover_img2 setImage:[UIImage imageWithData:dat2]];
    
}

-(void)setLabelTitle:(NSString*)titleDate{
    
    self.lbTitle.text = [self stringTextFormat:titleDate];
}

-(NSString*)stringTextFormat:(NSString*)date{
    NSString* strOut;
    NSInteger ndate = [date integerValue];
    
    if(ndate > 20130400){
        //april
        ndate = ndate - 20130400;
        strOut = [NSString stringWithFormat:@"%d เมษายน 2556", ndate];
    }else{
        ndate = ndate - 20130300;
        // march
        strOut = [NSString stringWithFormat:@"%d มีนาคม 2556", ndate];
    }
    
    return strOut;
    
}


@end
