//
//  QRRankingViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/15/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "QRRankingViewController.h"
#import "QRScanViewController.h"
#import "GDataXMLNode.h"
#import "AppDelegate.h"

@interface QRRankingViewController ()

@end

@implementation QRRankingViewController

@synthesize scrollView;

- (void) dealloc {
    [scrollView release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [scrollView setContentOffset:CGPointMake(0, 0)];
    [self renderData];
}

- (id) init {
    self = [super init];
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"QRRankingViewController");
    
    // init frame size
    CGRect masterRect = [[UIScreen mainScreen] bounds];
    CGRect navBarFrame = CGRectMake(0.0, 0.0, masterRect.size.width, 44.0);
    CGRect myScoreFrame = CGRectMake(0.0, masterRect.size.height - 64.0 - navBarFrame.size.height, masterRect.size.width, 64.0);
    CGRect scanQRFrame = CGRectMake(0.0, masterRect.size.height - 122 - navBarFrame.size.height - myScoreFrame.size.height, masterRect.size.width, 122);
    CGRect scanQRBtnFrame = CGRectMake((masterRect.size.width/2)-(179.00/2), masterRect.size.height-navBarFrame.size.height-(scanQRFrame.size.height/2)-144.00, 179.0, 144.0);
    CGRect rankingHeaderFrame = CGRectMake(0.0, navBarFrame.size.height, masterRect.size.width, 56.0);
    CGRect contentFrame = CGRectMake(0.0, navBarFrame.size.height+(rankingHeaderFrame.size.height-18), masterRect.size.width, masterRect.size.height - navBarFrame.size.height - rankingHeaderFrame.size.height - myScoreFrame.size.height - scanQRFrame.size.height);
    
    self.view = [[[UIView alloc] initWithFrame:masterRect] autorelease];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    // TODO :: init navigate
    UIView *navBar = [[[UIView alloc] initWithFrame:navBarFrame] autorelease];
    [navBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_head_bar.png"]]];
    
    UIButton *navBarBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [navBarBackBtn setFrame:CGRectMake(5.0, 6.0, 54.00, 30.00)];
    [navBarBackBtn setBackgroundImage:[UIImage imageNamed:@"qr_btn_subback.png"] forState:UIControlStateNormal];
    [navBarBackBtn addTarget:self action:@selector(navBarBackBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:navBarBackBtn];
    
    UIButton *btnScanQR = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnScanQR setFrame:CGRectMake(masterRect.size.width-67-5, 6, 67, 30)];
    [btnScanQR setBackgroundImage:[UIImage imageNamed:@"qr_btn_scan_blue.png"] forState:UIControlStateNormal];
    [btnScanQR addTarget:self action:@selector(btnScanQRPress:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:btnScanQR];
    
    UIImage *headerLabelImage = [UIImage imageNamed:@"qr_h_ranking.png"];
    UIImageView *headerLabelImageView = [[UIImageView alloc] initWithImage:headerLabelImage];
    headerLabelImageView.frame = CGRectMake(80.0, 8.0, 74.0, 34.0);
    [navBar addSubview:headerLabelImageView];
    
    [self.view addSubview:navBar];
    
    // TODO :: init scroll ranking header
    UIImage *rankingHeaderImg = [UIImage imageNamed:@"qr_he_ranking.png"];
    UIImageView *rankingHeaderImgView = [[UIImageView alloc] initWithImage:rankingHeaderImg];
    rankingHeaderImgView.frame = rankingHeaderFrame;
    
    [self.view addSubview:rankingHeaderImgView];
    
    
    // TODO :: init scroll ranking
    if(scrollView == nil){
        scrollView = [[UIScrollView alloc] initWithFrame:contentFrame];
        //scrollView.contentSize = CGSizeMake(masterRect.size.width, contentFrame.size.height);
        scrollView.contentSize = CGSizeMake(masterRect.size.width, 1000);
        scrollView.delegate = self;
        scrollView.backgroundColor = [UIColor blackColor];
        
        [self.view addSubview:scrollView];
    }
    
    // TODO :: init scan qr
    UIImage *scanQRImage = [UIImage imageNamed:@"qr_bottom_ranking.png"];
    UIImageView *scanQRImageView = [[UIImageView alloc] initWithImage:scanQRImage];
    scanQRImageView.frame = scanQRFrame;
    
    [self.view addSubview:scanQRImageView];
    
    // TODO :: init my score
    UIImage *myScoreImage = [UIImage imageNamed:@"qr_myscore_bar.png"];
    UIImageView *myScoreImageView = [[UIImageView alloc] initWithImage:myScoreImage];
    myScoreImageView.frame = myScoreFrame;
    
    // TODO :: init my score point
    NSString *myScorePointURLStr = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/games/qr/checkAccountPoint.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&APPVERSION=%@&APIVERSION=%@&MSISDN=%@", appDel.app_id, appDel.device, appDel.charset, appDel.appversion, appDel.apiversion, appDel.msisdn];
    NSURL *myScorePointURL = [[NSURL alloc] initWithString:myScorePointURLStr];
    NSError *myScorePointError;
    NSString *myScorePointData = [[NSString alloc] initWithContentsOfURL:myScorePointURL encoding:NSUTF8StringEncoding error:&myScorePointError];
    
    if(myScorePointData == nil){
        NSLog(@"[Connection ERROR]:%@", [myScorePointError description]);
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:myScorePointData
                                                                options:0 error:&myScorePointError];
    NSArray *dataArray;
    NSString *point = @"0";
    if(doc){
        //test out put
        dataArray = [doc.rootElement elementsForName:@"ACCOUNT_POINT"];
        GDataXMLElement *gDataPoint = [dataArray objectAtIndex:0];
        //NSLog(@"STATUS = %@", [status stringValue]);
        point = [gDataPoint stringValue];
    }else{
        NSLog(@"[XML ERROR] : %@", [myScorePointError description] );
    }
    
    UILabel *myScorePoint = [[UILabel alloc] initWithFrame:CGRectMake(myScoreImageView.frame.size.width-130, 22, 70, 25)];
    myScorePoint.text = point;
    [myScorePoint setBackgroundColor:[UIColor clearColor]];
    myScorePoint.textColor = [UIColor blackColor];
    myScorePoint.textAlignment = UITextAlignmentCenter;
    myScorePoint.font = [UIFont boldSystemFontOfSize:16.0f];
    [myScoreImageView addSubview:myScorePoint];
    
    [myScorePointData release];
    [doc release];
    [myScorePointURL release];
    
    [self.view addSubview:myScoreImageView];
    
    return self;
}

- (void) renderData {
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"call renderData method");
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/games/qr/topRanking.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&APPVERSION=%@&APIVERSION=%@&MSISDN=%@", appDel
                         .app_id, appDel.device, appDel.charset, appDel.app_id, appDel.apiversion, appDel.msisdn];
    NSLog(@"CALL API: %@", urlLink);
    
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    
    
    NSError *error;
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    NSArray *dataArray;
    if(doc){
        
        //test out put
        dataArray = [doc.rootElement elementsForName:@"RANK"];
        //GDataXMLElement *status = [dataArray objectAtIndex:0];
        //NSLog(@"STATUS = %@", [status stringValue]);
        
        // render data by xml
        //[self renderData:doc];
        
    }else{
        NSLog(@"[XML ERROR] : %@", [error description] );
        return;
    }
    
    int rows = 0;
    int cellHeight = 34;
    int cellWidth = self.view.frame.size.width;
    
    for (int i = 0; i < dataArray.count; i++) {
        
        GDataXMLElement *elementRank = [dataArray objectAtIndex:i];
        NSString *rankNumber = [[[elementRank elementsForName:@"RANK_NUMBER"] objectAtIndex:0] stringValue];
        NSString *rankName = [[[elementRank elementsForName:@"NAME"] objectAtIndex:0] stringValue];
        NSString *rankPoint = [[[elementRank elementsForName:@"POINT"] objectAtIndex:0] stringValue];
        NSString *rankDesc = [[[elementRank elementsForName:@"DESC"] objectAtIndex:0] stringValue];
        
        UIImage *cellImage;
        //= [UIImage imageNamed:@"qr_tabscore.png"];
        /*
         if( i == 0 ){
         cellImage = [UIImage imageNamed:@"qr_tabscore1.png"];
         }else if( i == 1 ){
         cellImage = [UIImage imageNamed:@"qr_tabscore3.png"];
         }else{
         cellImage = [UIImage imageNamed:@"qr_tabscore.png"];
         }
         */
        cellImage = [UIImage imageNamed:@"qr_tabscore.png"];
        
        //UIImageView *cellImageView = [[UIImageView alloc] initWithImage:cellImage];
        //cellImageView.frame = CGRectMake(0, cellHeight*i, cellWidth, cellHeight);
        UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0, cellHeight*i, cellWidth, cellHeight)];
        cellView.backgroundColor = [UIColor colorWithPatternImage:cellImage];
        
        // TODO :: create rank
        if (i == 0){
            UIImage *rankImg = [UIImage imageNamed:@"qr_star1.png"];
            UIImageView *rankImgView = [[UIImageView alloc] initWithImage:rankImg];
            rankImgView.frame = CGRectMake(15, 2, 30, 29);
            [cellView addSubview:rankImgView];
            [rankImgView release];
            
            UIImage *rankImgMask = [UIImage imageNamed:@"qr_tabscore1.png"];
            UIImageView *rankImgMaskView = [[UIImageView alloc] initWithImage:rankImgMask];
            rankImgMaskView.frame = CGRectMake(0, 0, cellWidth, cellHeight);
            [cellView addSubview:rankImgMaskView];
            [rankImgMaskView release];
        } else if (i == 1) {
            UIImage *rankImg = [UIImage imageNamed:@"qr_star2.png"];
            UIImageView *rankImgView = [[UIImageView alloc] initWithImage:rankImg];
            rankImgView.frame = CGRectMake(15, 2, 30, 29);
            [cellView addSubview:rankImgView];
            [rankImgView release];
            
            UIImage *rankImgMask = [UIImage imageNamed:@"qr_tabscore2.png"];
            UIImageView *rankImgMaskView = [[UIImageView alloc] initWithImage:rankImgMask];
            rankImgMaskView.frame = CGRectMake(0, 0, cellWidth, cellHeight);
            [cellView addSubview:rankImgMaskView];
            [rankImgMaskView release];
        } else if (i == 2) {
            UIImage *rankImg = [UIImage imageNamed:@"qr_star3.png"];
            UIImageView *rankImgView = [[UIImageView alloc] initWithImage:rankImg];
            rankImgView.frame = CGRectMake(15, 2, 30, 29);
            [cellView addSubview:rankImgView];
            [rankImgView release];
            
            UIImage *rankImgMask = [UIImage imageNamed:@"qr_tabscore3.png"];
            UIImageView *rankImgMaskView = [[UIImageView alloc] initWithImage:rankImgMask];
            rankImgMaskView.frame = CGRectMake(0, 0, cellWidth, cellHeight);
            [cellView addSubview:rankImgMaskView];
            [rankImgMaskView release];
        }
        
        // TODO :: set rank number
        UILabel *rankLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 59, 35)];
        rankLabel.text = [NSString stringWithFormat:@"%@", rankNumber];
        [rankLabel setBackgroundColor:[UIColor clearColor]];
        rankLabel.textColor = [UIColor whiteColor];
        rankLabel.textAlignment = UITextAlignmentCenter;
        rankLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [cellView addSubview:rankLabel];
        [rankLabel release];
        
        
        // TODO :: set rank mobile number
        rankName = [rankName stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
        NSString *rankNameSuccessfull = [NSString stringWithFormat:@"%@-%@-XXXX", [rankName substringWithRange:NSMakeRange(0, 3)], [rankName substringWithRange:NSMakeRange(3, 3)]];
        CGRect rankNameFrame;
        float fontSize = 16.0f;
        if(![rankDesc isEqualToString:@"-"]){
            rankNameFrame = CGRectMake(74, 0, 155, 20);
            fontSize =  16.0f;
            
            UILabel *rankFanclub = [[UILabel alloc] initWithFrame:CGRectMake(74, 20, 155, 15)];
            rankFanclub.text = rankDesc;
            [rankFanclub setBackgroundColor:[UIColor clearColor]];
            rankFanclub.textColor = [UIColor whiteColor];
            rankFanclub.font = [UIFont boldSystemFontOfSize:10.0f];
            [cellView addSubview:rankFanclub];
            [rankFanclub release];
        }else{
            rankNameFrame = CGRectMake(74, 0, 155, 35);
        }
        
        UILabel *rankMSISDN = [[UILabel alloc] initWithFrame:rankNameFrame];
        rankMSISDN.text = rankNameSuccessfull;
        [rankMSISDN setBackgroundColor:[UIColor clearColor]];
        rankMSISDN.textColor = [UIColor whiteColor];
        rankMSISDN.font = [UIFont boldSystemFontOfSize:fontSize];
        [cellView addSubview:rankMSISDN];
        [rankMSISDN release];
        
        
        // TODO :: set rank total score
        UILabel *rankTotalScore = [[UILabel alloc] initWithFrame:CGRectMake(scrollView.frame.size.width-101, 0, 99, 35)];
        rankTotalScore.text = rankPoint;
        [rankTotalScore setBackgroundColor:[UIColor clearColor]];
        rankTotalScore.textColor = [UIColor whiteColor];
        rankTotalScore.textAlignment = UITextAlignmentCenter;
        rankTotalScore.font = [UIFont boldSystemFontOfSize:16.0f];
        [cellView addSubview:rankTotalScore];
        [rankTotalScore release];
        
        
        // TODO :: set rank total score
        
        
        // TODO :: set cell overlay image
        //UIImage *overlayImg = [UIImage imageNamed:@"qr_tabscore1.png"];
        
        [scrollView addSubview:cellView];
        rows++;
    }
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, (rows+2)*cellHeight)];
    
    [data release];
    [doc release];
    [url release];
}

- (IBAction) navBarBackBtnPress:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction) btnScanQRPress:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel checkSubscribe];
    
    if(!appDel.subscriber){
        return;
    }
    UIViewController *qrScanViewController = [[[QRScanViewController alloc] init] autorelease];
    [self presentModalViewController:qrScanViewController animated:YES];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
