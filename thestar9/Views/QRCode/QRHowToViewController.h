//
//  HowToViewController.h
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/12/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

@interface QRHowToViewController : JWSlideMenuViewController {
    IBOutlet UIWebView *webView;
}

@property (retain, nonatomic) IBOutlet UIWebView *webView;

@end
