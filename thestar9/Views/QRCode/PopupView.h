//
//  PopupView.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/18/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupView : UIView{
    UIImageView* bg0;
    UIImageView* bg1;
    UIImageView* bg2;
    
    UIImageView* cover_img0;
    UIImageView* cover_img1;
    UIImageView* cover_img2;
    
    UILabel* lbName0;
    UILabel* lbName1;
    UILabel* lbName2;
    
    UILabel* lbDesc0;
    UILabel* lbDesc1;
    UILabel* lbDesc2;
    
    UILabel* lbTime0;
    UILabel* lbTime1;
    UILabel* lbTime2;
    
    UILabel* lbTitle;
    
    NSMutableArray *arData;
}

@property (retain, nonatomic)UIImageView* bg0;
@property (retain, nonatomic)UIImageView* bg1;
@property (retain, nonatomic)UIImageView* bg2;

@property (retain, nonatomic)UIImageView* cover_img0;
@property (retain, nonatomic)UIImageView* cover_img1;
@property (retain, nonatomic)UIImageView* cover_img2;

@property (retain, nonatomic)UILabel* lbName0;
@property (retain, nonatomic)UILabel* lbName1;
@property (retain, nonatomic)UILabel* lbName2;

@property (retain, nonatomic)UILabel* lbDesc0;
@property (retain, nonatomic)UILabel* lbDesc1;
@property (retain, nonatomic)UILabel* lbDesc2;

@property (retain, nonatomic)UILabel* lbTime0;
@property (retain, nonatomic)UILabel* lbTime1;
@property (retain, nonatomic)UILabel* lbTime2;

@property (retain, nonatomic)NSMutableArray *arData;

@property (retain, nonatomic)UILabel* lbTitle;

-(void)reDraw;
-(void)setLabelTitle:(NSString*)titleDate;
-(void)loadDataPreview:(NSMutableArray*)ar;
@end
