//
//  QRCalendarViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/17/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "QRCalendarViewController.h"

#import "QRScanViewController.h"

#import "AppDelegate.h"
#import "GDataXMLNode.h"


@interface QRCalendarViewController ()

@end

@implementation QRCalendarViewController

@synthesize vwDetail;
@synthesize dicDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self initUI];
        
        self.dicDate = [[NSMutableDictionary alloc] init];
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    [self callAPIQRCalendar];
    [self rendermaskDate];
}


-(void)initUI{
    
    // create navigation
    float navBarHeight = 44.0;
    CGRect masterRect = [[UIScreen mainScreen] bounds];
    CGRect navBarFrame = CGRectMake(0.0, 0.0, masterRect.size.width, navBarHeight);
    UIView *navBar = [[UIView alloc] initWithFrame:navBarFrame];
    
    // background nagigation image
    [navBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"qr_head_bar.png"]]];
    
    // button back
    UIButton *navBarBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [navBarBackBtn setFrame:CGRectMake(5.0, 6.0, 54.00, 30.00)];
    [navBarBackBtn setBackgroundImage:[UIImage imageNamed:@"qr_btn_subback.png"] forState:UIControlStateNormal];
    [navBarBackBtn addTarget:self action:@selector(navBarBackBtnPress:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:navBarBackBtn];
    
    // button scan qr
    UIButton *btnScanQR = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnScanQR setFrame:CGRectMake(masterRect.size.width-67-5, 6, 67, 30)];
    [btnScanQR setBackgroundImage:[UIImage imageNamed:@"qr_btn_scan_blue.png"] forState:UIControlStateNormal];
    [btnScanQR addTarget:self action:@selector(btnScanQRPress:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:btnScanQR];
    
    // head image
    UIImage *headerLabelImage = [UIImage imageNamed:@"qr_h_calendar.png"];
    UIImageView *headerLabelImageView = [[UIImageView alloc] initWithImage:headerLabelImage];
    headerLabelImageView.frame = CGRectMake(80.0, 8.0, 74.0, 34.0);
    [navBar addSubview:headerLabelImageView];
    [headerLabelImageView release];
    
    [self.view addSubview:navBar];
    [navBar release];
    
    // create background image
    UIImageView* ivBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qr_bg_main.png"]];
    [ivBg setFrame:CGRectMake(0, navBarHeight, ivBg.frame.size.width, ivBg.frame.size.height)];
    [self.view addSubview:ivBg];
    [ivBg release];
    
    // create logo head
    UIImageView* ivH = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"calendar_bar.png"]];
    [ivH setFrame:CGRectMake(0, navBarHeight, ivH.frame.size.width, ivH.frame.size.height)];
    [self.view addSubview:ivH];
    [ivH release];
    
    // create scroll
    float calWidth = 260;
    float calHeight = 300;
    UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(
                                                                              30,
                                                                              navBarHeight+ivH.frame.size.height,
                                                                              calWidth,
                                                                              480-(navBarHeight+ivH.frame.size.height+navBarHeight) )];
    [scrollview setShowsVerticalScrollIndicator:YES];
    scrollview.contentSize = CGSizeMake(calWidth, calHeight*2); // 2 is 2 month
    [self.view addSubview:scrollview];
    
    //create calendar 
    [self generateCalendarMonth:scrollview];
    
    [scrollview release];
    
    // ---------------------------------------------
    //create layout view of detail
    // pop up
    // ---------------------------------------------
    self.vwDetail = [[PopupView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    
    [self.view addSubview:vwDetail];
    [vwDetail setHidden:YES];
}



- (IBAction) navBarBackBtnPress:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}


// generate button method
-(void)generateCalendarMonth:(UIScrollView*)scview{
    
    // create month with for
    float calWidth = 260;
    float calHeight = 300;
    float monthSpaceCap = calHeight;
    NSMutableArray *arMonthName = [[NSMutableArray alloc] init];
    [arMonthName addObject:@"มีนาคม"];
    [arMonthName addObject:@"เมษายน"];
    
    float areaScrollHeight = 0;

    for(int i=0; i< [arMonthName count]; i++){
        // main view add all object
        NSString* imgNameBg = @"calendar_5.png";
        float areaH = monthSpaceCap;
//        if(i==0){
//            imgNameBg = @"calendar_6.png";
//            areaH += 37;
//        }
        
        UIView *vwCalendar = [[UIView alloc] initWithFrame:CGRectMake(0, areaScrollHeight, 260, monthSpaceCap)];
        
        areaScrollHeight += areaH;
        
        // background
        UIImageView *imBgCalMarch = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgNameBg]];
        [imBgCalMarch setFrame:CGRectMake(0, 0, imBgCalMarch.frame.size.width, imBgCalMarch.frame.size.height)];
        [vwCalendar addSubview:imBgCalMarch];
        [imBgCalMarch release];
        
        // label header
        float paddingTop = 8;
        UILabel *lbMonth = [[UILabel alloc] initWithFrame:CGRectMake(0, paddingTop, 259, 26)];
        lbMonth.text = [arMonthName objectAtIndex:i];
        [lbMonth setTextAlignment:NSTextAlignmentCenter];
        [lbMonth setFont:[UIFont boldSystemFontOfSize:18]];
        [lbMonth setBackgroundColor:[UIColor clearColor]];
        [lbMonth setTextColor:[UIColor whiteColor]];
        [vwCalendar addSubview:lbMonth];
        [lbMonth release];
        
        // generate day in month
        [self genDayOfMonth:i vwCal:vwCalendar];
        
        // add all to scroll view
        [scview addSubview:vwCalendar];
        
    }
    
    // recalculate size of frame
    [scview setContentSize:CGSizeMake(calWidth, areaScrollHeight)];
    
    [arMonthName release];

    
}

// generate day of month
-(void)genDayOfMonth:(int)i vwCal:(UIView*)vwCal{
    // 0 = March, 1 = April
    float btnH = 37;
    float btnW = 37;
    
    
    // create NSDate for NSDateComponents

    
    NSDateComponents *comps = [[[NSDateComponents alloc] init] autorelease];
    [comps setDay:1];
    [comps setMonth:i+3];
    [comps setYear:2013];

    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *dateChk = [gregorian dateFromComponents:comps];
    
    // find weekday
    int weekday = [self DayOfWeek:dateChk]; // 1 = sunday
    NSLog(@"weekday = %d", weekday);

    

    
    //custom calendar
    weekday -=2; // custom calendar
//    if(weekday < 0) weekday = 7;
    
    int days = [self DaysOfMonth:dateChk];
    int paddingTop = 84;
    
    
    // generate day button
    for(int d=0; d<days; d++){
        int daycount = d+weekday;
        NSString *strday = [NSString stringWithFormat:@"%d", d+1];
        float x=(daycount%7)*btnW;
        float y=(daycount/7)*btnH;
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(x, y+paddingTop, btnW, btnH)];
        [btn setTitleColor:[UIColor colorWithRed:21/255.0f green:32/255.0f blue:67/255.0f alpha:1] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnCalendarPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:strday forState:UIControlStateNormal];
        
        
        // set value click use tag
        // 20130300 + day = march+day
        // 20130400 + day = april+day
        if(i==0){
            btn.tag = 20130300 + d+1;
        }else{
            btn.tag = 20130400 + d+1;
        }
        
        // add to show
        [vwCal addSubview:btn];
    }
    
}

// util calendar
-(int)DayOfWeek:(NSDate*)dateChk{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents = [gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:dateChk];
//    NSInteger day = [weekdayComponents day];
    NSInteger weekday = [weekdayComponents weekday];
    return weekday;
}

-(int)DaysOfMonth:(NSDate*)dateChk{
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:dateChk];
    
    return days.length;
}
// ------------------------------------------------
// event click of calendar
// ------------------------------------------------
-(IBAction)btnCalendarPressed:(id)sender{
    UIButton *btn = (UIButton*)sender;
    int tagDay = btn.tag;
    NSLog(@"day = %d", tagDay);
    
    // get data array on click
    NSString* key = [NSString stringWithFormat:@"%d", tagDay];
    NSMutableArray *arSchedules = [self.dicDate objectForKey:key];
    
    if(arSchedules!=nil){
        
//        NSLog(@"%@", arSchedules);
        [self.vwDetail setLabelTitle:key];
        [self.vwDetail loadDataPreview:arSchedules];
        [self.vwDetail reDraw];
        [self.vwDetail setHidden:NO];
    }
}




// call api
// ------------------------------------------------
-(void)callAPIQRCalendar{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    
    // call api
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/games/qr/eventCalendar.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    
    if(doc){
        [self storeData:doc];
    }
}

-(void)storeData:(GDataXMLDocument*)doc{
    // fetch xml
    NSArray *ar = [doc.rootElement elementsForName:@"EVENT"];
    for(int i=0; i< [ar count]; i++){
        GDataXMLElement *elm = [ar objectAtIndex:i];
//        NSString* element_id = [[[elm elementsForName:@"EVENT_ID"] objectAtIndex:0] stringValue];
        NSString* name = [[[elm elementsForName:@"NAME"] objectAtIndex:0] stringValue];
        NSString* desc = [[[elm elementsForName:@"DESC"] objectAtIndex:0] stringValue];
//        NSString* thumbs_img = [[[elm elementsForName:@"THUMBS_IMG"] objectAtIndex:0] stringValue];
        NSString* cover_img = [[[elm elementsForName:@"COVER_IMG"] objectAtIndex:0] stringValue];

        NSString* active_date = [[[elm elementsForName:@"ACTIVE_DATE"] objectAtIndex:0] stringValue];
        
        
        [self addDataValueShowDetail:active_date name:name cover:cover_img desc:desc];
        
    }
//    NSLog(@"data = %@", self.dicDate);
}

// add data detail
// ---------------------------------
-(void)addDataValueShowDetail:(NSString*)active_date name:(NSString*)name cover:(NSString*)cover desc:(NSString*)desc{
    NSArray *ar_activedate = [active_date componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *strdate = [ar_activedate objectAtIndex:0];
    NSString *strtime = [ar_activedate objectAtIndex:1];

    strtime = [strtime substringToIndex:5];
    strdate = [strdate stringByReplacingOccurrencesOfString:@"-" withString:@""];
//    NSLog(@"strdate = %@, strtime = %@", strdate, strtime);
    
    // add value program
    NSMutableDictionary *dicData = [[NSMutableDictionary alloc] init];
    [dicData setValue:name forKey:@"name"];
    [dicData setValue:cover forKey:@"cover"];
    [dicData setValue:desc forKey:@"desc"];
    [dicData setValue:strtime forKey:@"time"];
    
    NSString* key = strdate;
    // check key
    NSMutableArray *ar = [self.dicDate objectForKey:key];
    if(ar ==nil){
        ar = [[NSMutableArray alloc] init];
        [ar addObject:dicData];
        [self.dicDate setValue:ar forKey:key];
    }else{
        [ar addObject:dicData];
    }

}
// ---------------------------------
-(void)rendermaskDate{
    NSArray *arDay = [self.dicDate allKeys];
    
    for(int i=0; i< [arDay count]; i++){
        NSString* dayOnAir = [arDay objectAtIndex:i];
        
        UIButton* btn = (UIButton*)[self.view viewWithTag:[dayOnAir integerValue]];
        
//        NSLog(@"text = %@, btn.tag = %d", btn.titleLabel.text, btn.tag);
        
//        [btn setTitle:@"x" forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_mask.png"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_mask_active.png"] forState:UIControlStateNormal];
        
    }
    
}

- (IBAction) btnScanQRPress:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel checkSubscribe];
    
    if(!appDel.subscriber){
        return;
    }
    UIViewController *qrScanViewController = [[[QRScanViewController alloc] init] autorelease];
    [self presentModalViewController:qrScanViewController animated:YES];
}

// dealocate memory
// ---------------------------------
-(void)dealloc{
    [vwDetail release];
    vwDetail = nil;
    
    [dicDate release];
    dicDate = nil;
    
    
    [super dealloc];
}

//----

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
