//
//  SecondProfileViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/3/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "SecondProfileViewController.h"

#import "DBManager.h"

#import "TheStarProfile.h"

@interface SecondProfileViewController ()

@end

@implementation SecondProfileViewController
@synthesize number;
@synthesize imProfile;
@synthesize scView;
@synthesize scv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-starprofile.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
- (id)init
{
    self = [super init];
    if (self) {
        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
        [self.view setBackgroundColor:[UIColor blackColor]];
        //        self.title = @"Video Detail";
        
        [self createUI];
    }
    
    return self;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-(void)createUI{
    NSLog(@"create ui SecondProfileViewController");
    
    // scroll
    if(self.scv == nil){
        self.scv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
        self.scv.contentSize = CGSizeMake(320, 600);
        [self.scv setShowsVerticalScrollIndicator:YES];
        [self.scv setShowsHorizontalScrollIndicator:NO];
        
        [self.view addSubview:self.scv];
    }
    
    
    // immage profile
    if(imProfile==nil){
        CGRect rec = CGRectMake(0, 0, 320, 480);
        imProfile = [[UIImageView alloc] initWithFrame:rec];
        [imProfile setFrame:rec];
//        [self.view addSubview:imProfile];
        [scv addSubview:imProfile];
    }

}
// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x

-(void)viewDidAppear:(BOOL)animated{
//    [self loadDataSecondProfile];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadImageDetail];
}

// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
-(void)loadDataSecondProfile{
    NSLog(@"loadDataSecondProfile");
    DBManager *dbMgr = [[DBManager alloc] init];
    NSString *cond = [NSString stringWithFormat:@"star_id = %d", number];
    NSMutableArray *arPro = [dbMgr fetchTheStarProfile:cond];
    
    if(arPro !=nil && [arPro count] > 0){
        NSLog(@"load data completd");
        TheStarProfile *starPro = [arPro objectAtIndex:0];
        NSData *dat = starPro.cover_img_profile;
        UIImage *im = [UIImage imageWithData:dat];
        
        float ratio = 230/720.0f;
        CGRect recIm = CGRectMake(0, 0, im.size.width*ratio, im.size.height*ratio);
        
        [imProfile setImage:im];
        [imProfile setFrame:recIm];
        NSLog(@"self.scv = %@, height = %f", self.scv, im.size.height*ratio);
        [self.scv setContentSize:CGSizeMake(im.size.width*ratio, im.size.height*ratio)];
//        self.scv.contentSize = CGSizeMake(im.size.width*ratio, (im.size.height*ratio) + 10);
//        self.scv.contentSize = CGSizeMake(320, 600);
        
        
    }else{
        NSLog(@"not found data in DB");
    }
    [dbMgr release];
}

-(void)loadImageDetail{
    NSString* fileImage = [NSString stringWithFormat:@"detail-%d.jpg", self.number];
    
    [imProfile setImage:[UIImage imageNamed:fileImage]];
}

-(void)dealloc{
    
    [imProfile release];
    imProfile = nil;
    
    [scView release];
    scView = nil;
    [super dealloc];
}
@end
