//
//  GalleryDetailViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/21/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JWSlideMenuViewController.h"
#import "MWPhotoBrowser.h"
@interface GalleryDetailViewController : JWSlideMenuViewController<UIScrollViewDelegate,MWPhotoBrowserDelegate>{
    
    NSString* gallery_id;
    
    UIScrollView *scrollview;
    UIView *captureView;
    UIView *alphaView;
    UILabel *lbLyric;
    
    CGPoint beginTouch;
    CGRect beginRect;
    
    NSMutableArray *arString;
    NSMutableArray *arImage;
    
    UIActivityIndicatorView *spinner;
    int page;
    
}

@property(retain, nonatomic) NSString* gallery_id;
@property(retain, nonatomic) UIView *alphaView;
@property(retain, nonatomic) UIScrollView *scrollview;
@property(retain, nonatomic)UILabel *lbLyric;
@property(retain, nonatomic)NSMutableArray *arString;
@property(retain, nonatomic)NSMutableArray *arImage;

@property(retain, nonatomic)UIActivityIndicatorView *spinner;
@property(retain, nonatomic) NSDictionary *galleryDic;
@property (retain, nonatomic) IBOutlet UILabel *lblHeadline;
@property (retain, nonatomic) IBOutlet UILabel *lblCaption;
@property (retain, nonatomic) IBOutlet UILabel *lblDate;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) NSMutableArray *photos;
@end
