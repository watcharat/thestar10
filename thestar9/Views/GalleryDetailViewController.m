//
//  GalleryDetailViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/21/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "GalleryDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "MWPhotoBrowser.h"
#import "AppDelegate.h"
#import "GDataXMLNode.h"

@interface GalleryDetailViewController ()
{
    NSArray* imageItem;
    
    int pointX;
    int pointY;
    int imageCount;
}
@end

@implementation GalleryDetailViewController
@synthesize gallery_id;
@synthesize scrollview;
@synthesize alphaView;
@synthesize lbLyric;
@synthesize photos;
@synthesize spinner;
@synthesize arString;
@synthesize arImage;
@synthesize galleryDic;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-gallery.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    [self createButtonSave];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)init
{
    self = [super init];
    if (self) {
        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
        [self.view setBackgroundColor:[UIColor blackColor]];
        self.title = @"";
//        [self createSpinner];
        [self createUI];
    }
    
    
    return self;
}

//create button save image
-(void)createButtonSave{
    NSLog(@"createButtonSave");
    
    // button back
    UIImage *image = [UIImage imageNamed:@"btn_subsave.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(btnSaveImagePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
}

// action when click save button
-(IBAction)btnSaveImagePressed:(id)sender{
    UIImage *img = [self.arImage objectAtIndex:page];
    UIImageWriteToSavedPhotosAlbum(img, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

//save error method
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo
{
    // Was there an error?
    if (error != NULL)
    {
        // Show error message...
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"การบันทึก" message:@"ไม่สารมารถบันทึกรูปภาพได้ กรุณาลองใหม่อีกครั้ง" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
        
    }
    else  // No errors
    {
        // Show message image successfully saved
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"การบันทึก" message:@"บันทึกรูปภาพสำเร็จแล้ว" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}

// view did appear when
-(void)viewDidAppear:(BOOL)animated{
    
    [self createUI];
    [self storePhoto];
    //    [self callGalleryDetailAPI];
    //    [spinner stopAnimating];
    //    [spinner setHidden:YES];
}


-(void)createSpinner{
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.center = CGPointMake(160, 240);
    spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    [spinner startAnimating];
}

-(void)createUI{
    
    _lblHeadline.text = [galleryDic valueForKey:@"headline"];
    _lblCaption.text =  [galleryDic valueForKey:@"caption"];
    _lblDate.text = [NSString stringWithFormat:@"Posted : %@",[galleryDic valueForKey:@"releaseDate"]];
    
    NSString* myText =[galleryDic valueForKey:@"desc"];
    //    @"dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq dafaq ffff";
    
    //SET THE WIDTH CONSTRAINTS FOR LABEL.
    CGFloat constrainedWidth = 300.0f;//YOU CAN PUT YOUR DESIRED ONE,THE MAXIMUM WIDTH OF YOUR LABEL.
    //CALCULATE THE SPACE FOR THE TEXT SPECIFIED.
    CGSize sizeOfText=[myText sizeWithFont:[UIFont systemFontOfSize:(18.0)] constrainedToSize:CGSizeMake(constrainedWidth, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];
    
    UILabel *messageLabel=[[UILabel alloc] initWithFrame:CGRectMake(10,60,constrainedWidth,sizeOfText.height)];
    messageLabel.font = [UIFont systemFontOfSize:15];
    messageLabel.text=myText;
    messageLabel.numberOfLines=0;//JUST TO SUPPORT MULTILINING.
    
    pointY =sizeOfText.height+60;
    NSLog(@"SIZE %f",sizeOfText.height);
    [self.scrollView addSubview:messageLabel];
    
    [messageLabel release];
    
    imageItem = [galleryDic valueForKey:@"galleryDetails"];
    
    NSLog(@"IMAGE ARRAY SIZE %d ",[imageItem count]);
    
    pointX = 15;
    
    if([imageItem count]>0&&[imageItem count]<10){
        imageCount = [imageItem count];
    }else{
        imageCount = 10;
    }
    
    NSLog(@"imageCount %d",imageCount);
    for (int i=0; i<imageCount; i++) {
        
        NSLog(@"%d %d %d",i,pointX,pointY);
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(pointX, pointY, 140, 140)];
        [image setImageWithURL:[NSURL URLWithString:[[imageItem objectAtIndex:i ] valueForKey:@"thumbImgDetail"]]];
        [image setTag:i];
        
        [image setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(viewImage:)];
        tap.numberOfTapsRequired = 1;
        [image addGestureRecognizer:tap];
        
        [_scrollView addSubview:image];
        
        if(i%2==0){
            pointX = 160;
        }else{
            pointY += 150;
            pointX= 15;
            
        }
        
        [image release];

    }
    //    pointY+= 150;
    [_scrollView setContentSize:CGSizeMake(320,pointY+10)];
    
}
-(void)loadMoreImage{
    
    int start = imageCount;
    if([imageItem count]<=imageCount){
        imageCount = [imageItem count];
    }else{
        imageCount+=10;
        if([imageItem count]<=imageCount)imageCount=[imageItem count];
    }
    //    pointX = 15;
    NSLog(@"imageCount %d",imageCount);
    
    for (int i=start; i<imageCount; i++) {
        NSLog(@"%d %d %d",i,pointX,pointY);
        
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(pointX, pointY, 140, 140)];
        [image setImageWithURL:[NSURL URLWithString:[[imageItem objectAtIndex:i ] valueForKey:@"thumbImgDetail"]]];
        [image setTag:i];
        
        [image setUserInteractionEnabled:YES];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(viewImage:)];
        tap.numberOfTapsRequired = 1;
        [image addGestureRecognizer:tap];
        
        [_scrollView addSubview:image];
        
        
        if(i%2==0){
            pointX = 160;
        }else{
            pointY += 150;
            pointX= 15;
            
        }
        
        [image release];
        
    }
    int lastPoint=pointY;
    if(imageCount == [imageItem count]){
        if (imageCount%2!=0) {
            lastPoint =  pointY+160;
            NSLog(@"last point %d ",lastPoint);
        }
    }
    
    [_scrollView setContentSize:CGSizeMake(320,lastPoint+20)];
    
}
-(void)storePhoto{
    
    NSMutableArray *photo = [[NSMutableArray alloc] init];
    for (int i=0; i<[imageItem count]; i++) {
        
        [photo addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[imageItem objectAtIndex:i ] valueForKey:@"thumbImgDetail"]]]];
    }

    self.photos = photo;
    [photo release];
}

-(IBAction)viewImage:(id)sender{
    
    UIView *view = [sender view]; //cast pointer to the derived class if needed
    NSLog(@"SENDER %d",[view tag]);
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.displayNavArrows = NO;
    browser.wantsFullScreenLayout = YES;
    browser.zoomPhotosToFill = NO;
    [browser setCurrentPhotoIndex:[view tag]];
    
    // Modal
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:nc animated:YES];
    [browser release];
}


-(void)scrollViewDidScroll:(UIScrollView *)sender {
    
    if (_scrollView.contentOffset.y == roundf(_scrollView.contentSize.height-_scrollView.frame.size.height)) {
        NSLog(@"we are at the endddd");
        NSLog(@"LAST X Y %d %d",pointX,pointY);
        [self loadMoreImage];
        //Call your function here...
        
    }
    
}

-(void)updateNewPosTheScroll{
    //    NSLog(@"page value = %d", page);
    
    CGRect frame;
    int frameWidth = 320;
    frame.origin.x = frameWidth * page;
    frame.origin.y = 0;
    frame.size = self.scrollview.frame.size;
    [self.scrollview scrollRectToVisible:frame animated:YES];
    
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

//- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
//    MWPhoto *photo = [self.photos objectAtIndex:index];
//    MWCaptionView *captionView = [[MWCaptionView alloc] initWithPhoto:photo];
//    return [captionView autorelease];
//}

//- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"ACTION!");
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %i", index);
}


// clear memory
-(void)dealloc{
    [lbLyric release];
    [arString release];
    [scrollview release];
    [alphaView release];
    [spinner release];
    [_lblHeadline release];
    [_lblCaption release];
    [_lblDate release];
    [_scrollView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setLblHeadline:nil];
    [self setLblCaption:nil];
    [self setLblDate:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}
@end
