//
//  VideoCell.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/17/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *imCover;
@property (retain, nonatomic) IBOutlet UILabel *lbDate;
@property (retain, nonatomic) IBOutlet UILabel *lbTitle;

@end
