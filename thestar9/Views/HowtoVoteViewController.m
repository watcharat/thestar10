//
//  HowtoVoteViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/23/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "HowtoVoteViewController.h"

@interface HowtoVoteViewController ()

@end

@implementation HowtoVoteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // set title image
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-howto.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(id)init{
    self = [super init];
    if (self) {
        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
        [self.view setBackgroundColor:[UIColor blackColor]];
//        self.title = @"How To Vote";
        
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UIImageView *ivHowto = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"content_p.png"]];
    [ivHowto setFrame:CGRectMake((320-302)/2, (420-362)/2, 302, 362)];
    [self.view addSubview:ivHowto];
    
    
}

@end
