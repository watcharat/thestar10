//
//  VoteViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "VoteViewController.h"

#import "AppDelegate.h"
#import "GDataXMLNode.h"

#import "DBManager.h"
#import "Vote.h"

#import "HowtoVoteViewController.h"

@interface VoteViewController ()

@end

@implementation VoteViewController

@synthesize btnVote1;
@synthesize btnVote2;
@synthesize btnVote3;
@synthesize btnVote4;
@synthesize btnVote5;
@synthesize btnVote6;
@synthesize btnVote7;
@synthesize btnVote8;

@synthesize arData;
@synthesize availableVote;
@synthesize imBanner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // set title image
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-vote.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    // load data
    [self loadBanner];
}

-(void)loadBanner{
    // load banner vote
    
    NSLog(@"loadBanner");
    NSString* urlPath = @"http://thestar9.gmmwireless.com/thestar9/image/baner/vote/640x100.jpg";
    NSData* dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPath]];
    [imBanner setImage:[UIImage imageWithData:dat]];
}

-(void)viewDidAppear:(BOOL)animated{
    
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(!appDel.fakeApple){
//        [self reloadData];
    }
    
    
}

-(void)reloadDataForVote{
    NSLog(@"reloadDataForVote");
    
    // send sms
    self.availableVote = false;
    [self loadListTheStarAPI];      // use for flg availableVote
    
    if(self.availableVote){
        [self loadVoteAPI];
    }
    
    [self loadThumbDBtoRender];
    [self renderFlgVoteData];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x

- (IBAction)btnVote1Pressed:(id)sender {
    
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [self showMessageFakeThanks];
        return;
    }
    
    //send sms
    MFMessageComposeViewController *smsCon = [[MFMessageComposeViewController alloc] init];
    if(smsCon == nil){
        return;
    }
    smsCon.body = @"*1_i";  // ข้อความที่จะส่ง
    NSString *tel = @"4242899";  // เบอร์โทรที่จะส่ง
//    NSString *tel = @"0858039803";  // test number of get
    smsCon.recipients = [NSArray arrayWithObjects:tel, nil];
    smsCon.messageComposeDelegate = self;
    [self presentViewController:smsCon animated:YES completion:nil];
    
    [smsCon release];
    
}
- (IBAction)btnVote2Pressed:(id)sender {
    
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [self showMessageFakeThanks];
        return;
    }
    
    // send sms
    MFMessageComposeViewController *smsCon = [[MFMessageComposeViewController alloc] init];
    if(smsCon == nil){
        return;
    }
    
    smsCon.body = @"*2_i";  // ข้อความที่จะส่ง
    NSString *tel = @"4242899";  // เบอร์โทรที่จะส่ง
    smsCon.recipients = [NSArray arrayWithObjects:tel, nil];
    smsCon.messageComposeDelegate = self;
    [self presentViewController:smsCon animated:YES completion:nil];
    
    [smsCon release];

}
- (IBAction)btnVote3Pressed:(id)sender {
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [self showMessageFakeThanks];
        return;
    }
    
    // send sms
    MFMessageComposeViewController *smsCon = [[MFMessageComposeViewController alloc] init];
    if(smsCon == nil){
        return;
    }
    
    smsCon.body = @"*3_i";  // ข้อความที่จะส่ง
    NSString *tel = @"4242899";  // เบอร์โทรที่จะส่ง
    smsCon.recipients = [NSArray arrayWithObjects:tel, nil];
    smsCon.messageComposeDelegate = self;
    [self presentViewController:smsCon animated:YES completion:nil];
    
    [smsCon release];

}
- (IBAction)btnVote4Pressed:(id)sender {
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [self showMessageFakeThanks];
        return;
    }
    
    // send sms
    
    MFMessageComposeViewController *smsCon = [[MFMessageComposeViewController alloc] init];
    if(smsCon == nil){
        return;
    }
    smsCon.body = @"*4_i";  // ข้อความที่จะส่ง
    NSString *tel = @"4242899";  // เบอร์โทรที่จะส่ง
    smsCon.recipients = [NSArray arrayWithObjects:tel, nil];
    smsCon.messageComposeDelegate = self;
    [self presentViewController:smsCon animated:YES completion:nil];
    
    [smsCon release];

}

- (IBAction)btnVote5Pressed:(id)sender {
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [self showMessageFakeThanks];
        return;
    }
    
    // send sms
    MFMessageComposeViewController *smsCon = [[MFMessageComposeViewController alloc] init];
    if(smsCon == nil){
        return;
    }
    smsCon.body = @"*5_i";  // ข้อความที่จะส่ง
    NSString *tel = @"4242899";  // เบอร์โทรที่จะส่ง
    smsCon.recipients = [NSArray arrayWithObjects:tel, nil];
    smsCon.messageComposeDelegate = self;
    [self presentViewController:smsCon animated:YES completion:nil];
    
    [smsCon release];

}
- (IBAction)btnVote6Pressed:(id)sender {
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [self showMessageFakeThanks];
        return;
    }
    
    // send sms
    MFMessageComposeViewController *smsCon = [[MFMessageComposeViewController alloc] init];
    if(smsCon == nil){
        return;
    }
    smsCon.body = @"*6_i";  // ข้อความที่จะส่ง
    NSString *tel = @"4242899";  // เบอร์โทรที่จะส่ง
    smsCon.recipients = [NSArray arrayWithObjects:tel, nil];
    smsCon.messageComposeDelegate = self;
    [self presentViewController:smsCon animated:YES completion:nil];
    
    [smsCon release];

}

- (IBAction)btnVote7Pressed:(id)sender {
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [self showMessageFakeThanks];
        return;
    }
    
    // send sms
    MFMessageComposeViewController *smsCon = [[MFMessageComposeViewController alloc] init];
    if(smsCon == nil){
        return;
    }
    smsCon.body = @"*7_i";  // ข้อความที่จะส่ง
    NSString *tel = @"4242899";  // เบอร์โทรที่จะส่ง
    smsCon.recipients = [NSArray arrayWithObjects:tel, nil];
    smsCon.messageComposeDelegate = self;
    [self presentViewController:smsCon animated:YES completion:nil];
    
    [smsCon release];

}

- (IBAction)btnVote8Pressed:(id)sender {
    //check fakeApple
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [self showMessageFakeThanks];
        return;
    }
    
    // send sms
    MFMessageComposeViewController *smsCon = [[MFMessageComposeViewController alloc] init];
    if(smsCon == nil){
        return;
    }
    smsCon.body = @"*8_i";  // ข้อความที่จะส่ง
    NSString *tel = @"4242899";  // เบอร์โทรที่จะส่ง
    smsCon.recipients = [NSArray arrayWithObjects:tel, nil];
    smsCon.messageComposeDelegate = self;
    [self presentViewController:smsCon animated:YES completion:nil];
    
    [smsCon release];

}


// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
-(void)loadVoteAPI{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/theStarVoteList.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){
        /*
         //test out put
         NSArray *ar = [doc.rootElement elementsForName:@"STATUS"];
         GDataXMLElement *status = [ar objectAtIndex:0];
         NSLog(@"STATUS = %@", [status stringValue]);
         */
        
        // render data by xml
        [self storeVoteData:doc];
        
    }else{
        NSLog(@"[XML ERROR] : %@", [error description] );
        
    }
    
    [data release];
    [doc release];
    [url release];
    
}

-(void)storeVoteData:(GDataXMLDocument*)doc{
    NSArray *ar = [doc.rootElement elementsForName:@"PROFILE"];
    for(int i=0; i< [ar count]; i++){
        GDataXMLElement *elm = [ar objectAtIndex:i];
        
        // xml value
        NSString* flgY = [[[elm elementsForName:@"IS_ACTIVE"] objectAtIndex:0] stringValue];
        
        // dat value
        NSString* thumb_path = [[[elm elementsForName:@"THUMBS_IMG"] objectAtIndex:0] stringValue];
        NSString* lastMod = [[[elm elementsForName:@"LAST_MODIFIED_DATE"] objectAtIndex:0] stringValue];
        
        DBManager *dbMgr = [[DBManager alloc] init];
        
        NSString* star_id = [NSString stringWithFormat:@"%d", i+1];
        NSString* cond = [NSString stringWithFormat:@"star_id = %d", i+1];
        
        // fetch check per data
        NSMutableArray *ar = [dbMgr fetchVote:cond];
        if(ar != nil && [ar count]){
            
            // update flg
            Vote* vote = [ar objectAtIndex:0];
            
            if([flgY isEqualToString:@"Y"]){
                vote.active = @"Y";
            }else{
                vote.active = @"N";
            }
            
            // check update image
            if(![lastMod isEqualToString:vote.updated]){
                NSLog(@"update image");
                NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumb_path]];
                vote.thumb_img = dat;
                
                vote.updated = lastMod;
            }
            
            
            [dbMgr updateVote:vote];
        }else{
            // insert flg
            Vote* vote = [dbMgr getSchemaVote];
            
            vote.star_id = star_id;
            if([flgY isEqualToString:@"Y"]){
                vote.active = @"Y";
            }else{
                vote.active = @"N";
            }
            
            // insert image
            NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumb_path]];
            vote.thumb_img = dat;
            vote.updated = lastMod;
            [dbMgr insertVote:vote];
        }
        [dbMgr release];
    }
}

// ------
-(void)renderFlgVoteData{
    DBManager *dbMgr = [[DBManager alloc] init];
    
    NSMutableArray *ar = [dbMgr fetchVote:@""];
    for(int i=0; i< [ar count]; i++){
        Vote* vote = [ar objectAtIndex:i];
        
        if(![vote.active isEqualToString:@"Y"]){
            // cannot click (enable = false)
            if(i==0){
                [btnVote1 setEnabled:NO];
            }else if(i==1){
                [btnVote2 setEnabled:NO];
            }else if(i==2){
                [btnVote3 setEnabled:NO];
            }else if(i==3){
                [btnVote4 setEnabled:NO];
            }else if(i==4){
                [btnVote5 setEnabled:NO];
            }else if(i==5){
                [btnVote6 setEnabled:NO];
            }else if(i==6){
                [btnVote7 setEnabled:NO];
            }else if(i==7){
                [btnVote8 setEnabled:NO];
            }
        }
    }
    
}

// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
-(void)loadThumbDBtoRender{
    int theStarVoteAll = 8;
    for(int i=0; i< theStarVoteAll; i++){
        
        DBManager *dbMgr = [[DBManager alloc] init];
        NSString* cond = [NSString stringWithFormat:@"star_id = %d", i+1];
        NSMutableArray *ar = [dbMgr fetchVote:cond];
        
        if(ar !=nil && [ar count] > 0){
            Vote* vote = [ar objectAtIndex:0];
            NSData *dat = vote.thumb_img;
            if(dat !=nil){
                if(i==0){
                    [btnVote1 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
                }else if(i==1){
                    [btnVote2 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
                }else if(i==2){
                    [btnVote3 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
                }else if(i==3){
                    [btnVote4 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
                }else if(i==4){
                    [btnVote5 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
                }else if(i==5){
                    [btnVote6 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
                }else if(i==6){
                    [btnVote7 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
                }else if(i==7){
                    [btnVote8 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
                }
            }
        }
        
        [dbMgr release];
    }
    
    /*
    DBManager *dbMgr = [[DBManager alloc] init];
    NSMutableArray *ar = [dbMgr fetchVote:@""];
    
    for(int i=0; i< [ar count]; i++){
        
        Vote *vote = [ar objectAtIndex:i];
        
        NSData *dat = vote.thumb_img;
        
        if(dat !=nil){
        
            if(i==0){
                [btnVote1 setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
            }else if(i==1){
                [btnVote2 setEnabled:NO];
            }else if(i==2){
                [btnVote3 setEnabled:NO];
            }else if(i==3){
                [btnVote4 setEnabled:NO];
            }else if(i==4){
                [btnVote5 setEnabled:NO];
            }else if(i==5){
                [btnVote6 setEnabled:NO];
            }else if(i==6){
                [btnVote7 setEnabled:NO];
            }else if(i==7){
                [btnVote8 setEnabled:NO];
            }
        }
    }
    [dbMgr release];
     */
    
    
}
// --- test
-(void)loadListTheStarAPI{
    // call api
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/theStarProfile.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){
        /*
         //test out put
         NSArray *ar = [doc.rootElement elementsForName:@"STATUS"];
         GDataXMLElement *status = [ar objectAtIndex:0];
         NSLog(@"STATUS = %@", [status stringValue]);
         */
        
        // render data by xml
        [self renderTheStarData:doc];
        
    }else{
        NSLog(@"[XML ERROR] : %@", [error description] );
    }
    
    [data release];
    [doc release];
    [url release];
}


-(void)renderTheStarData:(GDataXMLDocument*)doc{
    NSArray *ar = [doc.rootElement elementsForName:@"PROFILE"];
    NSLog(@"count list profile =  %d", [ar count]);
    
    // data collection
    
    if(arData !=nil){
        [arData removeAllObjects];
    }else{
        arData = [[NSMutableArray alloc] init];
    }
    
    BOOL flgAllYes = true;
    for(int i=0; i< [ar count]; i++){
        
        GDataXMLElement *elm = [ar objectAtIndex:i];
        NSString* star_id = [[[elm elementsForName:@"STAR_ID"] objectAtIndex:0] stringValue];
        NSString* name = [[[elm elementsForName:@"NAME"] objectAtIndex:0] stringValue];
        NSString* desc = [[[elm elementsForName:@"DESC"] objectAtIndex:0] stringValue];
        NSString* last_modified_date = [[[elm elementsForName:@"LAST_MODIFIED_DATE"] objectAtIndex:0] stringValue];
        
        NSString* thumbs_img = [[[elm elementsForName:@"THUMBS_IMG"] objectAtIndex:0] stringValue];
        NSString* cover_img = [[[elm elementsForName:@"COVER_IMG"] objectAtIndex:0] stringValue];
        NSString* cover_img_profile = [[[elm elementsForName:@"COVER_IMG_PROFILE"] objectAtIndex:0] stringValue];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        
        [dic setValue:star_id forKey:@"STAR_ID"];
        [dic setValue:name forKey:@"NAME"];
        [dic setValue:desc forKey:@"DESC"];
        [dic setValue:last_modified_date forKey:@"LAST_MODIFIED_DATE"];
        
        [dic setValue:thumbs_img forKey:@"THUMBS_IMG"];
        [dic setValue:cover_img forKey:@"COVER_IMG"];
        [dic setValue:cover_img_profile forKey:@"COVER_IMG_PROFILE"];
        
        
        [arData addObject:dic];
        
        
        //
        NSString* is_active = [[[elm elementsForName:@"IS_ACTIVE"] objectAtIndex:0] stringValue];
        if([is_active isEqualToString:@"N"]){
            flgAllYes = false;
        }
    }
    
    // flag available
    if(flgAllYes){
        
        self.availableVote = true;
        // update thumb
        /*
        for(int i=0; i<[arData count]; i++){
            NSMutableDictionary* dic = [arData objectAtIndex:i];
            
            BOOL updated = [self updateProfile:dic];
         
//            if(updated){
//                [self updateThumbVote:dic];
//            }
            
        }
         */
    }
}


// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x

-(BOOL)updateProfile:(NSMutableDictionary*)dic{
    
    // update thumbFile
    
    // get data
    NSString* updated = [dic objectForKey:@"LAST_MODIFIED_DATE"];
    NSString* star_id = [dic objectForKey:@"STAR_ID"];
    NSString* name = [dic objectForKey:@"NAME"];
    NSString* desc = [dic objectForKey:@"DESC"];
    
    NSString* thumb_cover = [dic objectForKey:@"THUMBS_IMG"];
    NSString* cover_img = [dic objectForKey:@"COVER_IMG"];
    NSString* cover_img_profile = [dic objectForKey:@"COVER_IMG_PROFILE"];
    
    
    // database manager
    DBManager *dbMgr = [[DBManager alloc] init];
    NSString *cond = [NSString stringWithFormat:@"star_id = %@", star_id];
    NSMutableArray* ar = [dbMgr fetchTheStarProfile:cond];
    
    
    // check found database
    if(ar !=nil &&[ar count] > 0){
        
        // found to update
        // --------------------
        TheStarProfile *starPro = [ar objectAtIndex:0];
        
        // check update string
        if([starPro.updated isEqualToString:updated]){
            return FALSE;
        }
        
        starPro.star_id = star_id;
        starPro.updated = updated;
        starPro.desc = desc;
        starPro.name = name;
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        starPro.thumb_img = datThub;
        
        NSData *datImg = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cover_img]];
        starPro.cover_img = datImg;
        
        NSData *datImgProfile = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cover_img_profile]];
        starPro.cover_img_profile = datImgProfile;
        
        [dbMgr updateTheStarProfile:starPro];
        
        NSLog(@"update");
        
    }else{
        // insert new
        TheStarProfile *starPro = [dbMgr getSchemaTheStarProfile];
        starPro.star_id = star_id;
        starPro.updated = updated;
        starPro.desc = desc;
        starPro.name = name;
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        starPro.thumb_img = datThub;
        
        NSData *datImg = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cover_img]];
        starPro.cover_img = datImg;
        
        NSData *datImgProfile = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cover_img_profile]];
        starPro.cover_img_profile = datImgProfile;
        
        [dbMgr insertTheStarProfile:starPro];
        NSLog(@"insert");
    }
    
    [dbMgr release];
    
    return TRUE;
}
// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
-(void)updateThumbVote:(NSMutableDictionary*)dic{
    // get data
    
    NSString* star_id = [dic objectForKey:@"STAR_ID"];
    NSString* thumb_cover = [dic objectForKey:@"THUMBS_IMG"];
    
    // database manager
    DBManager *dbMgr = [[DBManager alloc] init];
    NSString *cond = [NSString stringWithFormat:@"star_id = %@", star_id];
    NSMutableArray* ar = [dbMgr fetchVote:cond];
    
    
    
    // check found database
    if(ar !=nil &&[ar count] > 0){
        
        // found to update
        // --------------------
        Vote *vote = [ar objectAtIndex:0];
        vote.star_id = star_id;
        
        
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        
        vote.thumb_img = datThub;
        vote.active = @"Y";
        [dbMgr updateVote:vote];
        
        NSLog(@"update thumb load = star_id=%@, link = %@", star_id, thumb_cover);
        
    }else{
        // insert new
        Vote *vote = [dbMgr getSchemaVote];
        vote.star_id = star_id;
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        vote.thumb_img = datThub;
        vote.active = @"Y";
        [dbMgr insertVote:vote];
        NSLog(@"insert vote");
        
        NSLog(@"insert thumb load = star_id=%@, link = %@", star_id, thumb_cover);
    }
    
    [dbMgr release];
}

// update thumb vote
-(void)udpateThumbVote:(NSString*)star_id dat:(NSData*)dat flgShow:(BOOL)flgShow updated:(NSString*)updated{
    DBManager *dbMgr = [[DBManager alloc] init];
    
    NSString* cond = [NSString stringWithFormat:@"star_id = %@", star_id];
    NSMutableArray *ar = [dbMgr fetchVote:cond];
    if(ar !=nil && [ar count] > 0){
        // update
        Vote* vote = [ar objectAtIndex:0];
        vote.star_id = star_id;
        vote.updated = updated;
        vote.thumb_img = dat;
        
        [dbMgr updateVote:vote];
    }else{
        // insert
        Vote* vote = [dbMgr getSchemaVote];
        vote.star_id = star_id;
        vote.updated = updated;
        vote.thumb_img = dat;
        
        [dbMgr insertVote:vote];
        
    }
    
    
    
    [dbMgr release];
}


// -------------------------------------
// Delegate send mail
// -------------------------------------
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"กดปุ่มยกเลิก");
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
            
        case MessageComposeResultFailed:
            NSLog(@"การส่งผิดพลาด");
            break;
            
        case MessageComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"ส่ง message เรียบร้อยแล้วค่ะ" delegate:nil cancelButtonTitle:@"ตกลง" otherButtonTitles: nil];
            
            [alert show];
            [alert release];
            
            [controller dismissViewControllerAnimated:YES completion:nil];
            
            break;
        }
        default:
            break;
    }   
}

// void how to vote
- (IBAction)btnHowTo:(id)sender {
    HowtoVoteViewController *howto = [[HowtoVoteViewController alloc] init];
    [self.navigationController pushViewController:howto];
    [howto release];
    
}




// for fake message 
-(void)showMessageFakeThanks{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"ส่ง message เรียบร้อยแล้วค่ะ" delegate:nil cancelButtonTitle:@"ตกลง" otherButtonTitles: nil];
    
    [alert show];
    [alert release];

}

- (void)dealloc {
    [btnVote1 release];
    [btnVote2 release];
    [btnVote3 release];
    [btnVote4 release];
    [btnVote5 release];
    [btnVote6 release];
    [btnVote7 release];
    [btnVote8 release];
    [imBanner release];
    [super dealloc];
}
- (void)viewDidUnload {
    [btnVote1 release];
    btnVote1 = nil;
    [self setBtnVote1:nil];
    [btnVote2 release];
    btnVote2 = nil;
    [self setBtnVote2:nil];
    [btnVote3 release];
    btnVote3 = nil;
    [self setBtnVote3:nil];
    [btnVote4 release];
    btnVote4 = nil;
    [self setBtnVote4:nil];
    [btnVote5 release];
    btnVote5 = nil;
    [self setBtnVote5:nil];
    [btnVote6 release];
    btnVote6 = nil;
    [self setBtnVote6:nil];
    [btnVote7 release];
    btnVote7 = nil;
    [self setBtnVote7:nil];
    [btnVote8 release];
    btnVote8 = nil;
    [self setBtnVote8:nil];
    [imBanner release];
    imBanner = nil;
    [self setImBanner:nil];
    [super viewDidUnload];
}
@end
