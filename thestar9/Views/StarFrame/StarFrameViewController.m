//
//  StarFrameViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/27/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "StarFrameViewController.h"
#import "StarFrameHowtoViewController.h"
#import "StarFrameListFrameViewController.h"
#import "AppDelegate.h"

@interface StarFrameViewController ()

@end

@implementation StarFrameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnPlayPress:(id)sender {
    NSLog(@"btnPlayPress press action");
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel checkSubscribe];
    
    if(appDel.subscriber){
        StarFrameListFrameViewController *starFrameListFrameViewController = [[StarFrameListFrameViewController alloc] init];
        starFrameListFrameViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentModalViewController:starFrameListFrameViewController animated:NO];
    }
}

- (IBAction)btnHowtoPress:(id)sender {
    NSLog(@"btnHowtoPress press action");
    StarFrameHowtoViewController *starFrameHowto = [[StarFrameHowtoViewController alloc] init];
    starFrameHowto.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:starFrameHowto animated:NO];
    [starFrameHowto release];
}

- (IBAction)btnExitPress:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{}];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
