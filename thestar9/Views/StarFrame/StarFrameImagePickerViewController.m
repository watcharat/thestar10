//
//  StarFrameImagePickerViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/28/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "StarFrameImagePickerViewController.h"
#import "StarFramePreviewViewController.h"

@interface StarFrameImagePickerViewController ()

@end

@implementation StarFrameImagePickerViewController

@synthesize scrollView;
@synthesize frameSelectedImageView;
@synthesize selectedImageView;
@synthesize canvas;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [scrollView setContentOffset:CGPointMake(0, 0)];
    [self renderData];
    [self hideFrame];
}

- (id) init {
    self = [super init];
    
    if(self) {
        
        CGRect masterRect = CGRectMake(0, 0, 320, 480);
        CGRect menuBarFrame = CGRectMake(0.0, masterRect.size.height-85, masterRect.size.width, 66);
        CGRect frameBarFrame = CGRectMake(0.0, masterRect.size.height-85-103, masterRect.size.width, 103);
        
        self.view = [[[UIView alloc] initWithFrame:masterRect] autorelease];
        //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sf_camera_frame.png"]]];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIImage *frameBackgroundImage = [UIImage imageNamed:@"sf_camera_frame.png"];
        UIImageView *frameBackgroundImageView = [[UIImageView alloc] initWithImage:frameBackgroundImage];
        frameBackgroundImageView.frame = CGRectMake(0, -20, 320, 480);;
        [self.view addSubview:frameBackgroundImageView];
        [frameBackgroundImageView release];
        
        
        // TODO :: create bottom bar
        UIImage *menuBarImg = [UIImage imageNamed:@"sf_menu_bar.png"];
        UIView *menuBarView = [[UIView alloc] initWithFrame:menuBarFrame];
        [menuBarView setBackgroundColor:[UIColor colorWithPatternImage:menuBarImg]];
        //menuBarView.alpha = 0.9;
        
        
        // TODO :: create take button
        UIButton *takeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [takeBtn setFrame:CGRectMake((menuBarView.frame.size.width/2)-(122/2), (menuBarFrame.size.height - 48) / 2, 122, 48)];
        [takeBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_retake.png"] forState:UIControlStateNormal];
        [takeBtn addTarget:self action:@selector(takeBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:takeBtn];
        
        // TODO :: create back button
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setFrame:CGRectMake(30, (menuBarFrame.size.height - 40) / 2, 40, 40)];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_back.png"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:backBtn];
        
        // TODO :: create frame button
        UIButton *frameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [frameBtn setFrame:CGRectMake(320-40-30, (menuBarFrame.size.height - 40) / 2, 40, 40)];
        [frameBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_frame.png"] forState:UIControlStateNormal];
        [frameBtn addTarget:self action:@selector(frameBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:frameBtn];
        
        [self.view addSubview:menuBarView];
        [menuBarView release];
        
        if(canvas == nil){
            canvas = [[UIView alloc] initWithFrame:CGRectMake(48, 35, 223, 335)];
        }
        
        // TODO :: create selected image layer
        if(selectedImageView == nil){
            selectedImageView = [[UIImageView alloc] init];
            selectedImageView.frame = CGRectMake(0, 0, 223, 335);
            selectedImageView.userInteractionEnabled = YES;
            [canvas addSubview:selectedImageView];
        }
        
        // TODO :: create frame layer
        if(frameSelectedImageView == nil) {
            frameSelectedImageView = [[UIImageView alloc] init];
            frameSelectedImageView.frame = CGRectMake(0, 0, 223, 335);
            [canvas addSubview:frameSelectedImageView];
            //[self.view insertSubview:frameSelectedImageView aboveSubview:selectedImageView];
        }
        
        [self.view addSubview:canvas];
        
        // TODO :: create scroll view
        if(scrollView == nil){
            scrollView = [[UIScrollView alloc] initWithFrame:frameBarFrame];
            scrollView.contentSize = CGSizeMake(1000, 103);
            scrollView.delegate = self;
            [scrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sf_bg_frame.png"]]];
            [scrollView setShowsVerticalScrollIndicator:NO];
            scrollView.alpha = 1.0;
            [self.view addSubview:scrollView];
        }
        
        UIPinchGestureRecognizer *pinchRecognizer = [[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)] autorelease];
        [pinchRecognizer setDelegate:self];
        
        [canvas addGestureRecognizer:pinchRecognizer];
        
        UIPanGestureRecognizer *panRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)] autorelease];
        [panRecognizer setMinimumNumberOfTouches:1];
        [panRecognizer setMaximumNumberOfTouches:1];
        [panRecognizer setDelegate:self];
        [canvas addGestureRecognizer:panRecognizer];
        
        UIRotationGestureRecognizer *rotationRecognizer = [[[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotate:)] autorelease];
        [rotationRecognizer setDelegate:self];
        [canvas addGestureRecognizer:rotationRecognizer];
        
        [self.view sendSubviewToBack:canvas];
        
    }
    
    return self;
}

- (void) renderData {
    NSMutableArray *frameImageArray = [[NSMutableArray alloc] init];
    [frameImageArray addObject:@"st_frame_no1_01.png"];
    [frameImageArray addObject:@"st_frame_no1_02.png"];
    [frameImageArray addObject:@"st_frame_no2_01.png"];
    [frameImageArray addObject:@"st_frame_no2_02.png"];
    [frameImageArray addObject:@"st_frame_no3_01.png"];
    [frameImageArray addObject:@"st_frame_no3_02.png"];
    [frameImageArray addObject:@"st_frame_no4_01.png"];
    [frameImageArray addObject:@"st_frame_no4_02.png"];
    [frameImageArray addObject:@"st_frame_no5_01.png"];
    [frameImageArray addObject:@"st_frame_no5_02.png"];
    [frameImageArray addObject:@"st_frame_no6_01.png"];
    [frameImageArray addObject:@"st_frame_no6_02.png"];
    [frameImageArray addObject:@"st_frame_no7_01.png"];
    [frameImageArray addObject:@"st_frame_no7_02.png"];
    [frameImageArray addObject:@"st_frame_no8_01.png"];
    [frameImageArray addObject:@"st_frame_no8_02.png"];
    [frameImageArray addObject:@"sf_default.png"];
    
    int viewWidh = 0;
    
    for (int i=0; i<frameImageArray.count; i++) {
        NSString *frameImage = [frameImageArray objectAtIndex:i];
        UIButton *btnFrame = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnFrame setFrame:CGRectMake((60*i)+((i+1)*20), 10, 60, 89)];
        [btnFrame setBackgroundImage:[UIImage imageNamed:frameImage] forState:UIControlStateNormal];
        [btnFrame addTarget:self action:@selector(btnFramePress:) forControlEvents:UIControlEventTouchUpInside];
        [[btnFrame titleLabel] setText:frameImage];
        [scrollView addSubview:btnFrame];
        viewWidh = (60*i)+((i+1)*20);
    }
    
    [frameImageArray release];
    [scrollView setContentSize:CGSizeMake((viewWidh+60+20), 103)];
}

- (IBAction) takeBtnPress:(id)sender {    
    UIGraphicsBeginImageContext(canvas.bounds.size);
    [canvas.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *newCombinedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
   
    StarFramePreviewViewController *previewViewController = [[StarFramePreviewViewController alloc] init];
    previewViewController.previewImageView.image = newCombinedImage;
    [self presentModalViewController:previewViewController animated:NO];
}

- (IBAction) backBtnPress:(id)sender {
    NSLog(@"backBtnPress click");
    [self dismissViewControllerAnimated:NO completion:^{}];
}

- (IBAction) frameBtnPress:(id)sender {
    NSLog(@"frameBtnPress click");
    if(scrollView.frame.size.height == 0){
        [self showFrame];
    }else{
        [self hideFrame];
    }
}

- (IBAction)btnFramePress:(id)sender {
    UIButton *btn = (UIButton *) sender;
    frameSelectedImageView.image = [UIImage imageNamed:btn.titleLabel.text];
}

- (void) hideFrame {
    [UIView animateWithDuration:0.3 animations:^{  // animate the following:
        scrollView.frame = CGRectMake(0.0, 480-85, 320, 0); // move to new location
    }];
}

- (void) showFrame {
    [UIView animateWithDuration:0.3 animations:^{  // animate the following:
        scrollView.frame = CGRectMake(0.0, 480-85-103, 320, 103); // move to new location
    }];
}

-(void)scale:(id)sender {
 
    if([(UIPinchGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        _lastScale = 1.0;
    }
    
    CGFloat scale = 1.0 - (_lastScale - [(UIPinchGestureRecognizer*)sender scale]);
    
    CGAffineTransform currentTransform = selectedImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, scale, scale);
    
    [selectedImageView setTransform:newTransform];
    
    _lastScale = [(UIPinchGestureRecognizer*)sender scale];
    
}

-(void)move:(id)sender {
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        _firstX = [selectedImageView center].x;
        _firstY = [selectedImageView center].y;
    }
    
    translatedPoint = CGPointMake(_firstX+translatedPoint.x, _firstY+translatedPoint.y);
    [selectedImageView setCenter:translatedPoint];
    
}

-(void)rotate:(id)sender {
    if([(UIRotationGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        
        _lastRotation = 0.0;
        return;
    }
    
    CGFloat rotation = 0.0 - (_lastRotation - [(UIRotationGestureRecognizer*)sender rotation]);
    
    CGAffineTransform currentTransform = selectedImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransform,rotation);
    
    [selectedImageView setTransform:newTransform];
    
    _lastRotation = [(UIRotationGestureRecognizer*)sender rotation];
}

- (void) dealloc {
    [frameSelectedImageView release];
    [selectedImageView release];
    [canvas release];
    [scrollView release];
    [super dealloc];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
