//
//  StarFrameHowtoViewController.h
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/27/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarFrameHowtoViewController : UIViewController {
    IBOutlet UIWebView *webView;
}

@property (retain, nonatomic) IBOutlet UIWebView *webView;

@end
