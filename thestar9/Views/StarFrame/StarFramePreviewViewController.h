//
//  StarFramePreviewViewController.h
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/28/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook.h"
#import <Twitter/Twitter.h>

@interface StarFramePreviewViewController : UIViewController <FBUserSettingsDelegate, FBRequestDelegate> {
    UIImageView *previewImageView;
    UIView *shareBarView;
    BOOL facebookLogined;
    
    UIActivityIndicatorView* spinner;
}

@property (retain, nonatomic) UIImageView *previewImageView;
@property (retain, nonatomic) UIView *shareBarView;
@property (retain, nonatomic) UIActivityIndicatorView* spinner;

@end
