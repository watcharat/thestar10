//
//  StarFrameImageVideoViewController.h
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 3/1/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>


@interface StarFrameImageVideoViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate> {
    UIScrollView *scrollView;
    UIImageView *frameSelectedImageView;
    UIView *canvas;
    AVCaptureSession *session;
    AVCaptureStillImageOutput *stillImageOutput;
    BOOL frontCam;
    
    
    AVCaptureInput *inputDeviceFront;
    AVCaptureInput *inputDeviceBack;
    
    AVCaptureDevice *capDeviceFront;
    AVCaptureDevice *capDeviceBack;
}

@property (retain, nonatomic) UIScrollView *scrollView;
@property (retain, nonatomic) UIImageView *frameSelectedImageView;
@property (nonatomic, retain) IBOutlet UIView *canvas;
@property(nonatomic, retain) AVCaptureSession *session;
@property(retain, nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property(nonatomic, assign) BOOL frontCam;

@property (retain, nonatomic)AVCaptureInput *inputDeviceFront;
@property (retain, nonatomic)AVCaptureInput *inputDeviceBack;

@property (retain, nonatomic)AVCaptureDevice *capDeviceFront;
@property (retain, nonatomic)AVCaptureDevice *capDeviceBack;


@end
