//
//  StarFrameImagePickerViewController.h
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/28/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <QuartzCore/QuartzCore.h>

@interface StarFrameImagePickerViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate> {
    UIScrollView *scrollView;
    UIImageView *frameSelectedImageView;
    UIImageView *selectedImageView;
    UIView *canvas;
    CGFloat _lastRotation;
	CGFloat _firstX;
	CGFloat _firstY;
    CGFloat _lastScale;
}

@property (retain, nonatomic) UIScrollView *scrollView;
@property (retain, nonatomic) UIImageView *frameSelectedImageView;
@property (retain, nonatomic) UIImageView *selectedImageView;
@property (nonatomic, retain) IBOutlet UIView *canvas;

@end
