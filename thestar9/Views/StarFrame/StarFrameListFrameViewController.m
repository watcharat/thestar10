//
//  StarFrameListFrameViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/27/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "StarFrameListFrameViewController.h"
#import "BlockAlertView.h"
#import "BlockActionSheet.h"
#import "StarFrameImagePickerViewController.h"
#import "StarFrameImageVideoViewController.h"

@interface StarFrameListFrameViewController ()

@end

@implementation StarFrameListFrameViewController

@synthesize scrollView;
@synthesize frameView;
@synthesize imgPicker;
@synthesize frameImageSelected;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    frameImageSelected = [[UIImage alloc] init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [scrollView setContentOffset:CGPointMake(0, 0)];
    [self renderDate];
}

- (id) init {
    self = [super init];
    
    if(imgPicker == nil){
        imgPicker = [[UIImagePickerController alloc] init];
        imgPicker.allowsEditing = NO;
        imgPicker.delegate = self;
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    if(self){
        CGRect masterRect = [[UIScreen mainScreen] bounds];
        CGRect menuBarFrame = CGRectMake(0.0, masterRect.size.height - 103, masterRect.size.width, 103);
        CGRect contentFrame = CGRectMake(0, 0, masterRect.size.width, masterRect.size.height-103);
        
        self.view = [[[UIView alloc] initWithFrame:masterRect] autorelease];
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sf_bg_spotlight.png"]]];
        
        // TODO :: create bottom bar
        UIImage *menuBarImg = [UIImage imageNamed:@"sf_bg_frame.png"];
        UIView *menuBarView = [[UIView alloc] initWithFrame:menuBarFrame];
        [menuBarView setBackgroundColor:[UIColor colorWithPatternImage:menuBarImg]];
        menuBarView.alpha = 0.8f;
        
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setFrame:CGRectMake((menuBarView.frame.size.width/2)-(125/2), 18, 125, 50)];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_back_box.png"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        
        [menuBarView addSubview:backBtn];
        
        [self.view addSubview:menuBarView];
        [menuBarView release];
        
        // TODO :: create scroll view
        if(scrollView == nil){
            scrollView = [[UIScrollView alloc] initWithFrame:contentFrame];
            scrollView.contentSize = CGSizeMake(320, 1000);
            scrollView.delegate = self;
            [scrollView setShowsHorizontalScrollIndicator:NO];
            [self.view addSubview:scrollView];
        }
    }
    
    return self;
}

- (IBAction) backBtnPress:(id)sender {
    NSLog(@"backBtnPress click");
    [self dismissViewControllerAnimated:NO completion:^{}];
}

- (void) renderDate {
    
    NSMutableArray *frameImageArray = [[NSMutableArray alloc] init];
    [frameImageArray addObject:@"st_frame_no1_01.png"];
    [frameImageArray addObject:@"st_frame_no1_02.png"];
    [frameImageArray addObject:@"st_frame_no2_01.png"];
    [frameImageArray addObject:@"st_frame_no2_02.png"];
    [frameImageArray addObject:@"st_frame_no3_01.png"];
    [frameImageArray addObject:@"st_frame_no3_02.png"];
    [frameImageArray addObject:@"st_frame_no4_01.png"];
    [frameImageArray addObject:@"st_frame_no4_02.png"];
    [frameImageArray addObject:@"st_frame_no5_01.png"];
    [frameImageArray addObject:@"st_frame_no5_02.png"];
    [frameImageArray addObject:@"st_frame_no6_01.png"];
    [frameImageArray addObject:@"st_frame_no6_02.png"];
    [frameImageArray addObject:@"st_frame_no7_01.png"];
    [frameImageArray addObject:@"st_frame_no7_02.png"];
    [frameImageArray addObject:@"st_frame_no8_01.png"];
    [frameImageArray addObject:@"st_frame_no8_02.png"];
    [frameImageArray addObject:@"sf_default.png"];
    
    int rows = 0;
    int cols = 25;
    int viewHeight = 0;
    
    for (int i=0; i<frameImageArray.count; i++) {
        
        NSString *frameImage = [frameImageArray objectAtIndex:i];
        float marginTop;
        
        if(i > 0 && i % 2 == 0){
            cols = 25;
            rows += 1;
        }
        
        if(i %2 != 0){
            cols = 25+128+15;
        }
        
        marginTop = (rows*192)+((rows+1)*20);
        viewHeight = marginTop;
        
        UIButton *btnFrame = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnFrame setFrame:CGRectMake(cols, marginTop, 128, 192)];
        [btnFrame setBackgroundImage:[UIImage imageNamed:frameImage] forState:UIControlStateNormal];
        btnFrame.alpha = 0.0;
        [btnFrame addTarget:self action:@selector(btnFramePress:) forControlEvents:UIControlEventTouchUpInside];
        [[btnFrame titleLabel] setText:frameImage];
        
        [scrollView addSubview:btnFrame];
        [UIView animateWithDuration:1.0 animations:^{
            btnFrame.alpha = 1.0;
        }];
    }
    
    [frameImageArray release];
    
    [scrollView setContentSize:CGSizeMake(320, (viewHeight+(192+(rows*15)))-40)];
    
    
    CGRect frameRect = CGRectMake(0, 0, 320, viewHeight);
    [frameView setFrame:frameRect];
     
}

- (IBAction) btnFramePress:(id)sender {
    
    UIButton *btn = (UIButton *) sender;
    frameImageSelected = [UIImage imageNamed:btn.titleLabel.text];
    
    BlockAlertView *alert = [BlockAlertView alertWithTitle:@"" message:@""];
    
    [alert addButtonWithTitle:@"เลือกรูป" block:^{
        //[self presentModalViewController:self.imgPicker animated:YES];
        //NSLog(@"เลือกรูป click");
        //NSLog(@"%@", self.imgPicker);
        [self presentModalViewController:self.imgPicker animated:NO];
    }];
    
    [alert addButtonWithTitle:@"ถ่ายรูป" block:^{
        //[self btnFramePress:nil];
        NSLog(@"ถ่ายรูป click");
        StarFrameImageVideoViewController *imageVideoViewController = [[StarFrameImageVideoViewController alloc] init];
        imageVideoViewController.frameSelectedImageView.image = frameImageSelected;
        imageVideoViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentModalViewController:imageVideoViewController animated:NO];
    }];
    
    [alert setDestructiveButtonWithTitle:@"Cancel!" block:nil];
    
    [alert show];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    NSLog(@"didFinishPickingImage");
    [self dismissViewControllerAnimated:YES completion:^{
        //Present the new MVC
        StarFrameImagePickerViewController *starFrameImagePickerViewController = [[StarFrameImagePickerViewController alloc] init];
        starFrameImagePickerViewController.frameSelectedImageView.image = frameImageSelected;
        starFrameImagePickerViewController.selectedImageView.image = image;
        
        float frameWidth = 0;
        float frameHeight = 0;
        frameWidth = 223;
        frameHeight = (image.size.height/image.size.width)*223;
        
        starFrameImagePickerViewController.selectedImageView.frame = CGRectMake(0, 0, frameWidth, frameHeight);
        starFrameImagePickerViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentModalViewController:starFrameImagePickerViewController animated:NO];
    }];
}

- (void) dealloc {
    [scrollView release];
    [frameView release];
    [imgPicker release];
    [frameImageSelected release];
    [super dealloc];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
