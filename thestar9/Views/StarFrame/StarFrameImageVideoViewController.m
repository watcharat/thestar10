//
//  StarFrameImageVideoViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 3/1/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "StarFrameImageVideoViewController.h"
#import "StarFramePreviewViewController.h"


@interface StarFrameImageVideoViewController ()

@end

@implementation StarFrameImageVideoViewController

@synthesize scrollView;
@synthesize frameSelectedImageView;
@synthesize canvas;
@synthesize session;
@synthesize stillImageOutput;
@synthesize frontCam;


@synthesize inputDeviceFront;
@synthesize inputDeviceBack;

@synthesize capDeviceFront;
@synthesize capDeviceBack;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [scrollView setContentOffset:CGPointMake(0, 0)];
    [self renderData];
    [self hideFrame];
}

- (id) init {
    self = [super init];
    
    if(self) {
        
        frontCam = NO;
        
        CGRect masterRect = CGRectMake(0, 0, 320, 480);
        CGRect menuBarFrame = CGRectMake(0.0, masterRect.size.height-85, masterRect.size.width, 66);
        CGRect frameBarFrame = CGRectMake(0.0, masterRect.size.height-85-103, masterRect.size.width, 103);
        
        self.view = [[[UIView alloc] initWithFrame:masterRect] autorelease];
        //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sf_camera_frame.png"]]];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        UIImage *frameBackgroundImage = [UIImage imageNamed:@"sf_camera_frame.png"];
        UIImageView *frameBackgroundImageView = [[UIImageView alloc] initWithImage:frameBackgroundImage];
        frameBackgroundImageView.frame = CGRectMake(0, -20, 320, 480);;
        [self.view addSubview:frameBackgroundImageView];
        [frameBackgroundImageView release];
        
        // TODO :: create bottom bar
        UIImage *menuBarImg = [UIImage imageNamed:@"sf_menu_bar.png"];
        UIView *menuBarView = [[UIView alloc] initWithFrame:menuBarFrame];
        [menuBarView setBackgroundColor:[UIColor colorWithPatternImage:menuBarImg]];
        //menuBarView.alpha = 0.9;
        
        
        // TODO :: create take button
        UIButton *takeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [takeBtn setFrame:CGRectMake((menuBarView.frame.size.width/2)-(122/2), (menuBarFrame.size.height - 48) / 2, 122, 48)];
        [takeBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_retake.png"] forState:UIControlStateNormal];
        [takeBtn addTarget:self action:@selector(takeBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:takeBtn];
        
        // TODO :: create back button
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setFrame:CGRectMake(30, (menuBarFrame.size.height - 40) / 2, 40, 40)];
        [backBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_back.png"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:backBtn];
        
        // TODO :: create frame button
        UIButton *frameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [frameBtn setFrame:CGRectMake(320-40-30, (menuBarFrame.size.height - 40) / 2, 40, 40)];
        [frameBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_frame.png"] forState:UIControlStateNormal];
        [frameBtn addTarget:self action:@selector(frameBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:frameBtn];
        
        [self.view addSubview:menuBarView];
        [menuBarView release];
        
        if(canvas == nil){
            canvas = [[UIView alloc] initWithFrame:CGRectMake(48, 35, 223, 335)];
        }
        
        
        // TODO :: init camera
        session = [[AVCaptureSession alloc] init];
        AVCaptureVideoPreviewLayer  *previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        CGRect bounds = canvas.layer.bounds;
        previewLayer.bounds = bounds;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer.position = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
        
        [[[self canvas] layer] addSublayer:previewLayer];
        
        //AVCaptureDevice* camera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        capDeviceFront = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        capDeviceBack = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        //        AVCaptureDevice *captureDevice = nil;
        NSArray *videoDevices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        for(AVCaptureDevice *device in videoDevices){
            
            //            if(device.position == AVCaptureDevicePositionBack){
            ////            if(device.position == AVCaptureDevicePositionFront){
            //                frontCam = NO;
            //                captureDevice = device;
            //                break;
            //            }
            
            if(device.position == AVCaptureDevicePositionBack){
                NSLog(@"found back cam");
                //                captureDevice = device;
                frontCam = NO;
                capDeviceBack = device;
                NSError *error=nil;
                inputDeviceBack = [[AVCaptureDeviceInput alloc] initWithDevice:capDeviceBack error:&error];
            }if(device.position == AVCaptureDevicePositionFront){
                NSLog(@"found front cam");
                frontCam = YES;
                capDeviceFront = device;
                NSError *error=nil;
                inputDeviceFront = [[AVCaptureDeviceInput alloc] initWithDevice:capDeviceFront error:&error];
            }
            
        }
        
        
        [session setSessionPreset:AVCaptureSessionPreset640x480];
        
        if(frontCam == YES){
            if([session canAddInput:inputDeviceFront]){
                [session addInput:inputDeviceFront];
            }
        }else{
            if([session canAddInput:inputDeviceBack]){
                [session addInput:inputDeviceBack];
            }
        }
        
        stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG, AVVideoCodecKey, nil];
        [stillImageOutput setOutputSettings:outputSettings];
        if([session canAddOutput:stillImageOutput]){
            [session addOutput:stillImageOutput];
        }else{
            NSLog(@"add output error");
        }
        [session startRunning];
        
        /*
         if(frontCam){
         camera = capDeviceFront;
         }else{
         camera = capDeviceBack;
         NSError *error=nil;
         inputDeviceBack = [[AVCaptureDeviceInput alloc] initWithDevice:capDeviceBack error:&error];
         }
         */
        
        
        /*
         
         if(camera!=NULL){
         
         NSLog(@"found camera");
         //set quality
         //        [session setSessionPreset:AVCaptureSessionPresetHigh];
         //        [session setSessionPreset:AVCaptureSessionPreset640x480];
         //        [session setSessionPreset:AVCaptureSessionPresetPhoto];
         
         [session setSessionPreset:AVCaptureSessionPreset640x480];
         
         // set input session
         // ---------------------
         NSError *error=nil;
         AVCaptureInput *camInput = [[AVCaptureDeviceInput alloc] initWithDevice:camera error:&error];
         //            self.inputDeviceBack = camInput;
         //
         //            self.inputDeviceFront = [[AVCaptureDeviceInput alloc] initWithDevice:capDeviceFront error:&error];
         
         
         if(camInput==NULL){
         NSLog(@"error create camera capture: %@", error);
         }
         
         if([session canAddInput:camInput]){
         [session addInput:camInput];
         }else{
         NSLog(@"error cannot add to session : %@", error);
         }
         
         // add output (use AVCaptureStillImageOutput)
         // ---------------------
         stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
         NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG, AVVideoCodecKey, nil];
         [stillImageOutput setOutputSettings:outputSettings];
         if([session canAddOutput:stillImageOutput]){
         [session addOutput:stillImageOutput];
         }else{
         NSLog(@"add output error");
         }
         [session startRunning];
         
         }else{
         NSLog(@"not found camera device capture");
         }
         */
        
        
        // TODO :: create frame layer
        if(frameSelectedImageView == nil) {
            frameSelectedImageView = [[UIImageView alloc] init];
            frameSelectedImageView.frame = CGRectMake(0, 0, 223, 335);
            [canvas addSubview:frameSelectedImageView];
        }
        
        
        [self.view addSubview:canvas];
        
        // TODO :: create back button
        if(frontCam == YES){
            UIButton *switchCamBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [switchCamBtn setFrame:CGRectMake(222, 35, 50, 50)];
            [switchCamBtn setBackgroundImage:[UIImage imageNamed:@"sf_bt_cam_flip@2x.png"] forState:UIControlStateNormal];
            [switchCamBtn addTarget:self action:@selector(switchCamBtnPress:) forControlEvents:UIControlEventTouchUpInside];
            [switchCamBtn setAlpha:0.7];
            [self.view addSubview:switchCamBtn];
        }
        
        // TODO :: create scroll view
        if(scrollView == nil){
            scrollView = [[UIScrollView alloc] initWithFrame:frameBarFrame];
            scrollView.contentSize = CGSizeMake(1000, 103);
            scrollView.delegate = self;
            [scrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sf_bg_frame.png"]]];
            [scrollView setShowsVerticalScrollIndicator:NO];
            scrollView.alpha = 1.0;
            [self.view addSubview:scrollView];
        }
        
        //[self.view sendSubviewToBack:canvas];
    }
    
    return self;
}

- (IBAction) switchCamBtnPress:(id)sender {
    
    [session beginConfiguration];
    
    if(frontCam){
        //if([session canAddInput:inputDeviceBack]){
        [session removeInput:inputDeviceFront];
        [session addInput:inputDeviceBack];
        frontCam = NO;
        //}
    }else{
        //NSLog(@"is frontCam is false");
        //if([session canAddInput:inputDeviceFront]){
        if(inputDeviceFront == nil){
            return;
        }
        [session removeInput:inputDeviceBack];
        [session addInput:inputDeviceFront];
        frontCam = YES;
        //}
    }
    
    [session commitConfiguration];
}

- (void) renderData {
    NSMutableArray *frameImageArray = [[NSMutableArray alloc] init];
    [frameImageArray addObject:@"st_frame_no1_01.png"];
    [frameImageArray addObject:@"st_frame_no1_02.png"];
    [frameImageArray addObject:@"st_frame_no2_01.png"];
    [frameImageArray addObject:@"st_frame_no2_02.png"];
    [frameImageArray addObject:@"st_frame_no3_01.png"];
    [frameImageArray addObject:@"st_frame_no3_02.png"];
    [frameImageArray addObject:@"st_frame_no4_01.png"];
    [frameImageArray addObject:@"st_frame_no4_02.png"];
    [frameImageArray addObject:@"st_frame_no5_01.png"];
    [frameImageArray addObject:@"st_frame_no5_02.png"];
    [frameImageArray addObject:@"st_frame_no6_01.png"];
    [frameImageArray addObject:@"st_frame_no6_02.png"];
    [frameImageArray addObject:@"st_frame_no7_01.png"];
    [frameImageArray addObject:@"st_frame_no7_02.png"];
    [frameImageArray addObject:@"st_frame_no8_01.png"];
    [frameImageArray addObject:@"st_frame_no8_02.png"];
    [frameImageArray addObject:@"sf_default.png"];
    
    int viewWidh = 0;
    
    for (int i=0; i<frameImageArray.count; i++) {
        NSString *frameImage = [frameImageArray objectAtIndex:i];
        UIButton *btnFrame = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnFrame setFrame:CGRectMake((60*i)+((i+1)*20), 10, 60, 89)];
        [btnFrame setBackgroundImage:[UIImage imageNamed:frameImage] forState:UIControlStateNormal];
        [btnFrame addTarget:self action:@selector(btnFramePress:) forControlEvents:UIControlEventTouchUpInside];
        [[btnFrame titleLabel] setText:frameImage];
        [scrollView addSubview:btnFrame];
        viewWidh = (60*i)+((i+1)*20);
    }
    
    [frameImageArray release];
    [scrollView setContentSize:CGSizeMake((viewWidh+60+20), 103)];
}

- (IBAction) takeBtnPress:(id)sender {
    NSLog(@"takeBtnPress click");
    AVCaptureConnection *vdoCon=nil;
    for(AVCaptureConnection *connection in stillImageOutput.connections){
        for(AVCaptureInputPort *port in [connection inputPorts]){
            vdoCon = connection;
            break;
        }
        if(vdoCon){break;}
    }
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:vdoCon completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        
        CFDictionaryRef exifAttachments = CMGetAttachment( imageDataSampleBuffer, kCGImagePropertyExifDictionary, NULL);
        
        if(exifAttachments){
//            NSLog(@"attachements: %@", exifAttachments);
        }else{
            NSLog(@"no attachment");
        }
        
        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
        UIImage *img = [[UIImage alloc] initWithData:imageData];
        
        UIImage *forgroundImage = frameSelectedImageView.image;
        UIImage *backgroundImage = img;
        
        if(frontCam)
        {
            NSLog(@"image from front camera");
            backgroundImage = [UIImage imageWithCGImage:backgroundImage.CGImage scale:1.0f orientation:UIImageOrientationLeftMirrored];
        }
        
        
        CGSize newSize = CGSizeMake(421, 640);
        UIGraphicsBeginImageContext( newSize );
        
        // Use existing opacity as is
        [backgroundImage drawInRect:CGRectMake(-30,0,480,640)];
        
        //        CGContextScaleCTM(backgroundImage, -1.0, 1);
        
        // Apply supplied opacity if applicable
        [forgroundImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height) blendMode:kCGBlendModeNormal alpha:1.0];
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        StarFramePreviewViewController *previewViewController = [[StarFramePreviewViewController alloc] init];
        previewViewController.previewImageView.image = newImage;
        [self presentModalViewController:previewViewController animated:NO];
        [previewViewController release];
        
    }];
    
}

- (IBAction) backBtnPress:(id)sender {
    NSLog(@"backBtnPress click");
    [self dismissViewControllerAnimated:NO completion:^{}];
}

- (IBAction) frameBtnPress:(id)sender {
    NSLog(@"frameBtnPress click");
    if(scrollView.frame.size.height == 0){
        [self showFrame];
    }else{
        [self hideFrame];
    }
}

- (IBAction)btnFramePress:(id)sender {
    UIButton *btn = (UIButton *) sender;
    frameSelectedImageView.image = [UIImage imageNamed:btn.titleLabel.text];
}

- (void) hideFrame {
    [UIView animateWithDuration:0.3 animations:^{  // animate the following:
        scrollView.frame = CGRectMake(0.0, 480-85, 320, 0); // move to new location
    }];
}

- (void) showFrame {
    [UIView animateWithDuration:0.3 animations:^{  // animate the following:
        scrollView.frame = CGRectMake(0.0, 480-85-103, 320, 103); // move to new location
    }];
}

- (void) dealloc {
    [session release];
    [frameSelectedImageView release];
    [scrollView release];
    [canvas release];
    [stillImageOutput release];
    [inputDeviceFront release];
    [inputDeviceBack release];
    [capDeviceFront release];
    [capDeviceBack release];
    [super dealloc];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
