//
//  StarFramePreviewViewController.m
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/28/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "StarFramePreviewViewController.h"
#import "AppDelegate.h"
#import "BlockAlertView.h"

@interface StarFramePreviewViewController ()

@end

@implementation StarFramePreviewViewController

@synthesize previewImageView;
@synthesize shareBarView;
@synthesize spinner;

- (void) dealloc {
    [previewImageView release];
    [shareBarView release];
    [spinner release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [self.spinner setHidden:YES];
    [self hideFrame];
}

- (id) init {
    self = [super init];
    if(self) {
        
        CGRect masterRect = CGRectMake(0, 0, 320, 480);
        CGRect menuBarFrame = CGRectMake(0.0, masterRect.size.height-85, masterRect.size.width, 66);
        CGRect shareBarFrame = CGRectMake(0, masterRect.size.height-85-63, masterRect.size.width, 63);
        
        self.view = [[[UIView alloc] initWithFrame:masterRect] autorelease];
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sf_bg_spotlight.png"]]];
        
        // TODO :: create selected image layer
        if(previewImageView == nil){
            previewImageView.clipsToBounds = YES;
            previewImageView = [[UIImageView alloc] init];
            previewImageView.frame = CGRectMake(48, 35, 223, 335);
            previewImageView.contentMode = UIViewContentModeScaleAspectFill;
            [self.view addSubview:previewImageView];
        }
        
        // TODO :: create share bar
        if(shareBarView == nil){
            UIImage *shareBarImg = [UIImage imageNamed:@"sf_bg_share.png"];
            shareBarView = [[UIView alloc] initWithFrame:shareBarFrame];
            [shareBarView setBackgroundColor:[UIColor colorWithPatternImage:shareBarImg]];
            shareBarView.alpha = 1.0f;
        }
        
        // TODO :: create facebook share button
        UIButton *shareFacebookBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [shareFacebookBtn setFrame:CGRectMake(((shareBarFrame.size.width/2)/2)-(146/2), (shareBarFrame.size.height/2)-(43/2), 146, 43)];
        [shareFacebookBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_facebook.png"] forState:UIControlStateNormal];
        [shareFacebookBtn addTarget:self action:@selector(shareFacebookBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [shareBarView addSubview:shareFacebookBtn];
        
        // TODO :: create twitter share button
        UIButton *shareTweetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [shareTweetBtn setFrame:CGRectMake((shareBarFrame.size.width/2)+(((shareBarFrame.size.width/2)/2)-(146/2)), (shareBarFrame.size.height/2)-(43/2), 146, 43)];
        [shareTweetBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_twitter.png"] forState:UIControlStateNormal];
        [shareTweetBtn addTarget:self action:@selector(shareTweetBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [shareBarView addSubview:shareTweetBtn];
        
        [self.view addSubview:shareBarView];
        
        // TODO :: create menu bar
        UIImage *menuBarImg = [UIImage imageNamed:@"sf_menu_bar.png"];
        UIView *menuBarView = [[UIView alloc] initWithFrame:menuBarFrame];
        [menuBarView setBackgroundColor:[UIColor colorWithPatternImage:menuBarImg]];
        
        // TODO :: create re-take button
        UIButton *retakeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [retakeBtn setFrame:CGRectMake(((menuBarView.frame.size.width/2)/2)-(122/2), (menuBarFrame.size.height - 48) / 2, 122, 48)];
        [retakeBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_retake_text.png"] forState:UIControlStateNormal];
        [retakeBtn addTarget:self action:@selector(retakeBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:retakeBtn];
        
        // TODO :: create save button
        UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [saveBtn setFrame:CGRectMake(165, (menuBarFrame.size.height - 40) / 2, 40, 40)];
        [saveBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_save.png"] forState:UIControlStateNormal];
        [saveBtn addTarget:self action:@selector(saveBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:saveBtn];
        
        // TODO :: create share button
        UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [shareBtn setFrame:CGRectMake(220, (menuBarFrame.size.height - 40) / 2, 40, 40)];
        [shareBtn setBackgroundImage:[UIImage imageNamed:@"sf_btn_share.png"] forState:UIControlStateNormal];
        [shareBtn addTarget:self action:@selector(shareBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:shareBtn];
        
        
        // TODO :: create exit button
        UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [exitBtn setFrame:CGRectMake(275, (menuBarFrame.size.height - 40) / 2, 40, 40)];
        [exitBtn setBackgroundImage:[UIImage imageNamed:@"qr_btn_exit.png"] forState:UIControlStateNormal];
        [exitBtn addTarget:self action:@selector(exitBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [menuBarView addSubview:exitBtn];
        
        // 
        [self.view addSubview:menuBarView];
        [menuBarView release];
        
        //create indicator
        // create spinner
        float kScreenWidth = 320;
        float kScreenHeight = 480-40;
        
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.spinner setCenter:CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0)]; // I do this because I'm in landscape mode
        [self.view addSubview:spinner];
        [self.spinner setHidden:NO];
        [self.spinner startAnimating];
        
    }
    
    return self;
}

- (IBAction)exitBtnPress:(id)sender{

//    int count = [self findRootViewController:self];
//    NSLog(@"count %d", count);
//    if(count == 2)
//        [self dismissViewControllerAnimated:YES completion:^{}];
//    if(count == 3)
//        [self.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
//    if(count == 4)
//        [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
//    if(count == 5)
//        [self.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
//    if(count == 6)
//        [self.presentingViewController.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
    
    [self.presentingViewController.presentingViewController.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{}];
}

- (int) findRootViewController:(UIViewController*)vc
{
    if(vc)
    {
        return 1 + [self findRootViewController:vc.presentingViewController];
    }
    return 0;
}

- (IBAction) shareBtnPress:(id)sender {
    NSLog(@"frameBtnPress click");
    if(shareBarView.frame.size.height == 0){
        [self showFrame];
    }else{
        [self hideFrame];
    }
}

- (IBAction) retakeBtnPress:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{}];
}

- (IBAction) saveBtnPress:(id)sender {
    UIImageWriteToSavedPhotosAlbum(previewImageView.image, nil, nil, nil);
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                   message:@"Save image successful"
                                                  delegate:self
                                         cancelButtonTitle:@"Close"
                                         otherButtonTitles: nil];
    
    [alert show];
}

- (IBAction) shareFacebookBtnPress:(id)sender{
    NSLog(@"shareFacebookBtnPress click");
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:@"The Star 9 ชวนคุณมาโพสท่าถ่ายรูปกับเดอะสตาร์คนโปรดแบบใกล้ชิด, Download App http://goo.gl/5TSJM" forKey:@"name"];
    [params setObject:@"อวด...รูปคู่กับ The Star 9 ก่อนใคร พร้อมแชร์รูปสุดเจ๋งไปอวดเพื่อนๆกัน, Download App http://goo.gl/5TSJM" forKey:@"caption"];
    [params setObject:@"for iPhone and Android - Avaiable in App Store" forKey:@"description"];
    [params setObject:UIImagePNGRepresentation(previewImageView.image) forKey:@"picture"];
    
    if(FBSession.activeSession.isOpen){
        
        if ([FBSession.activeSession.permissions indexOfObject:@"user_photos"] == NSNotFound){
            NSLog(@"permission not found");
            [FBSession.activeSession reauthorizeWithPublishPermissions:[NSArray arrayWithObjects:@"user_about_me", @"publish_stream", @"user_photos", nil] defaultAudience:FBSessionDefaultAudienceFriends completionHandler:^(FBSession *session, NSError *error)
             {
                 
                 // If permissions granted, publish the story
                 if (!error) {
                     
                     NSLog(@"reauthorize success");
                     [self initPostFacebookWithParams:params];
                     
                 }else{
                     NSLog(@"reauthorize fail");
                 }
             }];
        } else {
            NSLog(@"permission found");
            [self initPostFacebookWithParams:params];
        }
        
    }else{
        [appDel openSessionWithAllowLoginUI:YES];
    }
    
}

- (IBAction) shareTweetBtnPress:(id)sender {
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet =
        [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:@"The Star 9 ชวนคุณมาโพสท่าถ่ายรูปกับเดอะสตาร์คนโปรดแบบใกล้ชิด, Download App http://goo.gl/5TSJM"];
        [tweetSheet addImage:previewImageView.image];
        [self presentModalViewController:tweetSheet animated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure                                   your device has an internet connection and you have                                   at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)initPostFacebookWithParams:(NSMutableDictionary *)params {
    [self.spinner setHidden:NO];
    [self.spinner startAnimating];
    
    [FBRequestConnection startWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        NSLog(@"ERROR : %@", error.localizedDescription);
        if (!error) {
            //[self.spinner stopAnimating];
            [self.spinner setHidden:YES];
            NSLog(@"Facebook Post Success..");
            BlockAlertView *alert = [BlockAlertView alertWithTitle:@"" message:@"Share Complete."];
            [alert setDestructiveButtonWithTitle:@"Close!" block:nil];
            [alert show];
        } else {
            //[self.spinner stopAnimating];
            [self.spinner setHidden:YES];
            NSLog(@"Facebook Post Failed..");
            BlockAlertView *alert = [BlockAlertView alertWithTitle:@"" message:@"Share Failed. Please try again."];
            [alert setDestructiveButtonWithTitle:@"Close!" block:nil];
            [alert show];
        }
        
        
    }]; }

- (void) hideFrame {
    [UIView animateWithDuration:0.3 animations:^{  // animate the following:
        shareBarView.frame = CGRectMake(0.0, 480-85, 320, 0); // move to new location
    }];
}

- (void) showFrame {
    [UIView animateWithDuration:0.3 animations:^{  // animate the following:
        shareBarView.frame = CGRectMake(0.0, 480-85-63, 320, 63); // move to new location
    }];
}


-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationPortrait | UIInterfaceOrientationMaskPortrait;
}


@end
