//
//  StarFrameListFrameViewController.h
//  thestar9
//
//  Created by Aukkarapong Vongsawat on 2/27/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarFrameListFrameViewController : UIViewController <UIScrollViewDelegate, UIImagePickerControllerDelegate> {
    UIScrollView *scrollView;
    UIView *frameView;
    UIImagePickerController *imgPicker;
    UIImage *frameImageSelected;
}

@property(retain, nonatomic) UIScrollView *scrollView;
@property(retain, nonatomic) UIView *frameView;
@property(retain, nonatomic) UIImagePickerController *imgPicker;
@property(retain, nonatomic) UIImage *frameImageSelected;

@end
