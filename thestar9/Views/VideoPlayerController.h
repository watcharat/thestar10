//
//  VideoPlayerController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/20/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JWSlideMenuViewController.h"

@interface VideoPlayerController : JWSlideMenuViewController<UIWebViewDelegate>{
    NSString* google_v;
    
    UIWebView *webview;
}

@property (retain, nonatomic) NSString* google_v;
@property (retain, nonatomic) UIWebView *webview;


@end
