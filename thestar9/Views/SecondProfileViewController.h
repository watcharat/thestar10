//
//  SecondProfileViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/3/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

@interface SecondProfileViewController : JWSlideMenuViewController{
    int number;
    UIImageView *imProfile;
    UIScrollView *scView;
    UIScrollView *scv;
    
}

@property (readwrite,assign)int number;
@property (retain, nonatomic)UIImageView *imProfile;
@property (retain, nonatomic)UIScrollView *scView;
@property (retain, nonatomic)UIScrollView *scv;
@end
