//
//  ListShelfDownloadCell.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/24/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "ListShelfDownloadCell.h"

#import "AppDelegate.h"

#import "DBManager.h"
#import "GDataXMLNode.h"

#define ALERTMODE_FILLNUMBER 0
#define ALERTMODE_OTP 1

#define ALERTMODE_NOTBUY 2
#define ALERTMODE_TOPUP 3


@implementation ListShelfDownloadCell

@synthesize imThumb, lbTitle, lbDesc, lbDate;
@synthesize contentCode, gmmdCode;
@synthesize btnDownload;
@synthesize pgbarView;
@synthesize service_id;
@synthesize downloadtype;
@synthesize datCover;

@synthesize strPhoneNum;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
    [imThumb release];
    [lbTitle release];
    [lbDesc release];
    [lbDate release];
    
    [contentCode release];
    [gmmdCode release];
    [btnDownload release];
    [pgbarView release];
    [datCover release];
    
    [super dealloc];
}

- (IBAction)btnDownloadListPressed:(id)sender {

    NSLog(@"download cell pressed: >> contentCode = %@, gmmdCode = %@", contentCode, gmmdCode);
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if(appDel.msisdn == nil || [appDel.msisdn isEqualToString:@""]){
        // call otp
        [self callAlertOTP];
    }else{
        
        [self checkLink];
        
        // add queue download
//        [self addQueueDownload];
    }
}


-(void)checkLink{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    
    
    
    
    // call api
    NSString* urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/download.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&GMMD_CODE=%@&SERVICE_ID=%@&MSISDN=%@&CP_ID=1&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         gmmdCode,
                         service_id,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        [data release];
        [url release];
        return;
    }
    NSLog(@"parse xml download ");
    
    //parse XML
    // ---------
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
    if(doc!=nil){
//        NSLog(@"xml format correctly.");
        NSString* status_code = [[[doc.rootElement elementsForName:@"status_code"]objectAtIndex:0]stringValue];
        NSString *detail = [[[doc.rootElement elementsForName:@"detail"] objectAtIndex:0] stringValue];
        
        if([status_code isEqualToString:@"200"]){
            [self addQueueDownload];
            
        }else if([status_code isEqualToString:@"202"] || [status_code isEqualToString:@"201"]){
            
            alertMode =ALERTMODE_TOPUP;
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @""
                                  message: detail
                                  delegate: self
                                  cancelButtonTitle:@"ยืนยัน"
                                  otherButtonTitles:@"ปิด", nil];
            
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
            
            [alert show];
            [alert release];
            
        }else if([status_code isEqualToString:@"203"]){
            
            alertMode = ALERTMODE_NOTBUY;
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle: @"เพลงยังไม่ซื้อ"
                                  message: detail
                                  delegate: self
                                  cancelButtonTitle:@"ไป"
                                  otherButtonTitles:@"ยกเลิก", nil];
            [alert show];
            [alert release];
        }
        
    }
}
// add queue download
-(void)addQueueDownload{
    

    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
    
    // --
    [appDel.downloadMgr addDownloadQueueWithGMMDCode:gmmdCode contentCode:contentCode service_id:self.service_id save_type:downloadtype title:lbTitle.text desc:lbDesc.text date:lbDate.text cover:datCover];
    
    NSLog(@"add progress bar");
    
    pgbarView.progress = 0.0f;
    pgbarView.tag = [gmmdCode intValue];
    [appDel.downloadMgr addProgressView:pgbarView];
    [appDel.downloadMgr startDownload];
    
    
    [self.btnDownload setHidden:YES];
    [pgbarView setFrame:CGRectMake(0, 61, 320, 2)];
    [pgbarView setHidden:NO];
}

-(void)callAlertOTP{
    alertMode = ALERTMODE_FILLNUMBER;
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: @"Telephone Number"
                          message: @"กรุณากรอกเบอร์โทรศัพท์"
                          delegate: self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:@"Cancel", nil];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [alert show];
    [alert release];
}

// alert message
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"alert callback");
    if(alertMode == ALERTMODE_FILLNUMBER){
        if (buttonIndex == 0) {
            NSLog(@"ok button fillnumber");
            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            self.strPhoneNum = [[alertView textFieldAtIndex:0] text];
            
            
            
            //validate phone number 08 to 668
            if ([self.strPhoneNum rangeOfString:@"0"].location == 0){
                self.strPhoneNum = [NSString stringWithFormat:@"66%@", [self.strPhoneNum substringFromIndex:1]];
                
            }
            NSLog(@"phone num = %@", self.strPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/sendOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                                 appDel.app_id,
                                 appDel.device,
                                 appDel.charset,
                                 self.strPhoneNum,
                                 appDel.appversion,
                                 appDel.apiversion
                                 ];
            
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
            if(data==nil){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                return;
            }
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                // -----
                
                if([status_code isEqualToString:@"200"]){
                    alertMode = ALERTMODE_OTP;
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: @"OTP Number"
                                          message: @"กรุณากรอกรหัส OTP จาก SMS"
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:@"Cancel", nil];
                    
                    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
                    [alert show];
                    [alert release];
                    
                }
            }
            [data release];
            [doc release];
            [url release];
        }
        else {
            NSLog(@"cancel button fillnumber");
        }
    }else if(alertMode== ALERTMODE_OTP){
        if (buttonIndex == 0) {
            
            NSLog(@"ok button fillnumber");
            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            NSString *txtOTP = [[alertView textFieldAtIndex:0] text];
            
            NSLog(@"phone num = %@", self.strPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/validateOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&ONETIMEPASS=%@&APPVERSION=%@&APIVERSION=%@",
                                 appDel.app_id,
                                 appDel.device,
                                 appDel.charset,
                                 self.strPhoneNum,
                                 txtOTP,
                                 appDel.appversion,
                                 appDel.apiversion
                                 ];
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            // connect api for return xml
            NSString* data = [[[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error] autorelease];
            
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                        options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                //
                if([status_code isEqualToString:@"200"]){
                    
                    // update number
                    appDel.msisdn = self.strPhoneNum;
                    
                    //update msisdn to database
                    [self updateMsisdnToDB];
                    
                    // add queue
                    [self addQueueDownload];
                }else{
                    NSLog(@"error status_code = %@", status_code);
                }
            }
            
            [doc release];
            [url release];
        }
        else {
            NSLog(@"cancel button fill number");
        }
    }
    
    else if (alertMode == ALERTMODE_TOPUP){
        NSString *serial = [[alertView textFieldAtIndex:0] text];
        
        // call topup api
        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/topup.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&SERIAL_CODE=%@&APPVERSION=%@&APIVERSION=%@",
                             appDel.app_id,
                             appDel.device,
                             appDel.charset,
                             appDel.msisdn,
                             serial,
                             appDel.appversion,
                             appDel.apiversion
                             ];
        NSLog(@"CALL API : %@", urlLink);
        NSURL *url = [[NSURL alloc] initWithString:urlLink];
        NSError *error;
        
        // connect api for return xml
        NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
        
        if([data isEqualToString:@""]){
            NSLog(@"[Connection ERROR]:%@", [error description]);
            [data release];
            [url release];
            return;
        }else{
            NSLog(@"data => %@", data);
        }
        
        //parse XML
        // ------------------------------------
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
        if(doc==nil){
            NSLog(@"parse xml document error");
        }else{
            
        }
        
        NSString *status_code = [[[doc.rootElement elementsForName:@"STATUS_CODE"]objectAtIndex:0] stringValue];
        if([status_code isEqualToString:@"200"]){
            NSLog(@"topup compeleted");
            [self addQueueDownload];
            
        }else{
            NSLog(@"topup not completed");
        }
    }
    // go to webview for purcharse
    else if (alertMode == ALERTMODE_NOTBUY){
        if(buttonIndex==0){
            NSString* gmmdcode_chk = gmmdCode;
            if([downloadtype isEqualToString:@"SPECIALCLIP"]){
                gmmdcode_chk = @"";
            }
            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=%@&GMMD_CODE=%@", appDel.app_id, appDel.device, appDel.msisdn, self.service_id, gmmdcode_chk];
        
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
        }
        
    }
}

-(void)updateMsisdnToDB{
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    // load msisdn from database
    DBManager *dbMgr = [[DBManager alloc] init];
    
    NSMutableArray *ar = [dbMgr fetchProfile:@""];
    if(ar !=nil && [ar count] > 0){
        NSLog(@"update msisdn : %@", appDel.msisdn);
        Profile *profile = [ar objectAtIndex:0];
        profile.msisdn = appDel.msisdn;
        [dbMgr updateProfile:profile];
        //
        [ar release];
    }else{
        NSLog(@"insert msisdn : %@", appDel.msisdn);
        Profile *newProfile = [dbMgr getSchemaProfile];
        newProfile.msisdn = self.strPhoneNum;
        [dbMgr insertProfile:newProfile];
        
        
    }
    [dbMgr release];
}




@end
