//
//  VideoDetailViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/17/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "VideoDetailViewController.h"

#import "VideoDetailCell.h"

#import "GDataXMLNode.h"
#import "AppDelegate.h"

#import "VideoPlayerController.h"

#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import "UIImageView+AFNetworking.h"
#import "XCDYouTubeVideoPlayerViewController.h"
@interface VideoDetailViewController (){
    NSArray* videoParts;
}

@end

@implementation VideoDetailViewController

@synthesize msisdnUtil;
@synthesize tbMain;
@synthesize btnBanner;
@synthesize videoId;
@synthesize desc;
@synthesize arDataTable;
@synthesize title_vdo;
@synthesize videoDic;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement loadView to create a view hierarchy programmatically, without using a nib.
//- (void)loadView
//{
//    NSLog(@"load view");
//}

-(void)addHeadDetail{
    // label head
    UILabel *lblHead = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 320, 30)];
    [lblHead setText:self.title_vdo];
    [lblHead setTextColor:[UIColor whiteColor]];
    [lblHead setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:lblHead];
    
    // textview
    
    
}

-(void)addBanner{
    if(self.btnBanner == nil){
        // create banner

        self.btnBanner = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.btnBanner setFrame:CGRectMake(0, 0, 320, 50)];
        
        //[self.btnBanner setBackgroundImage:[UIImage imageNamed:@"banner_pack.jpg"] forState:UIControlStateNormal];
        
        NSString* urlPath = @"http://thestar9.gmmwireless.com/thestar9/image/baner/pack/640x100.jpg";
        NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPath]];
        [btnBanner setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
        
        [self.btnBanner addTarget:self action:@selector(btnBannerPressed:)
            forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.btnBanner];

    }
}


- (void)viewDidUnload
{
    [self setBigVideoThumb:nil];
    [self setSubVideoView:nil];
    [self setVideoScrollView:nil];

    [self setTextView:nil];
    [self setLblHeadline:nil];
    [self setLblCaption:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)createTable{
    
    // check create table
    if(tbMain != nil){
        return;
    }
    CGRect masterRect = self.view.bounds;
    
//    CGRect tableFrame = CGRectMake(0.0, 22, masterRect.size.width, masterRect.size.height);
//    self.tableview = [[UITableView alloc] initWithFrame:tableFrame];
    

//    self.tableview.dataSource = self;
//    self.tableview.delegate = self;
//    self.tableview.backgroundColor = [UIColor blackColor];
//    self.tableview.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//    self.tableview.separatorColor = [UIColor darkGrayColor];
//    
//    [self.view addSubview:tableview];
    
    float bannerHeight = 50;
    float descHeight = 30;
    tbMain = [[UITableView alloc] initWithFrame:CGRectMake(0.0, bannerHeight+descHeight, masterRect.size.width, masterRect.size.height)];
    
    tbMain.delegate = self;
    tbMain.dataSource = self;
    tbMain.backgroundColor = [UIColor blackColor];
    tbMain.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tbMain.separatorColor = [UIColor darkGrayColor];
    
    [self.view addSubview:tbMain];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)viewWillAppear:(BOOL)animated{
    
//    NSLog(@"VIDEO DIC %@",videoDic);
    [self createView];
//    [self callVideoDetailAPI:self.videoId];
//    [self createTable];
//    [self addHeadDetail];
//    [self createPanelShare];
    
}
-(void)createView{
    
    [_textView setText:[NSString stringWithFormat:@"Posted : %@\n%@",[videoDic valueForKey:@"releaseDate"],[videoDic valueForKey:@"desc"]]];
    [_lblHeadline setText:[videoDic valueForKey:@"headline"]];
    [_lblCaption setText:[videoDic valueForKey:@"caption"]];
    

    videoParts = [videoDic objectForKey:@"videoParts"];
    [_bigVideoThumb setImageWithURL:[NSURL URLWithString:[[videoParts objectAtIndex:0] valueForKey:@"thumbsImgPart"]]];

    [_bigVideoThumb setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(playVideo:)];
    tap.numberOfTapsRequired = 1;
    [_bigVideoThumb addGestureRecognizer:tap];
    
//    if([videoParts count]>1){

//    }
    
    int testSubVid = 5;
    for (int i=0; i<testSubVid; i++) {
        UIImageView *videoImg = [[UIImageView alloc]initWithFrame:CGRectMake(i*117, 0, 117, 70)];
        [videoImg setImageWithURL:[NSURL URLWithString:[[videoParts objectAtIndex:0] valueForKey:@"thumbsImgPart"]]];
        [_videoScrollView addSubview:videoImg];
    }
    
    [_videoScrollView setContentSize:CGSizeMake(117*testSubVid, 70)];
//    NSLog(@"%@",videoParts);
    
}
-(void)createPanelShare{
    NSLog(@"create panel share");
    // share section
    // -x-x-x-x-
    UIImageView *imBgShareView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_share_bottom.jpg"]];
    imBgShareView.frame = CGRectMake(0, 420-48, 320, 48);
    [self.view addSubview:imBgShareView];
    [imBgShareView release];
    
    
    UIImageView *imSymbolShare = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"im_share.png"]];
    imSymbolShare.frame = CGRectMake(10, 420-48+10, 68, 28);
    [self.view addSubview:imSymbolShare];
    [imSymbolShare release];
    
    //button share twitter
    UIButton *btnShareTwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnShareTwitter setFrame:CGRectMake(90, 420-48+14, 100, 20)];
    [btnShareTwitter setBackgroundImage:[UIImage imageNamed:@"btn_share_comment_tw.png"] forState:UIControlStateNormal];
    [btnShareTwitter addTarget:self action:@selector(btnShareTwitterPressed:)
              forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnShareTwitter];
    
    
    //button share facebook
    UIButton *btnShareFacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnShareFacebook setFrame:CGRectMake(200, 420-48+14, 111, 20)];
    [btnShareFacebook setBackgroundImage:[UIImage imageNamed:@"btn_share_comment_fb.png"] forState:UIControlStateNormal];
    [btnShareFacebook addTarget:self action:@selector(btnShareFacebookPressed:)
               forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnShareFacebook];
}


-(void)viewDidLoad{
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-vdo.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
//    [self addHeadDetail];
}

//-(void)viewWillAppear:(BOOL)animated{
//
//    NSLog(@"tbMain reloadData");
//    [tbMain reloadData];
//}


// method connect api
-(void)callVideoDetailAPI:(NSString*)videoId{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    
    // use video highlight player
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/videoHightLight.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@&VIDEO_ID=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion,
                         self.videoId
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){
        /*
         //test out put
         NSArray *ar = [doc.rootElement elementsForName:@"STATUS"];
         GDataXMLElement *status = [ar objectAtIndex:0];
         NSLog(@"STATUS = %@", [status stringValue]);
         */
        // render data by xml
        [self storeData:doc];
        
    }else{
        NSLog(@"[XML ERROR] : %@", [error description] );
        
    }
    [data release];
    [doc release];
    [url release];

}

-(void)storeData:(GDataXMLDocument*)doc{
    NSArray *ar = [doc.rootElement elementsForName:@"VIDEO"];
    NSLog(@"video list = %d", [ar count]);
    
    // data list
    if(arDataTable==nil){
        arDataTable = [[NSMutableArray alloc] init];
    }else{
        [arDataTable removeAllObjects];
    }
    
    //foreach video
    for(int i=0; i< [ar count]; i++){
        GDataXMLElement *docVideo = [ar objectAtIndex:i];
        
        // data node xml
        GDataXMLElement* gId = [[docVideo elementsForName:@"ID"] objectAtIndex:0];
        GDataXMLElement* gImage_url = [[docVideo elementsForName:@"THUMBS_IMG"] objectAtIndex:0];
        GDataXMLElement* gHeadline = [[docVideo elementsForName:@"HEADLINE"] objectAtIndex:0];
        GDataXMLElement* gdDesc = [[docVideo elementsForName:@"DESC"] objectAtIndex:0];
        GDataXMLElement* gdGoogleV = [[docVideo elementsForName:@"GOOGLE_V_CODE"] objectAtIndex:0];
        
        NSData *imData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[gImage_url stringValue]]];
        
        // add to array
        NSMutableDictionary *dicDataRow = [[NSMutableDictionary alloc] init];
        [dicDataRow setValue:[gId stringValue] forKey:@"id"];
        [dicDataRow setValue:[gImage_url stringValue] forKey:@"image_url"];
        [dicDataRow setValue:imData forKey:@"im_data"];
        [dicDataRow setValue:[gdDesc stringValue] forKey:@"desc"];
        [dicDataRow setValue:[gHeadline stringValue] forKey:@"title"];
        
        [dicDataRow setValue:[gdGoogleV stringValue] forKey:@"google_v"];
        
        [arDataTable addObject:dicDataRow];
        [dicDataRow release];
    }
    
}


// delegate of table view
// --------------------------------------------------

//config table


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    NSLog(@"numberOfSectionsInTableView");
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    NSLog(@"numberOfRowsInSection");

    if(arDataTable!=nil && [arDataTable count] > 0){
        return [arDataTable count];
    }else{
        return 0;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"heightForRowAtIndexPath");
    return 80;
}

// render cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cellForRowAtIndexPath");
    static NSString *cellIdentifier = @"VideoDetailCell";
    
    VideoDetailCell *cell = (VideoDetailCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VideoDetailCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if(arDataTable!=nil && [arDataTable count] > 0){
        NSMutableDictionary *dicRow = [arDataTable objectAtIndex:indexPath.row];
        if(dicRow!=nil){
            
            //set title
            NSData *datImage = [dicRow objectForKey:@"im_data"];
            NSString* strDesc = [dicRow objectForKey:@"desc"];
            NSString* strTitle = [dicRow objectForKey:@"title"];
            
            //title
            [cell.imThumb setImage:[UIImage imageWithData:datImage]];
            [cell.lbDesc setText:strDesc];
            [cell.lbTitle setText:strTitle];
            
        }
    }

    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if(cell == nil)
//    {
//        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                       reuseIdentifier:cellIdentifier] autorelease];
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        cell.selectionStyle = UITableViewCellSelectionStyleGray;
//        // Do something here......................
//    }
//    
//    
//    cell.textLabel.text = @"test";
    
    
    return cell;
}
//action touch table
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    VideoPlayerController* playerCon = [[VideoPlayerController alloc] init] ;
    
    // google video id (watch)
    NSMutableDictionary *dic = [arDataTable objectAtIndex:indexPath.row];
    NSString* google_v = [dic objectForKey:@"google_v"];
    
    playerCon.google_v = google_v;
    
    [self.navigationController pushViewController:playerCon];
//    [self presentModalViewController:playerCon animated:YES];
    
    [playerCon release];
}

// --------------------------------------------------
-(void)dealloc{
//    [imBanner release];
//    [tbMain release];
//    [videoId release];
//    [arDataTable release];
    [_bigVideoThumb release];
    [_subVideoView release];
    [_videoScrollView release];
    [_textView release];
    [_lblHeadline release];
    [_lblCaption release];
    [super dealloc];
}

// ----
-(IBAction)btnBannerPressed:(id)sender{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@"appDel.msisdn = %@, count = %d",appDel.msisdn, [appDel.msisdn length] );
    if(appDel.msisdn ==nil || [appDel.msisdn isEqualToString:@""]){
        [self checkMSISDN];
    }else{
        [self goOutBuyWebview];
    }
}

-(void)checkMSISDN{
    
    if(self.msisdnUtil ==nil){
        self.msisdnUtil = [[MSISDNUtil alloc] init];
        self.msisdnUtil.delegate = self;
    }
    [self.msisdnUtil alertshow];
    
    
}
// delegate
-(void) receptOTPSuccess:(NSString*)msisdn{
    NSLog(@"OTP success");
    [self goOutBuyWebview];
}

-(void) receptOTPFails:(NSString*)status_code status_message:(NSString*)status_message{
    NSLog(@"OTP fails");
}


-(void)goOutBuyWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=&GMMD_CODE=", appDel.app_id, appDel.device, appDel.msisdn];
    
    NSLog(@"go out to charging: %@", strPath);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
}


// share section
// share button event
-(IBAction)btnShareTwitterPressed:(id)sender{
    NSString* strTwitter = @"The Star9 Video Highlight - ทุกบรรยากาศและคำเปิดใจ คอนเสิร์ตเปิด The Sar 9 http://goo.gl/5TSJM";
    /*
    NSString* strTwitter = @"The Star9 Video Highlight - ทุกบรรยากาศและคำเปิดใจ คอนเสิร์ตเปิด The Sar 9 http://goo.gl/5TSJM";
    // share twitter
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:strTwitter];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"ผิดพลาด"
                                  message:@"iOS ของท่านไม่ support twitter"
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    }
     */
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:strTwitter];
//        [tweetSheet addImage:previewImageView.image];
        [self presentModalViewController:tweetSheet animated:YES];
        [tweetSheet release];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure                                   your device has an internet connection and you have                                   at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }

}

-(IBAction)btnShareFacebookPressed:(id)sender{
    NSString* strImageShare = @"http://files.shopping8000.com/images//all/images/thestar9/appicon.png";
    NSString* linkURL = @"http://thestar9.gmmwireless.com/thestar9/mobilesite/index.php/store";
    NSString* caption = @"ดูคลิปน่ารัก สุดประทับใจจากน้องๆ The Star9 เพิ่มเติมได้ที่ thestar.gmember.com";
    NSString* name = @"The Star 9 Video Highlight - ทุกบรรยากาศและคำเปิดใจ คอนเสิร์ตเปิดตัว The Star9";
    NSString* desc = @"for iPhone and  Android - Avaiable in the App Store";
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel shareDataToFBByLinkURL:linkURL imageURL:strImageShare name:name caption:caption description:desc];
}

#pragma mark play video
-(IBAction)playVideo:(id)sender{
    NSLog(@"CLICK CLICK");

    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:[[videoParts objectAtIndex:0] valueForKey:@"googleVCodePart"]];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
}
@end
