//
//  GalleryViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "GalleryViewController.h"

#import "AppDelegate.h"
#import "GDataXMLNode.h"
#import "VideoCell.h"
#import "GalleryDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "AFJSONRequestOperation.h"

@interface GalleryViewController (){
    NSArray* galleryArray;
}

@end

@implementation GalleryViewController

@synthesize btnBanner;
@synthesize tbMain;
@synthesize galleryView;


@synthesize loadAPI;

@synthesize msisdnUtil;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    self.title = @"Gallery";
    // set title image
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-gallery.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    [self createVoteButton];
    [self loadBanner];
}

-(void)loadBanner{
    NSString* urlPath = @"http://thestar9.gmmwireless.com/thestar9/image/baner/pack/640x100.jpg";
    NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPath]];
    [btnBanner setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
}

//  create
//  --------------------------
-(void)createVoteButton{
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
}

// vote button pressed
// ---------------------------
-(IBAction)votePressed:(id)sender{
    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{

    if(self.loadAPI==false){
        self.loadAPI = true;
        [self callGalleryAPI];
    }

}

-(void)callGalleryAPI{
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    
    // call api
    NSString *urlLink = @"http://thestar9.gmmwireless.com:80/thestar9/star10/api//gallery.jsp?APP_ID=283&DEVICE=mockup_test&CHARSET=utf-8&MSISDN=66818667415&APPVERSION=1.0&APIVERSION=2.0.0";
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];

    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
            success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                            
                    NSDictionary *jsonDict = (NSDictionary *) JSON;
                    NSLog(@"JSON %@",jsonDict);
                    galleryArray = [jsonDict objectForKey:@"galleryDTOList"];
                
                    [tbMain reloadData];
                
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response,NSError *error, id JSON) {
                    NSLog(@"Request Failure Because %@",[error userInfo]);
    }];
    
    [operation start];
    
    
    [url release];
}

-(void)renderData:(GDataXMLDocument*)doc{
    
    // clear old if have old image
    if([[galleryView subviews] count] > 0){
        NSArray *arChilds = [galleryView subviews];
        for(int i=0; i< [arChilds count]; i++ ){
            UIView *obj = [arChilds objectAtIndex:i];
            [obj removeFromSuperview];
        }
    }

    // fetch xml
    NSArray *ar = [doc.rootElement elementsForName:@"GALLERRY"];
    NSLog(@"gallery list = %d", [ar count]);
    
    // set bound

//    [galleryView setFrame:CGRectMake(0, 0, 320, (([ar count]/2) * bgW)+headFirstH)];
//    [scrollview setContentSize:CGSizeMake(320, ([ar count]/2*bgH) + headFirstH)];
    
    float newH = (109+10+10+4)*([ar count]/2);
    float headFirstH = 220;
    newH += headFirstH;
//    [scrollview setContentSize:CGSizeMake(320, newH)];
    
    // add head

    GDataXMLElement *elmGallery = [ar objectAtIndex:0];
    NSString* gallery_id = [[[elmGallery elementsForName:@"GALLERRY_ID"] objectAtIndex:0]
                            stringValue];
    NSString* thumbs_img = [[[elmGallery elementsForName:@"THUMBS_IMG"] objectAtIndex:0]
                            stringValue];
    
    NSString* strTitle = [[[elmGallery elementsForName:@"CAPTION"] objectAtIndex:0]
                          stringValue];

    NSData *datPicture0 = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbs_img]];
    
    // label
    UILabel *label0 = [[[UILabel alloc] initWithFrame:CGRectMake(16, 190.0, 280.0, 21.0)] autorelease];
    label0.text = strTitle;
    [label0 setFont:[UIFont systemFontOfSize:11]];
    // button
    float space = 10;
    float simageH = 220;
    UIButton *btnThumb0 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnThumb0 setFrame:CGRectMake(space+6, space, 320-(space*2)-10, simageH-40)];
    [btnThumb0 setBackgroundImage:[UIImage imageWithData:datPicture0] forState:UIControlStateNormal];
    [btnThumb0 addTarget:self action:@selector(btnGalleryPressed:)
       forControlEvents:UIControlEventTouchUpInside];
    [[btnThumb0 titleLabel] setText:gallery_id];
    
    // image background
    UIImageView *imBg0;
    imBg0 = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_gallery_l.png"]] autorelease];
    [imBg0 setFrame:CGRectMake(8, 0, 306, simageH)];
    
    [galleryView addSubview:imBg0];
    [galleryView addSubview:label0];
    [galleryView addSubview:btnThumb0];
    
    //fetch xml doc
    for(int i=0; i< [ar count]; i++){
        
        if(i+1>= [ar count]){
            continue;
        }
        
        GDataXMLElement *elmGallery = [ar objectAtIndex:i+1];
        
        // data node xml
        NSString* gallery_id = [[[elmGallery elementsForName:@"GALLERRY_ID"] objectAtIndex:0]
                                stringValue];
        NSString* thumbs_img = [[[elmGallery elementsForName:@"THUMBS_IMG"] objectAtIndex:0]
                                stringValue];
        NSString* strTitle = [[[elmGallery elementsForName:@"CAPTION"] objectAtIndex:0]
                              stringValue];
        
        
//        NSLog(@"gallery_id = %@", gallery_id);
//        NSLog(@"thumbs_img = %@", thumbs_img);
        
        NSData *datPicture = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbs_img]];
        
        //image
        
        float imBgW = 144;
        float imBgH = 109;

        float x = (i%2)*160;
        float y = (i/2)*(imBgH+10);
        
        x += (160 - imBgW)/2;
        y += 10;
        
//        NSLog(@"x=%f, y=%f, width=%f, height=%f", x, y, bgW, bgH);
        
        float space = 10;
        
        // label
        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(x+space, y+space+headFirstH+(imBgH-40), 100.0, 25.0)] autorelease];
        label.text = strTitle;
        [label setFont:[UIFont systemFontOfSize:11]];
//        [label setFont:[UIFont fontWithName:@"Arial" size:18]];
        
        // button
        UIButton *btnThumb = [UIButton buttonWithType:UIButtonTypeCustom] ;
        [btnThumb setFrame:CGRectMake(x+space, y+space+headFirstH, imBgW-(space*2), imBgH-40)];
        [btnThumb setBackgroundImage:[UIImage imageWithData:datPicture] forState:UIControlStateNormal];
        [btnThumb addTarget:self action:@selector(btnGalleryPressed:)
           forControlEvents:UIControlEventTouchUpInside];
        [[btnThumb titleLabel] setText:gallery_id];
        
        // image background
        UIImageView *imBg;
        imBg = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_gallery_s.png"]] autorelease];
        [imBg setFrame:CGRectMake(x, y+headFirstH, imBgW, imBgH)];

        
        [galleryView addSubview:imBg];
        [galleryView addSubview:label];
        [galleryView addSubview:btnThumb];

    }
    
    float galleryHeight = 4000;
    galleryHeight = (436/2) +([ar count] * (237/2));
    CGRect galleryRec = CGRectMake(0, 0, 320, galleryHeight);
    [galleryView setFrame:galleryRec];
    
}
-(IBAction)btnGalleryPressed:(id)sender{
    UIButton *btn = (UIButton*)sender;
    
    GalleryDetailViewController *gaDetailCon = [[GalleryDetailViewController alloc] init];
    NSLog(@"gallery_id = %@", btn.titleLabel.text);
    gaDetailCon.gallery_id = btn.titleLabel.text;
    
    [self.navigationController pushViewController:gaDetailCon];
    
    [gaDetailCon release];
}


- (void)dealloc {
    [tbMain release];
    [msisdnUtil release];
    [galleryView release];
    [btnBanner release];
    [super dealloc];
}
// ------------------
- (void)viewDidUnload {

    [btnBanner release];
    btnBanner = nil;
    [self setBtnBanner:nil];
    [super viewDidUnload];
}

// -------------------
- (IBAction)btnBannerPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@"appDel.msisdn = %@, count = %d",appDel.msisdn, [appDel.msisdn length] );
    if(appDel.msisdn ==nil || [appDel.msisdn isEqualToString:@""]){
        [self checkMSISDN];
    }else{
        [self goOutBuyWebview];
    }
}


-(void)checkMSISDN{
    
    if(self.msisdnUtil ==nil){
        self.msisdnUtil = [[MSISDNUtil alloc] init];
        self.msisdnUtil.delegate = self;
    }
    [self.msisdnUtil alertshow];
    
    
}
// delegate
-(void) receptOTPSuccess:(NSString*)msisdn{
    NSLog(@"OTP success");
    [self goOutBuyWebview];
}

-(void) receptOTPFails:(NSString*)status_code status_message:(NSString*)status_message{
    NSLog(@"OTP fails");
}


-(void)goOutBuyWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=&GMMD_CODE=", appDel.app_id, appDel.device, appDel.msisdn];
    
    NSLog(@"go out to charging: %@", strPath);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
}

#pragma mark table delegate

// -- void delegate of uitable view cell
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //    NSLog(@"numberOfSectionsInTableView");
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"numberOfRowsInSection");
    return [galleryArray count];
}


// data row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    NSLog(@"cellForRowAtIndexPath");
    static NSString *CellId = @"Cell";
    
    VideoCell *cell = (VideoCell*)[tableView dequeueReusableCellWithIdentifier:CellId];
    
    if(cell==nil){
        // load xib file
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VideoCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // check data table
    if(galleryArray!=nil){
        NSDictionary *dicRow = (NSDictionary*)[galleryArray objectAtIndex:indexPath.row];
        if(dicRow!=nil){
            
            //set title
            NSURL* urlImage = [NSURL URLWithString:[dicRow objectForKey:@"thumbsImg"]];
            NSString* strDate = [dicRow objectForKey:@"releaseDate"];
            NSString* strTitle = [dicRow objectForKey:@"headline"];
            
            NSLog(@"CELL DATA %@ %@ %@",urlImage,strTitle,strDate);

            [cell.lbTitle sizeToFit];
            
            //title
            [cell.imCover setImageWithURL: urlImage];
            [cell.lbDate setText:strDate];
            [cell.lbTitle setText:strTitle];
        }
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // hide row
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    GalleryDetailViewController *gaDetailCon = [[GalleryDetailViewController alloc] initWithNibName:@"GalleryDetailViewController" bundle:nil];
    gaDetailCon.galleryDic =[galleryArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:gaDetailCon];
    
    [gaDetailCon release];
    
}

@end
