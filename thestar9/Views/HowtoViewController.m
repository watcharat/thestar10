//
//  HowtoViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/30/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "HowtoViewController.h"
#import "AppDelegate.h"

@interface HowtoViewController ()

@end

@implementation HowtoViewController

@synthesize webview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // button vote in right navigation
//    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Vote" style:UIBarButtonItemStylePlain target:self action:@selector(votePressed:)];
//    self.navigationItem.rightBarButtonItem = anotherButton;
//    [anotherButton release];

//    [self createVoteButton];
}

-(void)createVoteButton{
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can= be recreated.
}

- (id)init
{
    self = [super init];
    if (self) {
//        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
//        self.title = @"How to";
        
        UIImage* imTitle = [UIImage imageNamed:@"h_subvideo_howto.png"];
        UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
        self.navigationItem.titleView = ivTitle;
        [ivTitle release];
    }
    [self createWebview];
    
    
    
    return self;
}

// vote button pressed
-(IBAction)votePressed:(id)sender{

    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}

-(void)createWebview{
    webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 418)];
    [self.view addSubview:webview];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [self loadDataWebview];
}

-(void)loadDataWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSString* path = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/howToUse.jsp?APP_ID=%@&DEVICE=%@", appDel.app_id, appDel.device];
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:path]]];
    
    NSLog(@"path = %@", path);
}
@end
