//
//  ProfileViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
//#import <FacebookSDK/FacebookSDK.h>

#import "Facebook.h"

@interface ProfileViewController : JWSlideMenuViewController<FBUserSettingsDelegate, UIAlertViewDelegate>{

    IBOutlet UIImageView *imProfile;
    IBOutlet UIImageView *imFanclub;
    IBOutlet UIButton *btnTel;
    IBOutlet UIButton *btnFacebook;
    IBOutlet UIButton *btnTwitter;
    IBOutlet UILabel *lbName;
    BOOL facebookLogined;
    
    
    NSString *strPhoneNum;
    int alertMode;
    IBOutlet UILabel *lbMSISDN;
    NSString *follow_star_id;
}

@property (retain, nonatomic) IBOutlet UIImageView *imProfile;

@property (retain, nonatomic) IBOutlet UIImageView *imFanclub;
@property (retain, nonatomic) IBOutlet UIButton *btnTel;
@property (retain, nonatomic) IBOutlet UIButton *btnFacebook;

@property (retain, nonatomic) IBOutlet UILabel *lbName;
@property (retain, nonatomic) IBOutlet UIButton *btnHowto;
@property (retain, nonatomic) IBOutlet UIButton *btnNotices;

@property (retain, nonatomic) NSString *strPhoneNum;

@property (retain, nonatomic) IBOutlet UILabel *lbMSISDN;

@property (retain, nonatomic)NSString *follow_star_id;;

// share scection
@property (retain, nonatomic) IBOutlet UIImageView *imShareLogo;
@property (retain, nonatomic) IBOutlet UIButton *btnShareFB;
@property (retain, nonatomic) IBOutlet UIButton *btnShareTW;

 
@end
