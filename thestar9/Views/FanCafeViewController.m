//
//  FanCafeViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "FanCafeViewController.h"

@interface FanCafeViewController ()

@end

@implementation FanCafeViewController
@synthesize webview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"FanCafe";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    NSString *strWebURL = @"http://zixboo.gmember.com/FanCafeDev/index.php?artist_code=9999";

    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strWebURL]]];
    
}

-(void)dealloc {
    [webview release];
    [super dealloc];
}

@end
