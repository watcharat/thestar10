//
//  ProfileViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "ProfileViewController.h"
#import "AppDelegate.h"
#import "GDataXMLNode.h"

//#import "ThirdViewController.h"
#import "HowtoViewController.h"
//#import "NoticeViewController.h"
#import "NotiViewController.h"

#import "DBManager.h"
#import "Profile.h"

#import <Social/Social.h>
#import <Twitter/Twitter.h>

#define ALERTMODE_FILLNUMBER 0
#define ALERTMODE_OTP 1

@interface ProfileViewController ()

@end

@implementation ProfileViewController

@synthesize imProfile;
@synthesize imFanclub;
@synthesize btnTel;
@synthesize btnFacebook;

@synthesize btnHowto;
@synthesize btnNotices;

@synthesize lbName;
@synthesize strPhoneNum;

@synthesize lbMSISDN;

@synthesize follow_star_id;

// share section
@synthesize btnShareFB;
@synthesize btnShareTW;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = @"Profile";
    
    // set title 
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-profile.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(sessionStateChanged:)
//                                                 name:SCSessionStateChangedNotification
//                                               object:nil];
    
    
    [self createVoteButton];
    
    
}

-(void)createVoteButton{
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];

}

// vote button pressed
-(IBAction)votePressed:(id)sender{
    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.fakeApple){
        [lbMSISDN setHidden:YES];
        [btnTel setHidden:YES];
    }
    self.follow_star_id = @"";
    [self loadDataAccountDetail];
}

-(void)loadDataAccountDetail{
    
    // check session facebook
//    NSLog(@"FBSession.activeSession = %@", FBSession.activeSession);
    [self populateUserDetails];
//    
//    if (FBSession.activeSession.isOpen) {
//        [self populateUserDetails];
//    }else{
//        NSLog(@"FBSession.activeSession.isClose");
//    }
    
    
    // load by api
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id 
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    
    // call api 
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/getAccountDetailFromMSISDN.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){
        /*
        //test out put
        NSArray *ar = [doc.rootElement elementsForName:@"STATUS"];
        GDataXMLElement *status = [ar objectAtIndex:0]; 
        NSLog(@"STATUS = %@", [status stringValue]);
        */
        
        // render data by xml 
        [self renderData:doc];
        
    }else{
        NSLog(@"[XML ERROR] : %@", [error description] );
        
    }
    
    
    [data release];
    [doc release];
    [url release];
    
    // show text number
    [self showNumber];

}

-(void)renderData:(GDataXMLDocument*)doc{

    // button tel
    [self foundTelephoneNumber];
    
    //image fanclub follow
    [self loadFanClubFollow];
    
}

// load fan club follow
-(void)loadFanClubFollow{
    /*
    DBManager *dbMgr = [[DBManager alloc] init];
    NSMutableArray *ar = [dbMgr fetchProfile:@""];
    if(ar !=Nil && [ar count] > 0){
        // get star_id (follow)
        Profile * profile = [ar objectAtIndex:0];
        NSString *star_id = profile.follow_star_id;
        
        if(star_id!=nil && ![star_id isEqualToString:@""]){
            NSString* imgPath = [NSString stringWithFormat:@"bt_profile%@.png", star_id];

            self.follow_star_id = star_id;
            [imFanclub setImage:[UIImage imageNamed:imgPath]];
        }else{
            self.follow_star_id = @"";
            [imFanclub setImage:[UIImage imageNamed:@"thumb_non_thestar.png"]];
        }
        
    }else{
        [imFanclub setImage:[UIImage imageNamed:@"thumb_non_thestar.png"]];
        self.follow_star_id = @"";
    }
    
    [dbMgr release];
     */
    
    // call api
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    
    // call api
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/getAccountDetailFromEmail.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&EMAIL=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.email,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }else{
//        NSLog(@"data = %@", data);
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){
        NSString* star_id = [[[doc.rootElement elementsForName:@"STAR_ID"] objectAtIndex:0] stringValue];

        if(star_id!=nil && ![star_id isEqualToString:@""]){
            NSString* imgPath = [NSString stringWithFormat:@"bt_profile%@.png", star_id];    
            self.follow_star_id = star_id;
            [imFanclub setImage:[UIImage imageNamed:imgPath]];
        }else{
            self.follow_star_id = @"";
            [imFanclub setImage:[UIImage imageNamed:@"thumb_non_thestar.png"]];
        }
    }
    
    [data release];
    [doc release];

    
}

// button howto 
- (IBAction)btnHowTo:(id)sender {
    HowtoViewController *controller = [[[HowtoViewController alloc] init] autorelease];
    [self.navigationController pushViewController:controller];
    
}
- (IBAction)btnNotice:(id)sender {
//    NoticeViewController *controller = [[[NoticeViewController alloc] init] autorelease];
//    [self.navigationController pushViewController:controller];
    NotiViewController *notCon = [[NotiViewController alloc] initWithNibName:@"NotiViewController" bundle:nil];
    [self presentModalViewController:notCon animated:YES];
    [notCon release];
    

}

- (void)dealloc {
    [imProfile release];
    [imFanclub release];
    [btnTel release];
    [btnFacebook release];

    [btnHowto release];
    [btnNotices release];
    
    [lbName release];
    [imProfile release];
    [lbMSISDN release];
    [_imShareLogo release];
    [btnShareFB release];
    [btnShareTW release];
    [super dealloc];
}

-(void)populateUserDetails{
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 
                 AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
                 
                 NSLog(@"load user profile facebook.");
                 self.lbName.text = user.name;

                 appDel.fullname = user.name;
                 appDel.email = [user objectForKey:@"email"];
                 
                 NSString *fbid = [user objectForKey:@"id"];
                 
                 // change button for logout
                 [btnFacebook setImage:[UIImage imageNamed:@"btn_logout_fb.png"] forState:UIControlStateNormal];
                 
                 NSString *imgPath = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", fbid];
                 NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgPath]];
                 
                 [imProfile setImage:[UIImage imageWithData:dat]];
                 facebookLogined = true;
                 
                 // change button share
                 
                 

             }else{
                 NSLog(@"error ");
             }
         }];
    }else{
        NSLog(@"facebook !FBSession.activeSession.isOpen");
    }
 
}

-(void)viewDidDisappear:(BOOL)animated{
    // update msisdn
    NSLog(@"viewDidDisappear");
    [self updateMsisdnToDB];
}

- (void)sessionStateChanged:(NSNotification*)notification {
    // A more complex app might check the state to see what the appropriate course of
    // action is, but our needs are simple, so just make sure our idea of the session is
    // up to date and repopulate the user's name and picture (which will fail if the session
    // has become invalid).
    [self populateUserDetails];
}
- (void)viewDidUnload {
    [imProfile release];
    imProfile = nil;
    [self setImProfile:nil];
    [imProfile release];
    imProfile = nil;
    [self setImProfile:nil];
    [lbMSISDN release];
    lbMSISDN = nil;
    [self setLbMSISDN:nil];
    [self setImShareLogo:nil];
    [self setBtnShareFB:nil];
    [self setBtnShareTW:nil];
    [super viewDidUnload];
}


- (IBAction)btnFacebookPressed:(id)sender {
    NSLog(@"btn facebook pressed");
    if(facebookLogined){
        NSLog(@"logout facebook");
        // logout

        [FBSession.activeSession closeAndClearTokenInformation];
        [FBSession.activeSession close];
        
        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [appDel.facebook logout];
        
        //clear data
        lbName.text = @"My Profile";
        [imProfile setImage:[UIImage imageNamed:@"thumb_default_profile.png"]];
        [btnFacebook setImage:[UIImage imageNamed:@"btn_connect_fb.png"] forState:UIControlStateNormal];
        facebookLogined = false;
        
        [appDel checkFacebookProfile];
        
        // change image the star fanclub
        [imFanclub setImage:[UIImage imageNamed:@"thumb_non_thestar.png"]];
        
    }else{
        // login
        NSLog(@"login facebook ");

        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [appDel openSessionWithAllowLoginUI:YES];
        [btnFacebook setImage:[UIImage imageNamed:@"btn_logout_fb.png"] forState:UIControlStateNormal];
        
        [self loadDataAccountDetail];
        facebookLogined = true;
    }
    
    [self changeButtonShare];
}


// share button notification
-(void)changeButtonShare{
    if(facebookLogined){
        [btnShareTW setImage:[UIImage imageNamed:@"btn_select_share_tw.png"] forState:UIControlStateNormal];
        [btnShareFB setImage:[UIImage imageNamed:@"btn_select_share_fb.png"] forState:UIControlStateNormal];
    }else{
        [btnShareTW setImage:[UIImage imageNamed:@"btn_select_share_tw_inaction.png"] forState:UIControlStateNormal];
        [btnShareFB setImage:[UIImage imageNamed:@"btn_select_share_fb_inaction.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)btnTelPressed:(id)sender {
    if(![self foundTelephoneNumber]){
        alertMode = ALERTMODE_FILLNUMBER;
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Telephone Number"
                              message: @"กรุณากรอกเบอร์โทรศัพท์"
                              delegate: self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:@"Cancel", nil];
        
//        UITextField *txtPhoneNum = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
//        [alert addSubview:txtPhoneNum];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
        [alert show];
        [alert release];
        
    }else{
        // logout
        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        appDel.msisdn = @"";
        appDel.subscriber = false;
        [btnTel setImage:[UIImage imageNamed:@"btn_LogInTelNo.png"] forState:UIControlStateNormal];
    }
    
    [self showNumber];
}

// --------------------
-(BOOL)foundTelephoneNumber{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSLog(@"msisdn = %@", appDel.msisdn);
    
    if(appDel.msisdn == nil || [appDel.msisdn isEqualToString:@""]){

        //login telephone
        [btnTel setImage:[UIImage imageNamed:@"btn_LogInTelNo.png"] forState:UIControlStateNormal];
        return false;
    }else{
        // logout telephone
        [btnTel setImage:[UIImage imageNamed:@"btn_LogOutTelNo.png"] forState:UIControlStateNormal];
        return true;
    }
}

// alert message
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"alert callback");
    if(alertMode == ALERTMODE_FILLNUMBER){
        if (buttonIndex == 0) {
            NSLog(@"ok button fillnumber");
            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            self.strPhoneNum = [[alertView textFieldAtIndex:0] text];
            
            
            
            //validate phone number 08 to 668
            if ([self.strPhoneNum rangeOfString:@"0"].location == 0){
                self.strPhoneNum = [NSString stringWithFormat:@"66%@", [self.strPhoneNum substringFromIndex:1]];
                
            }
            NSLog(@"phone num = %@", self.strPhoneNum);

            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/sendOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                                 appDel.app_id,
                                 appDel.device,
                                 appDel.charset,
                                 self.strPhoneNum,
                                 appDel.appversion,
                                 appDel.apiversion
                                 ];
            
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            // connect api for return xml
            NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
            if(data==nil){
                NSLog(@"[Connection ERROR]:%@", [error description]);
                return;
            }
            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:&error];
            if(doc){
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                // -----
                
                if([status_code isEqualToString:@"200"]){
                    alertMode = ALERTMODE_OTP;
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle: @"OTP Number"
                                          message: @"กรุณากรอกรหัส OTP จาก SMS"
                                          delegate: self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:@"Cancel", nil];
                    
                    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
                    [alert show];
                    [alert release];

                }                
            }
            [data release];
            [doc release];
            [url release];
        }
        else {
            NSLog(@"cancel button fillnumber");
        }
    }else if(alertMode== ALERTMODE_OTP){
        if (buttonIndex == 0) {

            NSLog(@"ok button fillnumber");
            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            NSString *txtOTP = [[alertView textFieldAtIndex:0] text];
            
             NSLog(@"phone num = %@", self.strPhoneNum);
            
            // send otp
            NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/validateOTP.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&ONETIMEPASS=%@&APPVERSION=%@&APIVERSION=%@",
                                 appDel.app_id,
                                 appDel.device,
                                 appDel.charset,
                                 self.strPhoneNum,
                                 txtOTP,
                                 appDel.appversion,
                                 appDel.apiversion
                                 ];
            NSLog(@"CALL API : %@", urlLink);
            NSURL *url = [[NSURL alloc] initWithString:urlLink];
            NSError *error;
            
            // connect api for return xml
            NSString* data = [[[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error] autorelease];

            //parse xml
            GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                        options:0 error:&error];
            
            if(doc){
                
                NSArray *ar = [doc.rootElement elementsForName:@"STATUS_CODE"];
                NSString *status_code = [[ar objectAtIndex:0] stringValue];
                //
                if([status_code isEqualToString:@"200"]){

                    // update number
                    appDel.msisdn = self.strPhoneNum;

                    [btnTel setImage:[UIImage imageNamed:@"btn_LogOutTelNo.png"] forState:UIControlStateNormal];
                    [self showNumber];
                    
                    
                    //update msisdn to database
//                    [self updateMsisdnToDB];
                }else{
                    NSLog(@"error status_code = %@", status_code);
                }
            }
            
            [doc release];
            [url release];
        }
        else {
            NSLog(@"cancel button fill number");
        }
    }  
}

// ------------------------------
-(void)showNumber{
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString* telPhone = appDel.msisdn;
    

    // filter phone number
    if(telPhone.length > 0){
        NSString* chk2Num = [telPhone substringToIndex:2];
        if([chk2Num isEqualToString:@"66"]){
            NSString* tel = [telPhone substringFromIndex:2];
            NSString* telFinal = [NSString stringWithFormat:@"0%@", tel];
//            NSString* telfinal = [NSString stringWithFormat:@"%@xx", [telFinal substringToIndex:8] ];
            
            
            lbMSISDN.text = telFinal;
        }
    }else{
        lbMSISDN.text = @"";
    }
    
}

// --
-(void)updateMsisdnToDB{
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    // load msisdn from database
    DBManager *dbMgr = [[DBManager alloc] init];
    
    NSMutableArray *ar = [dbMgr fetchProfile:@""];
    if(ar !=nil && [ar count] > 0){
        NSLog(@"update msisdn : %@", appDel.msisdn);
        Profile *profile = [ar objectAtIndex:0];
        profile.msisdn = appDel.msisdn;
        [dbMgr updateProfile:profile];
        //
        [ar release];
    }else{
        NSLog(@"insert msisdn : %@", appDel.msisdn);
        Profile *newProfile = [dbMgr getSchemaProfile];
        newProfile.msisdn = self.strPhoneNum;
        [dbMgr insertProfile:newProfile];
        
    }
    [dbMgr release];
}

- (IBAction)btnShareFacebookPressed:(id)sender {
    NSLog(@"share facebook pressed");
    
    NSMutableDictionary *dicName = [[NSMutableDictionary alloc] init];
    
    [dicName setValue:@"The Star 9 Fanclub ตัวจริงเสียงจริง : No.1 ดี" forKey:@"1"];
    [dicName setValue:@"The Star 9 Fanclub ตัวจริงเสียงจริง : No.2 อ้น" forKey:@"2"];
    [dicName setValue:@"The Star 9 Fanclub ตัวจริงเสียงจริง : No.3 บูรณ์" forKey:@"3"];
    [dicName setValue:@"The Star 9 Fanclub ตัวจริงเสียงจริง : No.4 คริส" forKey:@"4"];
    [dicName setValue:@"The Star 9 Fanclub ตัวจริงเสียงจริง : No.5 แบมบี้" forKey:@"5"];
    [dicName setValue:@"The Star 9 Fanclub ตัวจริงเสียงจริง : No.6 เชอรีน" forKey:@"6"];
    [dicName setValue:@"The Star 9 Fanclub ตัวจริงเสียงจริง : No.7 ดิว" forKey:@"7"];
    [dicName setValue:@"The Star 9 Fanclub ตัวจริงเสียงจริง : No.8 ตั้ม" forKey:@"8"];

    
    if(![self.follow_star_id isEqualToString:@""]){
        
        NSString* strImageShare = @"http://thestar9.gmmwireless.com/thestar9/mobilesite/share/images/ts_1.png";
        NSString* linkURL = @"http://thestar9.gmmwireless.com/thestar9/mobilesite/index.php/store";
        NSString* caption = @"ฝากเชียร์ ฝากโหวต ให้น้องด้วยนะ ไปดูวิธีการโหวตกันได้ที่  http://thestar.gmember.com";
        NSString* name = @"No.0 ข่า";
        NSString* desc = @"for iPhone and  Android - Avaiable in the App Store";
        
        // filter data
        strImageShare = [NSString stringWithFormat:@"http://files.shopping8000.com/images/all/images/thestar9/ts_%@.jpg", self.follow_star_id];
//        strImageShare = [dicImage objectForKey:self.follow_star_id];
        name = [dicName objectForKey:self.follow_star_id];
        NSLog(@"name = %@, share image facebook > = %@", name, strImageShare);
        
        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [appDel shareDataToFBByLinkURL:linkURL imageURL:strImageShare name:name caption:caption description:desc];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Warrning"
                              message: @"กรุณาเลือกเป็นแฟนคลับในหน้า The Star Profile ก่อนนะคะ"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:@"Cancel", nil];
        
        
        [alert show];
        [alert release];
    }
    [dicName release];
    
}
// 
- (IBAction)btnTwitterPressed:(id)sender {
    
    if([self.follow_star_id isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Warrning"
                              message: @"กรุณาเลือกเป็นแฟนคลับในหน้า The Star Profile ก่อนนะคะ"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:@"Cancel", nil];
        
        
        [alert show];
        [alert release];
        return;
    }
    
    NSMutableDictionary *dicName = [[NSMutableDictionary alloc] init];
    
    [dicName setValue:@"The Star 9  Fanclub : No.1 ดี" forKey:@"1"];
    [dicName setValue:@"The Star 9  Fanclub : No.2 อ้น" forKey:@"2"];
    [dicName setValue:@"The Star 9  Fanclub : No.3 บูรณ์" forKey:@"3"];
    [dicName setValue:@"The Star 9  Fanclub : No.4 คริส" forKey:@"4"];
    [dicName setValue:@"The Star 9  Fanclub : No.5 แบมบี้" forKey:@"5"];
    [dicName setValue:@"The Star 9  Fanclub : No.6 เชอรีน" forKey:@"6"];
    [dicName setValue:@"The Star 9  Fanclub : No.7 ดิว" forKey:@"7"];
    [dicName setValue:@"The Star 9  Fanclub : No.8 ตั้ม" forKey:@"8"];
    
    NSString* strTwitter = [NSString stringWithFormat:@"%@ Download App http://goo.gl/5TSJM", [dicName objectForKey:self.follow_star_id] ];
    
    // share twitter
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet =
        [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:strTwitter];
        [self presentModalViewController:tweetSheet animated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure                                   your device has an internet connection and you have                                   at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//    {
//        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//        [tweetSheet setInitialText:strTwitter];
//        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
//    else
//    {
//        UIAlertView *alertView = [[UIAlertView alloc]
//                                  initWithTitle:@"ผิดพลาด"
//                                  message:@"iOS ของท่านไม่ support twitter"
//                                  delegate:nil
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil];
//        [alertView show];
//        [alertView release];
//    }
    
    [dicName release];
}


@end
