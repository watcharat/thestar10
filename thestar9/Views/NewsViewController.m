//
//  NewsViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "NewsViewController.h"

#import "AppDelegate.h"

@interface NewsViewController ()

@end

@implementation NewsViewController
@synthesize webview;
@synthesize msisdnUtil;
@synthesize btnBanner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = @"News";

    // set title image
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-news.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    [self createVoteButton];
    [self loadBanner];
}

-(void)loadBanner{
    NSString* urlPath = @"http://thestar9.gmmwireless.com/thestar9/image/baner/pack/640x100.jpg";
    NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPath]];
    [btnBanner setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
}

 
-(void)createVoteButton{
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
    
}

// vote button pressed
-(IBAction)votePressed:(id)sender{
    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}


-(void)viewWillAppear:(BOOL)animated{
    //load webview news
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
//    NSString* strWebURL = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/news.jsp?APP_ID=%@&DEVICE=%@",
//                           appDel.app_id, appDel.device];
    
    
    
    NSString* strWebURL = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/mobilesite/index.php/news?fbid=%@&accessToken=%@",
                           appDel.fb_id,
                           appDel.accessToken
                           ];
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strWebURL]]];
    
    //--------- m ------------//

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [webview release];
    [msisdnUtil release];
    [btnBanner release];
    [super dealloc];
}
// ---------
- (IBAction)btnBannerPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@"appDel.msisdn = %@, count = %d",appDel.msisdn, [appDel.msisdn length] );
    if(appDel.msisdn ==nil || [appDel.msisdn isEqualToString:@""]){
        [self checkMSISDN];
    }else{
        [self goOutBuyWebview];
    }
}

-(void)checkMSISDN{
    
    if(self.msisdnUtil ==nil){
        self.msisdnUtil = [[MSISDNUtil alloc] init];
        self.msisdnUtil.delegate = self;
    }
    [self.msisdnUtil alertshow];
    
    
}
// delegate
-(void) receptOTPSuccess:(NSString*)msisdn{
    NSLog(@"OTP success");
    [self goOutBuyWebview];
}

-(void) receptOTPFails:(NSString*)status_code status_message:(NSString*)status_message{
    NSLog(@"OTP fails");
}

-(void)goOutBuyWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=&GMMD_CODE=", appDel.app_id, appDel.device, appDel.msisdn];
    
    NSLog(@"go out to charging: %@", strPath);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
}



- (void)viewDidUnload {
    [self setBtnBanner:nil];
    [btnBanner release];
    btnBanner = nil;
    [super viewDidUnload];
}
@end
