//
//  ListShelfDownloadController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/24/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#define SHELF_MUSIC     1
#define SHELF_SPECIAL   2
#define SHELF_MV        3

#import "ListShelfDownloadController.h"

#import "ListShelfDownloadCell.h"

#import "AppDelegate.h"
#import "GDataXMLNode.h"

#import "MusicPlayerController.h"

#import "DBManager.h"
#import "Song.h"

@interface ListShelfDownloadController ()

@end

@implementation ListShelfDownloadController

@synthesize shelftype;
@synthesize tbMain;

@synthesize arDataTable;

@synthesize spinner;
@synthesize arListDB;

@synthesize loadAPI;
@synthesize msisdnUtil;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.loadAPI = false;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// other
- (id)init
{
    self = [super init];
    if (self) {
        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
        [self.view setBackgroundColor:[UIColor blackColor]];
//        self.title = @"Video Detail";
        
        [self createUI];
    }
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDel.tableMusicDownload = self.tbMain;
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [self.spinner setHidden:NO];
    [self.spinner startAnimating];
    
    if(self.loadAPI==false){
        self.loadAPI = true;
        [self callShelfDownloadAPI];
    }
    //[self.spinner stopAnimating];
    [self.spinner setHidden:YES];
}


// create ui
-(void)createUI{
    //create banner
//    UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"banner_thestar9.png"]];
//    UIImage *image = [UIImage imageNamed:@"banner_pack.jpg"];
    
    NSString* urlPath = @"http://thestar9.gmmwireless.com/thestar9/image/baner/pack/640x100.jpg";
    NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPath]];
    
    UIImage *im = [UIImage imageWithData:dat];
    UIButton *btnBanner = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBanner setBackgroundImage:im forState:UIControlStateNormal];
    [btnBanner addTarget:self action:@selector(btnBannerPressed:) forControlEvents:UIControlEventTouchUpInside];
    btnBanner.frame = CGRectMake(0.0, 0.0, 320, 50);
    
    [self.view addSubview:btnBanner];
//    [im release];
    
    //create table
    if(tbMain == nil){
        
        tbMain = [[UITableView alloc] initWithFrame:CGRectMake(0, 51, 320, 480-51-44-20) style:UITableViewStylePlain];
        tbMain.delegate = self;
        tbMain.dataSource = self;
        tbMain.backgroundColor = [UIColor clearColor];
        tbMain.separatorColor = [UIColor clearColor];
        [self.view addSubview:tbMain];
    }
    
    // create spinner
    float kScreenWidth = 320;
    float kScreenHeight = 480-40;

    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.spinner setCenter:CGPointMake(kScreenWidth/2.0, kScreenHeight/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.spinner setHidden:NO];
    [self.spinner startAnimating];
}

// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
- (IBAction)btnBannerPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@"appDel.msisdn = %@, count = %d",appDel.msisdn, [appDel.msisdn length] );
    if(appDel.msisdn ==nil || [appDel.msisdn isEqualToString:@""]){
        [self checkMSISDN];
    }else{
        [self goOutBuyWebview];
    }
}


-(void)checkMSISDN{
    
    if(self.msisdnUtil ==nil){
        self.msisdnUtil = [[MSISDNUtil alloc] init];
        self.msisdnUtil.delegate = self;
    }
    [self.msisdnUtil alertshow];
    
    
}
// delegate
-(void) receptOTPSuccess:(NSString*)msisdn{
    NSLog(@"OTP success");
    [self goOutBuyWebview];
}

-(void) receptOTPFails:(NSString*)status_code status_message:(NSString*)status_message{
    NSLog(@"OTP fails");
}


-(void)goOutBuyWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=&GMMD_CODE=", appDel.app_id, appDel.device, appDel.msisdn];
    
    NSLog(@"go out to charging: %@", strPath);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
}



-(void)callShelfDownloadAPI{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    
    
    
    NSString *urlLink;
    // call api 
    if(self.shelftype == SHELF_MUSIC){
        
        //check load first file
        if(appDel.arShelfMusic !=nil){
            self.arDataTable = appDel.arShelfMusic;
            [self.tbMain reloadData];
            return;
        }
        
        // gen new link
        urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/shelfFullSong.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&PAGE=&REC=&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    }else if(self.shelftype == SHELF_MV){
        //check load first file
        if(appDel.arShelfMV !=nil){
            self.arDataTable = appDel.arShelfMV;
            [self.tbMain reloadData];
            return;
        }
        
        // gen new link
        urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/shelfMV.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&PAGE=&REC=&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                   appDel.app_id,
                   appDel.device,
                   appDel.charset,
                   appDel.msisdn,
                   appDel.appversion,
                   appDel.apiversion
                   ];
    }else if(self.shelftype== SHELF_SPECIAL){
        
        //check load first file
        if(appDel.arShelfSpecialClip !=nil){
            self.arDataTable = appDel.arShelfSpecialClip;
            [self.tbMain reloadData];
            return;
        }
        
        // gen new link
        urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/shelfSpecialClip.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&PAGE=&REC=&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                   appDel.app_id,
                   appDel.device,
                   appDel.charset,
                   appDel.msisdn,
                   appDel.appversion,
                   appDel.apiversion
                   ];
    }else{
        urlLink = @"";
    }
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){        
        // render data by xml
        [self storeData:doc];
        
    }else{
        NSLog(@"[XML ERROR] : %@", [error description] );
    }
    
    [data release];
    [doc release];
    [url release];
}

-(void)storeData:(GDataXMLDocument*)doc{
    //clear data store
    if(arDataTable == nil){
        arDataTable = [[NSMutableArray alloc] init];
        
    }else{
        [arDataTable removeAllObjects];
    }
    NSArray *ar = [doc.rootElement elementsForName:@"Content"];
    
    for(int i=0; i< [ar count]; i++){
        GDataXMLElement *element = [ar objectAtIndex:i];

        NSString* contentCode = [[[element elementsForName:@"ContentCode"] objectAtIndex:0] stringValue];
        NSString* gmmdCode = [[[element elementsForName:@"GMMDCode"] objectAtIndex:0] stringValue];
        NSString* songnameEn = [[[element elementsForName:@"SongNameEN"] objectAtIndex:0] stringValue];
        NSString* songnameTh = [[[element elementsForName:@"SongNameTH"] objectAtIndex:0] stringValue];
        NSString* thmCover = [[[element elementsForName:@"thmCover"] objectAtIndex:0] stringValue];
        NSString* cover = [[[element elementsForName:@"cover"] objectAtIndex:0] stringValue];
        
        NSString* albumTh = [[[element elementsForName:@"AlbumTH"] objectAtIndex:0] stringValue];
        NSString* artistEn = [[[element elementsForName:@"ArtistEN"] objectAtIndex:0] stringValue];
        NSString* arthistTH = [[[element elementsForName:@"ArtistTH"] objectAtIndex:0] stringValue];
        
        NSString* preview = [[[element elementsForName:@"preview"] objectAtIndex:0] stringValue];
        
        NSMutableDictionary *dicDataRow = [[NSMutableDictionary alloc] init];
        
        [dicDataRow setValue:contentCode forKey:@"ContentCode"];
        [dicDataRow setValue:gmmdCode forKey:@"GMMDCode"];
        [dicDataRow setValue:songnameEn forKey:@"SongNameEN"];
        [dicDataRow setValue:songnameTh forKey:@"SongNameTH"];

        // image
//        NSData *datcover = [NSData dataWithContentsOfURL:[NSURL URLWithString:cover]];
        NSData *datThmCover = [NSData dataWithContentsOfURL:[NSURL URLWithString:thmCover]];
        [dicDataRow setValue:thmCover forKey:@"thmCover"];
        [dicDataRow setValue:datThmCover forKey:@"datThmCover"];
        [dicDataRow setValue:cover forKey:@"cover"];
//        [dicDataRow setValue:datcover forKey:@"datcover"];
        
        [dicDataRow setValue:albumTh forKey:@"AlbumTH"];
        [dicDataRow setValue:artistEn forKey:@"ArtistEN"];
        [dicDataRow setValue:arthistTH forKey:@"ArtistTH"];
        [dicDataRow setValue:preview forKey:@"preview"];
        
//        NSLog(@"contentCode = %@",contentCode);
        
        [arDataTable addObject:dicDataRow];
        [dicDataRow release];
    }
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(self.shelftype == SHELF_MUSIC){
        appDel.arShelfMusic = arDataTable;
    }else if(self.shelftype == SHELF_MV){
        appDel.arShelfMV = arDataTable;
    }else if(self.shelftype == SHELF_SPECIAL){
        appDel.arShelfSpecialClip = arDataTable;
    }
    
    
    NSLog(@"data completed");
    [tbMain reloadData];
}


-(void)loadDatabaseList{
    NSLog(@"load database");
    // clear
    if(arListDB == nil){
        arListDB = [[NSMutableArray alloc] init];
    }else{
        [arListDB removeAllObjects];
    }
    DBManager *dbMgr = [[DBManager alloc] init];
    
    if(shelftype == SHELF_MUSIC){
        NSMutableArray* ar = [dbMgr fetchSong:@""];
        
        //fetch
        for(int i=0; i< [ar count] ; i++){
            Song *song = (Song*)[ar objectAtIndex:i];
            [arListDB addObject:song.gmmd_code];
        }
        
    }else if(shelftype == SHELF_MV){
        NSMutableArray* ar = [dbMgr fetchMV:@""];
        
        //fetch
        for(int i=0; i< [ar count] ; i++){
            MV *mv = (MV*)[ar objectAtIndex:i];
            [arListDB addObject:mv.gmmd_code];
        }   
    }else if(shelftype == SHELF_SPECIAL){
        NSMutableArray* ar = [dbMgr fetchSpecialClip:@""];
        
        //fetch
        for(int i=0; i< [ar count] ; i++){
            SpecialClip *sp = (SpecialClip*)[ar objectAtIndex:i];
            [arListDB addObject:sp.gmmd_code];

        }
    }
    [dbMgr release];
    
    
}

// dealloc
-(void)dealloc{
    [tbMain release];
    [arDataTable release];
    
    [arListDB release];
    
    [super dealloc];
}


// ---------------------------------------------------------
// delegate cell table
// ---------------------------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    NSLog(@"numberOfSectionsInTableView");
    [self loadDatabaseList];
    return 1;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
//    NSLog(@"numberOfRowsInSection");
    return [arDataTable count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"heightForRowAtIndexPath");
    if(shelftype == SHELF_MUSIC){
        return 65;
    }else if(shelftype == SHELF_MV){
        return 65;
    }else{
        return 244;
    }

}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
//    NSLog(@"cellForRowAtIndexPath");
    
    ListShelfDownloadCell *cell = (ListShelfDownloadCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil){
        // load xib file
        NSArray *nib;
        if(shelftype == SHELF_MUSIC){
            nib = [[NSBundle mainBundle] loadNibNamed:@"ListShelfDownloadCell" owner:self options:nil];
        }else if(shelftype == SHELF_MV){
            nib = [[NSBundle mainBundle] loadNibNamed:@"ListShelfMVCell" owner:self options:nil];
        }else{
            nib = [[NSBundle mainBundle] loadNibNamed:@"ListShelfSpecialClip" owner:self options:nil];
        }
        cell = [nib objectAtIndex:0];
    }
    
    if(arDataTable !=nil){
        NSDictionary *dicRow = (NSDictionary*)[arDataTable objectAtIndex:indexPath.row];
        
        if(dicRow!=nil){
            
            //set title
            NSData *datImage = [dicRow objectForKey:@"datThmCover"];
            NSString* strDesc = [dicRow objectForKey:@"ArtistEN"];
            NSString* strTitle = [dicRow objectForKey:@"SongNameTH"];
            NSString* strContentCode = [dicRow objectForKey:@"ContentCode"];
            NSString* strGMMDCode = [dicRow objectForKey:@"GMMDCode"];
            
            
            //title
            if(datImage != nil){
                [cell.imThumb setImage:[UIImage imageWithData:datImage]];
                cell.datCover = datImage;
            }
            [cell.lbDate setText:@""];
            [cell.lbDesc setText:strDesc];
            [cell.lbTitle setText:strTitle];
            cell.contentCode = strContentCode;
            cell.gmmdCode = strGMMDCode;
            
            // Service_id & download type
            // FIX STRING "SONG", "MV", "SPECIALCLIP" for DownloadManager.m
            // for save to another table follow fix type string
            //
            
            if(shelftype == SHELF_MUSIC){
                cell.service_id = @"1";
                cell.downloadtype = @"SONG";
            }else if(shelftype == SHELF_MV){
                cell.service_id = @"2";
                cell.downloadtype = @"MV";
            }else if(shelftype == SHELF_SPECIAL){
                cell.service_id = @"3";
                cell.downloadtype = @"SPECIALCLIP";
            }
            
            // check click download available
            BOOL downloaded = false;
            for(int i=0; i< [arListDB count]; i++){
                NSString* gmmd_code = [arListDB objectAtIndex:i];
                if([gmmd_code isEqualToString:strGMMDCode]){
                    downloaded = true;
                    i = [arListDB count];
                }
            }
            
            if(downloaded){
                [cell.btnDownload setHidden:YES];
            }
            
            // progress bar set Height
            if(shelftype == SHELF_SPECIAL){
                [cell.pgbarView setFrame:CGRectMake(0, 0, 320, 4)];
            }
            
            // check in queue
            BOOL inQueue = false;
            
            NSString* keycode = [NSString stringWithFormat:@"%@%@",strGMMDCode, cell.service_id];
            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
            NSMutableArray* ar = [appDel.downloadMgr arDownloadList];
            for(int i=0; i < [ar count]; i++){
                NSMutableDictionary *dic = [ar objectAtIndex:i];
                if([[dic objectForKey:@"KEYCODE"] isEqualToString:keycode]){
                    inQueue = true;
                    i = [ar count];
                }
            }

//            AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
//            NSMutableArray* ar = [appDel.downloadMgr arDownloadList];
//            for(int i=0; i < [ar count]; i++){
//                NSMutableDictionary *dic = [ar objectAtIndex:i];
//                if([[dic objectForKey:@"GMMD_CODE"] isEqualToString:strGMMDCode]){
//                    inQueue = true;
//                    i = [ar count];
//                }
//            }
        
            if(inQueue){
                
                if ([[[[appDel.downloadMgr arDownloadList] objectAtIndex:0] objectForKey:@"GMMD_CODE"] isEqualToString:strGMMDCode]) {
                    [appDel.downloadMgr.request setDownloadProgressDelegate:cell.pgbarView];
                }
                
                [cell.pgbarView setHidden:NO];
                [cell.btnDownload setHidden:YES];
                
                // Set height progress
                [cell.pgbarView setFrame:CGRectMake(0, 61, 320, 2)];
                cell.pgbarView.progress = 0.0;
            }
        
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    // preview
    if(shelftype == SHELF_MUSIC){
        NSDictionary *dicRow = (NSDictionary*)[arDataTable objectAtIndex:indexPath.row];

        if(dicRow!=nil){

            MusicPlayerController *musicPlayerCon = [[MusicPlayerController alloc] init];

            //[musicPlayerCon setDataPlayerArray:arDataTable seq:indexPath.row];
//            NSLog(@"dat 1= %@", arDataTable);
//            NSLog(@"dat 2= %@", arListDB);
            
            [musicPlayerCon loadDataPlayer:arDataTable listDownloaded:arListDB seq:indexPath.row];
            
            [self presentModalViewController:musicPlayerCon animated:YES];
            [musicPlayerCon release];
        }else{
            NSLog(@"data row fails");
        }
    }
}

@end
