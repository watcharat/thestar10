//
//  NoticeViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/30/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "NoticeViewController.h"

#import "AppDelegate.h"

@interface NoticeViewController ()

@end

@implementation NoticeViewController
@synthesize webview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    [self createBackButton];
    // button vote in right navigation
//    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Vote" style:UIBarButtonItemStylePlain target:self action:@selector(votePressed:)];
//    self.navigationItem.rightBarButtonItem = anotherButton;
//    [anotherButton release];
    
    
//    [self createVoteButton];
}

-(void)createVoteButton{
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
    
}

// vote button pressed
-(IBAction)votePressed:(id)sender{
    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)init
{
    self = [super init];
    if (self) {
//        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
//        self.title = @"Notice";
        
        UIImage* imTitle = [UIImage imageNamed:@"h_subvideo_notice.png"];
        UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
        self.navigationItem.titleView = ivTitle;
        [ivTitle release];
    }
    [self createWebview];
    return self;
}

-(void)createWebview{   
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* path = [NSString stringWithFormat:@"http://gmmnotification.gmmwireless.com/feed/index.jsp?APP_ID=%@&DEVICE=%@", appDel.app_id, appDel.device];
    webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 418)];
    
    NSLog(@"path = %@", path);
    
    [self.view addSubview:webview];
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:path]]];
}

// create backp
/*
-(void)createBackButton{
    UIImage *buttonImage = [UIImage imageNamed:@"qr_btn_subback.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    
    
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    [customBarItem release];
    
}

-(void)back {
//    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popViewController];
}

 */
@end
