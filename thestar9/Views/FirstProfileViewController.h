//
//  FirstProfileViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/3/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

@interface FirstProfileViewController : JWSlideMenuViewController{
    int number;
    UIImageView *imProfile;
    UIButton *btnFanclub;
    
    BOOL followed;
    
}

@property (readwrite, assign)int number;
@property (readwrite, assign)BOOL followed;

@property (retain, nonatomic)UIImageView *imProfile;

@property (retain, nonatomic)UIButton *btnFanclub;


@end
