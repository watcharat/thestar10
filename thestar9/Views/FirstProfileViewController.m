//
//  FirstProfileViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/3/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "FirstProfileViewController.h"
#import "SecondProfileViewController.h"

#import "DBManager.h"

#import "TheStarProfile.h"
#import "AppDelegate.h"
#import "GDataXMLNode.h"

#import <Social/Social.h>
#import <Twitter/Twitter.h>

@interface FirstProfileViewController ()

@end

@implementation FirstProfileViewController
@synthesize number;
@synthesize followed;
@synthesize imProfile;
@synthesize btnFanclub;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // create header
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-starprofile.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
//    [self loadDataFirstProfile];
}
-(void)viewWillAppear:(BOOL)animated{
    
//    NSString *strHeadFile = [NSString stringWithFormat:@"txt_no%d.png", self.number];
//    UIImage* imTitle = [UIImage imageNamed:strHeadFile];
//    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
//    self.navigationItem.titleView = ivTitle;
//    [ivTitle release];
    [self loadImageTheProfile];
    [self checkFollowStarId:self.number];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
- (id)init
{
    self = [super init];
    if (self) {
        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
        [self.view setBackgroundColor:[UIColor blackColor]];
        //        self.title = @"Video Detail";
        
        [self createUI];

        
    }
    
    return self;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-(void)createUI{
    NSLog(@"create ui");
    
    // immage profile
    if(imProfile==nil){
        CGRect rec = CGRectMake(0, 0, 320, 480);
        imProfile = [[UIImageView alloc] initWithFrame:rec];
        [imProfile setFrame:rec];
        [self.view addSubview:imProfile];
    }
    // button fanclub
    
    self.btnFanclub = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnFanclub setFrame:CGRectMake(0, 420-32-48, 160, 32)];
    [self.btnFanclub setBackgroundImage:[UIImage imageNamed:@"bt_fanclub_off.png"] forState:UIControlStateNormal];
    [self.btnFanclub addTarget:self action:@selector(btnFanclubPressed:)
        forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.btnFanclub];
    
    // button to next detail
    UIButton *btnDetail = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDetail setFrame:CGRectMake(160, 420-32-48, 160, 32)];
    [btnDetail setBackgroundImage:[UIImage imageNamed:@"bt_profile_off.png"] forState:UIControlStateNormal];
    [btnDetail addTarget:self action:@selector(btnProfilePressed:)
        forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDetail];
    
    // share section
    // -x-x-x-x-
    UIImageView *imBgShareView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_share_bottom.jpg"]];
    imBgShareView.frame = CGRectMake(0, 420-48, 320, 48);
    [self.view addSubview:imBgShareView];
    [imBgShareView release];
    
    
    UIImageView *imSymbolShare = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"im_share.png"]];
    imSymbolShare.frame = CGRectMake(10, 420-48+10, 68, 28);
    [self.view addSubview:imSymbolShare];
    [imSymbolShare release];
    
    //button share twitter
    UIButton *btnShareTwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnShareTwitter setFrame:CGRectMake(90, 420-48+14, 100, 20)];
    [btnShareTwitter setBackgroundImage:[UIImage imageNamed:@"btn_share_comment_tw.png"] forState:UIControlStateNormal];
    [btnShareTwitter addTarget:self action:@selector(btnShareTwitterPressed:)
        forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnShareTwitter];
    
    
    //button share facebook
    UIButton *btnShareFacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnShareFacebook setFrame:CGRectMake(200, 420-48+14, 111, 20)];
    [btnShareFacebook setBackgroundImage:[UIImage imageNamed:@"btn_share_comment_fb.png"] forState:UIControlStateNormal];
    [btnShareFacebook addTarget:self action:@selector(btnShareFacebookPressed:)
        forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnShareFacebook];
    
}

// share button event
-(IBAction)btnShareTwitterPressed:(id)sender{
    
    NSMutableArray *arName = [[NSMutableArray alloc] init];
    
    [arName addObject:@""];
    [arName addObject:@"The Star 9  ตัวจริงเสียงจริง : No.1 ดี"];
    [arName addObject:@"The Star 9  ตัวจริงเสียงจริง : No.2 อ้น"];
    [arName addObject:@"The Star 9  ตัวจริงเสียงจริง : No.3 บูรณ์"];
    [arName addObject:@"The Star 9  ตัวจริงเสียงจริง : No.4 คริส"];
    [arName addObject:@"The Star 9  ตัวจริงเสียงจริง : No.5 แบมบี้"];
    [arName addObject:@"The Star 9  ตัวจริงเสียงจริง : No.6 เชอรีน"];
    [arName addObject:@"The Star 9  ตัวจริงเสียงจริง : No.7 ดิว"];
    [arName addObject:@"The Star 9  ตัวจริงเสียงจริง : No.8 ตั้ม"];
    
    NSString* strTwitter = [NSString stringWithFormat:@"%@ รู้ลึกรู้จริงกับประวัติน้องๆ The Star9. Download App http://goo.gl/5TSJM", [arName objectAtIndex:self.number] ];
    // share twitter
    if ([TWTweetComposeViewController canSendTweet])
    {
        TWTweetComposeViewController *tweetSheet =
        [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:strTwitter];
        [self presentModalViewController:tweetSheet animated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure                                   your device has an internet connection and you have                                   at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//    {
//        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//        [tweetSheet setInitialText:strTwitter];
//        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
//    else
//    {
//        UIAlertView *alertView = [[UIAlertView alloc]
//                                  initWithTitle:@"ผิดพลาด"
//                                  message:@"iOS ของท่านไม่ support twitter"
//                                  delegate:nil
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil];
//        [alertView show];
//        [alertView release];
//    }
    [arName release];

    
}
-(IBAction)btnShareFacebookPressed:(id)sender{
    NSMutableArray *arName = [[NSMutableArray alloc] init];
    
    [arName addObject:@""];
    [arName addObject:@"The Star 9 ตัวจริงเสียงจริง : No.1 ดี"];
    [arName addObject:@"The Star 9 ตัวจริงเสียงจริง : No.2 อ้น"];
    [arName addObject:@"The Star 9 ตัวจริงเสียงจริง : No.3 บูรณ์"];
    [arName addObject:@"The Star 9 ตัวจริงเสียงจริง : No.4 คริส"];
    [arName addObject:@"The Star 9 ตัวจริงเสียงจริง : No.5 แบมบี้"];
    [arName addObject:@"The Star 9 ตัวจริงเสียงจริง : No.6 เชอรีน"];
    [arName addObject:@"The Star 9 ตัวจริงเสียงจริง : No.7 ดิว"];
    [arName addObject:@"The Star 9 ตัวจริงเสียงจริง : No.8 ตั้ม"];
    
    NSString* strImageShare = @"http://thestar9.gmmwireless.com/thestar9/mobilesite/share/images/ts_1.png";
    NSString* linkURL = @"http://thestar9.gmmwireless.com/thestar9/mobilesite/index.php/store";
    NSString* caption = @"รู้ลึกรู้จริงกับประวัติน้องๆThe Star9 เข้าไปดูได้เลยที่ thestar.gmember.com";
    NSString* name = @"No.0 ข่า";
    NSString* desc = @"for iPhone and  Android - Avaiable in the App Store";
    
    // filter data
    strImageShare = [NSString stringWithFormat:@"http://files.shopping8000.com/images/all/images/thestar9/ts_%d.jpg", self.number];
    //        strImageShare = [dicImage objectForKey:self.follow_star_id];
    name = [arName objectAtIndex:self.number];
//    NSLog(@"name = %@, share image facebook > = %@", name, strImageShare);
    
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel shareDataToFBByLinkURL:linkURL imageURL:strImageShare name:name caption:caption description:desc];
    
    [arName release];
}

//------
-(void)checkFollowStarId:(int)star_id{

    DBManager *dbMgr = [[DBManager alloc] init];
    NSMutableArray *ar = [dbMgr fetchProfile:@""];
    Profile *profile;
    if(ar !=Nil && [ar count] > 0){
        // update
        profile = [ar objectAtIndex:0];
        NSString* starid = [NSString stringWithFormat:@"%d", star_id];
        if([profile.follow_star_id isEqualToString:starid]){
            [self.btnFanclub setBackgroundImage:[UIImage imageNamed:@"bt_fanclub_on.png"] forState:UIControlStateNormal];
            self.followed = true;
        }else{
            [self.btnFanclub setBackgroundImage:[UIImage imageNamed:@"bt_fanclub_off.png"] forState:UIControlStateNormal];
            self.followed = false;
        }
    }else{
    
        [self.btnFanclub setBackgroundImage:[UIImage imageNamed:@"bt_fanclub_off.png"] forState:UIControlStateNormal];
        self.followed = false;
        
    }
    
    [dbMgr release];
}

-(IBAction)btnFanclubPressed:(id)sender{
    
    DBManager *dbMgr = [[DBManager alloc] init];
    NSMutableArray *ar = [dbMgr fetchProfile:@""];
    Profile *profile;
    
    AppDelegate *appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString *urlLink;
    
    if(ar !=Nil && [ar count] > 0){
        // update
        profile = [ar objectAtIndex:0];
        if(!self.followed){
            //follow
            profile.follow_star_id = [NSString stringWithFormat:@"%d", self.number];
            urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/theStarFollow.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&EMAIL=%@&APPVERSION=%@&APIVERSION=%@&STAR_ID=%d",
                       appDel.app_id,
                       appDel.device,
                       appDel.charset,
                       appDel.email,
                       appDel.appversion,
                       appDel.apiversion,
                       self.number
                       ];
            
            
        }else{
            // un follow
            urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/theStarUnfollow.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&EMAIL=%@&APPVERSION=%@&APIVERSION=%@&STAR_ID=%d",
                       appDel.app_id,
                       appDel.device,
                       appDel.charset,
                       appDel.email,
                       appDel.appversion,
                       appDel.apiversion,
                       self.number
                       ];
            
            //clear 
            profile.follow_star_id = @"";
        }
        
        [dbMgr updateProfile:profile];
    }else{
        // insert
        profile = [dbMgr getSchemaProfile];
        if(!self.followed){
            // follow
            profile.follow_star_id = [NSString stringWithFormat:@"%d", self.number];
            
            urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/theStarFollow.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&EMAIL=%@&APPVERSION=%@&APIVERSION=%@&STAR_ID=%d",
                       appDel.app_id,
                       appDel.device,
                       appDel.charset,
                       appDel.email,
                       appDel.appversion,
                       appDel.apiversion,
                       self.number
                       ];

        }else{
            // un-follow
            urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/theStarUnfollow.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&EMAIL=%@&APPVERSION=%@&APIVERSION=%@&STAR_ID=%d",
                       appDel.app_id,
                       appDel.device,
                       appDel.charset,
                       appDel.email,
                       appDel.appversion,
                       appDel.apiversion,
                       self.number
                       ];
            
            profile.follow_star_id = @"";
            
        }
        [dbMgr insertProfile:profile];
    }
    
    [dbMgr release];
    
    // re - render button
    [self checkFollowStarId:self.number];
    
    

//    NSString* email = @"khajer@yahoo.com";
    // send api fanclub
    
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    
    if(doc){
        NSString *status_code = [[[doc.rootElement elementsForName:@"STATUS_CODE"] objectAtIndex:0] stringValue];
        if([status_code isEqualToString:@"200"]){
            NSLog(@"send  theStarFollow API - completed");
        }
    }
    
    [doc release];
}

-(IBAction)btnProfilePressed:(id)sender{
    SecondProfileViewController *secPro = [[SecondProfileViewController alloc] init];
    secPro.number = self.number;
    [self.navigationController pushViewController:secPro];
    [secPro release];
}

// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
-(void)loadDataFirstProfile{
    NSLog(@"loadDataFirstProfile");
    DBManager *dbMgr = [[DBManager alloc] init];
    NSString *cond = [NSString stringWithFormat:@"star_id = %d", self.number];
    NSMutableArray *arPro = [dbMgr fetchTheStarProfile:cond];
    
    if(arPro !=nil && [arPro count] > 0){
        NSLog(@"load data completd");
        TheStarProfile *starPro = [arPro objectAtIndex:0];
        NSData *dat = starPro.cover_img;
        UIImage *im = [UIImage imageWithData:dat];
        
        [imProfile setImage:im];
        
        
    }else{
        NSLog(@"not found data in DB");
    }
    [dbMgr release];
    
}


-(void)loadImageTheProfile{
    NSString* filename = [NSString stringWithFormat:@"pro_%d.jpg", self.number];
    NSLog(@"loadImageTheProfile %@", filename);
    [imProfile setImage:[UIImage imageNamed:filename]];
}

-(void)dealloc{
    
    [imProfile release];
    imProfile = nil;
    
//    [scView release];
//    scView = nil;
    [super dealloc];
}

@end
