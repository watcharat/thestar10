//
//  MyDownloadCell.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 2/7/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDownloadCell : UITableViewCell{
    
    IBOutlet UIImageView *imCover;
    IBOutlet UILabel *lbTitle;
    IBOutlet UILabel *lbDesc;
    IBOutlet UILabel *lbDate;
}
@property (retain, nonatomic) IBOutlet UIImageView *imCover;
@property (retain, nonatomic) IBOutlet UILabel *lbTitle;
@property (retain, nonatomic) IBOutlet UILabel *lbDesc;
@property (retain, nonatomic) IBOutlet UILabel *lbDate;

@end
