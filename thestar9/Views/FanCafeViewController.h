//
//  FanCafeViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

@interface FanCafeViewController : JWSlideMenuViewController{
    
    IBOutlet UIWebView *webview;
}
@property (retain, nonatomic) IBOutlet UIWebView *webview;

@end
