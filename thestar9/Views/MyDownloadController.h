//
//  MyDownloadController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/25/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

@interface MyDownloadController : JWSlideMenuViewController<UITableViewDataSource, UITableViewDelegate>{
    UITableView* tbMain;
    
    int downloadtype;
    NSMutableArray *arDataTable;
    
    UIButton *btnMyClip;
    UIButton *btnMyMV;
    UIButton *btnMySong;
    
    int listType;
    
}

@property (readwrite,assign)int downloadtype;
@property (retain, nonatomic) UITableView* tbMain;
@property (retain, nonatomic) NSMutableArray* arDataTable;

@property (retain, nonatomic)UIButton *btnMyClip;
@property (retain, nonatomic)UIButton *btnMyMV;
@property (retain, nonatomic)UIButton *btnMySong;

@end
