//
//  VideoPlayerController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/20/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "VideoPlayerController.h"

@interface VideoPlayerController ()

@end

@implementation VideoPlayerController
@synthesize google_v;

@synthesize webview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)init
{
    self = [super init];
    if (self) {
        self.view = [[[UIView alloc] initWithFrame:super.view.bounds] autorelease];
        [self.view setBackgroundColor:[UIColor blackColor]];
        self.title = @"Player";
        
        [self createWebview];
    }
    
    return self;
}


-(void)createWebview{
    if(webview == nil){
        webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 240)];
        [self.view addSubview:webview];
    }

    
}

-(void)viewWillAppear:(BOOL)animated{
    // load data webview
//    NSString* pathURL = [NSString stringWithFormat:@"https://www.youtube.com/embed/%@", self.google_v];
    NSString* htmlString = [NSString stringWithFormat:@"<html><head>"
                            "<meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = 320\"/></head>"
                            "<body style=\"background:#FFFFF;margin-top:20px;margin-left:0px\">"
                            "<div><object width=\"320\" height=\"240\">"
                            "<param name=\"wmode\" value=\"transparent\"></param>"
                            "<embed src=\"http://www.youtube.com/v/%@?f=user_favorites&app=youtube_gdata\""
                            "type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"320\" height=\"240\"></embed>"
                            "</object></div></body></html>", self.google_v];
    
//    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:htmlString]]];
    [webview loadHTMLString:htmlString baseURL:nil];
    webview.delegate = self;
    
}


//delegate method
- (void)webViewDidFinishLoad:(UIWebView *)_webView {
    UIButton *b = [self findButtonInView:_webView];
    [b sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (UIButton *)findButtonInView:(UIView *)view {
    UIButton *button = nil;
    
    if ([view isMemberOfClass:[UIButton class]]) {
        return (UIButton *)view;
    }
    
    if (view.subviews && [view.subviews count] > 0) {
        for (UIView *subview in view.subviews) {
            button = [self findButtonInView:subview];
            if (button) return button;
        }
    }
    return button;
}

-(void)dealloc{
    [webview release];
    [super dealloc];
}




@end
