//
//  DownloadViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
#import "MSISDNUtil.h"

@interface DownloadViewController : JWSlideMenuViewController<MSISDNUtilDelegate>{
    MSISDNUtil *msisdnUtil;
    IBOutlet UIButton *btnBanner;
}
@property (nonatomic, retain)MSISDNUtil *msisdnUtil;
@property (retain, nonatomic) IBOutlet UIButton *btnBanner;

@end
