//
//  MusicPlayerController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/26/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

#import <AVFoundation/AVFoundation.h>

#import "PlayerProgressView.h"

@interface MusicPlayerController : JWSlideMenuViewController <AVAudioPlayerDelegate, UITableViewDataSource, UITableViewDelegate>{
    
    // ui
    UIImageView *imBg;
    
    UILabel *lbSongname;
    
    UIButton *btnPlayPause;
    
    
    // data
    NSString *contentCode;
    NSString *gmmdCode;
    
    NSString *cover;
    
    NSString *pathPreview;
    NSString *songname;
    
    NSString *thmCover;
    
    //player
    AVAudioPlayer *audioPlayer;
    
    NSMutableArray *arData;
    int seqNum;
    
    NSMutableArray *arListDownloaded;
    
    // timer
    NSTimer *timer;
    
    //Progress
    PlayerProgressView *pgbarView;
    UILabel *lbStartTime;
    UILabel *lbFinishTime;
    
    // Amp: Lyrics
    BOOL isLyricOn;
    // Amp: PlaylistTable
    BOOL isPlaylistTableOn;
    // Amp: Player Controller
    BOOL isRepeate;
    BOOL isShuffle;
    
    
    // share panel
    UIView *panelShare;
}

// share
@property (retain, nonatomic)UIView *panelShare;


// ui
@property (retain, nonatomic) UIImageView *imBg;
@property (retain, nonatomic) UILabel *lbSongname;
@property (retain, nonatomic) UIButton *btnPlayPause;

@property (retain, nonatomic) NSMutableArray *arData;
@property (retain, nonatomic) NSMutableArray *arListDownloaded;

@property (readwrite,assign) int seqNum;
// data

@property(retain,nonatomic) NSString *contentCode;
@property(retain,nonatomic) NSString *gmmdCode;
@property(retain,nonatomic) NSString *cover;
@property(retain,nonatomic) NSString *pathPreview;
@property(retain,nonatomic) NSString *songname;
@property(retain,nonatomic) NSString *thmCover;

// player
@property(retain, nonatomic) AVAudioPlayer *audioPlayer;


// timer
@property (nonatomic, retain) NSTimer *timer;

// progress
@property (nonatomic, retain) PlayerProgressView *pgbarView;
@property (nonatomic, retain) UILabel *lbStartTime;
@property (nonatomic, retain) UILabel *lbFinishTime;

// Amp Lyrics
@property (nonatomic, retain) UITextView *lyricView;

// Amp: btn controll
@property (nonatomic, retain) UIButton *btnRepeat;
@property (nonatomic, retain) UIButton *btnSuffer;
@property (nonatomic, retain) UITableView *playlistTable;

// method set parameter preview

-(void)setDataPlayerArray:(NSMutableArray*)ardata seq:(int)seq;
-(void)loadDataPlayer:(NSMutableArray*)ardata listDownloaded:(NSMutableArray *)listDownloaded seq:(int)se;
@end
