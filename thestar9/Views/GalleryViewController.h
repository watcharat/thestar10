//
//  GalleryViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"
#import "MSISDNUtil.h"
@interface GalleryViewController : JWSlideMenuViewController<UITableViewDataSource,UITabBarDelegate,MSISDNUtilDelegate>{
        
    BOOL loadAPI;
    MSISDNUtil *msisdnUtil;
    
    IBOutlet UIButton *btnBanner;
}
@property (retain, nonatomic) IBOutlet UIView *galleryView;

@property (readwrite, assign) BOOL loadAPI;

@property (retain, nonatomic) MSISDNUtil *msisdnUtil;

@property (retain, nonatomic) IBOutlet UIButton *btnBanner;

@property (retain, nonatomic) IBOutlet UITableView *tbMain;

@end
