//
//  NoticeViewController.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/30/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWSlideMenuViewController.h"

@interface NoticeViewController : UIViewController{
    UIWebView *webview;
}
@property(retain, nonatomic) UIWebView *webview;
@end
