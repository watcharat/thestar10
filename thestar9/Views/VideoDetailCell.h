//
//  VideoDetailCell.h
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/17/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoDetailCell : UITableViewCell{
    
    IBOutlet UILabel *lbTitle;
    IBOutlet UILabel *lbDesc;
    
    IBOutlet UIImageView *imThumb;
}

@property (retain, nonatomic) IBOutlet UILabel *lbTitle;
@property (retain, nonatomic) IBOutlet UILabel *lbDesc;
@property (retain, nonatomic) IBOutlet UIImageView *imThumb;



@end
