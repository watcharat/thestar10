//
//  VideoHighlightViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "VideoHighlightViewController.h"
#import "AppDelegate.h"
#import "GDataXMLNode.h"

#import "VideoDetailViewController.h"

#import "VideoCell.h"

#import "ThirdViewController.h"
#import "AFJSONRequestOperation.h"
#import "UIImageView+AFNetworking.h"
@interface VideoHighlightViewController ()

@end

@implementation VideoHighlightViewController

@synthesize tbMain;
@synthesize arDataTable;
@synthesize btnBanner;

@synthesize loadAPI;

@synthesize msisdnUtil;
@synthesize videoHighlights;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    self.loadAPI = false;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = @"Video Highlight";
    
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-vdo.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    [self createVoteButton];
    [self loadBanner];
}

-(void)loadBanner{
    NSString* urlPath = @"http://thestar9.gmmwireless.com/thestar9/image/baner/pack/640x100.jpg";
    NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPath]];
    [btnBanner setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
}

-(void)createVoteButton{
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
    
}

// vote button pressed
-(IBAction)votePressed:(id)sender{
    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated{
    
    if(self.loadAPI==false){
        [self loadListVideoHighlightAPI];
        self.loadAPI = true;
        
    }
//    self.title = @"Video Highlight";
    
}


- (void)loadListVideoHighlightAPI{
    // call api
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    

    NSString* urlString = @"http://thestar9.gmmwireless.com:80/thestar9/star10/api//videoHightLight.jsp?APP_ID=283&DEVICE=mockup_test&CHARSET=utf-8&MSISDN=66818667415&APPVERSION=1.0&APIVERSION=2.0.0";
    NSLog(@"CALL API : %@", urlString);
    
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSDictionary *jsonDict = (NSDictionary *) JSON;
        videoHighlights = [jsonDict objectForKey:@"videoHighlights"];
        [videoHighlights enumerateObjectsUsingBlock:^(id obj,NSUInteger idx, BOOL *stop){
            NSString *headline = [obj objectForKey:@"headline"];
            NSLog(@"%d %@",idx,headline);
        }];
        [tbMain reloadData];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response,NSError *error, id JSON) {
        NSLog(@"Request Failure Because %@",[error userInfo]);
    }];
    
    [operation start];
    
    [url release];
    [urlString release];
}

// store

// -- void delegate of uitable view cell
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    NSLog(@"numberOfSectionsInTableView");
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    NSLog(@"numberOfRowsInSection");
    return [videoHighlights count];
}


// data row
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    NSLog(@"cellForRowAtIndexPath");
    static NSString *CellId = @"Cell";
    
    VideoCell *cell = (VideoCell*)[tableView dequeueReusableCellWithIdentifier:CellId];
    
    if(cell==nil){
        // load xib file
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VideoCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // check data table
    if(videoHighlights!=nil){
        NSDictionary *dicRow = (NSDictionary*)[videoHighlights objectAtIndex:indexPath.row];
        if(dicRow!=nil){
            
            //set title
            NSURL* urlImage = [NSURL URLWithString:[dicRow objectForKey:@"thumbsImg"]];
            NSString* strDate = [dicRow objectForKey:@"releaseDate"];
            NSString* strTitle = [dicRow objectForKey:@"headline"];
            [cell.lbTitle sizeToFit];

            //title
            [cell.imCover setImageWithURL: urlImage];
            [cell.lbDate setText:strDate];
            [cell.lbTitle setText:strTitle];
        }
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // hide row
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    VideoDetailViewController *controller = [[[VideoDetailViewController alloc] initWithNibName:@"VideoDetailViewController" bundle:nil] autorelease];
    controller.videoDic = [videoHighlights objectAtIndex:indexPath.row];

    [self.navigationController pushViewController:controller];
    
}

// ---------------------
// banner
// ---------------------
- (IBAction)btnBannerPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@"appDel.msisdn = %@, count = %d",appDel.msisdn, [appDel.msisdn length] );
    if(appDel.msisdn ==nil || [appDel.msisdn isEqualToString:@""]){
        [self checkMSISDN];
    }else{
        [self goOutBuyWebview];
    }
}
-(void)checkMSISDN{
    
    if(self.msisdnUtil ==nil){
        self.msisdnUtil = [[MSISDNUtil alloc] init];
        self.msisdnUtil.delegate = self;
    }
    [self.msisdnUtil alertshow];
    
    
}
// delegate
-(void) receptOTPSuccess:(NSString*)msisdn{
    NSLog(@"OTP success");
    [self goOutBuyWebview];
}

-(void) receptOTPFails:(NSString*)status_code status_message:(NSString*)status_message{
    NSLog(@"OTP fails");
}


-(void)goOutBuyWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=&GMMD_CODE=", appDel.app_id, appDel.device, appDel.msisdn];
    
    NSLog(@"go out to charging: %@", strPath);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
}

// ------
- (void)dealloc { 
    [msisdnUtil release];
    [tbMain release];
    [arDataTable release];
    [btnBanner release];
    [super dealloc];
}

- (void)viewDidUnload {
    [btnBanner release];
    btnBanner = nil;
    [self setBtnBanner:nil];
    [super viewDidUnload];
}
@end
