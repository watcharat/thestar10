//
//  TheStarProfileViewController.m
//  thestar9
//
//  Created by อิศรา ก้อนสมบัติ on 1/2/56 BE.
//  Copyright (c) 2556 อิศรา ก้อนสมบัติ. All rights reserved.
//

#import "GDataXMLNode.h"
#import "AppDelegate.h"
#import "TheStarProfileViewController.h"

#import "TheStarProfile.h"

#import "DBManager.h"


#import "FirstProfileViewController.h"


@interface TheStarProfileViewController ()

@end

@implementation TheStarProfileViewController

@synthesize msisdnUtil;
@synthesize arData;
@synthesize btnBanner;

@synthesize btnVote1, btnVote2, btnVote3, btnVote4, btnVote5, btnVote6, btnVote7, btnVote8;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.title = @"The Star Profile";
    // set title
    UIImage* imTitle = [UIImage imageNamed:@"txt_h-starprofile.png"];
    UIImageView *ivTitle = [[UIImageView alloc] initWithImage:imTitle];
    self.navigationItem.titleView = ivTitle;
    [ivTitle release];
    
    // button vote in right navigation
    [self createVoteButton];
    [self loadBanner];
}

// load banner button
// -----------------------------------------------
-(void)loadBanner{
    NSString* urlPath = @"http://thestar9.gmmwireless.com/thestar9/image/baner/pack/640x100.jpg";
    NSData *dat = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPath]];
    [btnBanner setImage:[UIImage imageWithData:dat] forState:UIControlStateNormal];
}

-(void)createVoteButton{
    UIImage *image = [UIImage imageNamed:@"btn_subvote.png"];
    
    UIButton *btnVote = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVote setBackgroundImage:image forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(votePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnVote.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height) ];
    [v addSubview:btnVote];
    UIBarButtonItem *vote = [[UIBarButtonItem alloc] initWithCustomView:v];
    
    self.navigationItem.rightBarButtonItem = vote;
    
    [v release];
    [image release];
    
}

// test
-(IBAction)votePressed:(id)sender{
    AppDelegate* appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDel.slideMenu openVotePage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
//    [self loadListTheStarAPI];
//    [self renderTheStarProfile];
}


-(void)loadListTheStarAPI{
    // call api
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    //validate app_id
    if(appDel.app_id == nil || [appDel.app_id isEqualToString:@""]){
        NSLog(@"cannot get data prepare(app delegate)");
        return;
    }
    NSString *urlLink = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/api/theStarProfile.jsp?APP_ID=%@&DEVICE=%@&CHARSET=%@&MSISDN=%@&APPVERSION=%@&APIVERSION=%@",
                         appDel.app_id,
                         appDel.device,
                         appDel.charset,
                         appDel.msisdn,
                         appDel.appversion,
                         appDel.apiversion
                         ];
    
    NSLog(@"CALL API : %@", urlLink);
    NSURL *url = [[NSURL alloc] initWithString:urlLink];
    NSError *error;
    
    // connect api for return xml
    NSString* data = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if(data==nil){
        NSLog(@"[Connection ERROR]:%@", [error description]);
        return;
    }
    
    //parse xml
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:data
                                                                options:0 error:&error];
    if(doc){
        /*
         //test out put
         NSArray *ar = [doc.rootElement elementsForName:@"STATUS"];
         GDataXMLElement *status = [ar objectAtIndex:0];
         NSLog(@"STATUS = %@", [status stringValue]);
         */
        
        // render data by xml
        [self renderData:doc];
        
    }else{
        NSLog(@"[XML ERROR] : %@", [error description] );
        
    }
    
    [data release];
    [doc release];
    [url release];
}

// render data
-(void)renderData:(GDataXMLDocument*)doc{
    NSArray *ar = [doc.rootElement elementsForName:@"PROFILE"];
    NSLog(@"count list profile =  %d", [ar count]);
    
    // data collection

    if(arData !=nil){
        [arData removeAllObjects];
    }else{
        arData = [[NSMutableArray alloc] init];
    }
    
    BOOL flgAllYes = true;
    for(int i=0; i< [ar count]; i++){
        
        GDataXMLElement *elm = [ar objectAtIndex:i];
        NSString* star_id = [[[elm elementsForName:@"STAR_ID"] objectAtIndex:0] stringValue];
        NSString* name = [[[elm elementsForName:@"NAME"] objectAtIndex:0] stringValue];
        NSString* desc = [[[elm elementsForName:@"DESC"] objectAtIndex:0] stringValue];
        NSString* last_modified_date = [[[elm elementsForName:@"LAST_MODIFIED_DATE"] objectAtIndex:0] stringValue];
        
        NSString* thumbs_img = [[[elm elementsForName:@"THUMBS_IMG"] objectAtIndex:0] stringValue];
        NSString* cover_img = [[[elm elementsForName:@"COVER_IMG"] objectAtIndex:0] stringValue];
        NSString* cover_img_profile = [[[elm elementsForName:@"COVER_IMG_PROFILE"] objectAtIndex:0] stringValue];

        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        
        [dic setValue:star_id forKey:@"STAR_ID"];
        [dic setValue:name forKey:@"NAME"];
        [dic setValue:desc forKey:@"DESC"];
        [dic setValue:last_modified_date forKey:@"LAST_MODIFIED_DATE"];

        [dic setValue:thumbs_img forKey:@"THUMBS_IMG"];
        [dic setValue:cover_img forKey:@"COVER_IMG"];
        [dic setValue:cover_img_profile forKey:@"COVER_IMG_PROFILE"];
        
        [arData addObject:dic];
        
        //
        NSString* is_active = [[[elm elementsForName:@"IS_ACTIVE"] objectAtIndex:0] stringValue];
        if([is_active isEqualToString:@"N"]){
            flgAllYes = false;
        }
    }

    // flag available
    if(flgAllYes){
        
        // update thumb
        for(int i=0; i<[arData count]; i++){
            NSMutableDictionary* dic = [arData objectAtIndex:i];            
            
            [self updateProfile:dic];
            /*
            BOOL updated = [self updateProfile:dic];
            if(updated){
                [self updateThumbVote:dic];
            }
            */

        }
    }
}



// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x

-(BOOL)updateProfile:(NSMutableDictionary*)dic{
    
    // update thumbFile
    
    // get data 
    NSString* updated = [dic objectForKey:@"LAST_MODIFIED_DATE"];
    NSString* star_id = [dic objectForKey:@"STAR_ID"];
    NSString* name = [dic objectForKey:@"NAME"];
    NSString* desc = [dic objectForKey:@"DESC"];
    
    NSString* thumb_cover = [dic objectForKey:@"THUMBS_IMG"];
    NSString* cover_img = [dic objectForKey:@"COVER_IMG"];
    NSString* cover_img_profile = [dic objectForKey:@"COVER_IMG_PROFILE"];
    
    
    // database manager
    DBManager *dbMgr = [[DBManager alloc] init];
    NSString *cond = [NSString stringWithFormat:@"star_id = %@", star_id];
    NSMutableArray* ar = [dbMgr fetchTheStarProfile:cond];

//
//    NSLog(@"[xml ] star_id = %@, update = %@",star_id, updated);
//    
//    NSMutableArray *arx = [dbMgr fetchTheStarProfile:@""];
//    NSLog(@"data all record = %d", [arx count]);
//    for(int i=0; i< [arx count]; i++){
//        TheStarProfile *p = [arx objectAtIndex:i];
//        NSLog(@"star_id = %@, last update = %@", p.star_id, p.updated);
//    }
    
    
    // check found database
    if(ar !=nil &&[ar count] > 0){

        // found to update
        // --------------------
        TheStarProfile *starPro = [ar objectAtIndex:0];
        
        // check update string
        if([starPro.updated isEqualToString:updated]){
            return FALSE;
        }
        
        starPro.star_id = star_id;
        starPro.updated = updated;
        starPro.desc = desc;
        starPro.name = name;
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        starPro.thumb_img = datThub;
        
        NSData *datImg = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cover_img]];
        starPro.cover_img = datImg;
        
        NSData *datImgProfile = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cover_img_profile]];
        starPro.cover_img_profile = datImgProfile;
        
        [dbMgr updateTheStarProfile:starPro];
        
        NSLog(@"update");
        
    }else{
        // insert new
        TheStarProfile *starPro = [dbMgr getSchemaTheStarProfile];
        starPro.star_id = star_id;
        starPro.updated = updated;
        starPro.desc = desc;
        starPro.name = name;
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        starPro.thumb_img = datThub;
        
        NSData *datImg = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cover_img]];
        starPro.cover_img = datImg;
        
        NSData *datImgProfile = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:cover_img_profile]];
        starPro.cover_img_profile = datImgProfile;
        
        [dbMgr insertTheStarProfile:starPro];
        NSLog(@"insert");
    }
    
    [dbMgr release];
    
    return TRUE;
}
// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x
-(void)updateThumbVote:(NSMutableDictionary*)dic{
    // get data

    NSString* star_id = [dic objectForKey:@"STAR_ID"];    
    NSString* thumb_cover = [dic objectForKey:@"THUMBS_IMG"];

    
    
    // database manager
    DBManager *dbMgr = [[DBManager alloc] init];
    NSString *cond = [NSString stringWithFormat:@"star_id = %@", star_id];
    NSMutableArray* ar = [dbMgr fetchVote:cond];
    
    //
    //    NSLog(@"[xml ] star_id = %@, update = %@",star_id, updated);
    //
    //    NSMutableArray *arx = [dbMgr fetchTheStarProfile:@""];
    //    NSLog(@"data all record = %d", [arx count]);
    //    for(int i=0; i< [arx count]; i++){
    //        TheStarProfile *p = [arx objectAtIndex:i];
    //        NSLog(@"star_id = %@, last update = %@", p.star_id, p.updated);
    //    }
    
    
    // check found database
    if(ar !=nil &&[ar count] > 0){
        
        // found to update
        // --------------------
        Vote *vote = [ar objectAtIndex:0];
        vote.star_id = star_id;
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        vote.thumb_img = datThub;
        
        [dbMgr updateVote:vote];
        
        NSLog(@"update vote");
        
    }else{
        // insert new
        Vote *vote = [dbMgr getSchemaVote];
        vote.star_id = star_id;
        
        NSData *datThub = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:thumb_cover]];
        vote.thumb_img = datThub;
        [dbMgr insertVote:vote];
        NSLog(@"insert vote");
    }
    
    [dbMgr release];
}

// render data
// -x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-
-(void)renderTheStarProfile{
    // load all data from database
    //
    //    NSLog(@"[xml ] star_id = %@, update = %@",star_id, updated);
    //
    DBManager *dbMgr = [[DBManager alloc] init];
    NSMutableArray *arPro = [dbMgr fetchTheStarProfile:@""];
    
    NSLog(@"data the star record = %d", [arPro count]);
    
//    for(int i=0; i< [arPro count]; i++){
//        TheStarProfile *p = [arPro objectAtIndex:i];
//        NSLog(@"star_id = %@, last update = %@", p.star_id, p.updated);
//    }
    TheStarProfile *starPro1 = [arPro objectAtIndex:0];
    [btnVote1 setImage:[UIImage imageWithData:starPro1.thumb_img] forState:UIControlStateNormal];

    TheStarProfile *starPro2 = [arPro objectAtIndex:1];
    [btnVote2 setImage:[UIImage imageWithData:starPro2.thumb_img] forState:UIControlStateNormal];

    TheStarProfile *starPro3 = [arPro objectAtIndex:2];
    [btnVote3 setImage:[UIImage imageWithData:starPro3.thumb_img] forState:UIControlStateNormal];
    
    TheStarProfile *starPro4 = [arPro objectAtIndex:3];
    [btnVote4 setImage:[UIImage imageWithData:starPro4.thumb_img] forState:UIControlStateNormal];
    
    TheStarProfile *starPro5 = [arPro objectAtIndex:4];
    [btnVote5 setImage:[UIImage imageWithData:starPro5.thumb_img] forState:UIControlStateNormal];
    
    TheStarProfile *starPro6 = [arPro objectAtIndex:5];
    [btnVote6 setImage:[UIImage imageWithData:starPro6.thumb_img] forState:UIControlStateNormal];
    
    TheStarProfile *starPro7 = [arPro objectAtIndex:6];
    [btnVote7 setImage:[UIImage imageWithData:starPro7.thumb_img] forState:UIControlStateNormal];
    
    TheStarProfile *starPro8 = [arPro objectAtIndex:7];
    [btnVote8 setImage:[UIImage imageWithData:starPro8.thumb_img] forState:UIControlStateNormal];
   
}

// button event
// ------------------------------------------------
- (IBAction)btn1Pressed:(id)sender {
    FirstProfileViewController *firstCon = [[FirstProfileViewController alloc] init];
    firstCon.number = 1;
    [self.navigationController pushViewController:firstCon];
    [firstCon release];
}

- (IBAction)btn2Pressed:(id)sender {
    FirstProfileViewController *firstCon = [[FirstProfileViewController alloc] init];
    firstCon.number = 2;
    [self.navigationController pushViewController:firstCon];
    [firstCon release];
}

- (IBAction)btn3Pressed:(id)sender {
    FirstProfileViewController *firstCon = [[FirstProfileViewController alloc] init];
    firstCon.number = 3;
    [self.navigationController pushViewController:firstCon];
    [firstCon release];
}

- (IBAction)btn4Pressed:(id)sender {
    FirstProfileViewController *firstCon = [[FirstProfileViewController alloc] init];
    firstCon.number = 4;
    [self.navigationController pushViewController:firstCon];
    [firstCon release];
}

- (IBAction)btn5Pressed:(id)sender {
    FirstProfileViewController *firstCon = [[FirstProfileViewController alloc] init];
    firstCon.number = 5;
    [self.navigationController pushViewController:firstCon];
    [firstCon release];
}

- (IBAction)btn6Pressed:(id)sender {
    FirstProfileViewController *firstCon = [[FirstProfileViewController alloc] init];
    firstCon.number = 6;
    [self.navigationController pushViewController:firstCon];
    [firstCon release];
}

- (IBAction)btn7Pressed:(id)sender {
    FirstProfileViewController *firstCon = [[FirstProfileViewController alloc] init];
    firstCon.number = 7;
    [self.navigationController pushViewController:firstCon];
    [firstCon release];
}

- (IBAction)btn8Pressed:(id)sender {
    FirstProfileViewController *firstCon = [[FirstProfileViewController alloc] init];
    firstCon.number = 8;
    [self.navigationController pushViewController:firstCon];
    [firstCon release];
}

// -------------------------------------------
- (IBAction)btnBannerPressed:(id)sender {
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@"appDel.msisdn = %@, count = %d",appDel.msisdn, [appDel.msisdn length] );
    if(appDel.msisdn ==nil || [appDel.msisdn isEqualToString:@""]){
        [self checkMSISDN];
    }else{
        [self goOutBuyWebview];
    }
}


-(void)checkMSISDN{
    
    if(self.msisdnUtil ==nil){
        self.msisdnUtil = [[MSISDNUtil alloc] init];
        self.msisdnUtil.delegate = self;
    }
    [self.msisdnUtil alertshow];
    

}
// delegate
-(void) receptOTPSuccess:(NSString*)msisdn{
    NSLog(@"OTP success");
    [self goOutBuyWebview];
}

-(void) receptOTPFails:(NSString*)status_code status_message:(NSString*)status_message{
    NSLog(@"OTP fails");
}


-(void)goOutBuyWebview{
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSString* strPath = [NSString stringWithFormat:@"http://thestar9.gmmwireless.com/thestar9/webview/charging.jsp?APP_ID=%@&DEVICE=%@&MSISDN=%@&SERVICE_ID=&GMMD_CODE=", appDel.app_id, appDel.device, appDel.msisdn];
    
    NSLog(@"go out to charging: %@", strPath);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPath]];
}




// 0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-

- (void)dealloc {
    [btnVote1 release];
    [btnVote2 release];
    [btnVote3 release];
    [btnVote4 release];
    [btnVote5 release];
    [btnVote6 release];
    [btnVote7 release];
    [btnVote8 release];
    
    [self.msisdnUtil release];

    [btnBanner release];
    [super dealloc];
}
- (void)viewDidUnload {
    [msisdnUtil release];
    
    [btnVote1 release];
    btnVote1 = nil;
    [btnVote2 release];
    btnVote2 = nil;
    [btnVote3 release];
    btnVote3 = nil;
    [btnVote4 release];
    btnVote4 = nil;
    [btnVote5 release];
    btnVote5 = nil;
    [btnVote6 release];
    btnVote6 = nil;
    [btnVote7 release];
    btnVote7 = nil;
    [btnVote8 release];
    btnVote8 = nil;
    [self setBtnVote1:nil];
    [self setBtnVote2:nil];
    [self setBtnVote3:nil];
    [self setBtnVote4:nil];
    [self setBtnVote5:nil];
    [self setBtnVote6:nil];
    [self setBtnVote7:nil];
    [self setBtnVote8:nil];
    [btnBanner release];
    btnBanner = nil;
    [self setBtnBanner:nil];
    [super viewDidUnload];
}
@end
